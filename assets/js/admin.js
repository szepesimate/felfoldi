var Admin = {
    toggleLoginRecovery: function () {
        var is_login_visible = $('#modal-login').is(':visible');
        (is_login_visible ? $('#modal-login') : $('#modal-recovery')).slideUp(300, function () {
            (is_login_visible ? $('#modal-recovery') : $('#modal-login')).slideDown(300, function () {
                $(this).find('input:text:first').focus();
            });
        });
    }
};
$(function () {
    $('.toggle-login-recovery').click(function (e) {
        Admin.toggleLoginRecovery();
        e.preventDefault();
    });
});
$(document).ready(function () {
    if ($('#sortable')[0]) {
        var maxLevels = 2;
        if ($('#sortable').hasClass('oneDimension'))
            maxLevels = 1;
        $('#sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            disableNesting: 'no-nest',
            forcePlaceholderSize: true,
            helper: 'clone',
            maxLevels: maxLevels,
            opacity: .6,
            placeholder: 'placeholder',
            stop: function () {
                $.ajax({
                    url: base_url + 'admin/' + uri_segment2 + '/reorder',
                    data: $('ol#sortable').nestedSortable('serialize'),
                    type: 'POST',
                    beforeSend: function () {
                        $('ol#sortable').css('opacity', 0.5);
                    },
                    success: function (result) {
                        if (result != 'success')
                            alert("Error. Reload this page please.");
                        $('ol#sortable').css('opacity', 1);
                    }
                });
            }
        });
    }
    if ($('a.switch')[0]) {
        $('div.box').on('click' , 'a.switch', function () {
            var element = $(this);
            var currentValue = element.attr('rel');
            $.ajax({
                url: base_url + 'admin/' + uri_segment2 + '/switchProperties',
                data: "dbField=" + element.attr('rev') + "&currentValue=" + currentValue + "&dbId=" + element.attr('name'),
                type: 'POST',
                beforeSend: function () {
                    if ($('ol#sortable')[0])
                        $('ol#sortable').css('opacity', 0.5);
                },
                success: function (result) {
                    if (result != 'success')
                        alert("Error. Reload this page please.");
                    else {
                        var newValue = currentValue == '1' ? 0 : 1;
                        element.attr('rel', newValue);
                        
                        if ( element.attr('rev') == 'featured' ){
                            document.location = base_url + 'admin/' + uri_segment2;
                            return;
                        }
                        
                        if (newValue) {
                            element.addClass('lathatosag');
                            element.removeClass('lathatosag2');
                        } else {
                            element.addClass('lathatosag2');
                            element.removeClass('lathatosag');
                        }
                    }
                    if ($('ol#sortable')[0])
                        $('ol#sortable').css('opacity', 1);
                }
            });
        })
    }
    if ($('.productListTable a.switch')[0]) {
        $('.productListTable a.switch').click(function () {
            var element = $(this);
            var currentValue = element.attr('rel');
            $.ajax({
                url: base_url + 'admin/' + uri_segment2 + '/switchProperties',
                data: "dbField=" + element.attr('rev') + "&currentValue=" + currentValue + "&dbId=" + element.attr('name'),
                type: 'POST',
                beforeSend: function () {
                    if ($('.productListTable')[0])
                        $('.productListTable').css('opacity', 0.5);
                },
                success: function (result) {
                    if (result != 'success')
                        alert("Error. Reload this page please.");
                    else {
                        var newValue = currentValue == '1' ? 0 : 1;
                        element.attr('rel', newValue);
                        if (newValue) {
                            element.addClass('active');
                        } else {
                            element.removeClass('active');
                        }
                    }
                    if ($('.productListTable')[0])
                        $('.productListTable').css('opacity', 1);
                }
            });
        })
    }
    if ($('.elhagyottKosarSend')[0]) {
        $('.elhagyottKosarSend').click(function () {
            var element = $(this);
            var f_user_basket_id = element.attr('rel');
            $.ajax({
                url: base_url + 'sendElhagyottKosarEmail/' + f_user_basket_id,
                data: null,
                type: 'POST',
                beforeSend: function () {
                    element.closest('.table').css('opacity', 0.5);
                },
                success: function (result) {
                    if (result != 'success')
                        alert("Error. Reload this page please.");
                    else {
                        element.fadeOut();
                        element.parent().append("E-mail kiküldve!");
                    }
                    element.closest('.table').css('opacity', 1);
                }
            });
        })
    }




    $(".seoForm input[type=text]").on("change", function (event) {
        $(this).closest('.seoForm').submit();
    });

    $(".seoForm").on("submit", function (event) {
        event.preventDefault();
        var formElement = $(this);
        $.ajax({
            type: "POST",
            url: base_url + 'admin/' + uri_segment2 + '/submitSeoForm',
            dataType: "json",
            data: formElement.serialize(),
            beforeSend: function () {
                formElement.closest('.table').css('opacity', 0.5);
            },
            success: function (rData) {
                if (rData.success) {
                    formElement.closest('.table').css('opacity', 1);
                } else {
                    alert("Error. Reload this page please.");
                }
            }
        });
    });



    $(".pencilForm input[type=text]").on("change", function (event) {
        $(this).closest('.pencilForm').submit();
    });

    $(".pencilForm").on("submit", function (event) {
        event.preventDefault();
        var formElement = $(this);
        $.ajax({
            type: "POST",
            url: base_url + 'admin/submitPencilForm',
            dataType: "json",
            data: formElement.serialize(),
            beforeSend: function () {
                formElement.closest('.table').css('opacity', 0.5);
            },
            success: function (rData) {
                if (rData.success) {
                    formElement.closest('.table').css('opacity', 1);
                } else {
                    alert("Error. Reload this page please.");
                }
            }
        });
    });





    $(".dictionaryForm input[type=text]").on("change", function (event) {
        $(this).closest('.dictionaryForm').submit();
    });

    $(".dictionaryForm").on("submit", function (event) {
        event.preventDefault();
        var formElement = $(this);
        $.ajax({
            type: "POST",
            url: base_url + 'admin/' + uri_segment2 + '/submitDictionaryForm',
            dataType: "json",
            data: formElement.serialize(),
            beforeSend: function () {
                formElement.closest('.table').css('opacity', 0.5);
            },
            success: function (rData) {
                if (rData.success) {
                    formElement.closest('.table').css('opacity', 1);
                } else {
                    alert("Error. Reload this page please.");
                }
            }
        });
    });



    $("#genLanguageFilesForm").on("submit", function (event) {
        event.preventDefault();
        var formElement = $(this);
        $.ajax({
            type: "POST",
            url: base_url + 'admin/' + uri_segment2 + '/genLanguageFilesForm',
            dataType: "json",
            data: formElement.serialize(),
            beforeSend: function () {
                formElement.closest('.table').css('opacity', 0.5);
            },
            success: function (rData) {

                if (rData.success) {

                    formElement.closest('.table').css('opacity', 1);
                    alert("Sikeres generálás!");
                } else {
                    alert("Error. Contact admin developer please.");

                }
            }
        });
    });








});

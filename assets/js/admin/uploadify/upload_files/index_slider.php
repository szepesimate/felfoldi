<?php

$variables = explode( ',', $_GET['args'] );
$width = $variables[0];
$height = $variables[1];
$max_width = $variables[2];
$max_height = $variables[3];

if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
	$targetTmpPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/../tmp/';
	$targetFileName = szepfajlnevesites( $_FILES['Filedata']['name'], '', true );
	
	$targetFile =  str_replace('//','/',$targetPath) . $targetFileName;
	$targetTmpFile =  str_replace('//','/',$targetTmpPath) . $targetFileName;
	
	$orientation = getimagesize( $tempFile );
	$type = ($orientation[0] < $orientation[1]) ? 'álló' : 'fekvő';
	
	// kivágás előtti lekicsinyített kép feltöltése
	$image = new SimpleImage();
	$image -> load( $tempFile );
	
	if( $type == 'fekvő' ) {
		if( $orientation[0] > 870 ) {
			$image -> resizeToWidth( 870 );
		}
	}
	else if( $type == 'álló' ) {
		if( $orientation[1] > 870 ) {
			$image -> resizeToHeight( 870 );
		}
	}
	
	$image -> save( $targetTmpFile );
	
	// végleges nagy kép mentése
	$image = new SimpleImage();
	$image -> load( $tempFile );
	
	if( $type == 'fekvő' ) {
		if( $orientation[0] > $max_width ) {
			$image -> resizeToWidth( $max_width );
		}
	}
	else if( $type == 'álló' ) {
		if( $orientation[1] > $max_width ) {
			$image -> resizeToHeight( $max_width );
		}
	}
	
	$image -> save( $targetFile );
	
	//move_uploaded_file($tempFile,$targetFile);
	//echo str_replace($_SERVER['DOCUMENT_ROOT'],'',$targetFile);
	echo get_options('directory') . "/includes/images/index_slider/," . get_options('directory') . "/includes/images/tmp/,{$targetFileName}";
}

?>
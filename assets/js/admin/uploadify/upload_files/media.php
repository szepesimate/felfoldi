<?php

$variables = explode( ',', $_GET['args'] );
$max_width = $variables[0];
$max_height = $variables[1];
$pre_size = $variables[2];

if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
	
	$targetFile =  str_replace('//','/',$targetPath) . szepfajlnevesites( $_FILES['Filedata']['name'], '', true );
	$targetTmpFile =  str_replace('//','/',$targetPath) . szepfajlnevesites( $_FILES['Filedata']['name'], 'admin-pre-', true );
	
	$orientation = getimagesize( $tempFile );
	$type = ($orientation[0] < $orientation[1]) ? 'álló' : 'fekvő';
	
	// preview feltöltése
	$tmp_image = new SimpleImage();
	$tmp_image -> load( $tempFile );
	$tmp_image -> crop();
	$tmp_image -> save( $targetTmpFile );
	$tmp_image = new SimpleImage();
	$tmp_image -> load( $targetTmpFile );
	$tmp_image -> resize( $pre_size, $pre_size );
	$tmp_image -> save( $targetTmpFile );
	
	// rendes kép feltöltése (átméretezéssel ha túl nagy méretű)
	$image = new SimpleImage();
	$image -> load( $tempFile );
	
	if( $type == 'fekvő' ) {
		if( $orientation[0] > $max_width ) {
			$image -> resizeToWidth( $max_width );
		}
	}
	else if( $type == 'álló' ) {
		if( $orientation[1] > $max_width ) {
			$image -> resizeToHeight( $max_width );
		}
	}
	
	$image -> save( $targetFile );
	
	//move_uploaded_file($tempFile,$targetFile);
	echo str_replace($_SERVER['DOCUMENT_ROOT'],'',$targetFile);
}

?>
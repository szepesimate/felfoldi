﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	CKEDITOR.config.toolbar_Basic = [
		['PasteText'],
		['Undo','Redo'],
		['SelectAll','RemoveFormat'],
		['Styles'],
		//['button-pre'],
		//['button-h1','button-h2','button-p','button-pre',],
		['Bold','Italic'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['BulletedList','NumberedList'],
		['Link','Unlink'],
		['Image'],
		['Maximize','ShowBlocks'],
		['Source']
	];
	config.toolbar = 'Basic';
	config.language = 'hu';
	config.height = 500;
	//config.extraPlugins = 'autogrow';
	config.removePlugins = 'resize';
	//config.extraPlugins = 'button-h1,button-h2,button-p,button-pre';
	//config.extraPlugins = 'button-h1,button-h2,button-p';
	config.extraPlugins = 'button-pre';
	
	// config.uiColor = '#AADC6E';
	config.entities = false;
};

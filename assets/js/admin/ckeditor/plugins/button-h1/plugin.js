(function(){
 var a= {
  exec:function(editor){
   var format = {
    element : "h1"
   };
   var style = new CKEDITOR.style(format);
   style.apply(editor.document);
  }
 },

 b="button-h1";
 CKEDITOR.plugins.add(b,{
  init:function(editor){
   editor.addCommand(b,a);
   editor.ui.addButton("button-h1",{
    label:"H1 button",
    icon: this.path + "button-h1.png",
    command:b
   });
  }
 });
})();
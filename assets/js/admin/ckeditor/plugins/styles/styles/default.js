﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.stylesSet.add('default', [
	{name:'normál bekezdés',element:'p'},
	{name:'fejléc 1',element:'h1'},
	{name:'fejléc 2',element:'h2'},
	{name:'fejléc 3',element:'h3'},
	//{name:'pre',element:'pre'},
	
	{name:'kicsi szöveg',element:'small'},
	{name:'nagy szöveg',element:'big'},
	{name:'áthúzott szöveg',element:'del'}
]);
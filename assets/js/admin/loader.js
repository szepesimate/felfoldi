
$(document).ready(function ()
{
    //----------------------------
    //      Web charts
    //----------------------------	
    $('table.stats').each(function () {

        if ($(this).attr('rel')) {
            var statsType = $(this).attr('rel');
        } else {
            var statsType = 'area';
        }

        var chart_width = ($(this).parent('div').width()) - 20;

        if (statsType == 'line' || statsType == 'pie') {
            $(this).hide().visualize({
                type: statsType, // 'bar', 'area', 'pie', 'line'
                width: chart_width,
                height: '240px',
                colors: ['#6fb9e8', '#ec8526', '#9dc453', '#ddd74c'],
                lineDots: 'double',
                interaction: true,
                multiHover: 5,
                tooltip: true,
                tooltiphtml: function (data) {
                    var html = '';
                    for (var i = 0; i < data.point.length; i++) {
                        html += '<p class="chart_tooltip"><strong>' + data.point[i].value + '</strong> ' + data.point[i].yLabels[0] + '</p>';
                    }
                    return html;
                }
            });
        } else {
            $(this).hide().visualize({
                type: statsType, // 'bar', 'area', 'pie', 'line'
                width: chart_width,
                height: '240px',
                colors: ['#6fb9e8', '#ec8526', '#9dc453', '#ddd74c']
            });
        }
    });
    //----------------------------
    //        Box toggling
    //----------------------------
    function boxToggle(target)
    {
        var boxparent = $(target).closest('.box'); //get box parent element

        if ($(boxparent).hasClass("hidden")) //parent is hidden //toggle box parent
        {
            $(boxparent).removeClass("hidden"); //remove hidden css class			
            $(boxparent).children('.content').slideDown(); //show box content
        } else
        {
            //parent is visible			
            $(boxparent).addClass("hidden"); //apply hidden css class
            $(boxparent).children('.content').slideUp(); //hide box content
        }
    }

    $('.box .header').click(function (e) /*header title*/
    {
        e.preventDefault();
        boxToggle($(this));

        if (!$(this).parent().hasClass('hidden')) {
            $('html, body').animate({scrollTop: $(this).offset().top});
        }
    });

    //----------------------------
    //        		Tabs
    //----------------------------
    // loop through all tabs container and
    // perform actions
    $('div.tabs').each(function (index) {

        $(this).children('.tab').hide(); //hide all tabs from current tabs container				 
        //set containers variables
        var container = $(this);
        var tabContainers = $(this).children('.tab')

        tabContainers.hide().filter(':first').show().addClass("active"); //show first tab in list and set to active			  

        $('ul.navigation li', this).click(function () { //handle tab navigation click events

            $(this).parent().find('li a.current').removeClass("current"); //remove current state from navigation			   
            $('a', this).addClass("current"); //add current state to clicked navigation			   
            container.find('.tab.active').hide().removeClass("active"); //hide current tab			   			
            //find tab to display and find it under the tabs container
            var activeTab = $(this).find("a").attr("href");
            $(activeTab, container).fadeIn().addClass("active");

            window.location.hash = $(this).children('a').attr('href').substr(4);

            return false;

        });

    });

    // tab at load
    var hash = '#tab' + window.location.hash.substr(1);

    if (hash != '#tab') {
        $('div.tabs ul.navigation li a').removeClass('current');
        $('div.tabs ul.navigation li a[href=' + hash + ']').addClass('current');

        $('div.tabs div.tab').removeClass('active').css('display', 'none');
        $('div.tabs div.tab' + hash).addClass('active').css('display', 'block');
    }
    //----------------------------
    //         Accordion
    //----------------------------		
    // loop through all accordions container and
    // perform actions
    $('div.accordion').each(function (index) {

        $(this).children('.panel').children('.content').hide(); //hide all contents from current accordion container				 
        //set containers variables
        var container = $(this);
        var trigger = $(this).children('.panel');
        var acordionContainer = $(this).children('.content');

        acordionContainer.hide();

        acordionContainer.filter(':first').show();//show first tab in list and set to active			  
        trigger.filter(':first').addClass('active');
        trigger.click(function (e) //handle tab navigation click events
        {
            e.preventDefault();
            if ($(this).next().is(':hidden'))
            {
                trigger.removeClass('active').next().slideUp();
                $(this).toggleClass('active').next().slideDown();
            }
            return false;
        });

    });
    //----------------------------
    //        	Messages
    //----------------------------			
    /*$('.message').live('click', function() {
     $(this).slideUp(250);
     });*/
    //----------------------------
    //     Child Navigation
    //----------------------------	
    if (!$('body').hasClass('altlayout'))
    {
        //$("#navigation").superfish({ speed: 'fast',delay: 300, autoArrows: false });
    }
    $('.altlayout #navigation li a').click(function (e) {
        if ($(this).attr("href") == '#') {
            $(this).parent().children('ul').children('li:first-child').children('a').click();
            return false;
        }
    });
    $('.altlayout #navigation li').hover(function (e) {
        if ($(this).children('a').attr("href") == '#' && $(this).children('ul').css("display") == 'none') {
            $(this).children('a').parent().siblings().find("ul").slideUp("normal");
            $(this).children('a').next().slideToggle("normal"); // Slide down the clicked sub menu
            //$('.altlayout #navigation li a').next().slideUp();
            //$(this).next().slideToggle('normal');
        }
        return false;
    }, function () {
        if ($(this).children('a').attr("href") == '#' && !$(this).children('ul').children('li').hasClass("current")) {
            $(this).children('a').parent().siblings().find("ul").slideUp("normal");
            $(this).children('a').next().slideToggle("normal"); // Slide down the clicked sub menu
            //$('.altlayout #navigation li a').next().slideUp();
            //$(this).next().slideToggle('normal');
        }
        return false;
    });
    // current menu
    $('.altlayout #navigation li.current ul').show();
    // jQuery Datables
    $('.datatable').dataTable({
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sProcessing": "Feldolgozás, kérem várjon…",
            "sLengthMenu": "Mutass _MENU_ elemet oldalanként",
            "sZeroRecords": "Nincs a megadott feltételeknek megfelelő találat!",
            "sEmptyTable": "Nem található adat a táblázatban!",
            "sLoadingRecords": "Betöltés…",
            "sInfo": "elemek: _START_-_END_, összesen _TOTAL_",
            "sInfoEmpty": "elemek: 0-0, összesen 0",
            "sInfoFiltered": "(szűrt eredmény összesen _MAX_ elemből)",
            "sSearch": "Keresés:",
            "oPaginate": {
                "sFirst": "első",
                "sPrevious": "«",
                "sNext": "»",
                "sLast": "utolsó"
            },
        }
    });
    // JQuery Uniform
    //$("select, input[type='checkbox'], input[type='radio'], input[type='file']").uniform();
    // jQuery Tipsy
    //$('[rel=tooltip]').tipsy({gravity:'s', fade:true}); // Tooltip Gravity Orientation: n | w | e | s
    // jQuery Superfish
    //$("#menu").superfish({ speed: 'fast',delay: 300,autoArrows: false });




    // navigation hack
    var navigation_offset = $('#navigation').offset().top - 51;
    var navigation_margin_top = $('#navigation').css('margin-top');
    $(window).scroll(function () {
        var offset = getScrollXY();
        offset = offset[1];
        if (offset >= navigation_offset) {
            $('#navigation').css({
                position: 'fixed',
                top: 0,
                'margin-top': 51
            });
        } else {
            $('#navigation').css({
                position: 'relative',
                'margin-top': navigation_margin_top
            });
        }
    });

    // select modd
    $('select').chosen();

    // header hover-re megjelenik a toggle gomb
    $('.box .header').hover(function () {
        $('<span class="toggle"></span>')
                .appendTo(this)
                .hide()
                .stop()
                .fadeIn(250);
    }, function () {
        $(this)
                .children('.toggle')
                .stop()
                .fadeOut(250, function () {
                    $(this).remove();
                });
    });

    // oldal tetejére görgetés
    $('a.top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    // toábbi opciók gomb
    $('button.hidetogglebuton').click(function () {
        $(this).slideUp(400);
        $(this).parent().next('.hidetoggle').slideDown(400);
        return false;
    });

});

function getScrollXY() {
    var scrOfX = 0, scrOfY = 0;
    if (typeof (window.pageYOffset) == 'number') {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
    } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
    } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
    }
    return [scrOfX, scrOfY];
}

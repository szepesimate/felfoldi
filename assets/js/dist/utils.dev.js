"use strict";

/*
 var windowWidth;
 var windowHeight;
 
 function checkWindowWitdh(){
 windowWidth = $(window).width();
 windowHeight = $(window).height();	
 
 
 }
 
 
 $(window).resize(function() {
 
 checkWindowWitdh();
 
 
 });
 
 */
function toggleMobilMenu() {
  var $menu = $('.nav-menu');
  var $nav = $('nav.nav');
  $('.nav-toggle').toggleClass('is-active');
  $menu.toggleClass('is-active');
  $nav.toggleClass('is-active');
}

function closeLeftMenu() {
  $('.menu.main').animate({
    left: -500
  }, 500, 'easeOutQuad', function () {
    $('.menu.main').removeClass('open');
  });
  $('#menu-shade').removeClass('open'); //toggleMobilMenu();
}

var scrolledDownCount = 0;
var lockAutoScroll = false;

function scrollDown() {
  var element = $('section.first-block');
  $('html, body').stop().animate({
    'scrollTop': element.offset().top - 85
  }, 500, 'swing');
}

var isHome = $('body').hasClass('index');

function loadMoreProductImages() {
  $('.webshop_box_visible').each(function () {
    var productImage = $(this).find('.product_img');
    var src = productImage.data("imgsrc");
    productImage.addClass('loaded');
    productImage.attr('src', src);
    $(this).removeClass('webshop_box_visible');
  });
}

function makeThemVisible() {
  $('.webshop_box.displayBlock').each(function () {
    //if ( $(this).css('display') == 'block' ){
    $(this).css('display', 'inline-block');
    $(this).css('opacity', '0'); //}

    $(this).removeClass('displayBlock');
    $(this).removeClass('hidden');
    $(this).animate({
      opacity: 1
    }, 500);
  });
}

function addPanelsFixed() {
  $('.loginPanel').addClass('fixed');
  $('.basketPanel').addClass('fixed');
}

function removePanelsFixed() {
  $('.loginPanel').removeClass('fixed');
  $('.basketPanel').removeClass('fixed');
}

$(document).ready(function () {
  $('button.cart').hover(function () {
    $('.loginPanel').stop().hide();
    $('.basketPanel').stop().slideDown();
  }, function () {});
  $('.basketPanel').hover(function () {}, function () {
    $('.basketPanel').stop().slideUp();
  });
  $('body').on('click', '.productLink', function (e) {
    if ($('.nav-toggle').css('display') === 'block') {
      e.preventDefault();
      $('.sublinks').slideToggle();
    }
  });
  $('.nav .magnifier').click(function () {
    if ($('.search-overlay-menu').hasClass('open')) {
      $('.search-overlay-menu').removeClass('open');
    } else {
      $('.search-overlay-menu').addClass('open');
    }
  });
  $('.search-overlay-close, .search-overlay-menu').click(function () {
    $('.search-overlay-menu').removeClass('open');
  });
  $('.search-overlay-menu input, .search-overlay-menu button').click(function (event) {
    event.stopPropagation();
  });
  /*if (isHome || $('body').hasClass('about')) {
   
   var $appeared = $('#appeared');
   var $disappeared = $('#disappeared');
   $('#waypoint').appear();
   $(document.body).on('appear', '#waypoint', function (e, $affected) {
   // this code is executed for each appeared element
   //alert(123);
   
   if (!countingLocked) {
   $('#waypoint').siblings('.counters').removeClass('is-hidden');
   startCounting();
   countingLocked = true;
   }
   });
   }*/

  if ($(".tile_gallery").length) {
    $(".tile_gallery").each(function () {
      $(this).unitegallery({
        gallery_theme: "tiles",
        gallery_images_preload_type: "visible",
        tiles_min_columns: 1,
        tiles_max_columns: 3,
        tiles_space_between_cols: 20,
        tile_enable_overlay: true,
        tile_overlay_opacity: 0.85,
        tile_overlay_color: "#012653"
      });
    });
  }

  if (isHome || $('body').hasClass('about')) {
    new WOW().init();
  }

  $('.nav-toggle').click(function () {
    toggleMobilMenu();
  });
  /*
   if ($('.pulze')[0]) {
   pulzePulze();
   }
   */

  $('.categoriesBtn').click(function () {
    if ($('.menu.main').hasClass('open')) {
      closeLeftMenu();
    } else {
      $('.menu.main').addClass('open');
      /*$('.menu.main').css('left', -1000);*/

      $('.menu.main').animate({
        left: 0
      }, 500, 'easeOutQuart');
      $('#menu-shade').addClass('open');
      if ($('.menu.main').css('position') == 'absolute') $('html,body').animate({
        scrollTop: 0
      }, 1000, 'easeOutQuart');
    }
  });
  $('.menu.main .close,  #menu-shade').click(function () {
    closeLeftMenu();
  });

  window.onscroll = function () {
    scrollTop = $(window).scrollTop(); //var halfOfHeader = $('section.hero').height() / 2;

    var topValue = $('.hero .hero-head').height(); //if (!$('header.nav').hasClass('fixedFixed')) {

    if (scrollTop > topValue && !$('header.nav').hasClass('fixed')) {
      $('header.nav').addClass('fixed');
      addPanelsFixed();
    } else if (scrollTop > topValue && $('header.nav').hasClass('posAbs')) {
      $('header.nav').removeClass('posAbs');
      addPanelsFixed();
    } else if (isHome && scrollTop <= topValue && $('header.nav').hasClass('fixed')) {
      $('header.nav').removeClass('fixed');
      removePanelsFixed();
      /*if ($('.hamburger').css('display') == 'block') {
       $('.menu.main').hide();
       }*/
    } else if (!isHome && scrollTop <= topValue && !$('header.nav').hasClass('posAbs')) {
      $('header.nav').addClass('posAbs');
      removePanelsFixed();
      /*if ($('.hamburger').css('display') == 'block') {
       $('.menu.main').hide();
       }*/
    } //}


    if ($('body').hasClass('product') && $('.nav-toggle').css('display') !== 'block') {
      if (scrollTop > 1) {
        if (!$('header.nav').hasClass('scrolled')) $('header.nav').addClass('scrolled');
      } else {
        $('header.nav').removeClass('scrolled');
        closeLeftMenu();
      }
    }
    /*if ($('header.nav').hasClass('fixed')) {
     lockAutoScroll = true;
     }*/

    /*        if (!lockAutoScroll && isHome) {
     scrolledDownCount++;
     if (scrolledDownCount > 10) {
     lockAutoScroll = true;
     scrollDown();
     }
     
     }*/

    /*if ($('body').hasClass('index') && $('body.index .indexPrev').css('display') === 'block') {
     lockAutoScroll = true;
     scrolledDownCount++;
     if (scrolledDownCount > 10) {
     lockAutoScroll = true;
     removeIndexKezd();
     }
     
     }
     */

  };

  $('.languageChooser').click(function () {
    $('.languageChooserPanel').slideToggle();
  });

  if ($('body').hasClass('product') && !$('body').hasClass('productDetail')) {
    $('.moreElementsTrigger').ScrollAppear({
      ElementAffect: '.webshop_box.hidden',
      Timeout: 1000,
      ElementsToShow: 10,
      AddClass: 'webshop_box_visible displayBlock',
      Callback: function Callback(state) {
        loadMoreProductImages();
        setTimeout(makeThemVisible(), 500);
      }
    });
  }

  $('button.profile, .loginBtnFizetes').click(function () {
    $('.basketPanel').stop().hide();
    $('.loginPanel').stop().slideToggle();
  }); //$(window).stellar({horizontalScrolling: false});

  /*
   $(".ajanlat_button, .subscribe_button").fancybox({
   'scrolling': 'no',
   'titleShow': false,
   });
   
   
   
   
   $( ".datepicker" ).datepicker({
   dateFormat: 'yy-mm-dd'
   });
   */

  /*
   // almenu
   $('.menu li').hover(function() {
   if ( $('.menu.main').css('position')!='absolute' ){
   $(this)
   .children('ul')
   .css('z-index',999)
   .stop(true, true)			
   .slideDown(200);
   }	
   }, function() {
   if ( $('.menu.main').css('position')!='absolute' ){
   $(this)
   .children('ul')
   .css('z-index',998)
   .stop(true, true)			
   .slideUp(2);
   }
   });
   
   $('.menu ul.outer ul').hover(function(){
   var aElement = $(this).closest('.menu-item').find('a:first');
   aElement.addClass('activeSubMenu');
   
   }, function(){
   var aElement = $(this).closest('.menu-item').find('a:first');
   aElement.removeClass('activeSubMenu');	
   });
   
   */

  /*
   var slider = $('.bxslider').bxSlider({
   pager: false,
   auto: true,
   controls: false,
   mode: 'fade',
   pause: 4000,	    
   minSlides: 1,
   maxSlides: 1,
   slideMargin: 0		
   
   });
   */

  var width = $(window).width();
  var numberOfVisibleSlides = 1;

  if (width < 600) {
    numberOfVisibleSlides = 1;
  } else if (width < 768) {
    numberOfVisibleSlides = 2;
  } else if (width < 1200) {
    numberOfVisibleSlides = 3;
  } else if (width < 1500) {
    numberOfVisibleSlides = 4;
  } else {
    numberOfVisibleSlides = 5;
  }

  var slider = $('.our-products .bxslider').bxSlider({
    nextSelector: '#slider-next',
    nextText: '<div style="height:100%;width:29px"><img src="' + base_url + 'assets/images/arrow_right.png" /></div>',
    prevSelector: '#slider-prev',
    prevText: '<div style="height:100%;width:29px"><img src="' + base_url + 'assets/images/arrow_left.png" /></div>',
    pager: false,
    controls: true,
    minSlides: numberOfVisibleSlides,
    maxSlides: numberOfVisibleSlides,
    moveSlides: 1,
    touchEnabled: true,
    infiniteLoop: true,
    responsive: true,
    slideWidth: 263,
    slideMargin: 10
  });
  $('.interactive').on('click', function (event) {
    var item = event.target;

    if ($(item).hasClass('closed')) {
      $(item).removeClass('closed');
      $(item).addClass('opened');
      $('.sub-items').hide(300);
      $('#sub-items-' + $(item).data('target')).show(300);
    } else {
      $(item).removeClass('opened');
      $(item).addClass('closed');
      $('.sub-items').hide(300);
    }
  });
  /*
   $('#slider_navi .next').click(function(){
   slider.goToNextSlide();
   return false;
   });
   
   $('#slider_navi .prev').click(function(){
   slider.goToPrevSlide();
   return false;
   });
   
   
   
   
   */

  $('.subPage.videogallery .gallery_type_boxes a').fancybox({
    prevEffect: 'fade',
    nextEffect: 'fade',
    margin: 1,
    padding: 0,
    helpers: {
      thumbs: {
        width: 100,
        height: 50
      }
    },
    beforeShow: function beforeShow() {
      this.width = '80%';
      this.height = '80%';
      var previewImageSrc = $.fancybox.inner.find('.fancybox-image').attr('src');
      $.fancybox.inner.find('.fancybox-image').remove();
      var youTubeUrl = preview_url_dict[previewImageSrc];
      youTubeUrl = youTubeUrl.replace(new RegExp("watch\\?v=", "i"), 'embed/');
      var iframe_ = '<iframe style="z-index:8041" width="100%" height="100%" src="' + youTubeUrl + '" frameborder="0" allowfullscreen></iframe>';
      var youtTubeIframe = $(iframe_);
      $.fancybox.inner.append(youtTubeIframe);
    },
    afterShow: function afterShow() {
      $('a.fancybox-nav').css('width', '55px');
    }
  });
  $('.thumbs a.thumb').fancybox({
    prevEffect: 'fade',
    nextEffect: 'fade',
    margin: 1,
    padding: 0,
    helpers: {
      thumbs: {
        width: 100,
        height: 50
      }
    }
  });
  $('.fancybox').fancybox({
    prevEffect: 'fade',
    nextEffect: 'fade',
    padding: 0,
    margin: 1
    /*helpers: {
     thumbs: {
     width: 100,
     height: 50
     }
     },*/

  });
  $('.embeddedGallery a.thumb').fancybox({
    prevEffect: 'fade',
    nextEffect: 'fade',
    helpers: {
      thumbs: {
        width: 100,
        height: 50
      }
    }
  });

  if ($('.anchor_link')[0]) {
    $('.anchor_link').click(function () {
      var target_id = $(this).attr('rel');
      var top_offset = $('#' + target_id).offset().top;
      $('html,body').animate({
        scrollTop: top_offset - 20
      }, 1000);
    });
  }

  if (isHome) {
    showSlide(nextSlideToShow, 0);
    sliderInterval = setInterval(function () {
      showSlide(nextSlideToShow, 2000);
    }, during);
  }
});
var sliderInterval = true;
var nextSlideToShow = 0;
var activeAnimate = null;

function showSlide(number, tranzTime) {
  var maxSlideIndex = $('.mySlide').length - 1; //$('.myNavItem').removeClass('active');
  //$('.myNavItem').eq(number).addClass('active');
  //if (typeof sliderInterval !== "undefined")
  //    activeAnimate = $('.myNavItem').eq(number).find('span').animate({width: '100%'}, during, function(){ $(this).css('width', '0px') });

  var prevNumber = number - 1 < 0 ? maxSlideIndex : number - 1;
  $('.mySlide').eq(prevNumber).animate({
    opacity: 0
  }, tranzTime, function () {
    $(this).css('z-index', 0);
    $('.mySlide').eq(number).css('z-index', 3);
  });
  $('.mySlide').eq(number).animate({
    opacity: 1
  }, tranzTime);
  nextSlideToShow++;

  if (nextSlideToShow > maxSlideIndex) {
    nextSlideToShow = 0;
  }
}

var pulzeParam = 1;

function pulzePulze() {
  return;
  $(".pulze").animate({
    "margin-top": "+=" + 10 * pulzeParam
  }, 500, 'swing', function () {
    pulzeParam *= -1;
    pulzePulze();
  });
}

var speed = 1000;
var during = 4000;

function bringStuffs(currentSlideHtmlObject) {
  currentSlideHtmlObject.find('.seekable').animate({
    top: '-=' + 300 + 'px'
  }, 1);

  if (currentSlideHtmlObject.find('.seekable1')[0]) {
    currentSlideHtmlObject.find('.seekable1').css('display', 'block');
    currentSlideHtmlObject.find('.seekable1').animate({
      top: '+=' + 300 + 'px',
      opacity: 1
    }, speed, "easeOutCubic");

    if (currentSlideHtmlObject.find('.seekable2')[0]) {
      setTimeout(function () {
        currentSlideHtmlObject.find('.seekable2').css('display', 'block');
        currentSlideHtmlObject.find('.seekable2').animate({
          top: '+=' + 300 + 'px',
          opacity: 1
        }, speed, "easeOutCubic");
      }, during);
    }
  }
}
/*
 window.onscroll = function() {
 
 var scrollTop = $(window).scrollTop();
 if ( scrollTop > 3 && !$('.header-top').hasClass('scrolled') ) {
 makeTopScrolled();
 }else if( scrollTop <= 3 && $('.header-top').hasClass('scrolled') ){
 makeTopDefault();
 }
 
 };
 
 
 
 function makeTopScrolled(){
 $('.header-top').addClass('scrolled');
 }
 
 function makeTopDefault(){
 $('.header-top').removeClass('scrolled');
 }
 */


function doAjax(url, data, inner, loading) {
  $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: data,
    beforeSend: function beforeSend() {
      if (loading) $('#' + inner).html("<img src=\"" + base_url + "assets/images/loading.gif\" />");else $('#' + inner).css('opacity', 0.5);
    },
    success: function success(rData) {
      $('#' + inner).html(rData.output);
    }
  });
}
/*  GOOGLE MAP  */


function initMap() {
  var xyBubble = '<center><b style="color:black">Bit Bathroom</b></center>';
  var xyLatLng_center = new google.maps.LatLng(47.5466832, 21.5963141);
  var xyLatLng = new google.maps.LatLng(47.5466832, 21.5963141);
  var mapCanvas = document.getElementById('google_map_inner');
  var mapOptions = {
    center: xyLatLng_center,
    zoom: 15,
    draggable: false,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var infowindow = new google.maps.InfoWindow({
    content: xyBubble
  });
  var image = base_url + 'assets/images/gmapMarker.png';
  var marker = new google.maps.Marker({
    position: xyLatLng,
    map: map,
    icon: image
  });
  google.maps.event.addDomListener(window, "resize", function () {
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
  });
  marker.addListener('click', function () {
    infowindow.open(map, marker);
  }); //infowindow.open(map, marker);
}

function initialize() {
  initMap();
}

function fbs_click() {
  u = location.href;
  t = document.title;
  window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
  return false;
}

function getScrollXY() {
  var x = 0,
      y = 0;

  if (typeof window.pageYOffset == 'number') {
    // Netscape
    x = window.pageXOffset;
    y = window.pageYOffset;
  } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
    // DOM
    x = document.body.scrollLeft;
    y = document.body.scrollTop;
  } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
    // IE6 standards compliant mode
    x = document.documentElement.scrollLeft;
    y = document.documentElement.scrollTop;
  }

  return [x, y];
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];

    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }

    if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
  }

  return "";
}

var fancyOpened = false;
/*
 $(document).ready(function () {
 
 var closePopupCookie = getCookie("closePopupShowed");
 
 $('html').mouseout(function (e) {
 
 if (closePopupCookie != "1" && !fancyOpened && e.pageY < 10 + getScrollXY()[1]) {
 console.log('Fancy opened!');
 
 $('#closepopuptriggerlink').click();
 
 fancyOpened = true;
 
 var d = new Date();
 d.setTime(d.getTime() + (2 * 60 * 60 * 1000));
 var expires = "expires=" + d.toUTCString();
 document.cookie = "closePopupShowed=1" + "; " + expires;
 }
 
 });
 
 });
 */

function runInPrintMode() {
  $('.hero').remove();
  $('.nav').remove();
  $('.breadCrumbHolder').remove();
  $('.social_row').remove();
  $('.thumbnails').remove();
  $('.section.related').remove();
  $('.section.cert-block').remove();
  $('.section.partners-block').remove();
  $('.section.news-block').remove();
  $('.section.footer').remove();
  $('.section.footer_bottom').remove();
  $('.column.right .notLoggedWarning').remove();
  $('.column.right .printIcon').remove();
  $('.subPage.product.productDetail .txt-article').css('padding-bottom', '40px');
  $('.subPage.product .txt-article').css('padding-top', '20px');
  $('.subPage.product.productDetail .container.narrower .columns .column.images .bigImage').css('box-shadow', 'none');
  setTimeout(function () {
    window.print();
  }, 1500);
}
"use strict";

$(document).ready(function () {
  $('#product_category_select').change(function () {
    document.location = $(this).val();
  });

  if ($(window).width() >= 1280) {
    $('.webshop_box .product_img').hover(function () {
      var this_ = $(this);

      if (this_.data('imgsrc2')) {
        this_.hide();
        this_.attr('src', this_.data('imgsrc2'));
        this_.fadeIn(1);
        /*
         this_.fadeTo(200, 0.50, function () {
         this_.attr('src', this_.data('imgsrc2'));
         }).fadeTo(200, 1);*/
      }
    }, function () {
      var this_ = $(this);

      if (this_.data('imgsrc')) {
        this_.hide();
        this_.attr('src', this_.data('imgsrc'));
        this_.fadeIn(1);
        /*
         this_.fadeTo(200, 0.50, function () {
         this_.attr('src', this_.data('src_bk'));
         }).fadeTo(200, 1);*/
      }
    });
  }

  fetchLastBasketItems();
  $('.button.kosar').click(function () {
    var isKapcsolodo = $(this).hasClass("kapcsolodo");
    var itemNumber = $(this).parent().find('.itemNumberHolder').val();
    var itemCount = 1;

    if ($(this).closest('.right').find('.rectangleButton.numberHolder')[0]) {
      itemCount = parseInt($('.rectangleButton.numberHolder').html());
    }

    $.ajax({
      type: "POST",
      url: base_url + "addToBasket/" + itemNumber + "/" + itemCount,
      dataType: "json",
      data: isKapcsolodo ? "isKapcsolodo=1" : null,
      beforeSend: function beforeSend() {//$('#'+inner).html('<img alt="Loading" src="images/loading.gif" />');
      },
      success: function success(rData) {
        $('.badge').html(rData.basketItemCount);
        $.growl.notice({
          title: "",
          message: "<div>" + rData.success_text + "</div>"
        });
        fetchLastBasketItems();
      }
    });
  });
  $('.subPage.productDetail .rectangleButton.plus').click(function () {
    var numberHolder = $(this).parent().find('.rectangleButton.numberHolder');
    increaseItemCountButton(numberHolder);
  });
  $('.subPage.productDetail .rectangleButton.minus').click(function () {
    var numberHolder = $(this).parent().find('.rectangleButton.numberHolder');
    decreaseItemCountButton(numberHolder);
  });
  $('.subPage.basket .rectangleButton.plus').click(function () {
    var numberHolder = $(this).parent().find('.rectangleButton.numberHolder');
    var newItemCount = increaseItemCountButton(numberHolder);
    var itemNumber = $(this).closest('td').find('.itemNumberHolder').val();
    changeBasketItemCount(itemNumber, newItemCount);
  });
  $('.subPage.basket .rectangleButton.minus').click(function () {
    var numberHolder = $(this).parent().find('.rectangleButton.numberHolder');
    var allowZero = true;
    var newItemCount = decreaseItemCountButton(numberHolder, allowZero);
    var itemNumber = $(this).closest('td').find('.itemNumberHolder').val();
    changeBasketItemCount(itemNumber, newItemCount);
  });

  if ($('#delivery_mode_select')[0]) {
    $('#delivery_mode_select').change(function () {
      refreshShippingAndOverallCosts();

      if ($(this).val() == "Personally") {
        if ($("#payment_mode_select option[value='Személyesen készpénzben']").length < 1) {
          $('#payment_mode_select').append($('<option>', {
            value: "Személyesen készpénzben",
            text: "Személyesen készpénzben"
          }));
        }
      } else {
        $("#payment_mode_select option[value='Személyesen készpénzben']").remove();
      }

      if ($(this).val() == "Courier") {
        if ($("#payment_mode_select option[value='Átvételkor a futárnak']").length < 1) {
          $('#payment_mode_select').append($('<option>', {
            value: "Átvételkor a futárnak",
            text: "Átvételkor a futárnak"
          }));
        }
      } else {
        $("#payment_mode_select option[value='Átvételkor a futárnak']").remove();
      }
    });
    $('#payment_mode_select').change(function () {
      refreshShippingAndOverallCosts();
    });
    $('#delivery_zip_field').change(function () {
      refreshShippingAndOverallCosts();
    });
    $('#delivery_region_field').change(function () {
      refreshShippingAndOverallCosts();
    });
    refreshShippingAndOverallCosts();
  }

  if ($('#ertekelesForm')[0]) {
    $('#ertekelesForm').on("submit", function (event) {
      event.preventDefault();
      var formElement = $(this);
      $.ajax({
        type: "POST",
        url: base_url + 'handleRankForm',
        dataType: "json",
        data: formElement.serialize(),
        beforeSend: function beforeSend() {
          /*$('#msgHolder').html("<img src=\""+base_url+"assets/images/loading.gif\" />");*/
        },
        success: function success(rData) {
          //$('#msgHolder').html(rData.output);			   	
          if (rData.success) {
            formElement.find('textarea').val('');
            $.growl.notice({
              title: "Sikeres értékelés!",
              message: rData.output
            });
            formElement.fadeOut();
          } else {
            $.growl.error({
              title: "Hiba!",
              message: rData.output
            });
          }
        }
      });
    });
  }
});

function refreshShippingAndOverallCosts() {
  var payment_mode = $('#payment_mode_select').val();
  var delivery_mode = $('#delivery_mode_select').val();
  var delivery_region = $('#delivery_region_field').val();
  $.ajax({
    type: "POST",
    url: base_url + "refreshShippingAndOverallCost",
    dataType: "json",
    data: "payment_mode=" + payment_mode + "&delivery_mode=" + delivery_mode + "&delivery_region=" + delivery_region,
    beforeSend: function beforeSend() {
      $('.basket_table').css('opacity', 0.5);
    },
    success: function success(rData) {
      $('.shipping_costHolder').html(rData.shipping_cost);
      $('#shipping_cost_hidden_field').val(rData.shipping_cost_value);
      $('.product_sumHolder').html(rData.product_sum);
      $('.overallHolder').html(rData.overall);
      $('.basket_table').css('opacity', 1);
    }
  });
}

function changeBasketItemCount(itemNumber, newItemCount) {
  $.ajax({
    type: "POST",
    url: base_url + "changeBasketItemCount/" + itemNumber + "/" + newItemCount,
    dataType: "json",
    data: null,
    beforeSend: function beforeSend() {
      $('.basket_table').css('opacity', 0.5);
    },
    success: function success(rData) {
      document.location = current_uri;
    }
  });
}

function increaseItemCountButton(numberHolder) {
  var itemCount = parseInt(numberHolder.html());
  var newItemCount = itemCount + 1;
  numberHolder.html(newItemCount);
  return newItemCount;
}

function decreaseItemCountButton(numberHolder, allowZero) {
  var itemCount = parseInt(numberHolder.html());
  var newItemCount = itemCount - 1;
  if (!allowZero && newItemCount < 1) newItemCount = 1;
  numberHolder.html(newItemCount);
  return newItemCount;
}

function removeFromBasket(itemNumber) {
  $.ajax({
    type: "POST",
    url: base_url + "removeFromBasket/" + itemNumber,
    dataType: "json",
    data: null,
    beforeSend: function beforeSend() {//$('#'+inner).html('<img alt="Loading" src="images/loading.gif" />');
    },
    success: function success(rData) {
      if ($('body').hasClass('basket')) {
        //ha kosar aloldalon vagyunk akkor toltsuk inkabb ujra az oldalt
        document.location = current_uri;
      } else {
        fetchLastBasketItems();
        var kiir = rData.basketItemCount ? rData.basketItemCount : '';
        $('.badge').html(kiir);
      }
    }
  });
}

function fetchLastBasketItems() {
  var itemCount = 3;
  $.ajax({
    type: "POST",
    url: base_url + "fetchLastBasketItems/" + itemCount,
    dataType: "json",
    data: null,
    beforeSend: function beforeSend() {//$('#'+inner).html('<img alt="Loading" src="images/loading.gif" />');
    },
    success: function success(rData) {
      $('.basketPanel .badge').html(rData.basketItemCount);
      $('.basketPanel .productInner').html(rData.rows_str);
      $('.basketPanel .summary span').html(rData.overall_str);
    }
  });
}
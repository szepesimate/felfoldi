"use strict";

$(document).ready(function () {
  $("#newsSearchForm, #searchform").on("submit", function (event) {
    event.preventDefault();
    var keyword = $(this).find('input[name=newsSearchField]').val();
    if (keyword.length < 4) $.growl.error({
      title: "",
      message: short_keyword_msg
    });else document.location = base_url + productPageUrl + '/?search=' + encodeURI(keyword);
  });
  $("#category_select").change(function () {
    if ($(this).val() != "") document.location = $(this).val();
  });
  $("#archive_select").change(function () {
    if ($(this).val() != "") document.location = $(this).val();
  });
  $(".subPage.contact #contactForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'contact_ctrl/handle_form',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {
        /*$('#msgHolder').html("<img src=\""+base_url+"assets/images/loading.gif\" />");*/
      },
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);			   	
        if (rData.success) {
          formElement.find('.input').val('');
          formElement.find('textarea').val('');
          $.growl.notice({
            title: "",
            message: rData.output
          });
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });
  $(".langSwitcherForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    var redirectUrl = formElement.find(".redirectUrl").val();

    if (redirectUrl != "") {
      document.location = base_url + redirectUrl;
    } else {
      $.ajax({
        type: "POST",
        url: base_url + 'forms_ctrl/switchLanguage',
        dataType: "json",
        data: formElement.serialize(),
        beforeSend: function beforeSend() {},
        success: function success(rData) {
          if (rData.success) {
            document.location = base_url;
          }
        }
      });
    }
  });
  $("#loginForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'forms_ctrl/handleLoginForm',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {
        /*$('#msgHolder').html("<img src=\""+base_url+"assets/images/loading.gif\" />");*/
      },
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);			   	
        if (rData.success) {
          formElement.find('.field').val('');
          $.growl.notice({
            title: "",
            message: rData.output
          });
          $(".loginPanel").fadeOut(1000);
          setTimeout(function () {
            document.location = current_uri;
          }, 1000);
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });
  $("#logoutForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'forms_ctrl/handleLogoutForm',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {
        /*$('#msgHolder').html("<img src=\""+base_url+"assets/images/loading.gif\" />");*/
      },
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);			   	
        if (rData.success) {
          $(".loginPanel").fadeOut(500);
          setTimeout(function () {
            document.location = current_uri;
          }, 100);
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });
  $(".subPage.reg #reg_form").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'reg_ctrl/handle_form',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {
        /*$('#msgHolder').html("<img src=\""+base_url+"assets/images/loading.gif\" />");*/
      },
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);			   	
        if (rData.success) {
          formElement.find('.field').val('');
          $.growl.notice({
            title: "",
            message: rData.output
          });
          setTimeout(function () {
            document.location = base_url;
          }, 3000);
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });
  $(".subPage.profil #profilForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'Profil_ctrl/handle_form',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {
        /*$('#msgHolder').html("<img src=\""+base_url+"assets/images/loading.gif\" />");*/
      },
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);
        if (rData.success) {
          $.growl.notice({
            title: "",
            message: rData.output
          });
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });
  $(".subPage.forgot_password #forgotPasswordForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'forgot_password_ctrl/handle_email_sender_form',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {
        /*$('#msgHolder').html("<img src=\""+base_url+"assets/images/loading.gif\" />");*/
      },
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);			   	
        if (rData.success) {
          formElement.find('.field').val('');
          $.growl.notice({
            title: "",
            message: rData.output
          });
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });
  $(".subPage.forgot_password #renewPasswordForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'forgot_password_ctrl/handle_password_save_form',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {
        /*$('#msgHolder').html("<img src=\""+base_url+"assets/images/loading.gif\" />");*/
      },
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);			   	
        if (rData.success) {
          formElement.find('.field').val('');
          $.growl.notice({
            title: "",
            message: rData.output
          });
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });
  $("#newsletterForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'forms_ctrl/handleNewsletterForm',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {},
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);			   	
        if (rData.success) {
          formElement.find('.field').val('');
          $.growl.notice({
            title: "",
            message: rData.output
          });
          formElement.trigger('reset');
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });
  $("#unsubForm").on("submit", function (event) {
    event.preventDefault();
    var formElement = $(this);
    $.ajax({
      type: "POST",
      url: base_url + 'forms_ctrl/handleUnsubForm',
      dataType: "json",
      data: formElement.serialize(),
      beforeSend: function beforeSend() {},
      success: function success(rData) {
        //$('#msgHolder').html(rData.output);			   	
        if (rData.success) {
          formElement.find('.field').val('');
          $.growl.notice({
            title: "",
            message: rData.output
          });
        } else {
          $.growl.error({
            title: "",
            message: rData.output
          });
        }
      }
    });
  });

  if ($('.fieldCopyCheckbox')[0]) {
    $('.fieldCopyCheckbox').click(function () {
      if ($(this).is(':checked')) {
        $('input[name=delivery_name]').val($('input[name=billing_name]').val());
        $('input[name=delivery_zip]').val($('input[name=billing_zip]').val());
        $('input[name=delivery_city]').val($('input[name=billing_city]').val());
        $('input[name=delivery_street]').val($('input[name=billing_street]').val());
        $('select[name=delivery_region]').val($('select[name=billing_region]').val());
      } else {
        $('input[name=delivery_name]').val('');
        $('input[name=delivery_zip]').val('');
        $('input[name=delivery_city]').val('');
        $('input[name=delivery_street]').val('');
        $('select[name=delivery_region]').val('');
      }
    });
  }
});
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF {

    function __construct() {
        parent::__construct();
    }

}

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        if ($this->invoiceMode) {
            return;
        }
        $this->SetFont('freesans', '', 8);

        $header_string = <<<EOD
        <p style="text-align:right;line-height:1.5"><br/>
<b>FELFOLDI</b><br/>
<b>Address:</b> 03540 Alicante, Av. Conrado Albaladejo 39./60., España<br/>
<b>E-Mail:</b> info@felfoldi.com</p>
EOD;

        $this->writeHTML($header_string, true, false, true, false, '');

        $image_file = './assets/images/logo_big.jpg';
        $this->Image($image_file, 15, 10, 50, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

        $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'phase' => 0, 'color' => array(0, 0, 0));
        $this->Line(15, 24, 195, 24, $style);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-17);
        // Set font
        $this->SetFont('freesans', '', 8);
        // Page number

        if (!$this->invoiceMode) {

            $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'phase' => 0, 'color' => array(0, 0, 0));
            $this->Line(15, $this->getY() - 2, 195, $this->getY() - 2, $style);

            $footer_string = <<< EOD
<p style="text-align:center;line-height:1.5">
<b>If You need more information regarding our services and products, do not hesitate to contact Us!<br/>
Thank you!</b>
</p>
EOD;

            $this->writeHTML($footer_string, true, false, true, false, '');
        } else {
            $image_file = './assets/images/pdf_footer_logo.jpg';
            $this->Image($image_file, 85, 250, 40, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        }
        //$this->SetY(-15);
        //$this->Cell(0, 10, $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */

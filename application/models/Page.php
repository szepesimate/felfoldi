<?php

class Page extends CI_Model {

    public $parentPage = null;
    public $page = null;
    public $productPageName = null;
    public $category = null;
    public $newProductCategoryFullUrl = null;

    public function __construct() {
        //$this->load->database();
        /*
        if ($this->input->get('dev_mode')) {
            $this->session->set_userdata('dev_mode', 1);
        }
        
        if (!$this->session->userdata('dev_mode')) {
            die();
        }
*/
        if (!$this->session->userdata('site_lang')) {
            Page::setAndLoadDefLanguage();
        }
    }

    public function init_data_with_common_items() {
        $data = [];

        $data['menuItems'] = $this->getMenuItems();
        $template = $this->getTemplate();
        $data['isHome'] = $template == 'index';



        //$data['galleryChooserPageUrl'] = Page::pageUrlByTpl('gallery_chooser');
        $data['newsPageUrl'] = Page::pageUrlByTpl('news');
        $data['productPageUrl'] = Page::pageUrlByTpl('product');
        $data['basketPageUrl'] = Page::pageUrlByTpl('basket');
        
        $data['basketItemCount'] = $this->session->userdata('basket') == false ? 0 : count($this->session->userdata('basket'));

        $data['profilPageUrl'] = Page::pageUrlByTpl('profil'); 
        $data['unsubscribePageUrl'] = Page::pageUrlByTpl('unsubscribe'); 

        $data['forgot_url'] = Page::pageUrlByTpl('forgot_password');
        $data['reg_url'] = Page::pageUrlByTpl('reg');

        $data['adatvedelem_page_url'] = Page::pageUrlById(55);
        $data['aszf_page_url'] = Page::pageUrlById(34);
        $data['deliveryInformation'] = Page::pageUrlById(45);
        $data['payInformation'] = Page::pageUrlById(61);
        $data['contact'] = Page::pageUrlById(31);
        //$data['giftCards'] = Page::pageUrlById(32);
        $data['aboutUs'] = Page::pageUrlById(65);
        $data['career'] = Page::pageUrlById(64);
        $data['jogiPage'] = Page::pageUrlById(62);
        $data['cookiePolicy'] = Page::pageUrlById(63);


        $data['footer_fb_url'] = 'https://www.facebook.com/FelfoldiInyencIzmuves/';
        $data['footer_twitter_url'] = 'https://twitter.com/';
        $data['footer_gplus_url'] = 'https://plus.google.com/';
        $data['footer_pinterest_url'] = 'https://www..com/';
        $data['footer_yt_url'] = 'https://www.youtube.com/';
        $data['footer_insta_url'] = 'https://www.instagram.com/felfoldi_shop_debrecen/';


        $data['categoryMenuItems'] = $this->getCategoryMenuItems();

        /*$recent_post = $this->article->getIndexArticlesByDate(29, 3);
        if ($recent_post) {
            $data['recent_post'] = $recent_post;
        }*/

        
        
        $data['template'] = $template;
        $data['bodyClasses'] = array();
        $data['bodyClasses'][] = $template == 'index' ? '' : 'subPage';
        $data['bodyClasses'][] = $template;

        $subItems_str = "";
        foreach ($data['menuItems'] as $item) {
            if ($item["id"] != $this->page->parent) {
                continue;
            }

            foreach ($item['subItems'] as $subItem) {
                if ($subItem["inSidebarMenu"] == 0) {
                    continue;
                }

                $extraClass = $subItem["urlSegment"] == $this->uri->segment(2) ? ' class="active"' : '';
                $subItems_str .= "<li$extraClass><a href=\"" . $subItem["urlFriendly"] . "\"><nobr><i class=\"fa fa-angle-right fa-2x\" aria-hidden=\"true\"></i> " . lang($subItem["name"]) . "</nobr></a></li>";
            }
        }

        if ($subItems_str != '') {
            $data['bodyClasses'][] = 'hasSideBarMenu';
            $data['subItems_str'] = $subItems_str;
        }

        $seo = $this->getSeoMetaData($productParam = null, $curCatArrParam = null);
        if ($seo) {
            $data['seo_title'] = $seo->title;
            $data['seo_keywords'] = $seo->keywords;
            $data['seo_desc'] = $seo->description;
        }

        $languages = $this->utils->getResultObjectWithQuery("select * from langs where lang != '" . $this->session->userdata('site_lang') . "' ");
        if ($languages) {
            $data['languages'] = $languages;

            $urlTranslations = [];
            foreach ($languages as $lang) {
                $urlTranslations[$lang->lang] = $this->translateUrl($lang->lang);
            }

            if ($urlTranslations)
                $data['urlTranslations'] = $urlTranslations;
        }

        $data['sesslang'] = $this->session->userdata('site_lang');
        $data['langTag'] = $this->session->userdata('site_lang') == $this->config->item('def_lang') ? '' : '_' . $this->session->userdata('site_lang');
        
        return $data;
    }

    public static function setAndLoadDefLanguage() {
        $CI = & get_instance();

        $lang = $CI->session->userdata('site_lang') ? $CI->session->userdata('site_lang') : $CI->config->item('def_lang');
        $CI->session->set_userdata('site_lang', $lang);

        Page::loadLanguage();
    }

    public static function setAndLoadLanguage($lang) {
        $CI = & get_instance();

        $CI->session->set_userdata('site_lang', $lang);

        Page::loadLanguage();
    }

    public static function loadLanguage() {
        $CI = & get_instance();

        $lang = $CI->session->userdata('site_lang');

        if (is_file('./application/language/' . $lang . '/main_lang.php')) {
            $CI->lang->load('main', $lang);
        }
    }

    public static function pageUrl($pageName) {
        $CI = & get_instance();
        $q = $CI->db->get_where("pages", array("name" => $pageName));
        if ($q->num_rows()) {
            $page_id = $q->result_object()[0]->id;
            $q = $CI->db->get_where("seo", array("itemType" => "page", "itemId" => $page_id, 'lang' => $CI->session->userdata('site_lang')));
            if ($q->num_rows()) {
                $urlFriendly = $q->result_object()[0]->urlFriendly;
                return $urlFriendly;
            }
        }

        return "404";
    }

    public static function pageUrlByTpl($tpl) {
        $CI = & get_instance();
        $q = $CI->db->get_where("pages", array("tpl" => $tpl));
        if ($q->num_rows()) {
            $page_id = $q->result_object()[0]->id;
            $q = $CI->db->get_where("seo", array("itemType" => "page", "itemId" => $page_id, 'lang' => $CI->session->userdata('site_lang')));
            if ($q->num_rows()) {
                $urlFriendly = $q->result_object()[0]->urlFriendly;
                return $urlFriendly;
            }
        }

        return "404";
    }

    public static function pageUrlById($page_id) {
        $CI = & get_instance();

        $q = $CI->db->get_where("seo", array("itemType" => "page", "itemId" => $page_id, 'lang' => $CI->session->userdata('site_lang')));
        if ($q->num_rows()) {
            $urlFriendly = $q->result_object()[0]->urlFriendly;
            return $urlFriendly;
        }

        return "404";
    }

    public function getPageAndSubPageUrlArray($subPageId) {
        $lang = $this->session->userdata('site_lang');

        $subPage = $this->utils->getFirstObject('pages', ['id' => $subPageId]);
        $subPageSeo = $this->utils->getFirstObject("seo", ["itemType" => "page", "itemId" => $subPageId, 'lang' => $lang]);

        if (!$subPage || !$subPageSeo || $subPage->parent == 0)
            return null;

        $parentPage = $this->utils->getFirstObject('pages', ['id' => $subPage->parent]);

        if (!$parentPage)
            return null;

        $parentPageSeo = $this->utils->getFirstObject("seo", ["itemType" => "page", "itemId" => $parentPage->id, 'lang' => $lang]);

        if (!$parentPageSeo)
            return null;

        return [$parentPageSeo->urlFriendly, $subPageSeo->urlFriendly];
    }

    public function translateUrl($lang) {
        $redirectPage = $this->parentPage ? $this->parentPage : $this->page;


        if ($redirectPage) {
            $page_seo = $this->utils->getFirstObject("seo", ['itemType' => 'page', 'itemId' => $redirectPage->id, 'lang' => $lang]);
            if ($page_seo)
                return $page_seo->urlFriendly;
        }

        return "";
    }

    public function getMenuItems() {
        $ret_arr = array();

        $lang = $this->session->userdata('site_lang');

        $q = $this->db->query("select pages.*, seo.urlFriendly from pages,seo where pages.id = seo.itemId AND seo.itemType = 'page' AND parent = 0 AND seo.lang = '$lang' order by number asc ");
        if ($q->num_rows()) {
            foreach ($q->result_object() as $o) {

                $subItems = array();
                $q2 = $this->db->query("select pages.*, seo.urlFriendly from pages,seo where pages.id = seo.itemId AND seo.itemType = 'page' AND parent = {$o->id} AND seo.lang = '$lang' order by number asc ");
                if ($q2->num_rows()) {
                    foreach ($q2->result_object() as $o2) {

                        $url = base_url() . $o->urlFriendly . '/' . $o2->urlFriendly;
                        if ($o->id == 1) {
                            $url = base_url() . $o2->urlFriendly;
                        }


                        $subItems[$o2->id] = array(
                            "urlFriendly" => $url,
                            "urlSegment" => $o2->urlFriendly,
                            "id" => $o2->id,
                            "name" => $o2->name,
                            "template" => $o2->tpl,
                            "inMainMenu" => $o2->inMainMenu,
                            "inSidebarMenu" => $o2->inSidebarMenu,
                            "inFooterMenu" => $o2->inFooterMenu,
                        );
                    }
                }

                $urlF = base_url() . $o->urlFriendly;
                /* if ($subItems){
                  $urlF = "javascript:void(0)";
                  }else */ if ($o->id == 1) {
                    $urlF = base_url();
                }

                if ($o->tpl == 'product') {
                    $this->productPageName = $o->name;
                }

                $ret_arr[$o->id] = array(
                    "urlFriendly" => $urlF,
                    "urlSegment" => $o->urlFriendly,
                    "id" => $o->id,
                    "name" => $o->name,
                    "template" => $o->tpl,
                    "inMainMenu" => $o->inMainMenu,
                    "inSidebarMenu" => $o->inSidebarMenu,
                    "inFooterMenu" => $o->inFooterMenu,
                    "subItems" => $subItems
                );
            }
        }


        return $ret_arr;
    }

    public function getCategoryMenuItems() {
        $ret_arr = array();

        $lang = $this->session->userdata('site_lang');

        $q = $this->db->query("select product_cats.*, seo.urlFriendly from product_cats,seo where product_cats.id = seo.itemId AND seo.itemType = 'category' AND parent = 0 AND seo.lang = '$lang' order by number asc ");
        if ($q->num_rows()) {
            $termekekUrl = Page::pageUrlByTpl("product");

            foreach ($q->result_object() as $o) {

                if ($o->urlFriendly == '') {
                    continue;
                }

                $subItems = array();
                $q2 = $this->db->query("select product_cats.*, seo.urlFriendly from product_cats,seo where product_cats.id = seo.itemId AND seo.itemType = 'category' AND parent = {$o->id} AND seo.lang = '$lang' order by number asc ");
                if ($q2->num_rows()) {
                    foreach ($q2->result_object() as $o2) {
                        $subItems[$o2->id] = array(
                            "urlFriendly" => base_url() . $termekekUrl . '/' . $o->urlFriendly . '/' . $o2->urlFriendly,
                            "name" => $o2->name,
                            "id" => $o2->id,
                        );
                    }
                }


                $tmp = array(
                    "urlFriendly" => /* $subItems ? 'javascript:void(0)' : */ base_url() . $termekekUrl . '/' . $o->urlFriendly,
                    "id" => $o->id,
                    "name" => $o->name,
                    "description" => $o->description,
                    "subItems" => $subItems
                );

                $ret_arr[$o->id] = $tmp;

                if ($o->urlFriendly == $this->uri->segment(2)) {
                    $this->category = $tmp;
                }
            }
        }


        return $ret_arr;
    }

    public function findPageByUrlParts($urlParts) {

        $url = $urlParts[0];

        if ($url != "") {
            $seo_obj = $this->utils->getFirstObject("seo", array("urlFriendly" => $url, "itemType" => "page"));
            if ($seo_obj) {
                //van ilyen url, beállítjuk a nyelvet ez alapján
                Page::setAndLoadLanguage($seo_obj->lang);
            }
        } else {
            Page::setAndLoadDefLanguage();
        }


        if ($urlParts[1] != "" && $urlParts[1] != "search") {
            $url = $urlParts[1];
        }


        $lang = $this->session->userdata('site_lang');

        if ($url == "") {
            //főoldalt kell megjeleníteni tpl alapján, a sessionben levő lang szerint
            //$q_ = $this->db->get_where("pages", array("tpl"=>"index"));
            //if ($q_->num_rows()){
            $page_id = 1; //$q_->row()->id;
            $seo_obj = $this->utils->getFirstObject("seo", array("itemId" => $page_id, "itemType" => "page", 'lang' => $lang));
            //}
        } else {
            $seo_obj = $this->utils->getFirstObject("seo", array("urlFriendly" => $url, "itemType" => "page"));
        }

        if ($seo_obj) {

            $q = $this->db->get_where("pages", array("id" => $seo_obj->itemId));
            if ($q->num_rows() == 0)
                return false;

            $foundPage = $q->result_object()[0];

            if ($url != "" && $url == $urlParts[1]) {
                //kell hogy legyen szülője
                if ($foundPage->parent == 0)
                    return false;

                $q = $this->db->get_where("pages", array("id" => $foundPage->parent));
                if ($q->num_rows() == 0)
                    return false;
                $foundParentPage = $q->result_object()[0];

                $this->parentPage = $foundParentPage;

                $q = $this->db->get_where("seo", array("itemId" => $foundParentPage->id, "itemType" => "page", 'lang' => $lang));
                if ($q->num_rows() == 0)
                    return false;
                $parentSeo = $q->result_object()[0];
                if (!$parentSeo || $parentSeo->urlFriendly != $urlParts[0]) {
                    return false;
                }
            } else {
                //nem szabad, hogy legyen szülője
                if ($foundPage->parent != 0)
                    return false;
            }

            $this->page = $foundPage;
            return true;
        }

        return false;
    }

    public function find_page_by_url($url) {

        if ($url != "") {
            $seo_obj = $this->utils->getFirstObject("seo", array("urlFriendly" => $url, "itemType" => "page"));
            if ($seo_obj) {
                //van ilyen url, beállítjuk a nyelvet ez alapján
                Page::setAndLoadLanguage($seo_obj->lang);
            }
        } else {
            Page::setAndLoadDefLanguage();
        }


        $lang = $this->session->userdata('site_lang');

        if ($url == "") {
            //főoldalt kell megjeleníteni tpl alapján, a sessionben levő lang szerint
            //$q_ = $this->db->get_where("pages", array("tpl"=>"index"));
            //if ($q_->num_rows()){
            $page_id = 1; //$q_->row()->id;
            $seo_obj = $this->utils->getFirstObject("seo", array("itemId" => $page_id, "itemType" => "page", 'lang' => $lang));
            //}
        } else {
            $seo_obj = $this->utils->getFirstObject("seo", array("urlFriendly" => $url, "itemType" => "page"));
        }

        if ($seo_obj) {

            $q = $this->db->get_where("pages", array("id" => $seo_obj->itemId));
            if ($q->num_rows() == 0)
                return false;

            $foundPage = $q->result_object()[0];

            //nem szabad, hogy legyen szülője
            if ($foundPage->parent != 0)
                return false;


            $this->page = $foundPage;
            return true;
        }

        return false;
    }

    public function pageFound() {
        return $this->page ? true : false;
    }

    public function getTemplate() {
        return $this->page->tpl;
    }

    public function getSeoMetaData($productParam, $curCatArr) {

        $lang = $this->session->userdata('site_lang');

        if ($productParam != null) {

            $q = $this->db->get_where('seo', ['itemType' => 'product', 'itemId' => $productParam->id, 'lang' => $lang]);
            if ($q->num_rows()) {
                $obj = $q->result_object()[0];
                if ($obj->title != "")
                    return $obj;
            }

            //default product metas:
            //product name - categories
            $custom_seo_obj = new stdClass();
            $custom_seo_obj->title = $productParam->name . " - ";
            $custom_seo_obj->keywords = $productParam->name . ", ";
            $custom_seo_obj->description = $productParam->name . ", ";

            if ($curCatArr != null) {
                if (isset($curCatArr["subCat"])) {
                    $custom_seo_obj->keywords .= $curCatArr["subCat"]->name . ', ';
                    $custom_seo_obj->description .= $curCatArr["subCat"]->name . ', ';
                    $custom_seo_obj->title .= $curCatArr["subCat"]->name . ', ';
                }

                if (isset($curCatArr["cat"])) {
                    $custom_seo_obj->keywords .= $curCatArr["cat"]->name . ', ';
                    $custom_seo_obj->description .= $curCatArr["cat"]->name . ', ';
                    $custom_seo_obj->title .= $curCatArr["cat"]->name . ', ';
                }

                if (isset($curCatArr["mainCat"])) {
                    $custom_seo_obj->keywords .= $curCatArr["mainCat"]->name;
                    $custom_seo_obj->description .= $curCatArr["mainCat"]->name;
                    $custom_seo_obj->title .= $curCatArr["mainCat"]->name;
                }
            }

            return $custom_seo_obj;
        } else {

            if ($curCatArr != null) {
                //default product cat page metas:
                //categories
                $custom_seo_obj = new stdClass();
                $custom_seo_obj->title = "";
                $custom_seo_obj->keywords = "";
                $custom_seo_obj->description = "";

                if (isset($curCatArr["subCat"])) {
                    $custom_seo_obj->keywords .= $curCatArr["subCat"]->name . ', ';
                    $custom_seo_obj->description .= $curCatArr["subCat"]->name . ', ';
                    $custom_seo_obj->title .= $curCatArr["subCat"]->name . ', ';
                }

                if (isset($curCatArr["cat"])) {
                    $custom_seo_obj->keywords .= $curCatArr["cat"]->name . ', ';
                    $custom_seo_obj->description .= $curCatArr["cat"]->name . ', ';
                    $custom_seo_obj->title .= $curCatArr["cat"]->name . ', ';
                }

                if (isset($curCatArr["mainCat"])) {
                    $custom_seo_obj->keywords .= $curCatArr["mainCat"]->name;
                    $custom_seo_obj->description .= $curCatArr["mainCat"]->name;
                    $custom_seo_obj->title .= $curCatArr["mainCat"]->name;
                }

                return $custom_seo_obj;
            } else {
                //ha cikkes részletek aloldal
                if ($this->page->tpl == 'news' && $this->uri->segment(4)) {
                    $article_id = $this->uri->segment(4);
                    $obj = $this->utils->getFirstObject('seo', ['itemType' => 'article', 'itemId' => $article_id, 'lang' => $lang]);
                    if ($obj && $obj->title != "")
                        return $obj;
                }

                //ha sima aloldal
                $obj = $this->utils->getFirstObject('seo', ['itemType' => 'page', 'itemId' => $this->page->id, 'lang' => $lang]);
                if ($obj && $obj->title != "")
                    return $obj;
            }

            //ha semmi sem volt, akkor default aloldal seo metas:
            $obj = $this->utils->getFirstObject('seo', ['itemType' => 'page', 'itemId' => 1, 'lang' => $lang]);
            if ($obj) {
                return $obj;
            }
        }



        return null;
    }

    public function getSliderElements() {
        $sliders = $this->utils->getResultObjectWithQuery("select * from sliders order by number asc");

        return $sliders;
    }

    public function getTopicUrlArray($parent) {

        $lang = $this->session->userdata('site_lang');

        $ret_arr = [];

        $q = $this->db->query("select pages.*, seo.urlFriendly from pages,seo where pages.id = seo.itemId AND seo.itemType = 'page' AND parent = $parent AND seo.lang = '$lang' order by number asc ");
        if ($q->num_rows()) {
            foreach ($q->result_object() as $o) {
                $ret_arr[$o->id] = $o->urlFriendly;
            }
        }

        return $ret_arr;
    }

    public function getTopicUrlArrayBoth() {
        $lang = $this->session->userdata('site_lang');

        $ret_arr = [];

        $q = $this->db->query("select pages.*, seo.urlFriendly from pages,seo where pages.id = seo.itemId AND seo.itemType = 'page' AND parent IN (3,4) AND seo.lang = '$lang' order by number asc ");
        if ($q->num_rows()) {
            foreach ($q->result_object() as $o) {
                $ret_arr[$o->id] = $o->urlFriendly;
            }
        }

        return $ret_arr;
    }

    public function getNewsPagesUrlArray() {
        $lang = $this->session->userdata('site_lang');

        $ret_arr = [];

        $q = $this->db->query("select pages.*, seo.urlFriendly from pages,seo where pages.id = seo.itemId AND seo.itemType = 'page' AND pages.id IN (3,4) AND seo.lang = '$lang' order by number asc ");
        if ($q->num_rows()) {
            foreach ($q->result_object() as $o) {
                $ret_arr[$o->id] = $o->urlFriendly;
            }
        }

        return $ret_arr;
    }

    public function get_child_pages() {
        return $this->utils->getResultObjectWithQuery("select * from pages where parent = " . $this->page->id);
    }

}

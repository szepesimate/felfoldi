<?php

class Gallery extends CI_Model {


	static function get_youtube($url){
/*
        $youtube = "http://www.youtube.com/oembed?url=". $url ."&format=json";

        $curl = curl_init($youtube);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($return, true);

        $video_thumb = $result['thumbnail_url'];
        $video_thumb = str_replace('"\"','',$video_thumb);

        return $video_thumb;
*/

        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );

        $video_id = $my_array_of_vars['v'];
        return "http://img.youtube.com/vi/".$video_id."/0.jpg";

    }

    function reorderVideogallerieItems($orderArr){
        $number = 1;
        foreach ($orderArr as $id){
            $data = array(
               'number' => $number++
            );
            $this->db->where('id', $id);
            $this->db->update('videogalleries_videos', $data);
        }
    }

    function get_videogalleriesItems($id){
        $this->load->model("utils");
        return $this->utils->getResultObject("videogalleries_videos", ["gallery_id" => $id], ['number','asc']);
    }

    function insertVideogalleryItemToDb($insertArray){
        $this->db->insert('videogalleries_videos', $insertArray);
    }


    function deleteVideoGalleryItemById($videoId){
        $this->db->where('id', $videoId);
		$this->db->delete("videogalleries_videos");
    }


	function getVideoGalleries(){
        $ret_arr = false;
        $q = $this->db->order_by('number','asc')->get("videogalleries");
        if ($q->num_rows()){

            foreach ($q->result_object() as $videogallery){
                $videos = $this->get_videogalleriesItems($videogallery->id);
                if ($videos){
                    $ret_arr[] = [
                        "videoGalleryName" => $videogallery->name,
                        "videoGalleryPreview" => $videos[0]->preview,
                        "videos" => $videos
                    ];
                }
            }
        }

        return $ret_arr;
    }






    function reorderGallerieItems($orderArr){
        $number = 1;
        foreach ($orderArr as $id){
            $data = array(
               'number' => $number++
            );
            $this->db->where('id', $id);
            $this->db->update('gallery_images', $data);
        }
    }

    function get_galleriesItems($id){
        $this->load->model("utils");
        return $this->utils->getResultObject("gallery_images", ["gallery_id" => $id], ['number','asc']);
    }
    
    function get_featuredGalleriesItems(){
        $this->load->model("utils");
        $featuredGallery = $this->utils->getFirstObject("galleries", ["featured" => 1]);
        if ($featuredGallery){
            return $this->utils->getResultObjectWithQuery("select * from gallery_images where gallery_id = $featuredGallery->id order by number asc");
        }
        
        return null;
    }
    
    function insertGalleryItemToDb($insertArray){
        $this->db->insert('gallery_images', $insertArray);
    }

    function deleteGalleryItemById($galleryId){
        $this->db->where('id', $galleryId);
		$this->db->delete("gallery_images");
    }

    function getGalleries(){
        $ret_arr = false;
        $q = $this->db->order_by('number','asc')->where('status',1)->get("galleries");
        if ($q->num_rows()){

            foreach ($q->result_object() as $gallery){
                $images = $this->get_galleriesItems($gallery->id);
                if ($images){
                    $ret_arr[] = [
                        "id" => $gallery->id,
                        "galleryName" => $gallery->name,
                        "galleryDescription" => $gallery->description,
                        "images" => $images
                    ];
                }
            }
        }

        return $ret_arr;
    }

    function getGallery($gallery_name) {
        $ret_arr = false;

        $this->db->select('*')->from('galleries')->where("`name` LIKE '$gallery_name'");

        $q = $this->db->get();

        if ($q->num_rows()){

                $gallery = $q->result_object()[0];

                $images = $this->get_galleriesItems($gallery->id);
                if ($images){
                    $ret_arr[] = [
                        "galleryName" => $gallery->name,
                        "galleryDescription" => $gallery->description,
                        "images" => $images
                    ];
                }

        }

        return $ret_arr;
    }

}


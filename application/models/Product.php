<?php

class Product extends CI_Model {

    public $currentCategoryArray = null;
    public $bottomCat = null;

    public function __construct() {
        
    }

    function getFirstCategoryUrl() {
        $category_obj = $this->utils->getFirstObjectWithQuery('select id from product_cats order by id asc limit 1');
        if ($category_obj) {
            $category_url = $this->getProductCategoriesUrl($category_obj->id);
            return $category_url;
        }

        return "";
    }

    function getAllProductCategory() {
        $category_obj = $this->utils->getResultObjectWithQuery('select name, description, imageFileName, id, "" as catUrl from product_cats where parent = 0 order by number');
        if ($category_obj) {
            for ($i = 0; $i < count($category_obj); $i++) {
                $category_obj[$i]->catUrl = $this->getProductCategoriesUrl($category_obj[$i]->id);
            }
          return $category_obj;
        }
  
        return "";
    }

    function insertProductPicToDb($insertArray) {
        $this->db->select_max('number');
        $query = $this->db->get('product_images');
        $maxNumber = 0;
        if ($query->result_object()[0]->number != NULL) {
            $maxNumber = $query->result_object()[0]->number;
        }

        $insertArray['number'] = $maxNumber + 1;
        $this->db->insert('product_images', $insertArray);
    }

    function get_productImages($itemNumber) {
        $query = $this->db->from('product_images')
                ->where(array('itemNumber' => $itemNumber))
                ->order_by('number', 'ASC')
                ->limit(400)
                ->get();
        return $query->result_object();
    }

    function reorderProductImages($orderArr) {
        $number = 1;
        foreach ($orderArr as $id) {
            $data = array(
                'number' => $number++
            );
            $this->db->where('id', $id);
            $this->db->update('product_images', $data);
        }
    }

    function deleteProductImageById($imageId) {
        $fileNameOnServer = $this->getProductImageFileNameById($imageId);

        if (is_file('./assets/images/webshop/big/' . $fileNameOnServer))
            unlink('./assets/images/webshop/big/' . $fileNameOnServer);

        if (is_file('./assets/images/webshop/thumb/' . $fileNameOnServer))
            unlink('./assets/images/webshop/thumb/' . $fileNameOnServer);

        $this->db->delete('product_images', array('id' => $imageId));
    }

    function getProductIdByItemNumber($itemNumber) {
        $query = $this->db->get_where('products', array('itemNumber' => $itemNumber));
        return $query->row()->id;
    }

    function getProductsByBrand($brand, $limit) {

        $query = $this->db->from('products')
                ->where(array('isVisible' => 1, 'gyarto' => $brand))
                ->order_by('number', 'asc')
                ->limit($limit)
                ->get();

        return $this->getProductsArrayWithQueryObject($query);
    }

    function getProductImageFileNameById($imageId) {
        $query = $this->db->get_where('product_images', array('id' => $imageId));
        return $query->row()->fileName;
    }

    function getFirstProductImageFileNameByItemNumber($itemNumber) {
        $query = $this->db->order_by('number', 'asc')->get_where('product_images', ['itemNumber' => $itemNumber], 1);
        if ($query->num_rows())
            return $query->result_object()[0]->fileName;

        return null;
    }

    function deleteProductImagesByProductId($productId) {
        $query = $this->db->from('products')
                ->where('id', $productId)
                ->get();

        $itemNumber = $query->result_object()[0]->itemNumber;

        $query = $this->db->from('product_images')
                ->where('itemNumber', $itemNumber)
                ->get();
        if ($query->num_rows()) {
            foreach ($query->result_object() as $image) {
                $this->deleteProductImageById($image->id);
            }
        }
    }

    function get_categoryOptions() {
        $query = $this->db->from('product_cats')
                ->order_by('number', 'ASC')
                ->get();

        $retArr = null;
        if ($query->num_rows()) {
            $retArr = array();
            foreach ($query->result_object() as $category) {
                $cname = $category->name;
                if ($category->parent != 0)
                    $cname = "--" . $cname;

                $q = $this->db->get_where("product_cats", ["id" => $category->parent]);
                if ($q->num_rows()) {
                    $parent = $q->result_object()[0];
                    if ($parent->parent != 0)
                        $cname = "--" . $cname;
                }

                $retArr[$category->id] = $cname;
            }
        }

        return $retArr;
    }

    function getProductCategoriesUrl($productCatId) {
        $categoriesUrl = "";


        /*
          EZ LASSABB VOLT:

          $helper_row = $this->utils->getFirstObjectWithQuery("select * from product_helper where mCat_id = $productCatId OR cat_id = $productCatId OR sCat_id = $productCatId ");

          if ($helper_row){

          if ($helper_row->sCat_id){
          $cid = $helper_row->sCat_id;
          $q = $this->db->get_where('seo', array('itemType' => 'category', 'itemId' => $cid));
          if ($q->num_rows()) $categoriesUrl = $q->row()->urlFriendly;
          }

          if ($helper_row->cat_id){
          $cid = $helper_row->cat_id;
          $q = $this->db->get_where('seo', array('itemType' => 'category', 'itemId' => $cid));
          if ($q->num_rows()) $categoriesUrl = $q->row()->urlFriendly;
          }

          if ($helper_row->mCat_id){
          $cid = $helper_row->mCat_id;
          $q = $this->db->get_where('seo', array('itemType' => 'category', 'itemId' => $cid));
          if ($q->num_rows()) $categoriesUrl = $q->row()->urlFriendly;
          }

          }

         */


        $cat = $this->db->get_where('product_cats', array('id' => $productCatId))->row();
        $q = $this->db->get_where('seo', array('itemType' => 'category', 'itemId' => $cat->id, 'lang' => $this->session->userdata('site_lang')));
        if ($q->num_rows())
            $categoriesUrl = $q->row()->urlFriendly;

        if ($cat->parent != 0) {
            $cat = $this->db->get_where('product_cats', array('id' => $cat->parent))->row();
            $q = $this->db->get_where('seo', array('itemType' => 'category', 'itemId' => $cat->id, 'lang' => $this->session->userdata('site_lang')));
            if ($q->num_rows())
                $categoriesUrl = $q->row()->urlFriendly . '/' . $categoriesUrl;

            if ($cat->parent != 0) {
                $cat = $this->db->get_where('product_cats', array('id' => $cat->parent))->row();
                $q = $this->db->get_where('seo', array('itemType' => 'category', 'itemId' => $cat->id, 'lang' => $this->session->userdata('site_lang')));
                if ($q->num_rows())
                    $categoriesUrl = $q->row()->urlFriendly . '/' . $categoriesUrl;
            }
        }


        return $categoriesUrl;
    }

    function getProductCategoriesForArukereso($productCatId) {
        $categoriesStr = "";

        $cat = $this->db->get_where('product_cats', array('id' => $productCatId))->row();
        $categoriesStr = $cat->name;

        if ($cat->parent != 0) {
            $cat = $this->db->get_where('product_cats', array('id' => $cat->parent))->row();
            $categoriesStr = $cat->name . ' > ' . $categoriesStr;

            if ($cat->parent != 0) {
                $cat = $this->db->get_where('product_cats', array('id' => $cat->parent))->row();
                $categoriesStr = $cat->name . ' > ' . $categoriesStr;
            }
        }

        $categoriesStr = str_replace('x > ', 'y > ', $categoriesStr);
        $categoriesStr = str_replace('z > ', 'zs > ', $categoriesStr);

        return $categoriesStr;
    }

    function getPopularProducts() {

        $query = $this->db->from('products')
                ->where(array('isVisible' => 1))
                ->order_by('rand()', 'ASC')
                ->limit(12)
                ->get();

        return $this->getProductsArrayWithQueryObject($query);
    }

    function getProductVoteCount($product_id) {

        $votes_count = $this->utils->getFirstObject("votes_count", ["item_id" => $product_id]);

        if ($votes_count)
            return $votes_count->votes_count;

        return 0;
    }

    function getProductVote($product_id) {

        $votes = $this->utils->getFirstObject("votes", ["item_id" => $product_id]);

        if ($votes)
            return $votes->rank;

        return 0;
    }

    public function alreadyRatedItemWithIp($item_type, $item_id, $ip) {
        $ret = $this->utils->getFirstObject("vote_entries", ["item_type" => $item_type, "item_id" => $item_id, "ip" => $ip]) == false ? false : true;
        return $ret;
    }

    public function rateItem($item_type, $item_id, $ip, $rating) {
        $this->db->insert("vote_entries", [
            "item_type" => $item_type,
            "item_id" => $item_id,
            "ip" => $ip,
            "vote" => $rating,
            "date" => date("Y-m-d H:i:s")
        ]);

        $vote = $this->getProductVote($item_id);
        $vote_count = $this->getProductVoteCount($item_id);
        return [$vote, $vote_count];
    }

    function getSearchResultProducts($limit) {

        $searchParam = $this->input->get('search', true);

        $search_cond = "";

        if ($searchParam) {
            $convertedKey = htmlentities($searchParam, ENT_QUOTES, "UTF-8");

            $search_cond = " AND (
                itemNumber like '%$searchParam%' OR
                name like '%$searchParam%' OR
                description like '%$convertedKey%'
            ) ";
        }


        $query = $this->db->query("select * from products where isVisible = 1 $search_cond
        order by store_date desc limit $limit ");

        return $this->getProductsArrayWithQueryObject($query);
    }

    function getRecentProducts($limit) {
        $query = $this->db->from('products')
                ->where(array('isVisible' => 1))
                ->order_by('number', 'asc')
                ->limit($limit)
                ->get();

        return $this->getProductsArrayWithQueryObject($query);
    }

    function getRecentProductsExcludingCat($limit, $excludingCategory) {
        $query = $this->db->from('products')
                ->where(array('isVisible' => 1))
                ->where(array('category !=' => $excludingCategory))
                ->order_by('number', 'asc')
                ->limit($limit)
                ->get();

        return $this->getProductsArrayWithQueryObject($query);
    }

    function getDiscountProducts($limit) {
        $query = $this->db->from('products')
                ->where(array('isVisible' => 1))
                ->where('discountPrice >', 0)
                ->order_by('rand()', 'ASC')
                ->limit($limit)
                ->get();

        return $this->getProductsArrayWithQueryObject($query);
    }

    function getProductsArrayWithQueryObject($query) {
        $retArr = null;


        if ($query->num_rows()) {
            $retArr = array();
            $productPageUrl = base_url();
            $productPageUrl .= Page::pageUrlByTpl("product");



            foreach ($query->result_object() as $product) {
                $arrItem = array();
                $arrItem["productObject"] = $product;
                $productUrl = $this->getProductUrlByProductId($product->id);
                if ($productUrl == null) {
                    continue;
                }

                $categoriesUrl = $this->getProductCategoriesUrl($product->category);
                $arrItem["link"] = $productPageUrl . '/' . $categoriesUrl . '/' . $productUrl;
                //$arrItem["rating"] = $this->getProductVote($product->id);
                //$arrItem["vote_count"] = $this->getProductVoteCount($product->id);

                $q = $this->db->order_by("number", "asc")->get_where('product_images', array('itemNumber' => $product->itemNumber), 2);
                if ($q->num_rows()) {
                    $results = $q->result_object();
                    $imageFileName = $results[0]->fileName;

                    $arrItem["imageFileName"] = base_url() . "assets/images/webshop/thumb/" . $imageFileName;
                    //$arrItem["imageSize"] = getimagesize($arrItem["imageFileName"]);

                    if (isset($results[1])) {
                        $imageFileName2 = $results[1]->fileName;
                        $arrItem["imageFileName2"] = base_url() . "assets/images/webshop/thumb/" . $imageFileName2;
                    }
                } else {
                    $arrItem["imageFileName"] = base_url() . "assets/images/webshop/thumb/default.jpg";
                }
                $retArr[] = (object) $arrItem;
            }
        }

        return $retArr;
    }

    function getCategoryByUrlAndWithParent($url, $parent) {
        $in = array();

        $query = $this->db->from('seo')
                ->where(array('itemType' => 'category', 'urlFriendly' => $url))
                ->get();

        if ($query->num_rows()) {
            foreach ($query->result_object() as $row) {
                $in[] = $row->itemId;
            }
        } else {
            return null;
        }


        $query = $this->db->from('product_cats')
                ->where(array('parent' => $parent))
                ->where_in('id', $in)
                ->get();

        if ($query->num_rows()) {
            return $query->result_object()[0];
        } else {
            return null;
        }
    }

    function setCurrentCategoryArray() {
        $cats = array();

        if ($this->uri->segment(2) == "")
            return null;

        $mainCat = $this->getCategoryByUrlAndWithParent($this->uri->segment(2), 0);
        if (!$mainCat)
            return null;
        $cats['mainCat'] = $mainCat;

        if ($mainCat && $this->uri->segment(3) != "") {

            $cat = $this->getCategoryByUrlAndWithParent($this->uri->segment(3), $mainCat->id);
            $cats['cat'] = $cat;

            if ($cat && $this->uri->segment(4) != "") {

                $subCat = $this->getCategoryByUrlAndWithParent($this->uri->segment(4), $cat->id);
                $cats['subCat'] = $subCat;

                //return $subCat;
            } else {
                //return $cat;
            }
        } else {
            //return $mainCat;
        }


        $this->currentCategoryArray = $cats;

        if (isset($this->currentCategoryArray['subCat'])) {
            $this->bottomCat = $this->currentCategoryArray['subCat'];
        } else if (isset($this->currentCategoryArray['cat'])) {
            $this->bottomCat = $this->currentCategoryArray['cat'];
        } else {
            $this->bottomCat = $this->currentCategoryArray['mainCat'];
        }
    }

    function getProductUrlByProductId($productId) {

        $row = $this->utils->getFirstObject('seo', array('itemType' => 'product', 'itemId' => $productId, 'lang' => $this->session->userdata('site_lang')));

        if (!$row) {
            $row = $this->utils->getFirstObject('seo', array('itemType' => 'product', 'itemId' => $productId));
        }

        if ($row) {
            $productUrl = $row->urlFriendly;
            return $productUrl;
        }

        return null;
    }

    function getCategoryUrlByCategoryId($categoryId) {
        $query = $this->db->get_where('seo', array('itemType' => 'category', 'itemId' => $categoryId));
        if ($query->num_rows()) {
            $productUrl = $query->row()->urlFriendly;
            return $productUrl;
        }

        return null;
    }

    function getProductByUrl($productUrl) {
        $query = $this->db->get_where('seo', array('itemType' => 'product', 'urlFriendly' => $productUrl));
        if ($query->num_rows()) {
            $productId = $query->row()->itemId;
            $query = $this->db->get_where('products', array('id' => $productId, 'isVisible' => 1));
            if ($query->num_rows()) {
                if ($query->row()->category != $this->bottomCat->id)
                    return null;

                return $query->row();
            }
        }
        return null;
    }

    function getProductObjectByItemNumber($itemNumber) {
        $query = $this->db->get_where('products', array('itemNumber' => $itemNumber));
        if ($query->num_rows()) {
            return $query->row();
        }

        return null;
    }

    function get_allProduct() {
        $query = $this->db->get_where('products', ['isVisible' => 1]);
        if ($query->num_rows()) {
            return $query->result_object();
        }

        return null;
    }

    function checkProductUrlExists($productUrl) {
        $query = $this->db->get_where('seo', array('itemType' => 'product', 'urlFriendly' => $productUrl));

        if ($query->num_rows()) {
            return true;
        }

        return false;
    }

    function getProductDetailPageUrl() {
        $productUrl = null;

        if ($this->uri->segment(5)) {
            //van mindhárom kategoria, és utána a termék url
            $productUrl = $this->uri->segment(5);
        } else if (!isset($this->currentCategoryArray['subCat']) && $this->uri->segment(4)) {
            //nincs subCat, de mégis van abban az url segmentben valami, az lesz a termék url
            $productUrl = $this->uri->segment(4);
        } else if (!isset($this->currentCategoryArray['cat']) && $this->uri->segment(3)) {
            //nincs cat, de mégis van abban az url segmentben valami, az lesz a termék url
            $productUrl = $this->uri->segment(3);
        }


        return $productUrl;
    }

    function getSimilarProducts($product) {

        $query = $this->db->from('products')
                ->where(['isVisible' => 1, 'category' => $product->category])
                ->where('itemNumber !=', $product->itemNumber)
                ->order_by('rand()', 'ASC')
                ->limit(5)
                ->get();

        return $this->getProductsArrayWithQueryObject($query);
    }

    function getConnectedProducts($itemNumber) {

        $retArr = null;

        $q = $this->db->get_where('product_conns', ['itemNumber' => $itemNumber]);
        if ($q->num_rows()) {
            $retArr = array();

            $itemNumbersArr = array();
            foreach ($q->result_object() as $pc) {
                $itemNumbersArr[] = $pc->toItemNumber;
            }

            $productPageUrl = base_url();
            $productPageUrl .= Page::pageUrlByTpl("product");

            $query = $this->db->from('products')
                    ->where(['isVisible' => 1])
                    ->where_in("itemNumber", $itemNumbersArr)
                    ->order_by('rand()', 'ASC')
                    ->limit(4)
                    ->get();

            $retArr = $this->getProductsArrayWithQueryObject($query);
        }

        return $retArr;
    }

    function getConnectedProductObjects($itemNumber) {
        $query = $this->db->get_where('product_conns', array('itemNumber' => $itemNumber));

        if ($query->num_rows()) {
            $retArr = array();
            foreach ($query->result_object() as $conn) {
                $q = $this->db->get_where('products', ['itemNumber' => $conn->toItemNumber, 'isVisible' => 1]);
                if ($q->num_rows())
                    $retArr[] = $q->result_object()[0];
            }
            return $retArr;
        }

        return null;
    }

    function resetConnectedProducts($itemNumber) {
        $this->db->where('itemNumber', $itemNumber);
        $this->db->delete('product_conns');
    }

    function setConnectedProductsWithString($itemNumber, $connectedProductsStr) {
        $connectedProductNames_arr = explode(',', $connectedProductsStr);
        if ($connectedProductNames_arr) {
            $this->resetConnectedProducts($itemNumber);
            foreach ($connectedProductNames_arr as $productName) {
                $q = $this->db->get_where('products', ['name' => trim($productName)]);
                if ($q->num_rows()) {
                    $connectedProduct = $q->result_object()[0];
                    $data = array(
                        'itemNumber' => $itemNumber,
                        'toItemNumber' => $connectedProduct->itemNumber
                    );

                    $this->db->insert('product_conns', $data);
                }
            }
        }
    }

    function getFirstProductImageFileNameInCategory($category_id) {
        $q = $this->db->get_where("products", ["category" => $category_id]);
        if ($q->num_rows()) {
            foreach ($q->result_object() as $product) {
                $imageFileName = $this->getFirstProductImageFileNameByItemNumber($product->itemNumber);
                if ($imageFileName)
                    return $imageFileName;
            }
        }

        return null;
    }

    function getCategoryItemCount($mCatId) {
        $cnt = 0;

        if ($mCatId == 85) {

            $res = $this->utils->getFirstObjectWithQuery("select count(*) as cntt from products where cukormentes = 1 OR laktozmentes = 1 OR glutenmentes = 1 OR vegan = 1 ");
            if ($res)
                return $res->cntt;
        }else {

            $subCats = $this->utils->getResultObjectWithQuery("select * from product_cats where parent = $mCatId");
            if ($subCats) {
                foreach ($subCats as $subCat) {
                    $subCats2 = $this->utils->getResultObjectWithQuery("select * from product_cats where parent = $subCat->id ");
                    if ($subCats2) {
                        foreach ($subCats2 as $subCat) {
                            $res = $this->utils->getFirstObjectWithQuery("select count(*) as cntt from products where category = $subCat->id ");
                            $cnt += $res->cntt;
                        }
                    } else {
                        $res = $this->utils->getFirstObjectWithQuery("select count(*) as cntt from products where category = $subCat->id ");
                        $cnt += $res->cntt;
                    }
                }
            } else {
                $res = $this->utils->getFirstObjectWithQuery("select count(*) as cntt from products where category = $mCatId ");
                $cnt += $res->cntt;
            }

            return $cnt;
        }

        /*
          EZ VOLT A LASSABB:
          $res = $this->utils->getFirstObjectWithQuery("select count(*) as cntt from product_helper where mCat_id = $mCatId ");
          if ($res)
          return $res->cntt;
         */


        return 0;
    }

    function getCategoryBoxes() {
        $retArr = null;

        if ($this->currentCategoryArray) {
            //van -e alkategória az aktuális url szerinti kategóriában
            $query = $this->db->order_by("number", "asc")->get_where("product_cats", ["parent" => $this->bottomCat->id]);
            if ($query->num_rows()) {
                $retArr = array();

                $productPageUrl = base_url();
                $productPageUrl .= Page::pageUrlByTpl("product");
                $productPageUrl .= '/';

                $categoriesUrl = $this->uri->segment(2) . "/";
                if (isset($this->currentCategoryArray['cat']))
                    $categoriesUrl .= $this->uri->segment(3) . "/";
                if (isset($this->currentCategoryArray['subCat']))
                    $categoriesUrl .= $this->uri->segment(4) . "/";

                foreach ($query->result_object() as $category) {
                    $arrItem = array();
                    $arrItem["catObject"] = $category;
                    $categoryUrl = $this->getCategoryUrlByCategoryId($category->id);
                    if ($categoryUrl == null)
                        continue;

                    $arrItem["link"] = $productPageUrl . $categoriesUrl . $categoryUrl;
                    //megnézni, van e kategoria kép
                    $this->load->helper('text');
                    $catFileName = url_title(convert_accented_characters($category->name), '-', true) . ".jpg";
                    $catImagePath = "./assets/images/webshop/categories/" . $catFileName;
                    if (is_file($catImagePath)) {
                        $arrItem["imageFileName"] = base_url() . "assets/images/webshop/categories/" . $catFileName;
                    } else {
                        $firstProductImageFileName = $this->getFirstProductImageFileNameInCategory($category->id);

                        if ($firstProductImageFileName) {
                            $arrItem["imageFileName"] = base_url() . "assets/images/webshop/thumb/" . $firstProductImageFileName;
                        }
                    }

                    //$arrItem["imageSize"] = getimagesize($arrItem["imageFileName"]);

                    $retArr[] = (object) $arrItem;
                }
            }
        } else {
            //nincs semmi értelmezhető termék kategória, legyen 404
            redirect(base_url() . '404');
        }

        return $retArr;
    }

    function getProductsWithCriteria($search_cond) {
        $retArr = null;

        $order = ' store_date asc ';

        $query = $this->db->query("select *, IF(discountPrice = 0, price, discountPrice) as orderable
                from products WHERE $search_cond AND isVisible = 1
                order by $order
                  ");

        if ($query->num_rows()) {
            $retArr = array();
            $productPageUrl = base_url();
            $productPageUrl .= Page::pageUrlByTpl("product");
            $productPageUrl .= '/';

            foreach ($query->result_object() as $product) {

                $categoriesUrl = "";

                //ha nem főkategoria a bottomCat, akkor most valamelyik alkategoriában vagyunk
                if ($this->bottomCat && $this->bottomCat->parent != 0) {
                    $categoriesUrl = $this->uri->segment(2) . "/";
                    if (isset($this->currentCategoryArray['cat']))
                        $categoriesUrl .= $this->uri->segment(3) . "/";
                    if (isset($this->currentCategoryArray['subCat']))
                        $categoriesUrl .= $this->uri->segment(4) . "/";
                }else {
                    $categoriesUrl = $this->getProductCategoriesUrl($product->category);
                    $categoriesUrl .= '/';
                }




                $arrItem = array();
                $arrItem["productObject"] = $product;
                $productUrl = $this->getProductUrlByProductId($product->id);
                if ($productUrl == null)
                    continue;

                $arrItem["link"] = $productPageUrl . $categoriesUrl . $productUrl;
                //$arrItem["rating"] = $this->getProductVote($product->id);
                ////$arrItem["vote_count"] = $this->getProductVoteCount($product->id);

                $q = $this->db->order_by('number', 'asc')->get_where('product_images', array('itemNumber' => $product->itemNumber), 1);
                if ($q->num_rows()) {
                    $results = $q->result_object();
                    $imageFileName = $results[0]->fileName;

                    $arrItem["imageFileName"] = base_url() . "assets/images/webshop/thumb/" . $imageFileName;
                    //$arrItem["imageSize"] = getimagesize($arrItem["imageFileName"]);

                    if (isset($results[1])) {
                        $imageFileName2 = $results[1]->fileName;
                        $arrItem["imageFileName2"] = base_url() . "assets/images/webshop/thumb/" . $imageFileName2;
                    }
                }
                $retArr[] = (object) $arrItem;
            }
        }



        return $retArr;
    }

    function getProductsInCurrentCategory($category_ids) {
        $retArr = null;

        $searchParam = $this->input->get('search', true);


        $category_cond = " category = " . $this->bottomCat->id;


        $findSubCats = $this->utils->getResultObject('product_cats', ['parent' => $this->bottomCat->id], ['id', 'asc']);
        if ($findSubCats) {
            $category_ids = [];
            foreach ($findSubCats as $c) {
                $category_ids[] = $c->id;
            }
        }


        if ($category_ids != null) {
            /* if ($category_ids == 'special_products') {
              $category_cond = " ( cukormentes = 1 OR laktozmentes = 1 OR glutenmentes = 1 OR vegan = 1 ) ";
              } else */ {
                $category_cond = " category IN (" . implode(',', $category_ids) . ") ";
            }
        } else {

            /* if ($this->bottomCat->id == 11) {
              $category_cond = " paleo = 1 ";
              } else if ($this->bottomCat->id == 12) {
              $category_cond = " energy_reduced = 1 ";
              } */
        }

        $search_cond = "";

        if ($searchParam) {
            $convertedKey = htmlentities($searchParam, ENT_QUOTES, "UTF-8");
            ;
            $search_cond = " AND (
                itemNumber like '%$searchParam%' OR
                name like '%$searchParam%' OR
                description like '%$convertedKey%'
            ) ";
        }

        if ($this->currentCategoryArray) { {
                //termék listázó aloldal
                /*
                  $query = $this->db->from('products')
                  ->where(array('category' => $this->bottomCat->id, 'isVisible' => 1))
                  ->order_by('id', 'ASC')
                  ->get();
                 */



                $order = ' number asc ';

                $order_session = $this->session->userdata('product_order');
                if ($order_session) {
                    if ($order_session == 'recent')
                        $order = ' store_date desc ';
                    else if ($order_session == 'price_asc')
                        $order = ' orderable asc ';
                    else if ($order_session == 'price_desc')
                        $order = ' orderable desc ';
                }



                $query = $this->db->query("select *, IF(discountPrice = 0, price, discountPrice) as orderable
                from products WHERE $category_cond  $search_cond  AND isVisible = 1
                order by $order
                  ");

                if ($query->num_rows()) {
                    $retArr = array();
                    $productPageUrl = base_url();
                    $productPageUrl .= Page::pageUrlByTpl("product");
                    $productPageUrl .= '/';




                    foreach ($query->result_object() as $product) {

                        $categoriesUrl = "";

                        //ha nem főkategoria a bottomCat, akkor most valamelyik alkategoriában vagyunk
                        if ($this->bottomCat->parent != 0) {
                            $categoriesUrl = $this->uri->segment(2) . "/";
                            if (isset($this->currentCategoryArray['cat']))
                                $categoriesUrl .= $this->uri->segment(3) . "/";
                            if (isset($this->currentCategoryArray['subCat']))
                                $categoriesUrl .= $this->uri->segment(4) . "/";
                        }else {
                            $categoriesUrl = $this->getProductCategoriesUrl($product->category);
                            $categoriesUrl .= '/';
                        }




                        $arrItem = array();
                        $arrItem["productObject"] = $product;
                        $productUrl = $this->getProductUrlByProductId($product->id);
                        if ($productUrl == null)
                            continue;

                        $arrItem["link"] = $productPageUrl . $categoriesUrl . $productUrl;
                        //$arrItem["rating"] = $this->getProductVote($product->id);
                        //$arrItem["vote_count"] = $this->getProductVoteCount($product->id);

                        $q = $this->db->order_by('number', 'asc')->get_where('product_images', array('itemNumber' => $product->itemNumber), 2);
                        if ($q->num_rows()) {
                            $results = $q->result_object();
                            $imageFileName = $results[0]->fileName;

                            $arrItem["imageFileName"] = base_url() . "assets/images/webshop/thumb/" . $imageFileName;
                            //$arrItem["imageSize"] = getimagesize($arrItem["imageFileName"]);

                            if (isset($results[1])) {
                                $imageFileName2 = $results[1]->fileName;
                                $arrItem["imageFileName2"] = base_url() . "assets/images/webshop/thumb/" . $imageFileName2;
                            }
                        }
                        $retArr[] = (object) $arrItem;
                    }
                }
            }
        } else {
            //nincs semmi értelmezhető termék kategória, legyen 404
            redirect(base_url() . '404');
        }

        return $retArr;
    }

    public function getConnectedProductsFromOrders() {
        $q = $this->db->query("SELECT itemNumber, name, count(id) as cntt  FROM `order_items` where isConnected = 1 group by itemNumber");
        if ($q->num_rows()) {
            return $q->result_object();
        }

        return null;
    }

    public function storeProductRank($fields) {
        $insertSuccess = $this->db->insert('rates', $fields);
        return $insertSuccess;
    }

    public function getRatesByItemNumber($itemNumber) {

        $q = $this->db->order_by('date', 'desc')->get_where("rates", ["itemNumber" => $itemNumber, "public" => 1]);
        if ($q->num_rows()) {
            $ret_arr = array();

            foreach ($q->result_object() as $rate) {
                $order = $this->db->get_where('orders', ['id' => $rate->order_id]);
                $ret_arr[] = [
                    "rateObject" => $rate,
                    "name" => $order->result_object()[0]->name
                ];
            }


            return $ret_arr;
        }

        return null;
    }

    public function getProductById($id) {
        return $this->utils->getResultObjectWithQuery("select * from products where id = $id");
    }

}

<?php

class Testimonial extends CI_Model {
    public function getTestimonials() {

        $q = $this->db->get_where('testimonials', array('status' => 1));

        if ($q->num_rows()) {
            $testimonials = $q->result_object();
            return $testimonials;
        }

        return null;
    }
}

<?php

class Crud_model extends CI_Model {

    private $tableName;
    private $searchFieldName = "name";
    private $def_order = "id";

    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct() {
        //$this->load->database();
    }

    public function setTableName($tableName) {
        $this->tableName = $tableName;
    }

    public function setSearchFieldName($searchFieldName) {
        $this->searchFieldName = $searchFieldName;
    }

    public function setDefaultOrder($def_order) {
        $this->def_order = $def_order;
    }

    public function getModuls() {
        $menuItems = array();

        if (!$this->session->userdata('admin_uid'))
            return false;
;
        $q = $this->db->query("SELECT admin_moduls.* FROM admin_moduls, admin_permission WHERE parent = 0 AND admin_moduls.id = admin_permission.modul_id AND admin_permission.user_id = " . $this->session->userdata('admin_uid') . "
order by number asc");
        if ($q->num_rows()) {
            foreach ($q->result_object() as $mainMenuItem) {
                $tmp_arr = ["mainMenuItem" => $mainMenuItem];
                $q2 = $this->db->query("SELECT admin_moduls.* FROM admin_moduls, admin_permission
                WHERE parent = " . $mainMenuItem->id . " AND admin_moduls.id = admin_permission.modul_id AND admin_permission.user_id = " . $this->session->userdata('admin_uid') . "
order by number asc");
                if ($q2->num_rows())
                    $tmp_arr["subMenuItems"] = $q2->result_object();
                $menuItems[] = $tmp_arr;
            }
        }

        return $menuItems;
    }

    public function getCurrentModul() {
        $query = $this->db->get_where('admin_moduls', array('url' => $this->uri->segment(2)));
        return $query->row();
    }

    public function checkPermission($data) {
        if (!$data["moduls"]){
            redirect('/admin/');
        }
        foreach ($data["moduls"] as $modul) {
            if ($data["currentModul"]->url == $modul["mainMenuItem"]->url)
                return true;

            if (isset($modul["subMenuItems"]) && $modul["subMenuItems"]) {

                foreach ($modul["subMenuItems"] as $subModul) {
                    if ($data["currentModul"]->url == $subModul->url)
                        return true;
                }
            }
        }

        return false;
    }

    /**
     * Get result_array by his is
     * @param int $id
     * @return array
     */
    public function get_row_by_id($id) {
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Fetch data from the database
     * possibility to mix search, filter and order
     * @param string $search_string
     * @param strong $order
     * @param string $order_type
     * @param int $limit_start
     * @param int $limit_end
     * @param int $parent
     * @return array
     */
    public function get_rows($search_string = null, $order = null, $order_type = 'Asc', $limit_start, $limit_end, $parent = null) {

        $this->db->select('*');
        $this->db->from($this->tableName);

        if ($search_string) {
            $this->db->like($this->searchFieldName, $search_string);
        }

        if ($parent) {
            $this->db->where('parent', $parent);
        }

        if ($this->tableName == "seo") {
            $this->db->where_not_in('itemType', ['article']);
        }


        if ($order) {
            $this->db->order_by($order, $order_type);
        } else {
            $this->db->order_by($this->def_order, $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_rowsTree($search_string = null, $order = null, $order_type = 'Asc', $limit_start, $limit_end) {

        $this->db->select('*');
        $this->db->from($this->tableName);

        if ($order) {
            $this->db->order_by($order, $order_type);
        } else {
            $this->db->order_by('number', $order_type);
        }

        $this->db->where('parent', 0);


        $query = $this->db->get();

        $retArr = array();

        foreach ($query->result_object() as $rootObj) {
            $tmp = array();
            $tmp["rootObj"] = $rootObj;

            $this->db->select('*');
            $this->db->from($this->tableName);
            $this->db->where('parent', $rootObj->id);
            $this->db->order_by('number', $order_type);
            $subObjects = $this->db->get()->result_object();
            if ($subObjects)
                $tmp["subObjects"] = $subObjects;

            $retArr[] = $tmp;
        }

        return $retArr;
    }

    /**
     * Count the number of rows
     * @param int $search_string
     * @param int $order
     * @return int
     */
    function count_rows($search_string = null, $order = null) {
        $this->db->select('*');
        $this->db->from($this->tableName);

        if ($search_string) {
            $this->db->like($this->searchFieldName, $search_string);
        }
        if ($order) {
            $this->db->order_by($order, 'Asc');
        } else {
            $this->db->order_by('id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
     * Store the new item into the database
     * @param array $data - associative array with data to store
     * @return boolean
     */
    function store_row($data) {
        $insert = $this->db->insert($this->tableName, $data);
        return $insert;
    }

    /**
     * Update row
     * @param array $data - associative array with data to store
     * @return boolean
     */
    function update_row($id, $data) {
        $this->db->where('id', $id);
        $res = $this->db->update($this->tableName, $data);
        //$report = array();
        //$report['error'] = $this->db->_error_number();
        //$report['message'] = $this->db->_error_message();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete row
     * @param int $id - id
     * @return boolean
     */
    function delete_row($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->tableName);
    }

    function delete_seo_entry($id, $itemType, $lang = null) {
        //delete from seo table
        $this->db->where('itemId', $id);
        $this->db->where('itemType', $itemType);
        if ($lang)
            $this->db->where('lang', $lang);
        $this->db->delete('seo');
    }

    function insert_seo_entry($itemId, $urlFriendly, $itemType, $lang) {

        $data = array(
            'itemType' => $itemType,
            'itemId' => $itemId,
            'urlFriendly' => $urlFriendly,
            'lang' => $lang
        );

        $this->db->insert('seo', $data);
    }

    function getSeoEntryForArticle($itemId, $lang) {
        $this->load->model("utils");
        return $this->utils->getFirstObject("seo", ["itemType" => "article", "itemId" => $itemId, 'lang' => $lang]);
    }

    function insert_seo_entry_article($itemId, $metas, $lang) {
        $data = array(
            'itemType' => 'article',
            'itemId' => $itemId,
            'title' => $metas[0],
            'keywords' => $metas[1],
            'description' => $metas[2],
            'lang' => $lang
        );

        $this->db->insert('seo', $data);
    }

    function insert_seo_entry_tanfolyam($itemId, $metas, $lang) {
        $this->load->model('utils');

        $data = array(
            'itemType' => 'tanfolyam',
            'itemId' => $itemId,
            'title' => $metas[0],
            'keywords' => $metas[1],
            'urlFriendly' => $this->utils->convertUrlFormat($metas[0]),
            'description' => $metas[2],
            'lang' => $lang
        );

        $this->db->insert('seo', $data);
    }

    function update_seo_entry_article($itemId, $metas, $lang) {

        $this->load->model("utils");
        $seo_entry_exists = $this->utils->getFirstObject('seo', ['itemType' => 'article', 'itemId' => $itemId, 'lang' => $lang]);

        if ($seo_entry_exists == false) {
            $this->insert_seo_entry_article($itemId, $metas, $lang);
        } else {
            $data = array(
                'title' => $metas[0],
                'keywords' => $metas[1],
                'urlFriendly' => $this->utils->convertUrlFormat($metas[0]),
                'description' => $metas[2]
            );
            $this->db->where('itemId', $itemId)->where('itemType', 'article')->where('lang', $lang)->update('seo', $data);
        }
    }

    function update_seo_entry_tanfolyam($itemId, $metas, $lang) {

        $this->load->model("utils");
        $seo_entry_exists = $this->utils->getFirstObject('seo', ['itemType' => 'tanfolyam', 'itemId' => $itemId, 'lang' => $lang]);

        if ($seo_entry_exists == false) {
            $this->insert_seo_entry_tanfolyam($itemId, $metas, $lang);
        } else {
            $data = array(
                'title' => $metas[0],
                'keywords' => $metas[1],
                'description' => $metas[2]
            );
            $this->db->where('itemId', $itemId)->where('itemType', 'tanfolyam')->where('lang', $lang)->update('seo', $data);
        }
    }

    function getSeoEntryForPage($itemId, $lang) {
        $this->load->model("utils");
        return $this->utils->getFirstObject("seo", ["itemType" => "page", "itemId" => $itemId, 'lang' => $lang]);
    }

    function insert_seo_entry_page($itemId, $metas, $lang) {
        $data = array(
            'itemType' => 'page',
            'itemId' => $itemId,
            'title' => $metas[0],
            'description' => $metas[1],
            'urlFriendly' => $metas[2],
            'lang' => $lang
        );

        $this->db->insert('seo', $data);
    }

    function update_seo_entry_page($itemId, $metas, $lang) {

        $this->load->model("utils");
        $seo_entry_exists = $this->utils->getFirstObject('seo', ['itemType' => 'page', 'itemId' => $itemId, 'lang' => $lang]);

        if ($seo_entry_exists == false) {
            $this->insert_seo_entry_article($itemId, $metas, $lang);
        } else {
            $data = array(
                'title' => $metas[0],
                'description' => $metas[1],
                'urlFriendly' => $metas[2],
            );
            $this->db->where('itemId', $itemId)->where('itemType', 'page')->where('lang', $lang)->update('seo', $data);
        }
    }

    function reorderTable($orderArr) {
        $number = 1;
        foreach ($orderArr as $id => $parent) {
            if (!$parent || $parent == 'null')
                $parent = 0;
            $data = array(
                'parent' => $parent,
                'number' => $number++
            );
            $this->db->where('id', $id);
            $this->db->update($this->tableName, $data);
        }
    }

    function switchProperties($dbId, $dbField, $currentValue) {
        $newValue = $currentValue ? 0 : 1;
        $data = array(
            $dbField => $newValue
        );
        $this->db->where('id', $dbId);
        $this->db->update($this->tableName, $data);
    }

    function get_pageOptions() {
        $query = $this->db->from('pages')
                ->order_by('number', 'ASC')
                ->get();

        $retArr = null;
        if ($query->num_rows()) {
            $retArr = array();
            foreach ($query->result_object() as $category) {
                $retArr[$category->id] = ($category->parent ? '--' : '') . $category->name;
            }
        }

        return $retArr;
    }

    function get_main_page_objects() {
        return $this->utils->getResultObject('pages', ['parent' => 0/* , 'inMainMenu' => 1 */], ['number', 'ASC']);
    }

    function reset_admin_permission($admin_user_id, $modul_id_array) {
        $this->db->where(['user_id' => $admin_user_id])->delete('admin_permission');

        if ($modul_id_array) {
            foreach ($modul_id_array as $modul_id) {
                $this->db->insert("admin_permission", ['user_id' => $admin_user_id, 'modul_id' => $modul_id]);
            }
        }
    }

}

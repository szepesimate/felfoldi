<?php

class Users extends CI_Model {

    function __construct() {
        
    }

    function registrate($fields) {

        $this->db->insert('f_user', $fields);

        return $this->db->insert_id();
    }

    function activateUser($id) {

        $this->db->where('id', $id);
        $this->db->update('f_user', array("activated" => 1));

        return true;
    }

    function loginWithIdAndEmail($id, $email, $name) {
        $data = array(
            'uid' => $id,
            'email' => $email,
            'name' => $name
        );
        $this->session->set_userdata($data);

        $this->db->where('id', $id);
        $this->db->update('f_user', array("last_login_date" => date("Y-m-d H:i:s")));
    }

    function login($email, $password) {
        $query = $this->db->get_where('f_user', array("email" => $email, "password" => md5($password), "activated" => 1, "account_allowed" => 1));
        if ($query->num_rows()) {
            $this->loginWithIdAndEmail($query->result_object()[0]->id, $email, $query->result_object()[0]->name);
            return true;
        }

        return false;
    }

    function logout() {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('uid');
        $this->session->unset_userdata('name');
    }

    function get_user_data($id) {
        $q = $this->db->get_where('f_user', ['id' => $id]);
        if ($q->num_rows())
            return $q->result_object()[0];

        return false;
    }

    function updateUserData($fields, $id) {
        $this->db->where('id', $id);
        $this->db->update('f_user', $fields);
    }

    function changeUserPasswordByEmail($password, $email) {
        $this->db->where('email', $email);
        $this->db->update('f_user', ["password" => md5($password)]);
    }

    function checkUserEmailExists($email) {
        $q = $this->db->get_where("f_user", ["email" => $email]);
        return $q->num_rows() ? true : false;
    }

    function getUserData($id) {
        if (!$id)
            return null;
        $query = $this->db->get_where('f_user', array("id" => $id));
        if ($query->num_rows())
            return $query->result_object()[0];

        return null;
    }

    function get_all_user() {

        $query = $this->db->order_by('name', 'asc')->where('name !=', '')->where('nickname !=', '')->get('f_user');
        if ($query->num_rows())
            return $query->result_object();

        return null;
    }

    function get_friends() {
        $my_user_id = $this->session->userdata('uid');
        $ret_arr = [];
        $q = $this->db->query("select * from friends where rejected = 0 AND ( user_id = {$my_user_id} OR friend_id = {$my_user_id} )");
        if ($q->num_rows()) {
            foreach ($q->result_object() as $friend_id_obj) {
                $friend_id = $friend_id_obj->friend_id != $my_user_id ? $friend_id_obj->friend_id : $friend_id_obj->user_id;
                $q2 = $this->db->get_where('f_user', ['id' => $friend_id]);
                if ($q2->num_rows()) {
                    $ret_arr[] = $q2->result_object()[0];
                }
            }
        }

        return $ret_arr;
    }

    function user_already_my_friend($friend_id) {
        $my_user_id = $this->session->userdata('uid');
        $query = $this->db->query("select id from friends where rejected = 0 AND ( (user_id = {$my_user_id} AND friend_id = {$friend_id}) OR (user_id = {$friend_id} AND friend_id = {$my_user_id}) )");

        if ($query->num_rows())
            return true;

        return false;
    }

    function getF_user_basket($id) {
        if (!$id)
            return null;
        $query = $this->db->get_where('f_user_basket', array("id" => $id));
        if ($query->num_rows())
            return $query->result_object()[0];

        return null;
    }

    function setF_user_basketEmailSent($f_user_basket_id) {
        $this->db->where('id', $f_user_basket_id);
        $this->db->update('f_user_basket', ['emailSent' => 1]);
    }

    function storeOrderData($data) {
        //$this->load->model('basket_model');
        $basketTableRows = $this->basket_model->getBasketTableRows();

        $this->db->insert('orders', $data);
        $order_id = $this->db->insert_id();

        $batch_arr = array();

        $connectedProducts = $this->session->userdata('itemsFromConnectedProducts');

        foreach ($basketTableRows["rows"] as $row) {
            $isConnected = $connectedProducts && isset($connectedProducts[$row->itemNumber]) ? 1 : 0;
            $data_arr = array(
                "order_id" => $order_id,
                "itemNumber" => $row->itemNumber,
                "name" => $row->name,
                "count" => $row->count,
                "price" => $row->displayPrice,
                "isConnected" => $isConnected
            );
            
            //decrease stock count
            $this->db->query("update products SET stock = stock - 1 WHERE itemNumber = '$row->itemNumber'");

            $batch_arr[] = $data_arr;
        }


        $this->db->insert_batch('order_items', $batch_arr);
        
        
        return $order_id;
    }

    function sendOrderEmails($order_id) {
        $shortDomainName = $this->config->item('shortDomainName');

        $q = $this->db->get_where('orders', ["id" => $order_id]);
        if ($q->num_rows()) {
            $order_obj = $q->result_object()[0];

            $message = "
			".lang('Kedves '). $order_obj->name ." <br/><br/>
			".lang('Köszönjük szépen, hogy minket választottál!')." <br/>
			".lang('Rendelésedet sikeresen megkaptuk, jelenleg feldolgozás alatt áll.')." <br/>
			".lang('Dolgozunk azon, hogy minél előbb kézhez kaphasd kiválasztott
            finomságaidat!')." <br/>
			".lang('A rendelésed adatait, kosarad pontos tartalmát lent találod.')." <br/><br/>

			".lang('Name').": $order_obj->name <br/>
			E-mail: $order_obj->email <br/>
			".lang('Phone').": $order_obj->phone <br/>
                        ".lang('Delivery mode').": ".lang($order_obj->delivery_mode)." <br/>
                        ".lang('Payment mode').": ".lang($order_obj->payment_mode)." <br/>
			".lang('Delivery name').": $order_obj->delivery_name <br/>
			".lang('Delivery address').": $order_obj->delivery_zip $order_obj->delivery_city, $order_obj->delivery_street <br/>                        
			".lang('Billing name').": $order_obj->billing_name <br/>
			".lang('Billing address').": $order_obj->billing_zip $order_obj->billing_city, $order_obj->billing_street <br/>

			<br/>
			".lang('Comment').": $order_obj->comment

			<br/><br/>

			".lang('Products').": <br/><br/>

			<table border=\"1\" cellpadding=\"10\">
			<tr><th>".lang('Item number')."</th><th>".lang('Product')."</th><th>".lang('Item price')."</th><th>".lang('Quantity')."</th><th>".lang('Value')."</th></tr>
";

            $q = $this->db->get_where('order_items', ["order_id" => $order_id]);
            $sum = 0;
            if ($q->num_rows()) {
                foreach ($q->result_object() as $order_item) {
                    $ertek = number_format($order_item->count * $order_item->price, 0, '', '.');
                    $price = number_format($order_item->price, 0, '', '.');
                    $message .= <<<EOF
					<tr><td>$order_item->itemNumber</td><td>$order_item->name</td><td>$price Ft</td>
					<td>$order_item->count</td><td>$ertek Ft</td></tr>
EOF;
                    $sum += $order_item->count * $order_item->price;
                }
            }

            $shipping_cost_str = number_format($order_obj->shipping_cost, 0, '', '.');

            $overall_str = number_format($order_obj->shipping_cost + $sum, 0, '', '.');


            $message .= "

			<tr><td colspan=\"4\">".lang('Shipping cost')."</td>
					<td>$shipping_cost_str Ft</td></tr>
			</table>

			<br/><br/>

			".lang('Total').": $overall_str Ft
                        
                        <br/><br/>
                    
                        ".lang('A szállítás várható ideje a megrendeléstől/utalás megérkezésétől
                        számított 2-5 munkanap (1-2 napos késés előfordulhat a csomagszám
                        megnövekedése esetén, ez esetben türelmedet kérjük).')."

                        <br/><br/>
                    
                        ".lang('A további teendőkkel kapcsolatban kollégánk hamarosan fel fogja venni
                        veled a kapcsolatot. A csomagod kiszállításával kapcsolatban
                        futárszolgálatunk keresni fog a megadott elérhetőségeid egyikén.').":

                        <br/><br/>
                    
			".lang('Best Regards').", <br/>
";

            $message .= ucfirst($shortDomainName);

            $success = $this->utils->sendmail('noreply@' . $shortDomainName, ucfirst($shortDomainName), $order_obj->email, lang('Order confirmation').' - ' . ucfirst($shortDomainName), $message);
            if ($order_obj->email != 'adamjuhasz@me.com' || $order_obj->email != 'szepesi.mate.shedoz@gmail.com' ){
                $success = $this->utils->sendmail('noreply@' . $shortDomainName, ucfirst($shortDomainName), 'webshop@felfoldishop.hu', lang('Order confirmation').' - ' . ucfirst($shortDomainName), $message);
                $success = $this->utils->sendmail('noreply@' . $shortDomainName, ucfirst($shortDomainName), 'info@madsolution.ie', lang('Order confirmation').' - ' . ucfirst($shortDomainName), $message);
            }
        }




        return $success;
    }

    function subscribeNewsletter($email, $name) {
        $this->load->model("utils");
        $exist = $this->utils->getFirstObject("newsletter", ["email" => $email]);
        if ($exist)
            return true;

        return $this->db->insert("newsletter", ["email" => $email, "name" => $name]);
    }

    function unsubNewsletter($email) {
        $this->load->model("utils");
        $exist = $this->utils->getFirstObject("newsletter", ["email" => $email]);
        if ($exist) {
            $this->db->where('email', $email)->delete('newsletter');
            return 'successfully deleted';
        } else {
            return 'not found';
        }
    }

}

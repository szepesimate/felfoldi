<?php

class Basket_model extends CI_Model {

    public function __construct() {
        if ($this->session->userdata('basket') == false) {
            $this->session->set_userdata('basket', array());
        }
    }

    function deleteBasketFromDb() {
        if ($this->session->userdata('uid')) {
            $f_user_id = $this->session->userdata('uid');
            $this->db->where('f_user_id', $f_user_id)->delete('f_user_basket');
        }
    }

    function updateBasketInDb() {

        if ($this->session->userdata('uid')) {
            $f_user_id = $this->session->userdata('uid');
            $this->db->where('f_user_id', $f_user_id)->delete('f_user_basket');

            $currentBasket = $this->session->userdata('basket');
            if ($currentBasket && !empty($currentBasket)) {
                $basket_content = json_encode($currentBasket);
                $this->load->model('page');
                $this->db->insert('f_user_basket', ['f_user_id' => $f_user_id, 'basket_content' => $basket_content, 'site' => $this->config->item('shortDomainName'), 'date' => date('Y-m-d H:i:s')]);
            }
        }
    }

    function restoreBasketWithArray($array) {
        $this->session->set_userdata('basket', (array) $array);
    }

    function addItem($itemNumber, $count) {

        $currentBasket = $this->session->userdata('basket');
        if (isset($currentBasket[$itemNumber])) {
            $currentBasket[$itemNumber] = $currentBasket[$itemNumber] + $count;
        } else {
            $currentBasket[$itemNumber] = $count;
        }


        $this->session->set_userdata('basket', $currentBasket);
        $this->updateBasketInDb();

        if ($this->input->post("isKapcsolodo")) {
            $currentConnected = $this->session->userdata('itemsFromConnectedProducts');
            $currentConnected[$itemNumber] = true;
            $this->session->set_userdata('itemsFromConnectedProducts', $currentConnected);
        }
    }

    function removeItem($itemNumber) {

        $currentBasket = $this->session->userdata('basket');
        if (isset($currentBasket[$itemNumber])) {
            unset($currentBasket[$itemNumber]);
        }

        $this->session->set_userdata('basket', $currentBasket);
        $this->updateBasketInDb();
    }

    function changeBasketItemCount($itemNumber, $count) {

        if ($count == 0) {
            $this->removeItem($itemNumber);
        } else {
            $currentBasket = $this->session->userdata('basket');
            $currentBasket[$itemNumber] = $count;

            $this->session->set_userdata('basket', $currentBasket);
            $this->updateBasketInDb();
        }
    }

    function getBasketTableRows() {
        $ret_arr = array();
        $row_sum = 0;
        $currentBasket = $this->session->userdata('basket');
        
        $langTag = $this->session->userdata('site_lang') == $this->config->item('def_lang') ? '' : '_' . $this->session->userdata('site_lang');

        $weight = 0;
        if ($currentBasket) {

            foreach ($currentBasket as $itemNumber => $count) {
                //imageFileName, url, name, inStock, oldPrice, displayPrice, count, itemNumber, partialSumPrice

                $this->load->model('product');
                $product = $this->product->getProductObjectByItemNumber($itemNumber);
                if (!$product)
                    continue;

                if ($product->discountPrice > 0) {
                    $oldPrice = $product->price;
                    $displayPrice = $product->discountPrice;
                } else {
                    $oldPrice = 0;
                    $displayPrice = $product->price;
                }

                $typeString = '';
                if (!empty($type)) $typeString = $type;

                $packageString = '';
                if (!empty($package)) $packageString = $package;


                $rowArr = array();
                $rowArr["itemNumber"] = $itemNumber;
                $rowArr["count"] = $count;
                $rowArr["imageFileName"] = $this->product->getFirstProductImageFileNameByItemNumber($itemNumber);
                $rowArr["name"] = $product->{"name$langTag"} . '<span class="is-hidden-mobile">, ' .  $product->type . ', ' . $product->packaging . '</span>';
                $rowArr["barionName"] = $product->{"name$langTag"} . ' ' . $product->type . ', ' . $product->packaging;
                $rowArr["inStock"] = $product->stock ? 1 : 0;
                $rowArr["oldPrice"] = $oldPrice;
                $rowArr["displayPrice"] = $displayPrice;
                $rowArr["partialSumPrice"] = $displayPrice * $count;

                $row_sum += $rowArr["partialSumPrice"];
                //$weight += $product->weight;

                $url = base_url();
                $url .= Page::pageUrlByTpl("product") . '/';
                $url .= $this->product->getProductCategoriesUrl($product->category) . '/';
                $url .= $this->product->getProductUrlByProductId($product->id);

                $rowArr["url"] = $url;



                $ret_arr["rows"][] = (object) $rowArr;
            }
        }



        //$ret_arr["shipping_cost"] = 0;
        //$ret_arr["overall"] = $ret_arr["shipping_cost"] + $row_sum;
        //info:  száll költséget és végösszeget itt nem is adok, mert minden oldal letöltéskor
        //ajaxszal töltödik ki a táblázatban ez a két mező, és hidden field is.

        return $ret_arr;
    }

    function calculateNettoFuvardij($payment_mode, $weight, $products_sum, $delivery_region) {

        $szall_dij = 15;

        if ($delivery_region == 'UK - England'){
            $szall_dij = 10;
        }

        if ($products_sum > 150) {
            $szall_dij = 0;
        }

        return $szall_dij;
    }

    function getShippingCostLimits() {
        $ret_arr = array();
        /*
          $q = $this->db->from("shippingCostLimits")->get();
          if ($q->num_rows()){
          foreach ($q->result_object() as $row){
          $ret_arr[$row->type] = $row->value;
          }

          return $ret_arr;
          }
         */
        return null;
    }

    public function fetchLastBasketItems($itemCount) {
        $currentBasket = $this->session->userdata('basket');
        $row_sum = 0;
        $rows_str = "";
        
        $langTag = $this->session->userdata('site_lang') == $this->config->item('def_lang') ? '' : '_' . $this->session->userdata('site_lang');

        $this->load->model("page");
        $productPageUrl = Page::pageUrlByTpl("product");

        $cnt = 0;
        if ($currentBasket) {
            $currentBasket = array_reverse($currentBasket);
            $this->load->model('product');
            foreach ($currentBasket as $itemNumber => $count) {

                $product = $this->product->getProductObjectByItemNumber($itemNumber);

                if ($product->discountPrice > 0) {
                    $oldPrice = $product->price;
                    $displayPrice = $product->discountPrice;
                } else {
                    $oldPrice = 0;
                    $displayPrice = $product->price;
                }

                $row_sum += $displayPrice * $count;


                if (++$cnt > $itemCount)
                    continue;

                $url = base_url();
                $url .= $productPageUrl . '/';
                $url .= $this->product->getProductCategoriesUrl($product->category) . '/';
                $url .= $this->product->getProductUrlByProductId($product->id);

                $productImgPath = base_url() . "assets/images/webshop/thumb/" . $this->product->getFirstProductImageFileNameByItemNumber($itemNumber);

                $rows_str .= "
                    <div class=\"row\">
                        <a href=\"$url\">
                            <img width=\"78\" alt=\"\" src=\"$productImgPath\" />
                        </a>
                        <span class=\"count\">$count x</span> ".$product->{"name$langTag"}." <br/>
                        <span class=\"price\">" . $displayPrice . " Ft</span>

                        <a class=\"remove\" href=\"javascript:void(0)\" onclick=\"removeFromBasket('$itemNumber')\"><i class=\"ion ion-close\"></i></a>
                    </div>
                    <div class=\"clear clearfix\"></div>
                    ";
            }
        }


        return [
            "overall" => $row_sum . " Ft",
            "rows_str" => $rows_str
        ];
    }

    public function calcShippingAndOverallCosts($payment_mode, $delivery_mode, $delivery_region) {

        $row_sum = 0;
        $currentBasket = $this->session->userdata('basket');
        $weight = 0;
        //$freeShippingProductInBasket = false;
        if ($currentBasket) {
            $this->load->model('product');
            foreach ($currentBasket as $itemNumber => $count) {

                $product = $this->product->getProductObjectByItemNumber($itemNumber);

                if ($product->discountPrice > 0) {
                    $oldPrice = $product->price;
                    $displayPrice = $product->discountPrice;
                } else {
                    $oldPrice = 0;
                    $displayPrice = $product->price;
                }

                $row_sum += $displayPrice * $count;
                //$weight += $product->weight * $count;
                //if ($product->isFreeShipping) $freeShippingProductInBasket = true;
            }
        }

        $felhasznalt_kupon_ertek = 0;
        $bevaltva = "";
        if ($row_sum > 10000 && $this->session->userdata('coupon')) {

            $bevaltva = "Kuponkedvezmény: ";

            $value = floatval($this->session->userdata('coupon')['value']);
            if ($value < 100) {
                $row_sum = $row_sum - ( $row_sum * ( $value / 100 ) );
                $bevaltva .= $value . " %";
            } else {
                $row_sum -= $value;
                $bevaltva .= $value . " Ft";
            }

            $felhasznalt_kupon_ertek = $value;
        }

        if ($delivery_mode == 'Personally') {
            $brutto_shipping_cost = 0;
        } else {
            // $netto_fuvardij = $this->calculateNettoFuvardij($payment_mode, $weight, $row_sum, $delivery_region);
            // $brutto_shipping_cost = $netto_fuvardij/*             * 1.27 */;
            if ($row_sum >= 15000) {
                $brutto_shipping_cost = 0;
            }
            else {
                $brutto_shipping_cost = 1300; 
            }
        }


        $overall = $brutto_shipping_cost + $row_sum;

        if ($delivery_mode != 'Personally' && $brutto_shipping_cost == 0)
            $brutto_shipping_cost_str = "Ingyenes";
        else
            $brutto_shipping_cost_str = $brutto_shipping_cost . " Ft";


        $ret_arr = [
            "product_sum" => $row_sum . " Ft",
            "shipping_cost_value" => $brutto_shipping_cost,
            "shipping_cost" => $brutto_shipping_cost_str,
            "overall" => $overall . " Ft",
            "bevaltva" => $bevaltva,
            "felhasznalt_kupon_ertek" => $felhasznalt_kupon_ertek
        ];

        return $ret_arr;
    }

    public function getCoupon($code) {
        $this->load->model('utils');
        return $this->utils->getFirstObject('coupons', ['code' => $code]);
    }

}

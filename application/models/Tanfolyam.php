<?php

class Tanfolyam extends CI_Model {

    public function __construct() {
        //$this->load->database();
    }

    public function getTanfolyamok($order_arr) {
        $q = $this->db->query("select * from tanfolyamok order by " . $order_arr[0] . " " . $order_arr[1]);

        if ($q->num_rows()) {
            $articles = $q->result_object();
            return $articles;
        }

        return null;
    }

    public function getTanfolyamById($id) {
        $q = $this->db->get_where("tanfolyamok", array("id" => $id));
        if ($q->num_rows()) {
            return $q->result_object()[0];
        }

        return null;
    }

    public function getKiemeltTanfolyamok() {
        $q = $this->db->get_where("tanfolyamok", array("kiemelt" => 1));
        if ($q->num_rows()) {
            return $q->result_object();
        }

        return null;
    }

}

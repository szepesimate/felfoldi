<?php

class Article extends CI_Model {

    public function __construct() {
        //$this->load->database();
    }

    public function get_first_article($page_id) {
        return $this->utils->getFirstObject('articles', ['parent' => $page_id]);
    }

    public function getArticles($parent, $order_arr) {
        $lang = $this->session->userdata('site_lang');

        $q = $this->db->query("select * from articles WHERE parent = $parent AND lang = '$lang' order by " . $order_arr[0] . " " . $order_arr[1]);

        if ($q->num_rows()) {
            $articles = $q->result_object();
            return $articles;
        }

        return null;
    }
    
    public function getAllArticlesWithPager($parent, $limit_start, $limit_end) {
        $lang = $this->session->userdata('site_lang');
        $articles = $this->utils->getResultObjectWithQuery("select * from articles where parent = $parent AND lang = '$lang' order by startDate desc 
			LIMIT $limit_end, $limit_start");
        if (!$articles && $lang != 'hungarian') {
            $lang = 'hungarian';

            $articles = $this->utils->getResultObjectWithQuery("select * from articles where parent = $parent AND lang = '$lang' order by startDate desc 
			LIMIT $limit_end, $limit_start");
        }


        return $articles;
    }

    public function getNewsArticles($parent, $order_arr) {
        $lang = $this->session->userdata('site_lang');

        $q = $this->db->query("select * from articles WHERE parent = $parent AND lang = '$lang' order by " . $order_arr[0] . " " . $order_arr[1]);

        if ($q->num_rows()) {
            $articles = $q->result_object();
            return $articles;
        }

        return null;
    }

    public function getIndexArticlesByCategory($category, $limit) {

        $lang = $this->session->userdata('site_lang');

        $ret_arr = null;

        $articles = $this->utils->getResultObjectWithQuery("select * from articles WHERE categories like '%$category%' AND lang = '$lang' order by startDate desc limit $limit ");

        if ($articles) {
            foreach ($articles as $article) {
                $urlArray = $this->page->getPageAndSubPageUrlArray($article->parent);

                $tmp_arr = [];
                $tmp_arr["object"] = $article;
                $tmp_arr["parentPageUrl"] = base_url() . $urlArray[0] . '/' . $urlArray[1];

                $ret_arr[] = $tmp_arr;
            }
        }




        return $ret_arr;
    }

    public function getIndexArticlesByDate($parent, $limit) {

        $lang = $this->session->userdata('site_lang');

        $ret_arr = null;

        $articles = $this->utils->getResultObjectWithQuery("select * from articles WHERE parent = $parent AND lang = '$lang' order by startDate desc limit $limit ");

        if (!$articles && $lang != 'hungarian') {
            $lang = 'hungarian';
            $articles = $this->utils->getResultObjectWithQuery("select * from articles WHERE parent = $parent AND lang = '$lang' order by startDate desc limit $limit ");
        }

        if ($articles) {

            //$urlArray = $this->page->getPageAndSubPageUrlArray($article->parent);

            foreach ($articles as $article) {
                $tmp_arr = [];
                $tmp_arr["object"] = $article;
                //$tmp_arr["parentPageUrl"] = base_url() . $urlArray[0] . '/' . $urlArray[1];

                $ret_arr[] = $tmp_arr;
            }
        }




        return $ret_arr;
    }

    public function getPrevNewArticleById($id) {
        $q = $this->db->query("select s.urlFriendly as urlFriendly, a.id as id from articles as a join seo as s on a.id = s.itemId where a.id < $id and a.status = 1 and a.parent = 60 and s.itemType like 'article' order by a.id desc");
        if ($q->num_rows()) {
            return $q->result_object()[0];
        }

        return null;
    }

    public function getNextNewArticleById($id) {
			$q = $this->db->query("select s.urlFriendly as urlFriendly, a.id as id from articles as a join seo as s on a.id = s.itemId where a.id > $id and a.status = 1 and a.parent = 60 and s.itemType like 'article' order by a.id asc");
			if ($q->num_rows()) {
			return $q->result_object()[0];
			}

			return null;
		}

    public function getArticleById($id) {
	    $q = $this->db->get_where("articles", array("id" => $id));
	    if ($q->num_rows()) {
		    return $q->result_object()[0];
	    }
    	return null;
    }
    
    public function getArticleByBrand($brand){
        $lang = $this->session->userdata('site_lang');
        $q = $this->db->get_where("articles", array("property1" => $brand, 'status' => 1, 'lang' => $lang));
        if ($q->num_rows()) {
            return $q->result_object()[0];
        }

        return null;
    }

    public function getArticlesByParent($parent) {
        $lang = $this->session->userdata('site_lang');
        $q = $this->db->get_where("articles", array("parent" => $parent, 'status' => 1, 'lang' => $lang));
        if ($q->num_rows()) {
            return $q->result_object();
        }

        return null;
    }

    public function getNewsSearchResultByKeyword($keyword) {
        $lang = $this->session->userdata('site_lang');

        $converted_key = htmlentities($keyword, ENT_QUOTES, "UTF-8");

        $q = $this->db->query("select * from articles WHERE parent = " . $this->page->page->id . " AND
		(
			preview like '%$converted_key%' OR
			content like '%$converted_key%' OR
			title like '%$keyword%' OR
			tags like '%$keyword%' OR
			categories like '%$keyword%'
		)
		AND lang = '$lang'
		order by startDate desc ");

        if ($q->num_rows()) {
            $articles = $q->result_object();
            return $articles;
        }

        return null;
    }

    public function getNewsSearchResultByCategory($keyword) {
        $lang = $this->session->userdata('site_lang');

        $q = $this->db->query("select * from articles WHERE parent = " . $this->page->page->id . " AND
		(
			categories like '%$keyword%'
		)
		AND lang = '$lang'
		order by startDate desc ");

        if ($q->num_rows()) {
            $articles = $q->result_object();
            return $articles;
        }

        return null;
    }

    public function getNewsSearchResultByTag($keyword) {
        $lang = $this->session->userdata('site_lang');

        $q = $this->db->query("select * from articles WHERE parent = " . $this->page->page->id . " AND
		(
			tags like '%$keyword%'
		)
		AND lang = '$lang'
		order by startDate desc ");

        if ($q->num_rows()) {
            $articles = $q->result_object();
            return $articles;
        }

        return null;
    }

    public function getNewsSearchResultByArchive($keyword) {
        $lang = $this->session->userdata('site_lang');

        $q = $this->db->query("select * from articles WHERE parent = " . $this->page->page->id . " AND
		(
			startDate between '$keyword' AND '" . date('Y-m-t 23:59:59', strtotime($keyword)) . "'
		)
		AND lang = '$lang'
		order by startDate desc ");

        if ($q->num_rows()) {
            $articles = $q->result_object();
            return $articles;
        }

        return null;
    }

    public function getIndexNews($newsPageId) {

        $q = $this->db->query("select * from articles where parent = $newsPageId order by startDate desc limit 3 ");

        if ($q->num_rows()) {
            return $q->result_object();
        }


        return null;
    }

    public function getNewsTopArticle($parent) {
        $lang = $this->session->userdata('site_lang');
        return $this->utils->getFirstObject("articles", ["parent" => $parent, 'lang' => $lang]);
    }

    public function getNewsIndexArticle() {
        $lang = $this->session->userdata('site_lang');
        return $this->utils->getFirstObject("articles", ["parent" => 3, 'lang' => $lang]);
    }

    public function getAllCategories() {
        $lang = $this->config->item('def_lang');

        $q = $this->db->query("select distinct categories from articles where categories != '' AND lang = '$lang' ");
        if ($q->num_rows()) {
            $arr = [];
            foreach ($q->result_object() as $categories) {
                $categories = str_replace(', ', ',', $categories->categories);
                $arr = array_merge($arr, explode(',', $categories));
            }
            $arr = array_unique($arr);
            return $arr;
        }

        return false;
    }

    public function getNewsArchives() {
        $lang = $this->session->userdata('site_lang');

        $ret_arr = [];
        $q = $this->db->query("select startDate from articles where parent = " . $this->page->page->id . " AND lang = '$lang' order by startDate desc ");
        if ($q->num_rows()) {
            foreach ($q->result_object() as $obj) {
                $startDate = date('Y-m-01', strtotime($obj->startDate));
                if (!in_array($startDate, $ret_arr))
                    $ret_arr[] = $startDate;
            }

            return $ret_arr;
        }

        return false;
    }

    public function getAllTags() {
        $lang = $this->config->item('def_lang');

        $q = $this->db->query("select distinct tags from articles where tags != '' AND lang = '$lang' ");
        if ($q->num_rows()) {
            $arr = [];
            foreach ($q->result_object() as $tags) {
                $tags = str_replace(', ', ',', $tags->tags);
                $arr = array_merge($arr, explode(',', $tags));
            }
            $arr = array_unique($arr);
            return $arr;
        }

        return false;
    }

    public function getAllTagsForTopic($parent) {
        $lang = $this->session->userdata('site_lang');

        $q = $this->db->query("select distinct tags from articles where parent = $parent AND tags != '' AND lang = '$lang' ");
        if ($q->num_rows()) {
            $arr = [];
            foreach ($q->result_object() as $tags) {
                $tags = str_replace(', ', ',', $tags->tags);
                $arr = array_merge($arr, explode(',', $tags));
            }
            $arr = array_unique($arr);
            return $arr;
        }

        return false;
    }

    public function get_gyarto_options() {
        return $this->utils->getResultObjectWithQuery("select distinct property2 from articles  where property2 != '' ");
    }

    public function getLatestProgrammes($maxItem = null) {

    	if($maxItem !== null) {
		    $maxItem = "limit $maxItem";
	    }
      $q = $this->db->query("select a.*, s.urlFriendly from articles as a join seo as s on a.id = s.itemId  where a.status = 1 and a.parent = 57 and s.itemType like 'article' order by a.startDate desc $maxItem");
      if ($q->num_rows()) {
        foreach ($q->result_object() as $tags) {
        	$arr[] = $tags;
        }
        return $arr;
      }

      return false;
    }

    public function getAvailableLocations() {
	    $lang = $this->session->userdata('site_lang');
	    $q = $this->db->query("select property1 from articles where parent = 57 AND lang = '$lang' GROUP BY property1 ");
	    if ($q->num_rows()) {
		    $arr = [];
		    foreach ($q->result_object() as $locations) {
			    $arr[] = $locations->property1;
		    }
		    return $arr;
	    }
    }

}

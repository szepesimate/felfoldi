<?php

class Utils extends CI_Model {

    function getResultObject($tableName, $whereArray, $orderArray) {

        $q = $this->db->order_by($orderArray[0], $orderArray[1])->get_where($tableName, $whereArray);
        if ($q->num_rows()) {
            return $q->result_object();
        }

        return false;
    }

    function getResultObjectWithQuery_app($query) {
        $appdb = $this->load->database('app', true);
        $q = $appdb->query($query);
        if ($q->num_rows()) {
            return $q->result_object();
        }

        return false;
    }

    function getResultObjectWithQuery($query) {
        $q = $this->db->query($query);
        if ($q->num_rows()) {
            return $q->result_object();
        }

        return false;
    }

    function getFirstObjectWithQuery_app($query) {
        $appdb = $this->load->database('app', true);
        $q = $appdb->query($query);
        if ($q->num_rows()) {
            return $q->result_object()[0];
        }

        return false;
    }

    function getFirstObjectWithQuery($query) {
        $q = $this->db->query($query);
        if ($q->num_rows()) {
            return $q->result_object()[0];
        }

        return false;
    }

    function getFirstObject($tableName, $whereArray) {

        $q = $this->db->get_where($tableName, $whereArray);
        if ($q->num_rows()) {
            return $q->result_object()[0];
        }

        return false;
    }

    function convertUrlFormat($string) {
        $this->load->helper('text');
        return url_title(convert_accented_characters($string), '-', true);
    }

    function getMonthName($m) {

        if ($this->config->item('def_lang') != 'hungarian') {
            return $this->getMonthName_en($m);
        }

        $month = "";
        switch ($m) {
            case '01': $month = "január";
                break;
            case '02': $month = "február";
                break;
            case '03': $month = "március";
                break;
            case '04': $month = "április";
                break;
            case '05': $month = "május";
                break;
            case '06': $month = "június";
                break;
            case '07': $month = "július";
                break;
            case '08': $month = "augusztus";
                break;
            case '09': $month = "szeptember";
                break;
            case '10': $month = "október";
                break;
            case '11': $month = "november";
                break;
            case '12': $month = "december";
                break;
        }

        return $month;
    }

    function getMonthName_en($m) {
        $month = "";
        switch ($m) {
            case '01': $month = "january";
                break;
            case '02': $month = "february";
                break;
            case '03': $month = "march";
                break;
            case '04': $month = "april";
                break;
            case '05': $month = "may";
                break;
            case '06': $month = "june";
                break;
            case '07': $month = "july";
                break;
            case '08': $month = "august";
                break;
            case '09': $month = "september";
                break;
            case '10': $month = "october";
                break;
            case '11': $month = "november";
                break;
            case '12': $month = "december";
                break;
        }

        return $month;
    }

    public function getAllCountries() {

        $country_list = array(
            'AF' => 'Afghanistan',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'BQ' => 'British Antarctic Territory',
            'IO' => 'British Indian Ocean Territory',
            'VG' => 'British Virgin Islands',
            'BN' => 'Brunei',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CT' => 'Canton and Enderbury Islands',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos [Keeling] Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo - Brazzaville',
            'CD' => 'Congo - Kinshasa',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'CI' => 'Côte d’Ivoire',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'NQ' => 'Dronning Maud Land',
            'DD' => 'East Germany',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'FQ' => 'French Southern and Antarctic Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island and McDonald Islands',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong SAR China',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JT' => 'Johnston Island',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Laos',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macau SAR China',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'FX' => 'Metropolitan France',
            'MX' => 'Mexico',
            'FM' => 'Micronesia',
            'MI' => 'Midway Islands',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar [Burma]',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NT' => 'Neutral Zone',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'KP' => 'North Korea',
            'VD' => 'North Vietnam',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PC' => 'Pacific Islands Trust Territory',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territories',
            'PA' => 'Panama',
            'PZ' => 'Panama Canal Zone',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'YD' => 'People\'s Democratic Republic of Yemen',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn Islands',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RO' => 'Romania',
            'RU' => 'Russia',
            'RW' => 'Rwanda',
            'RE' => 'Réunion',
            'BL' => 'Saint Barthélemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts and Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre and Miquelon',
            'VC' => 'Saint Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'CS' => 'Serbia and Montenegro',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia and the South Sandwich Islands',
            'KR' => 'South Korea',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard and Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syria',
            'ST' => 'São Tomé and Príncipe',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands',
            'TV' => 'Tuvalu',
            'UM' => 'U.S. Minor Outlying Islands',
            'PU' => 'U.S. Miscellaneous Pacific Islands',
            'VI' => 'U.S. Virgin Islands',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'SU' => 'Union of Soviet Socialist Republics',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'ZZ' => 'Unknown or Invalid Region',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VA' => 'Vatican City',
            'VE' => 'Venezuela',
            'VN' => 'Vietnam',
            'WK' => 'Wake Island',
            'WF' => 'Wallis and Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        );

        return $country_list;
    }

    public function sendmail($fromEmail, $fromName, $toEmail, $subject, $message) {

        $this->load->library('email');

        $success = $this->email
                ->from($fromEmail, $fromName)
                ->to($toEmail)
                ->subject($subject)
                ->message($message)
                ->send();

        return $success;
    }

    public function preg_match_youtube($content) {

        preg_match_all('/\[(.*?)\]/', $content, $matches);
        if (isset($matches[1])) {
            foreach ($matches[1] as $youtubeUrl) {
                preg_match('/(?<=v=)[^&]+/', $youtubeUrl, $vid_id);
                if (isset($vid_id[0])) {
                    $content = str_replace('[' . $youtubeUrl . ']', '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $vid_id[0] . '" frameborder="0" allowfullscreen></iframe>', $content);
                }
            }
        }

        return $content;
    }

    public function preg_match_gallery($content) {

        $this->load->model('gallery');

        preg_match_all('/\[gallery: (.*?)\]/', $content, $matches);
        if (isset($matches[1])) {
            foreach ($matches[1] as $galleryId) {

                $gal_content_str = "";

                $gallery_items = $this->gallery->get_galleriesItems($galleryId);
                if ($gallery_items) {
                    $gal_content_str .= "<div class=\"fw embeddedGallery\">";
                    foreach ($gallery_items as $image) {
                        $gal_content_str .= '<a class="thumb" rel="slideshow-thumbs" title="' . $image->title . '" href="' . base_url() . 'assets/images/gallery/big/' . $image->filename . '" style="background-image:url(' . base_url() . 'assets/images/gallery/thumb/' . $image->filename . ')"></a>';
                    }
                    $gal_content_str .= "</div>";
                }


                $content = str_replace('[gallery: ' . $galleryId . ']', $gal_content_str, $content);
            }
        }

        return $content;
    }

    public function getWelcomeName() {
        $expl = explode(' ', $this->session->userdata('name'));

        if ($this->session->userdata('site_lang') == 'hungarian') {
            return trim(array_pop($expl));
        } else {
            return trim($expl[0]);
        }
    }

    public function get_tplOptions() {
        $tplOptions = array(
            "index" => "főoldal",
            "contact" => "kapcsolat",
            "about" => "rólunk",
            "gallery" => "galéria",
            "videogallery" => "videógaléria",
            "gallery_chooser" => "galéria főoldal",
            "catalogs" => "katalógusok",
            "text" => "szöveges típusú aloldal",
            "news" => "hír típusú aloldal",
            "competition" => "pályázat típusú aloldal",
            "product" => "termék aloldal",
            "sitemap" => "sitemap",
            "reg" => "regisztráció aloldal",
            "forgot_password" => "elfelejtett jelszó aloldal",
            "profil" => "profil aloldal",
            "basket" => "kosár aloldal",
            "stripe" => "stripe",
            "barion" => "barion",
            "jogi_nyilatkozat" => "Jogi nyilatkozat",
            "cookie_policy" => "Süti nyilatkozat",
            "pay_information" => "Fizetés",
            "career" => "Karrier",
            "about_us" => "Rólunk",
            "unsubscribe" => "unsubscribe aloldal",
        );

        return $tplOptions;
    }

    function getProductFields() {
        return [
            'type' => 'Fajta / íz',
            'packaging' => 'Kiszerelés / tömeg',
            'ingredients' => 'Összetevők',
            'preparation' => 'Előkészületek',
            'composition' => 'Összetétel',
            'allergyInformation' => 'Allergének',            
            
            'alcoholContent' => 'Alcohol tartalma',
            
            
        ];
    }

    function get_gyartoOptions() {
        return [
            'Adrienne Feller' => 'Adrienne Feller',
            'Hello Soap' => 'HelloSoap',
            'L’amia Natura' => 'L’amia Natura',
            'ZIA' => 'ZIA',
            '﻿Marianna Herrhofer' => '﻿Marianna Herrhofer',
        ];
    }

    function get_brand_urls() {
        $brands = array_keys($this->utils->get_gyartoOptions());
        $brand_urls = [];
        foreach ($brands as $brand) {
            $brand_urls[] = $this->utils->convertUrlFormat($brand);
        }
        return $brand_urls;
    }
    
    function findBrandByUrl($brand_url){
        $brands = array_keys($this->utils->get_gyartoOptions());
        foreach ($brands as $brand) {
            if ( $brand_url == $this->utils->convertUrlFormat($brand)){
                return $brand;
            }
        }
        
        return null;
    }
    
    function getRegionOptions(){
        return [
            'Magyarorszag' => 'Magyarország',
            // 'UK - Wales' => 'UK - Wales',
            // 'UK - Scotland' => 'UK - Scotland',
            // 'UK - England' => 'UK - England',
            // 'UK - Northern Ireland' => 'UK - Northern Ireland',
            // '﻿Republic of Ireland' => '﻿Republic of Ireland',
        ];
    }

}

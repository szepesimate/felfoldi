<?php

class Admin_seo extends CI_Controller {

    private $loadedModel;
    private $data;

    /**
     * Responsable for auto load the model
     * @return void
     */
    public function __construct() {
        global $data;
        parent::__construct();
        $this->load->model('utils');
        $this->load->model('crud_model');
        $this->crud_model->setTableName("seo");
        $this->crud_model->setSearchFieldName("urlFriendly");
        $this->loadedModel = $this->crud_model;

        $data["moduls"] = $this->loadedModel->getModuls();
        $data["currentModul"] = $this->loadedModel->getCurrentModul();

        if (!$this->loadedModel->checkPermission($data) || !$this->session->userdata('is_logged_in')) {
            redirect('admin/login');
        }

        /*
          if ($this->input->get('devMode') == 1) {
          $this->load->model('product');

          $this->load->helper('text');

          $all = $this->utils->getResultObjectWithQuery('select * from products');
          foreach ($all as $p) {

          $this->loadedModel->insert_seo_entry($p->id, url_title(convert_accented_characters($p->name), '-', true) . '-' . url_title(convert_accented_characters($p->fajta), '-', true), 'product', $this->config->item('def_lang'));

          //magyart is
          $this->loadedModel->insert_seo_entry($p->id, url_title(convert_accented_characters($p->name_hungarian) . '-' . url_title(convert_accented_characters($p->fajta_hungarian), '-', true), '-', true), 'product', 'hungarian');
          }
          }
         */
    }

    /**
     * Load the main view with all the current model model's data.
     * @return void
     */
    public function index() {
        global $data;

        $lang = $this->input->post('lang') ? $this->input->post('lang') : $this->config->item('def_lang');
        $data['lang'] = $lang;

        $data['allPages'] = $this->utils->getResultObjectWithQuery("select * from pages order by id asc");
        $data['allArticles'] = $this->utils->getResultObjectWithQuery("select * from articles order by id asc");

        $data['allProductCats'] = $this->utils->getResultObjectWithQuery("select * from product_cats order by id asc");
        $data['allProducts'] = $this->utils->getResultObjectWithQuery("select * from products order by id asc");

        $all_seo_value_arr = [];
        $all_seo_row = $this->utils->getResultObjectWithQuery("select * from seo WHERE lang = '$lang' order by id asc");
        if ($all_seo_row) {
            foreach ($all_seo_row as $row) {
                $all_seo_value_arr[$row->itemType . "_" . $row->itemId] = ['title' => $row->title, 'description' => $row->description, 'urlFriendly' => $row->urlFriendly];
            }
        }

        $data['all_seo_value_arr'] = $all_seo_value_arr;

        $data['langOptions'] = [];

        $allLangs = $this->utils->getResultObjectWithQuery("select * from langs order by id asc");
        if ($allLangs) {
            foreach ($allLangs as $lang) {
                $data['langOptions'][$lang->lang] = $lang->lang;
            }
        }





        //load the view
        $data['main_content'] = 'admin/' . $this->uri->segment(2) . '/list';
        $this->load->view('admin/includes/template', $data);
    }

//index

    public function submitSeoForm() {
        $success = false;

        $lang = $this->db->escape_str($this->input->post("lang"));
        $fieldName = $this->db->escape_str($this->input->post("fieldName"));
        $itemType = $this->db->escape_str($this->input->post("itemType"));
        $itemId = $this->db->escape_str($this->input->post("itemId"));
        $value = $this->db->escape_str($this->input->post("value"));


        $objXst = $this->utils->getFirstObject("seo", ['lang' => $lang, 'itemType' => $itemType, 'itemId' => $itemId]);
        if ($objXst) {
            $success = $this->db->simple_query("update seo SET $fieldName = '$value' WHERE id = $objXst->id ");
        } else {
            $success = $this->db->simple_query("insert into seo (itemType, itemId, $fieldName, lang) values('" . $itemType . "','" . $itemId . "','" . $value . "','" . $lang . "')");
        }


        echo json_encode(["success" => $success]);
    }

}

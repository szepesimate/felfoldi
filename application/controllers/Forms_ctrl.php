<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forms_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        Page::loadLanguage();
    }

    public function validLoginRule($password) {
        $email = $this->input->post("email");
        if ($email == "")
            return true; //ha nincs email akkor ne foglalkozzunk ilyesmivel

        $this->load->model("users");
        $success = $this->users->login($email, $password);
        return $success;
    }

    public function termsCheckRule($field_value) {
        return $field_value == "on" || $field_value == 1 ? true : false;
    }

    public function allowCheckRule($field_value) {
        return $field_value == "on" || $field_value == 1 ? true : false;
    }

    public function handleLoginForm() {
        $success = false;
        $output = lang("Error!");
        //form validation

        $this->form_validation->set_rules('email', 'E-mail', 'required');
        $this->form_validation->set_rules('password', lang('Password'), 'required|callback_validLoginRule');

        $this->form_validation->set_message('required', '%s ' . lang('field is required').'!');
        $this->form_validation->set_message('validLoginRule', lang('Wrong e-mail or password!'));

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');

        //if the form has passed through the validation
        if ($this->form_validation->run()) {

            $user_obj = $this->users->getUserData($this->session->userdata('uid'));
            if ($user_obj) {
                $success = true;
                $output = lang("Welcome") . " " . $user_obj->name . "!";
            } else {
                $success = false;
            }
        } else {
            $output = validation_errors();
        }

        echo json_encode(array("output" => $output, "success" => $success));
    }

    public function handleLogoutForm() {
        $output = "";

        $this->load->model("users");
        $user_obj = $this->users->logout();

        $success = true;

        echo json_encode(array("output" => $output, "success" => $success));
    }

    public function aszfCheckRule($field_value) {
        return $field_value == "on" || $field_value == 1 ? true : false;
    }

    function handleNewsletterForm() {
        $success = false;
        $output = "Hiba!";

        //form validation
        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
        $this->form_validation->set_rules('name', 'Teljes név', 'required');
        //$this->form_validation->set_rules('aszf', 'Adatvédelmi nyilatkozat', 'required');

        $this->form_validation->set_rules('aszf', 'Terms', 'callback_aszfCheckRule');
        $this->form_validation->set_message('aszfCheckRule', lang('You must accept the Terms&Conditions!'));

        $this->form_validation->set_message('required', '%s ' . lang("field is required") . '!');
        $this->form_validation->set_message('valid_email', lang('The E-mail field must contain a valid email address!'));

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');

        //if the form has passed through the validation
        if ($this->form_validation->run()) {

            $this->load->model('users');
            $success = $this->users->subscribeNewsletter($this->input->post("email"), $this->input->post("name"));
            if ($success)
                $output = "<div>" . lang("Thank you!") . "</div>";
        }else {
            $output = validation_errors();
        }

        echo json_encode(array("output" => $output, "success" => $success));
    }

    public function handleUnsubForm() {
        $success = false;
        $output = "Error!";
        //form validation
        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');

        $this->form_validation->set_message('required', '%s ' . lang('field is required!'));
        $this->form_validation->set_message('valid_email', lang('The E-mail field must contain a valid email address!'));

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');

        //if the form has passed through the validation
        if ($this->form_validation->run()) {

            $this->load->model('users');
            $ret = $this->users->unsubNewsletter($this->input->post("email"));
            if ($ret == 'successfully deleted'){
                $output = "<div>" . lang("Sikeresen leíratkoztál a hírlevél listánkról!") . "</div>";
            }else if ($ret == 'not found'){
                $output = "<div>" . lang("A megadott e-mail cím nem található az adatbázisunkban.") . "</div>";
            }
            $success = true;
        }else {
            $output = validation_errors();
        }

        echo json_encode(array("output" => $output, "success" => $success));
    }

    public function switchLanguage() {
        $success = false;

        $lang = $this->input->post("lang");

        $this->session->set_userdata('site_lang', $lang);

        $success = true;

        echo json_encode(array("success" => $success));
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Basket extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('basket_model');
        $this->load->model('page');
        Page::loadLanguage();
    }

    public function addItem($itemNumber, $count) {

        $this->basket_model->addItem($itemNumber, $count);

        echo json_encode(array("basketItemCount" => count($this->session->userdata('basket')), 'success_text' => lang('A termék a kosárba került!')));
    }

    public function removeItem($itemNumber) {

        $this->basket_model->removeItem($itemNumber);

        echo json_encode(array("basketItemCount" => count($this->session->userdata('basket'))));
    }

    public function changeBasketItemCount($itemNumber, $count) {
        $this->basket_model->changeBasketItemCount($itemNumber, $count);

        echo json_encode(array("basketItemCount" => count($this->session->userdata('basket'))));
    }

    function refreshShippingAndOverallCost() {

        $value_arr = $this->basket_model->calcShippingAndOverallCosts($this->input->post('payment_mode'), $this->input->post('delivery_mode'), $this->input->post('delivery_region'));

        $product_sum_str = $value_arr["product_sum"];
        $brutto_shipping_cost = $value_arr["shipping_cost_value"];
        $brutto_shipping_cost_str = $value_arr["shipping_cost"];
        $overall_str = $value_arr["overall"];
        $bevaltva = $value_arr["bevaltva"];


        echo json_encode(["product_sum" => $product_sum_str, "shipping_cost_value" => $brutto_shipping_cost, "shipping_cost" => $brutto_shipping_cost_str, "overall" => $overall_str, 'bevaltva' => $bevaltva]);
    }

    function fetchLastBasketItems($itemCount) {

        $value_arr = $this->basket_model->fetchLastBasketItems($itemCount);

        $rows_str = $value_arr["rows_str"];
        $overall_str = $value_arr["overall"];

        echo json_encode(array("basketItemCount" => count($this->session->userdata('basket')), "overall_str" => $overall_str, "rows_str" => $rows_str));
    }

    function setProductOrder($order) {
        $this->session->set_userdata('product_order', $order);
        echo json_encode(['success' => 1]);
    }

    function handleCouponForm() {
        $success = false;

        $code = $this->input->post('couponCode');

        if ($code == "") {
            $output = "Kérjük, adja meg kuponkódját";
        } else {
            $coupon = $this->basket_model->getCoupon($code);
            if (!$coupon) {
                $output = "Hibás kuponkód!";
            } else {
                $value = $coupon->value;
                $this->session->set_userdata('coupon', ['code' => $code, 'value' => $value]);
                $output = "Sikeres kuponbeváltás!";
                $success = true;
            }
        }



        echo json_encode(['success' => $success, 'output' => $output]);
    }

}

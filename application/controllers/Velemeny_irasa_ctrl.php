<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Velemeny_irasa_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model('course');
    }

    public function index() {


        $this->page->findPageByUrlParts([$this->uri->segment(1),$this->uri->segment(2)]);

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

         
        $tanfolyamOptions = $this->course->get_all();
        $data['tanfolyamOptions'] = [];
        $data['tanfolyamOptions'][''] = 'Milyen képzésre jártál?';
        $data['tanfolyamOptions']['Bármixer'] = 'Bármixer képzés';
        $data['tanfolyamOptions']['Barista'] = 'Barista képzés';
        $data['tanfolyamOptions']['Pincér'] = 'Pincér képzés';
        
        $tanfolyam_idk = $this->course->get_all();       
        
        foreach ($tanfolyam_idk as $u){
            $data['tanfolyamOptions'][$u->id] = $u->nev;
        }

        $this->load->view("template/" . $data['template'], $data);
    }
    
    
    public function handle_form() {
        $success = false;
        $output = "Hiba az üzenet küldése során!";
        //form validation

        if ($this->input->post('phone')) {
            die();
        }

        $this->form_validation->set_rules('name', 'Név', 'required');
        $this->form_validation->set_rules('tanfolyam_id', 'Képzés', 'required');
        $this->form_validation->set_rules('city', 'Város', 'required');        
        $this->form_validation->set_rules('comment', 'Vélemény', 'required');


        $this->form_validation->set_message('required', 'A(z) %s ' . lang("mező kitöltése kötelező") . '!');

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');

        //if the form has passed through the validation
        if ($this->form_validation->run()) {
            /*$name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone_');
            $message = $this->input->post('message');*/
            
            
            $this->db->insert('velemenyek', [
                'name' => $this->input->post('name'),
                'tanfolyam_id' => $this->input->post('tanfolyam_id'),
                'city' => $this->input->post('city'),
                'comment' => $this->input->post('comment'),
                'store_date' => date('Y-m-d H:i:s')
            ]);
            
            $success = true;
            //$success = $this->sendContactFormEmail($name, $phone, $email, $message);
            
            $output = "<div>" . lang("Köszönjük értékelésed") . "!</div>";
        }else {
            $output = validation_errors();
        }

        echo json_encode(array("output" => $output, "success" => $success));
    }

}

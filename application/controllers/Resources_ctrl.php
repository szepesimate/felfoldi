<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Resources_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model('users');
        $this->load->model('product');
    }

    public function index() {
        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

        if (!$this->session->userdata('uid')) {
            redirect(base_url());
        }
        
        $data['recentProducts'] = $this->product->getRecentProducts(8888888888);

        $this->load->view("template/" . $data['template'], $data);
    }

    

}

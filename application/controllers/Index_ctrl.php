<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Index_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model('product');
    }

    public function index() {
 
        //$this->output->cache(10);
        
        

        $this->page->find_page_by_url($this->uri->segment(1));

        
        $data = $this->page->init_data_with_common_items();

        $data["sliderElements"] = $this->page->getSliderElements();
        
        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

        //$data['latestProductBoxes'] = $this->product->getRecentProducts(5);

        $data['categoryBoxes'] = $this->product->getAllProductCategory();
        
        //$data['specialProductBoxes'] = $this->product->getDiscountProducts(5);
        
        $data['latestNews'] = $this->article->getNewsArticles(60, ['startDate', 'desc']);

        $this->load->view("template/" . $data['template'], $data);
    }

}

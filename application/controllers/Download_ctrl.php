<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Download_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function download($type, $fileName) {

        $path = '';
        switch ($type) {
            case 'catalog': $path = 'docs/';
                break;
            case 'productImage': $path = 'images/webshop/big/';
                break;
        }

        $data = file_get_contents('./assets/' . $path . $fileName);

        $this->load->helper('download');
        force_download($fileName, $data);
    }

}

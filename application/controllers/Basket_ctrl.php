<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Basket_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('users');
        $this->load->model('article');
        $this->load->model('basket_model');
    }

    public function aszfRule($field_value) {

        if ($field_value) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function index() {
        $this->page->find_page_by_url($this->uri->segment(1));

        $data = $this->page->init_data_with_common_items();


        if (isset($_POST['megrendeles'])) {

            $this->form_validation->set_rules('name', lang('Name'), 'required');
            $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
            $this->form_validation->set_rules('phone', lang('Phone'), 'required');
            $this->form_validation->set_rules('delivery_mode', lang('Delivery mode'), 'required');
            $this->form_validation->set_rules('payment_mode', lang('Payment mode'), 'required');
            
            $this->form_validation->set_rules('billing_region', lang('Billing region'), 'required');

            $this->form_validation->set_rules('billing_name', lang('Billing name'), 'required');
            $this->form_validation->set_rules('billing_zip', lang('Billing zip'), 'required');
            $this->form_validation->set_rules('billing_city', lang('Billing city'), 'required');
            $this->form_validation->set_rules('billing_street', lang('Billing address'), 'required');

            if ($this->input->post("delivery_mode") == "Courier") {
                
                $this->form_validation->set_rules('delivery_region', lang('Delivery region'), 'required');

                $this->form_validation->set_rules('delivery_name', lang('Delivery name'), 'required');
                $this->form_validation->set_rules('delivery_zip', lang('Delivery zip'), 'required');
                $this->form_validation->set_rules('delivery_city', lang('Delivery city'), 'required');
                $this->form_validation->set_rules('delivery_street', lang('Delivery address'), 'required');
            }

            $this->form_validation->set_rules('aszf', '', 'callback_aszfRule');
            $this->form_validation->set_message('aszfRule', lang('You must accept the Terms&Conditions and the Privacy Policy!'));
            $this->form_validation->set_message('valid_email', lang('The E-mail field must contain a valid email address!'));

            $this->form_validation->set_message('required', '%s ' . lang('field is required') . '!');
            $this->form_validation->set_error_delimiters('<div class="notification is-danger">', '</div>');

            if ($this->form_validation->run()) {

                //itt újra ki kell számolni a száll költséget
                $costs_arr = $this->basket_model->calcShippingAndOverallCosts($this->input->post('payment_mode'), $this->input->post('delivery_mode'), $this->input->post('delivery_region'));

                $fields = array(
                    'user_id' => $this->session->userdata('uid') ? $this->session->userdata('uid') : 0,
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone'),
                    'comment' => $this->input->post('comment'),
                    'delivery_mode' => $this->input->post('delivery_mode'),
                    'payment_mode' => $this->input->post('payment_mode'),
                    'delivery_name' => $this->input->post('delivery_name'),
                    'delivery_zip' => $this->input->post('delivery_zip'),
                    'delivery_region' => $this->input->post('delivery_region'),
                    'delivery_city' => $this->input->post('delivery_city'),
                    'delivery_street' => $this->input->post('delivery_street'),
                    'billing_name' => $this->input->post('billing_name'),
                    'billing_zip' => $this->input->post('billing_zip'),
                    'billing_region' => $this->input->post('billing_region'),
                    'billing_city' => $this->input->post('billing_city'),
                    'billing_street' => $this->input->post('billing_street'),
                    //itt újra ki kell számolni a száll költséget
                    'shipping_cost' => $costs_arr["shipping_cost_value"], //$this->input->post('shipping_cost'),
                    //'coupon_value' => $costs_arr["felhasznalt_kupon_ertek"],
                    'date' => date('Y-m-d H:i:s')
                );

                $order_id = $this->users->storeOrderData($fields);
                

                // $this->session->unset_userdata('basket');
                // $this->session->unset_userdata('itemsFromConnectedProducts');
                // $this->basket_model->deleteBasketFromDb();

                if ( $this->input->post('payment_mode') == 'Káryás fizetés' ){
                    // $this->session->set_userdata('order_id_for_barion', $order_id);
                    // redirect(base_url() . Page::pageUrlByTpl('stripe'));

                    require_once(getcwd() . '/application/libraries/barion-web-php-master/library/BarionClient.php');

                    $myPosKey = "d2d9f78c590548789af2be5d520112bf";
                    // $myPosKey = "24d40ff638f44dbd9491024cffba525f";

                    $apiVersion = 2;

                    // Test environment
                    $environment = BarionEnvironment::Test;

                    // Production environment
                    // $environment = BarionEnvironment::Prod;

                    $products = $this->basket_model->getBasketTableRows();

                    $BC = new BarionClient($myPosKey, $apiVersion, $environment);

                    $trans = new PaymentTransactionModel();

                    $sumPrice = 0;
                    $barionProducts = [];
                    foreach($products['rows'] as $product) {

                        $item = new ItemModel();
                        $item->Name = $product->barionName;
                        $item->Description = $product->barionName;
                        $item->Quantity = $product->count;
                        $item->Unit = "piece";
                        $item->UnitPrice = $product->displayPrice;
                        $item->ItemTotal = $product->partialSumPrice;
                        $item->SKU = $product->itemNumber;
                        $barionProducts[] = $item;
                        $sumPrice += $product->partialSumPrice;
                    }

                    $trans->POSTransactionId = $order_id;
                    $trans->Payee = "info@felfoldishop.hu";
                    // $trans->Payee = "szepesi.mate.shedoz@gmail.com";
                    $trans->Total = $sumPrice;
                    $trans->Currency = Currency::HUF;
                    $trans->Comment = "";
                    $trans->AddItems($barionProducts);

                    $ppr = new PreparePaymentRequestModel();
                    $ppr->GuestCheckout = true;
                    $ppr->PaymentType = PaymentType::Immediate;
                    $ppr->FundingSources = array(FundingSourceType::All);
                    $ppr->PaymentRequestId = $order_id;
                    $ppr->PayerHint = $this->input->post('email');
                    $ppr->Locale = UILocale::EN;
                    $ppr->OrderNumber = $order_id;
                    $ppr->Currency = Currency::HUF;
                    $ppr->RedirectUrl = base_url() . "barion/sikeres";
                    $ppr->CallbackUrl = base_url() . "barion/sikertelen";
                    $ppr->AddTransaction($trans);

                    $myPayment = $BC->PreparePayment($ppr);

                    if ($myPayment->PaymentId != null) {
                    // Barion sandbox mode
                    // redirect('https://secure.test.barion.com/pay?id=' . $myPayment->PaymentId);

                    // Barion live mode
                    redirect('https://secure.barion.com/pay?id=' . $myPayment->PaymentId);
                    }

                }else{
                    $this->users->sendOrderEmails($order_id);
                    $this->session->unset_userdata('basket');
                    $this->session->unset_userdata('itemsFromConnectedProducts');
                    $this->basket_model->deleteBasketFromDb();
                    redirect(base_url() . Page::pageUrl("Thank you"));
                }

            } else {
                $data["validation_userData"] = validation_errors();
            }
        }

        $data['basketTableRows'] = $this->basket_model->getBasketTableRows();

        $data["userData"] = $this->users->getUserData($this->session->userdata('uid'));
        $data["deliveryOptions"] = ["" => lang("Delivery mode"), "Courier" => lang("Futárszolgálat"), "Personally" => lang("Személyesen átveszem a boltban")];

        $data["payOptions"] = 
            ["" => lang("Payment mode"),
                "Személyesen készpénzben" => lang("Személyesen készpénzben"),
                "Átvételkor a futárnak" => lang("Átvételkor a futárnak"),
                "Bankkártyával" => lang("Bankkártyával"),
                "Káryás fizetés" => lang("Káryás fizetés")
            ];
        if (isset($_POST["delivery_mode"]) && $_POST["delivery_mode"] == "Personally") {
            $data["payOptions"]["Cash"] = lang("Cash");
        }
        if (isset($_POST["delivery_mode"]) && $_POST["delivery_mode"] == "Courier") {
            //$data["payOptions"]["Utánvét"] = lang("Utánvét");
        }

        $data['regionOptions'] = array_merge($this->utils->getRegionOptions());

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);



        $this->load->view("template/" . $data['template'], $data);
    }

}

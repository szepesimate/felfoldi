<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csv extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('product');
		$this->load->model('page');
    }


	public function argep(){
		$allProducts = $this->product->get_allProduct();
        if ( $allProducts ){
			$productPageUrl = base_url() . Page::pageUrl("Termékek");
			header('Content-Type: text/csv');
			echo "Cikkszám|Terméknév|BruttóÁr|Fotólink|Terméklink\n";
			foreach ($allProducts as $product){
				$displayPrice = $product->discountPrice > 0 ? $product->discountPrice : $product->price;
				$photo = base_url() . 'assets/images/webshop/big/' . $this->product->getFirstProductImageFileNameByItemNumber($product->itemNumber);

				$categoriesUrl = $this->product->getProductCategoriesUrl($product->category);
				$productUrl = $this->product->getProductUrlByProductId($product->id);
				$productLink = $productPageUrl.'/'.$categoriesUrl.'/'.$productUrl;
				echo "$product->itemNumber|$product->name|$displayPrice|$photo|$productLink\n";
			}
		}
	}


	public function arukereso(){
		$allProducts = $this->product->get_allProduct();
		if ( $allProducts ){
			$productPageUrl = base_url() . Page::pageUrl("Termékek");
			header('Content-Type: text/csv');
			echo "identifier;manufacturer;name;category;product_url;price;net_price;image_url\n";
			foreach ($allProducts as $product){
				if ( $product->manufacturer == "" ) continue;
				$displayPrice = $product->discountPrice > 0 ? $product->discountPrice : $product->price;
				$photo = base_url() . 'assets/images/webshop/big/' . $this->product->getFirstProductImageFileNameByItemNumber($product->itemNumber);

				$categoriesUrl = $this->product->getProductCategoriesUrl($product->category);
				$productUrl = $this->product->getProductUrlByProductId($product->id);
				$productLink = $productPageUrl.'/'.$categoriesUrl.'/'.$productUrl;

				$manufacturer = $product->manufacturer;
				$category = $this->product->getProductCategoriesForArukereso($product->category);
				$displayNetPrice = round( $displayPrice / 1.27 );

				echo "$product->itemNumber;$manufacturer;\"$product->name\";\"$category\";$productLink;$displayPrice;$displayNetPrice;$photo\n";
			}
		}
	}

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model('users');
    }

    public function index() {
        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

        if (!$this->session->userdata('uid')) {
            redirect(base_url());
        }


        $data["userData"] = $this->users->getUserData($this->session->userdata('uid'));


        $this->load->view("template/" . $data['template'], $data);
    }

    public function passwordMatchRule($field_value) {
        $password = $this->input->post('password');
        $password_re = $this->input->post('password_re');

        if ($password == $password_re) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function handle_form() {
        $success = false;
        $output = "Error!";

        $this->form_validation->set_rules('name', lang('Company name'), 'required');
        $this->form_validation->set_rules('email', lang('E-mail'), 'required');
                


        $password = $this->input->post('password');
        $password_re = $this->input->post('password_re');

        if ($password || $password_re) {
            $this->form_validation->set_rules('password', lang('Password'), 'required');
            $this->form_validation->set_rules('password_re', lang('Retype password'), 'required|callback_passwordMatchRule');
            $this->form_validation->set_message('passwordMatchRule', lang('Password mismatch!'));
        }

        $this->form_validation->set_message('required', '%s ' . lang('field is required') . '!');

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');

        //if the form has passed through the validation
        if ($this->form_validation->run()) {


            $fields = array(
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone_'),
                
                'billing_name' => $this->input->post('billing_name'),
                'billing_zip' => $this->input->post('billing_zip'),
                'billing_city' => $this->input->post('billing_city'),
                'billing_street' => $this->input->post('billing_street'),
                'delivery_name' => $this->input->post('delivery_name'),
                'delivery_zip' => $this->input->post('delivery_zip'),
                'delivery_city' => $this->input->post('delivery_city'),
                'delivery_street' => $this->input->post('delivery_street'),
            );

            if ($password != "" && $password == $password_re) {
                $fields["password"] = md5($password);
            }





            $this->users->updateUserData($fields, $this->session->userdata('uid'));


            $success = true;
            $output = lang("Successfully saved!");
        } else {
            $output = validation_errors();
        }

        echo json_encode(array("output"=>$output, "success"=>$success));
    }

}

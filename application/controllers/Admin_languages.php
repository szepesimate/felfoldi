<?php
class Admin_languages extends CI_Controller {

    private $loadedModel;
    private $data;
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        global $data;
        parent::__construct();
        $this->load->model('crud_model');
        $this->crud_model->setTableName("langs");
        $this->crud_model->setSearchFieldName("lang");
        $this->loadedModel = $this->crud_model;

        $data["moduls"] = $this->loadedModel->getModuls();
        $data["currentModul"] = $this->loadedModel->getCurrentModul();

        if(!$this->loadedModel->checkPermission($data) || !$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }

    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
        global $data;
        //$this->output->cache(1);
        //all the posts sent by the view
        $search_string = $this->input->post('search_string');
        $parent = $this->input->post('parent');
        $order = $this->input->post('order');
        $order_type = $this->input->post('order_type');

        //pagination settings
        $config['per_page'] = 50;
        $config['base_url'] = base_url().'admin/'.$this->uri->segment(2);
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        }

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session?
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false || $this->uri->segment(3) == true){

            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected
            */


            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            //$this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays


            $data['count_pages']= $this->loadedModel->count_rows($search_string, $order);
            $config['total_rows'] = $data['count_pages'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['rows'] = $this->loadedModel->get_rows($search_string, $order, $order_type, $config['per_page'],$limit_end, $parent);
                }else{
                    $data['rows'] = $this->loadedModel->get_rows($search_string, '', $order_type, $config['per_page'],$limit_end, $parent);
                }
            }else{
                if($order){
                    $data['rows'] = $this->loadedModel->get_rows('', $order, $order_type, $config['per_page'],$limit_end, $parent);
                }else{
                    $data['rows'] = $this->loadedModel->get_rows('', '', $order_type, $config['per_page'],$limit_end, $parent);
                }
            }

        }else{

            //clean filter data inside section

            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            //$this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';

            $data['order'] = 'id';

            //fetch sql data into arrays

            $data['count_pages']= $this->loadedModel->count_rows();
            $data['rows'] = $this->loadedModel->get_rows('', $order_type, $config['per_page'],$limit_end);
            $config['total_rows'] = $data['count_pages'];

        }

        //initializate the panination helper
        $this->pagination->initialize($config);

        $data['categoryOptions'] = $this->loadedModel->get_pageOptions();

        //load the view
        $data['main_content'] = 'admin/'.$this->uri->segment(2).'/list';
        $this->load->view('admin/includes/template', $data);

    }//index



    public function add()
    {
        global $data;
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            $this->form_validation->set_rules('lang', 'Nyelv', 'required');


            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {


                $data_to_store = array(
                    'lang' => $this->input->post('lang'),
                );

                //if the insert has returned true then we show the flash message
                if($this->loadedModel->store_row($data_to_store)){
                    $insert_id = $this->db->insert_id();



                    $data['flash_message'] = TRUE;

                    redirect('admin/'.$this->uri->segment(2));
                }else{
                    $data['flash_message'] = FALSE;
                }

            }

        }


        $data['langOptions'] = [
        	'' => 'Válassz',
        	'hungarian' => 'Magyar',
        	'english' => 'Angol',
        	'german' => 'Német',
        	'french' => 'Francia',
        	'italian' => 'Olasz',
        	'spanish' => 'Spanyol',
        	'russian' => 'Orosz',
        	'romanian' => 'Román',
        	'slovak' => 'Szlovák',
        ];



        //load the view
        $data['main_content'] = 'admin/'.$this->uri->segment(2).'/add';
        $this->load->view('admin/includes/template', $data);
    }





    public function delete()
    {
        //row id
        $id = $this->uri->segment(4);

        $this->loadedModel->delete_row($id);

        redirect('admin/'.$this->uri->segment(2));
    }




}

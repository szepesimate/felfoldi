<?php
class Admin_tanfolyamok extends CI_Controller {

    private $loadedModel;
    private $data;
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        global $data;
        parent::__construct();
        $this->load->model('utils');
        $this->load->model('crud_model');
        $this->load->model('course');
        $this->crud_model->setTableName("tanfolyamok");
        $this->crud_model->setSearchFieldName("nev");
        $this->loadedModel = $this->crud_model;

        $data["moduls"] = $this->loadedModel->getModuls();
        $data["currentModul"] = $this->loadedModel->getCurrentModul();

        if(!$this->loadedModel->checkPermission($data) || !$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }

    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
        global $data;
        //$this->output->cache(1);
        //all the posts sent by the view
        $search_string = $this->input->post('search_string');
        $parent = $this->input->post('parent');
        $order = $this->input->post('order');
        $order_type = $this->input->post('order_type');

        //pagination settings
        $config['per_page'] = 50000;
        $config['base_url'] = base_url().'admin/'.$this->uri->segment(2);
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        }

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session?
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false || $this->uri->segment(3) == true){

            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected
            */


            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            //$this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays


            $data['count_pages']= $this->loadedModel->count_rows($search_string, $order);
            $config['total_rows'] = $data['count_pages'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['rows'] = $this->loadedModel->get_rows($search_string, $order, $order_type, $config['per_page'],$limit_end, $parent);
                }else{
                    $data['rows'] = $this->loadedModel->get_rows($search_string, '', $order_type, $config['per_page'],$limit_end, $parent);
                }
            }else{
                if($order){
                    $data['rows'] = $this->loadedModel->get_rows('', $order, $order_type, $config['per_page'],$limit_end, $parent);
                }else{
                    $data['rows'] = $this->loadedModel->get_rows('', '', $order_type, $config['per_page'],$limit_end, $parent);
                }
            }

        }else{

            //clean filter data inside section

            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            //$this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';

            $data['order'] = 'id';

            //fetch sql data into arrays

            $data['count_pages']= $this->loadedModel->count_rows();
            $data['rows'] = $this->loadedModel->get_rows('', $order_type, $config['per_page'],$limit_end);
            $config['total_rows'] = $data['count_pages'];

        }

        //initializate the panination helper
        $this->pagination->initialize($config);

        $data['categoryOptions'] = $this->loadedModel->get_pageOptions();
        $data['page_objects'] = $this->loadedModel->get_main_page_objects();
        

        //load the view
        $data['main_content'] = 'admin/'.$this->uri->segment(2).'/list';
        $this->load->view('admin/includes/template', $data);

    }//index



    public function add()
    {
        global $data;

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $filename = null;
            if ($_FILES['articleImage']['size'] > 0){
                $imgData = base64_encode(file_get_contents($_FILES['articleImage']['tmp_name']));
                $filename = date('YmdHis').'_'.$_FILES['articleImage']['name'];
                $dataToWrite = base64_decode($imgData);
                echo file_put_contents('./assets/images/news/'.$filename, $dataToWrite);

                // clear config array
                $config = array();

                //create resized image
                $config['image_library'] = 'GD2';
                $config['source_image']	= './assets/images/news/'.$filename;
                $config['new_image'] = './assets/images/news/'.$filename;
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = true;


                $config['width'] = 1000;
                $config['height'] = 800;

                $this->load->library('image_lib', $config);

                if ( getimagesize( './assets/images/news/'.$filename )[0] > 1000 ){
                    $this->image_lib->resize();
                }


            }
            //form validation
            $this->form_validation->set_rules('nev', 'nev', 'required');
            //$this->form_validation->set_rules('parent', 'Parent', 'required');
            //$this->form_validation->set_rules('preview', 'Preview', 'xss_clean');
            //$this->form_validation->set_rules('description', 'Description', 'xss_clean');

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    'kategoria' => $this->input->post('kategoria'),
                    'nev' => $this->input->post('nev'),
                    'idotartam' => $this->input->post('idotartam'),
                    'ar' => intval(str_replace(".", "", $this->input->post('ar'))),
                    'akcios_ar' => intval(str_replace(".", "", $this->input->post('akcios_ar'))),
                    'akcio_kezdete' => $this->input->post('akcio_kezdete'),
                    'akcio_vege' => $this->input->post('akcio_vege'),
                    'leiras' => $this->input->post('leiras'),
                    'tematika' => $this->input->post('tematika'),
                    'upsell_id' => $this->input->post('upsell_id'),
                    'video' => $this->input->post('video'),
                    'status' => 1,
                    'kiemelt' => $this->input->post('kiemelt') ? 1 : 0,
                );

                if ($filename != null) $data_to_store['imageFileName'] = $filename;

                //if the insert has returned true then we show the flash message
                if($this->loadedModel->store_row($data_to_store)){
                    $insert_id = $this->db->insert_id();

                    /*$seo_title = $this->input->post('nev');
                    $seo_keywords = "";
                    $seo_description = $this->input->post('preview');

                    $this->loadedModel->insert_seo_entry_tanfolyam($insert_id, [$seo_title, $seo_keywords, $seo_description], 'hungarian' );*/

                    $data['flash_message'] = TRUE;

                    redirect('admin/'.$this->uri->segment(2));
                }else{
                    $data['flash_message'] = FALSE;
                }

            }

        }

        /*$this->load->model('article');


        $allCategories = $this->article->getAllCategories();
        if ($allCategories)
            $data['allCategories'] = $allCategories;


        $allTags = $this->article->getAllTags();
        if ($allTags)
            $data['allTags'] = $allTags;

        $data['categoryOptions'] = $this->loadedModel->get_pageOptions();

        $data['langOptions'] = [];

        $allLangs = $this->utils->getResultObjectWithQuery("select * from langs order by id asc");
        if ($allLangs){
	        foreach ($allLangs as $lang){
		    	$data['langOptions'][$lang->lang] = $lang->lang;
	        }
        }*/

        $upsell_items = $this->course->get_all();
        $data['upsell_items'] = [];
        $data['upsell_items'][0] = 'Válassz';
        foreach ($upsell_items as $u){
            $data['upsell_items'][$u->id] = $u->nev;
        }

        $this->editor('../../assets/js/ckfinder/', 605, 500);

        //load the view
        $data['main_content'] = 'admin/'.$this->uri->segment(2).'/add';
        $this->load->view('admin/includes/template', $data);
    }



    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        global $data;
        //id
        $id = $this->uri->segment(4);

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            /*$seo_title = $this->input->post('nev');
            $seo_keywords = "";
            $seo_description = $this->input->post('preview');

            $this->loadedModel->update_seo_entry_tanfolyam($id, [$seo_title, $seo_keywords, $seo_description], 'hungarian' );*/


            //form validation
            $this->form_validation->set_rules('nev', 'Title', 'required');
            //$this->form_validation->set_rules('parent', 'Parent', 'required');
            //$this->form_validation->set_rules('preview', 'Preview', 'xss_clean');
            //$this->form_validation->set_rules('description', 'Description', 'xss_clean');

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                /*$categories = trim($this->input->post('categories'));
                if ( substr($categories,-1) == ',' ) $categories = substr($categories,0,-1);

                $tags = trim($this->input->post('tags'));
                if ( substr($tags,-1) == ',' ) $tags = substr($tags,0,-1);*/

                $data_to_store = array(
                    'kategoria' => $this->input->post('kategoria'),
                    'nev' => $this->input->post('nev'),
                    'idotartam' => $this->input->post('idotartam'),
                    'ar' => intval(str_replace(".", "", $this->input->post('ar'))),
                    'akcios_ar' => intval(str_replace(".", "", $this->input->post('akcios_ar'))),
                    'akcio_kezdete' => $this->input->post('akcio_kezdete'),
                    'akcio_vege' => $this->input->post('akcio_vege'),
                    'leiras' => $this->input->post('leiras'),
                    'tematika' => $this->input->post('tematika'),
                    'upsell_id' => $this->input->post('upsell_id'),
                    'video' => $this->input->post('video'),
                    'status' => 1,
                    'kiemelt' => $this->input->post('kiemelt') ? 1 : 0,
                    'crop_percent' => $this->input->post('crop_percent'),
                );


                if ($_FILES['articleImage']['size'] > 0){
                    $imgData = base64_encode(file_get_contents($_FILES['articleImage']['tmp_name']));
                    $filename = date('YmdHis').'_'.$_FILES['articleImage']['name'];
                    $dataToWrite = base64_decode($imgData);
                    echo file_put_contents('./assets/images/news/'.$filename, $dataToWrite);

                    $data_to_store['imageFileName'] = $filename;

                    // clear config array
                    $config = array();

                    // create resized image
                    $config['image_library'] = 'GD2';
                    $config['source_image']	= './assets/images/news/'.$filename;
                    $config['new_image'] = './assets/images/news/'.$filename;
                    $config['create_thumb'] = false;
                    $config['maintain_ratio'] = true;
                    $config['width'] = 1000;
                    $config['height'] = 800;

                    $this->load->library('image_lib', $config);

                    if ( getimagesize( './assets/images/news/'.$filename )[0] > 1000 ){
                        $this->image_lib->resize();
                    }


                }

                $_filename = $this->loadedModel->get_row_by_id($id)[0]['imageFileName'];

                if($this->input->post('thumbnail_remove')) {
                    if(is_file('./assets/images/news/'.$_filename)) {
                        unlink('./assets/images/news/'.$_filename);
                    }
                    $data_to_store['imageFileName'] = "";
                }

                //if the insert has returned true then we show the flash message
                if($this->loadedModel->update_row($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');

                    redirect('admin/'.$this->uri->segment(2));
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }


            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //row data
        $data['row'] = $this->loadedModel->get_row_by_id($id);

        $this->load->model('article');
        /*
        $allCategories = $this->article->getAllCategories();
        if ($allCategories)
            $data['allCategories'] = $allCategories;




        $allTags = $this->article->getAllTags();
        if ($allTags)
            $data['allTags'] = $allTags;

        $data['categoryOptions'] = $this->loadedModel->get_pageOptions();

        $data['langOptions'] = [];

        $allLangs = $this->utils->getResultObjectWithQuery("select * from langs order by id asc");
        if ($allLangs){
	        foreach ($allLangs as $lang){
		    	$data['langOptions'][$lang->lang] = $lang->lang;
	        }
        }*/

        $this->editor('../../../assets/js/ckfinder/', 605, 500);

        if ( $data['row'][0]['imageFileName'] != "" ){
            $data['currentImgFullUrl'] = base_url()."assets/images/news/".$data['row'][0]['imageFileName'];
        }

        /*$data['seo_title'] = "";
        $data['seo_keywords'] = "";
        $data['seo_description'] = "";

        $seo_obj = $this->loadedModel->getSeoEntryForArticle($id, $data['row'][0]["lang"]);
        if ($seo_obj){
            $data['seo_title'] = $seo_obj->title;
            $data['seo_keywords'] = $seo_obj->keywords;
            $data['seo_description'] = $seo_obj->description;
        }*/
        
        $upsell_items = $this->course->get_many_by('id !=', $id);
        $data['upsell_items'] = [];
        $data['upsell_items'][0] = 'Válassz';
        foreach ($upsell_items as $u){
            $data['upsell_items'][$u->id] = $u->nev;
        }

        //load the view
        $data['main_content'] = 'admin/'.$this->uri->segment(2).'/edit';
        $this->load->view('admin/includes/template', $data);

    }//update


    function editor($path,$width,$height) {
        //Loading Library For Ckeditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        //configure base path of ckeditor folder
        $this->ckeditor->basePath = base_url().'assets/js/ckeditor/';
        $this->ckeditor-> config['toolbar'] = array(
                array( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike','Subscript','Superscript', '-',
                    'Cut','Copy','Paste','PasteFromWord','-',
                    'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock', '-',
                    'Link','Unlink', '-','Image','-',
                    'BulletedList', 
                    'Format',
                    )
                );
        $this->ckeditor->config['language'] = 'hu';
        $this->ckeditor-> config['width'] = $width;
        //$this->ckeditor-> config['height'] = $height;
        //configure ckfinder with ckeditor config
        $this->ckfinder->SetupCKEditor($this->ckeditor,$path);
    }

    public function delete()
    {
        //row id
        $id = $this->uri->segment(4);

        $article = $this->utils->getFirstObject('tanfolyamok', ['id' => $id]);

        $this->loadedModel->delete_row($id);
        $this->loadedModel->delete_seo_entry($id, 'tanfolyam', $article->lang);

        redirect('admin/'.$this->uri->segment(2));
    }//edit


    public function switchProperties(){
        $dbId = $this->input->post('dbId');
        $dbField = $this->input->post('dbField');
        $currentValue = $this->input->post('currentValue');

        $this->loadedModel->switchProperties($dbId, $dbField, $currentValue);
        echo 'success';
    }
}

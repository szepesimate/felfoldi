<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_cats_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
    }

    public function index() {

        $this->page->findPageByUrlParts([$this->uri->segment(1), $this->uri->segment(2)]);

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();
        $template = $data['template'];

        if ( $this->uri->segment(2) ){
            //a css megjelenés kedvéért a parent class-ját is hozzáadom:
            $data['bodyClasses'][] = "product_cats";

            $filter_str = "";
            if ( $this->input->get('gyarto') ){
                $filter_str = " AND property2 like '".$this->input->get('gyarto')."' ";
            }

            $data['articles'] = $this->utils->getResultObjectWithQuery("select * from articles where parent = {$this->page->page->id} $filter_str order by id asc ");

            $data['gyarto_options'] = $this->article->get_gyarto_options();

        }else{

            $data['boxes'] = [];

            $childPages = $this->page->get_child_pages();

            if ($childPages){
                foreach ($childPages as $cp){
                    $tmp = [];
                    $tmp['article'] = $this->article->get_first_article($cp->id);
                    $tmp['subpage_urlFriendly'] = Page::pageUrlById($cp->id);
                    $data['boxes'][] = $tmp;
                }
            }

        }


        $this->load->view("template/" . $template, $data);
    }

}

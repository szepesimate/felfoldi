<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Barion_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model('users');
        $this->load->model('basket_model');
    }


    public function index() {

        // if (!$this->session->userdata('order_id_for_stripe')) {
        //     redirect('/');
        // }

        $this->page->find_page_by_url($this->uri->segment(1));
        $data = $this->page->init_data_with_common_items();

        if ($this->uri->segment(2) != null && $this->uri->segment(2) != "") {
            if ($this->uri->segment(2) === "sikeres") {
                $this->session->unset_userdata('basket');
                $this->session->unset_userdata('itemsFromConnectedProducts');
                $this->basket_model->deleteBasketFromDb();
                $data['template'] = "barion_success";
            }
            else if ($this->uri->segment(2) === "sikertelen") {
                $data['template'] = "barion_failed";
            }
        }
        else {
            redirect('/');
        }

        $this->load->view("template/" . $data['template'], $data);
    }
}
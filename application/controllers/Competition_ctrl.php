<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Competition_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
    }

    public function index() {

        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();
        $template = $data['template'];




        $last_index = $this->uri->total_segments();
        $last_segment = $this->uri->segment($last_index);


        if (is_numeric($last_segment)) {
            //részletek aloldal
            $template = 'competitionDetail';
            $data['bodyClasses'][] = $template;
            $newsId = $last_segment;
            $data["article"] = $this->article->getArticleById($newsId);

            //$data['breadCrumb'][] = $data["article"]->title;
            //if ($data["article"]->imageFileName != "")
            //$data['og_img_url'] = base_url() . "assets/images/news/" . $data["article"]->imageFileName;
        } else {

            //$data['articles'] = $this->article->getArticles($this->page->page->id, ['startDate', 'desc']);

            $order_arr = ['startDate', 'desc'];


            //pagination settings
            $config['per_page'] = 13;
            $config['base_url'] = base_url() . $this->uri->segment(1);
            $config['page_query_string'] = TRUE;
            $config['use_page_numbers'] = TRUE;
            $config['num_links'] = 20;

            //limit end
            //$last = $this->uri->total_segments();
            $page = $this->input->get('per_page');

            //math to get the initial record to be select in the database
            $limit_end = ($page * $config['per_page']) - $config['per_page'];
            if ($limit_end < 0) {
                $limit_end = 0;
            }


            $allArticles = $this->article->getArticles($this->page->page->id, $order_arr);

            $config['total_rows'] = count($allArticles);


            $articles = $this->article->getAllArticlesWithPager($this->page->page->id, $config['per_page'], $limit_end);

            if ($articles) {
                $data['articles'] = $articles;
            }



            $config['full_tag_open'] = '<ul>';
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a>';
            $config['cur_tag_close'] = '</a></li>';

            //initializate the panination helper 
            $this->pagination->initialize($config);
        }



        $this->load->view("template/" . $template, $data);
    }

}

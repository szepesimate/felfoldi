<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reg_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model("users");
    }

    public function index() {
        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

        if ($this->session->userdata('uid')) {
            redirect(base_url() . Page::pageUrlByTpl('profil'));
        }


        $this->load->view("template/" . $data['template'], $data);
    }

    public function passwordMatchRule($field_value) {

        $password = $this->input->post('password');
        $password_re = $this->input->post('password_re');

        if ($password == $password_re) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function termsCheckRule($field_value) {
        return $field_value == "on" || $field_value == 1 ? true : false;
    }

    public function is_unique_in_app_db($field_value) {
        $user_exists = $this->users->checkUserEmailExists($field_value);
        if ($user_exists)
            return false;
        else
            return true;
    }

    public function handle_form() {
        $success = false;
        $output = "Error!";
        //form validation

        if ($this->input->post("phone") != "")
            die();


        $this->form_validation->set_rules('name', lang('Name'), 'required');
        $this->form_validation->set_rules('email', lang('E-mail'), 'required');
        //$this->form_validation->set_rules('phone_', lang('Phone'), 'required');


        $this->form_validation->set_rules('password', lang('Password'), 'required');
        $this->form_validation->set_rules('password_re', lang('Retype password'), 'required|callback_passwordMatchRule');
        $this->form_validation->set_message('passwordMatchRule', lang('Password mismatch!'));

        $this->form_validation->set_rules('terms', 'Terms', 'callback_termsCheckRule');
        $this->form_validation->set_message('termsCheckRule', lang('You must accept the Terms&Conditions!'));



        $this->form_validation->set_message('required', '%s ' . lang('field is required') . '!');
        $this->form_validation->set_message('is_unique_in_app_db', lang('This e-mail is already registered!'));

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');


        if ($this->form_validation->run()) {
            $email = $this->input->post('email');
            $name = $this->input->post('name');

            $fields = array(
                "name" => $name,
                "email" => $email,
                "password" => md5($this->input->post('password')),
                "activated" => 1,
                "account_allowed" => 1,
                "reg_date" => date("Y-m-d H:i:s")
            );

            $inserted_id = $this->users->registrate($fields);

            if ($inserted_id) {
                /*
                  $shortDomainName = $this->config->item('shortDomainName');

                  $this->load->library('encrypt');
                  $encrypted_string = $this->encrypt->encode("action=user_activation&id=" . $inserted_id);
                  $token_encrypted = str_replace(array('+', '/', '='), array('-', '_', '~'), $encrypted_string);

                  $emailContent = "
                  " . lang("Dear") . " $name!
                  <br/><br/>

                  " . lang("Thank you for your registration!") . "<br/><br/>
                  " . lang("Please click the following link to verify your email address") . ":  <br/><br/>
                  <a href=\"" . base_url() . "Reg_ctrl/activate_reg/$token_encrypted\">" . lang("E-mail address verification") . "</a>
                  <br/><br/>

                  " . ucfirst($shortDomainName);

                  $success = $this->utils->sendmail('noreply@' . $shortDomainName, ucfirst($shortDomainName), $email, lang('Registration') . ' - ' . $shortDomainName, $emailContent);
                 */
                $success = true;
                $output = lang("Thank you!");
            }
        } else {
            $output = validation_errors();
        }

        echo json_encode(array("output" => $output, "success" => $success));
    }

    function activate_reg($token) {

        $this->load->library('encrypt');
        $key = str_replace(array('-', '_', '~'), array('+', '/', '='), $token);
        $decrypted_string = $this->encrypt->decode($key);
        $key_arr = array();
        parse_str($decrypted_string, $key_arr);
        if (isset($key_arr["action"]) && $key_arr["action"] == "user_activation") {
            $id = $key_arr["id"];
            $this->users->activateUser($id);
            $userObj = $this->users->getUserData($id);
            $name = $userObj->name;
            $email = $userObj->email;

            $this->users->loginWithIdAndEmail($id, $email, $name);

            $shortDomainName = $this->config->item('shortDomainName');

            $emailContent = "
                                " . lang("Dear") . " $name!
                                <br/><br/>

                                " . lang("You have succesfully verified your email!") . "<br/><br/>
                                " . lang("Our colleague will activate your account as soon as possible.") . "<br/><br/>    
                                
                                " . lang("Thank you!") . "<br/><br/>
                                
                                <br/><br/>

                                " . ucfirst($shortDomainName);



            $success = $this->utils->sendmail('noreply@' . $shortDomainName, ucfirst($shortDomainName), $email, lang('Registration') . ' - ' . $shortDomainName, $emailContent);

            $this->utils->sendmail('noreply@' . $shortDomainName, ucfirst($shortDomainName), $this->config->item('infoEmailAddress'), lang('Registration') . ' - ' . $shortDomainName, "New registration: $email");


            $koszonoOldalUrl = Page::pageUrlById(42);
            redirect(base_url() . $koszonoOldalUrl);
        } else {
            redirect(base_url());
        }
    }

}

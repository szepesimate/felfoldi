<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cookie_policy_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
    }

    public function index() {


        $this->page->findPageByUrlParts([$this->uri->segment(1),$this->uri->segment(2)]);

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

        $this->load->view("template/" . $data['template'], $data);
    }

}

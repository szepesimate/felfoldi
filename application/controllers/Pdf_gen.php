<?php

class Pdf_gen extends CI_Controller {

    private $pdf_obj = null;

    public function __construct() {
        parent::__construct();

        $this->load->model('page');
        $this->load->model('product');
        Page::loadLanguage();


        /* $token = $this->input->get('token');

          if ($token) {
          $user_id = $this->input->get('user_id');
          if ($token == md5($user_id . "_" . $this->uri->segment(3))) {
          $data = array(
          'admin_uid' => $user_id,
          'user_name' => 'iPhone',
          'is_logged_in' => true
          );
          $this->session->set_userdata($data);
          }
          } */

        $this->load->library('Pdf');
        /* if (!$this->session->userdata('is_logged_in')) {
          redirect('login');
          } */
    }

    public function generate_card($productId) {
        global $pdf_obj;

        $product = $this->utils->getFirstObject('products', ['id' => $productId]);
        if (!$product) {
            die('product not found');
        }
        $productImages = $this->product->get_productImages($product->itemNumber);
        $langTag = $this->session->userdata('site_lang') == $this->config->item('def_lang') ? '' : '_' . $this->session->userdata('site_lang');

        $productName = $product->{"name$langTag"};
        $productFajta = $product->{"subtitle$langTag"};
        //$productKiszereles = $product->{"kiszereles$langTag"};
        $productDesc = $product->{"description$langTag"};

        $pdf_obj = null;
        $this->set_my_pdf_header();

        $pdf_obj->SetFont('freesans', '', 7, '', true);

        $pdf_obj->AddPage();

        $html = <<<EOD
<table border="0">
<tr><td valign="top">
EOD;

        if ($productImages) {
            $html .= '<img src="' . base_url() . 'assets/images/webshop/big/' . $productImages[0]->fileName . '" />';
        }

        $html .= <<<EOD
</td>
<td valign="top">

    <h1>{$productName}</h1>
    <div>{$productFajta}</div>

{$productDesc}

    <br/>
EOD;
        $langTag_ = $langTag;
        $productFields = $this->utils->getProductFields();
        foreach ($productFields as $key => $value) {
            $langTag = $key == 'gyarto' ? '' : $langTag_;
            if ($product->{"$key$langTag"} != '') {
                $prdField = lang($productFields[$key]);
                $prdVal = $product->{"$key$langTag"};
                $html .= <<<EOD
<p><b>{$prdField}:</b> 
EOD;
                if (strstr($product->{"$key$langTag"}, '<p>')) {
                    $html .= '</p>';
                }
                $html .= $prdVal;
                if (!strstr($product->{"$key$langTag"}, '<p>')) {
                    $html .= '</p>';
                } else {
                    $html .= '<br/>';
                }
            }
        }

        $html .= '
</td>
</tr>
</table>';




        $pdf_obj->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


        /*
          if ($offer_images) {
          $pdf_obj->ln(10);

          $cnt = 0;
          $y = $pdf_obj->getY();
          foreach ($offer_images as $image) {
          $x = $cnt % 2 ? 100 : 15;
          if ($cnt % 2 == 0) {
          $pdf_obj->ln(10);
          $y = $pdf_obj->getY();
          }
          $image_file = base_url() . 'assets/images/cars/' . $image->fileName;
          //$pdf_obj->Image($image_file, $x, $y, 73, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
          //$pdf_obj->writeHTMLCell(100, 50, 10, 10, '<img src="'.$image_file.'" style="width:45%" /> ');
          $pdf_obj->writeHTMLCell(150, 0, '', '', '<img src="' . $image_file . '" style="" /> ', 0, 1, 0, true, '', true);

          $cnt++;
          }
          }


          $pdf_obj->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
         */

        if ($this->input->get('forceDownload')) {
            $pdf_obj->Output('BITBATH_P_' . intval($productId) . '_' . date('mdHis') . '.pdf', 'D');
        } else {
            $pdf_obj->Output('BITBATH_P_' . intval($productId) . '_' . date('mdHis') . '.pdf', 'I');
        }
    }

    public function generate_invoice($orderId) {
        global $pdf_obj;

        $order = $this->utils->getFirstObject('orders', ['id' => $orderId]);
        if (!$order) {
            die('order not found');
        }

        $order_items = $this->utils->getResultObject('order_items', ['order_id' => $orderId], ['id', 'asc']);
        if (!$order_items) {
            die('order items not found');
        }

        $kelt_date = date('d/m/Y');
        $invoice_number = $order->id;

        $langTag = '';
        /*
          $productName = $product->{"name$langTag"};
          $productFajta = $product->{"subtitle$langTag"};
          //$productKiszereles = $product->{"kiszereles$langTag"};
          $productDesc = $product->{"description$langTag"};
         */
        $pdf_obj = null;
        $this->set_my_pdf_header();
        $pdf_obj->invoiceMode = true;

        $pdf_obj->SetFont('freesans', '', 11, '', true);

        $pdf_obj->AddPage();

        $pdf_obj->setY(12);

        $html = <<<EOD
<table border="0">
<tr><td valign="top"><img width="180" src="./assets/images/logo_big.png" />
    <br/><br/>            
    <br/><br/>
    Email: info@felfoldi.com<br/>
    Web: www.felfoldishop.com

EOD;



        $html .= <<<EOD
</td>
<td valign="top"><p style="font-size: 3px;"> </p><span style="font-size:20px">FACTURA SIMPLIFICADA</span>
    <br/><br/>
    Numero factura simplificada: {$invoice_number} <br/>
    Fecha factura simplificada: {$kelt_date}
    <br/><br/>                
    <span style="font-size:18px;">DATOS DEL CLIENTE:</span> <br/>
    {$order->billing_name}<br/>
    {$order->billing_zip} {$order->billing_city}<br/>
    {$order->billing_street}<br/>
    {$order->billing_region}

EOD;


        $html .= '
</td>
</tr>
</table>
<br/><br/><br/><br/><br/><br/><br/><br/>';

        $pdf_obj->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);








        $html = <<<EOD
<table width="100%" border="0" cellspacing="2" cellpadding="3" style="font-size:14px">
<tr style="background-color:#f1729e;color:white;"><td width="300" align="center">PRODUCTO</td><td align="center" width="160">PRECIO POR UNIDAD</td><td align="center" width="90">CANTIDAD</td><td align="center" width="70">TOTAL</td></tr>
EOD;
        $overall_total = 0;
        foreach ($order_items as $order_item) {
            $row_total = $order_item->count * $order_item->price;
            $overall_total += $row_total;
            $row_total_str = number_format($row_total, 2, '.', ' ');
            $unit_price_str = number_format($order_item->price, 2, '.', ' ');
            $html .= "<tr style=\"background-color:#f6f5f5\"><td> {$order_item->name}</td><td align=\"center\">{$unit_price_str}</td><td align=\"center\">{$order_item->count}</td><td align=\"center\">{$row_total_str}</td></tr>";
        }
        
        $shipping_cost_str = number_format($order->shipping_cost, 2, '.', ' ');
        $html .= "<tr style=\"background-color:#f6f5f5\"><td> Shipping cost</td><td align=\"center\">{$shipping_cost_str}</td><td align=\"center\">1</td><td align=\"center\">{$shipping_cost_str}</td></tr>";
        
        $overall_total += $order->shipping_cost;
        
        $total_str = number_format($overall_total, 2, '.', ' ');
        $html .= "
</table>
<br/><br/>
";

        $pdf_obj->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


        $html = "<p style=\"text-align:right\"><b>TOTAL FACTURA SIMPLIFICADA: {$total_str} Ft</b><br/>
La factura simplificada incluye el 21% de IVA.</p>";


        $pdf_obj->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);





        //$pdf_obj->Output('invoice_' . $orderId . '.pdf', 'I');
        $pdf_obj->Output(getcwd() . '/assets/docs/pdf/invoice_' . $orderId . '.pdf', 'F');

        $this->db->where('id', $orderId)->update('orders', ['closed' => 1]);

        redirect(base_url() . 'assets/docs/pdf/invoice_' . $orderId . '.pdf');
    }

    function set_my_pdf_header() {
        global $pdf_obj;

        $pdf_obj = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf_obj->SetCreator(PDF_CREATOR);
        $pdf_obj->SetAuthor('BITBATH');
        $pdf_obj->SetTitle('Pdf');
        $pdf_obj->SetHeaderData(null, null, '', '', array(0, 0, 0), array(0, 0, 0));
        $pdf_obj->setFooterData(array(0, 0, 0), array(0, 0, 0));
        $pdf_obj->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf_obj->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf_obj->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf_obj->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
        $pdf_obj->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf_obj->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf_obj->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf_obj->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf_obj->setFontSubsetting(true);
        $pdf_obj->SetFont('freesans', '', 10, '', true);
    }

}

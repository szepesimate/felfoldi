<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('product');
        $this->load->model('article');
    }

    public function index() {

        $this->page->find_page_by_url($this->uri->segment(1));

        $data = $this->page->init_data_with_common_items();

        $data['searchParam'] = $this->input->get('search', true);

        $this->product->setCurrentCategoryArray();
        $curCatArr = $this->product->currentCategoryArray;

        $productDetailPageUrl = $this->product->getProductDetailPageUrl();

        $data['breadCrumb'] = array(lang("Home") => base_url());

        if ($curCatArr) {
            $mainCatObj = $curCatArr["mainCat"];

            if (isset($curCatArr["cat"])) {
                $catObj = $curCatArr["cat"];
            }
            if (isset($curCatArr["subCat"])) {
                $subCatObj = $curCatArr["subCat"];
            }

            $productUrl = Page::pageUrlByTpl("product");

            $data['breadCrumb'][lang($this->page->productPageName)] = base_url() . $productUrl;

            if ($mainCatObj) {
                $mainCatUrl = $this->product->getProductCategoriesUrl($mainCatObj->id);
                $data['breadCrumb'][$mainCatObj->name] = base_url() . $productUrl . '/' . $mainCatUrl;

                if (isset($catObj) && $catObj) {
                    $catUrl = $this->product->getProductCategoriesUrl($catObj->id);
                    $data['breadCrumb'][$catObj->name] = base_url() . $productUrl . '/' . $catUrl;

                    if (isset($subCatObj) && $subCatObj) {
                        $subCatUrl = $this->product->getProductCategoriesUrl($subCatObj > id);
                        $data['breadCrumb'][$subCatObj > name] = base_url() . $productUrl . '/' . $subCatUrl;
                    }
                }
            }
        }


        if ($productDetailPageUrl == null) {

            $brand_urls = $this->utils->get_brand_urls();

            if (!$this->uri->segment(2) || $this->uri->segment(2) == "") {
                //termékek főoldal

                $data['template'] = 'product_main';
                $data['bodyClasses'][] = $data['template'];

                $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

                if ($data['searchParam']) {
                    //main product search

                    $data['searchResultProducts'] = $this->product->getSearchResultProducts(20);
                } else {

                    //redirect($data['productPageUrl'] . '/' . $data['firstCatUrl']);

                    $data['recentProducts'] = $this->product->getRecentProducts(8888888888);

                    //$data['discountProducts'] = $this->product->getDiscountProducts(8);
                }
            } else if ($this->uri->segment(2) == 'special-offers') {
                //special offer page
                
                $data['template'] = 'product_so';
                $data['bodyClasses'][] = $data['template'];
                
                $data['breadCrumb'][lang('Special offers')] = current_url();

                $data['special_offer_products'] = $this->product->getDiscountProducts(8888888888);
                
            } else if (in_array($this->uri->segment(2), $brand_urls)) {
                //brand page
                
                $brand = $this->utils->findBrandByUrl($this->uri->segment(2));

                $data['template'] = 'product_brand';
                $data['bodyClasses'][] = $data['template'];

                $data['article'] = $this->article->getArticleByBrand($brand);

                $data['brandProducts'] = $this->product->getProductsByBrand($brand, 8888888888);
            } else {

                $data['bottomCat'] = $this->product->bottomCat;

                $data['productBoxes'] = $this->product->getProductsInCurrentCategory(null);

                $seo = $this->page->getSeoMetaData(null, $this->product->currentCategoryArray);
                if ($seo) {
                    $data['seo_title'] = $seo->title;
                    $data['seo_keywords'] = $seo->keywords;
                    $data['seo_desc'] = $seo->description;
                }

                $data['categoryMenuItems'] = $this->page->getCategoryMenuItems();
            }
        } else {

            $data['template'] = 'productDetail';
            $data['bodyClasses'][] = $data['template'];

            $data['product'] = $this->product->getProductByUrl($productDetailPageUrl);

            if ($data['product'] == null) {
                redirect(base_url() . '404');
            }

            $data['productImages'] = $this->product->get_productImages($data['product']->itemNumber);
            
            if ($data['productImages']){
                $data['og_img_url'] = base_url() . "assets/images/webshop/big/" . $data['productImages'][0]->fileName;
            }

            $seo = $this->page->getSeoMetaData($data['product'], $this->product->currentCategoryArray);
            if ($seo) {
                $data['seo_title'] = $seo->title;
                $data['seo_keywords'] = $seo->keywords;
                $data['seo_desc'] = $seo->description;
            }

            $data['categoryMenuItems'] = $this->page->getCategoryMenuItems();

            $data['productBoxes'] = $this->product->getSimilarProducts($data['product']);

            $data['breadCrumb'][$data['product']->{"name"}] = current_url();
        }

        $this->load->view("template/" . $data['template'], $data);
    }

}

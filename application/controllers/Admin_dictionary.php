<?php

class Admin_dictionary extends CI_Controller {

    private $loadedModel;
    private $data;

    /**
     * Responsable for auto load the model
     * @return void
     */
    public function __construct() { 
        global $data;
        parent::__construct();
        $this->load->model('utils');
        $this->load->model('crud_model');
        $this->crud_model->setTableName("lang_dictionary");
        $this->loadedModel = $this->crud_model;

        $data["moduls"] = $this->loadedModel->getModuls();
        $data["currentModul"] = $this->loadedModel->getCurrentModul();

        
        if (!$this->loadedModel->checkPermission($data) || !$this->session->userdata('is_logged_in')) {
            redirect('admin/login');
        }
        
    }

    /**
     * Load the main view with all the current model model's data.
     * @return void
     */
    public function index() {
        global $data;

        if ($this->input->post('addLangKey')) {

            $langKey = $this->input->post('langKey');

            $chkXst = $this->utils->getFirstObject('langkeys', ['langKey' => $langKey]);

            if ($chkXst) {
                $data['exist_error_message'] = TRUE;
            } else {
                $this->db->insert('langkeys', ['langKey' => $langKey]);
            }
        }

        //get all languages
        $data['allLanguages'] = $this->utils->getResultObjectWithQuery("select * from langs order by id asc");

        $lang_keys = $this->utils->getResultObjectWithQuery("select * from langkeys order by id asc");

        $lang_keys_pages = $this->utils->getResultObjectWithQuery("select name as langKey from pages where inMainMenu = 1 OR inSidebarMenu = 1 OR inFooterMenu = 1 order by id asc");

        $lang_keys_galleries = $this->utils->getResultObjectWithQuery("select name as langKey from galleries order by id asc");
        
        $lang_keys_videogalleries = $this->utils->getResultObjectWithQuery("select name as langKey from videogalleries order by id asc");
        
        $lang_keys_prodcat = $this->utils->getResultObjectWithQuery("select name as langKey from product_cats order by id asc");
        

        if ($lang_keys) {
            $data['lang_keys'] = $lang_keys;
        }

        if ($lang_keys_pages) {
            $data['lang_keys_pages'] = $lang_keys_pages;
        }
        
        if ($lang_keys_galleries) {
            $data['lang_keys_galleries'] = $lang_keys_galleries;
        }

        if ($lang_keys_videogalleries) {
            $data['lang_keys_videogalleries'] = $lang_keys_videogalleries;
        }

        if ($lang_keys_prodcat) {
            $data['lang_keys_prodcat'] = $lang_keys_prodcat;
        }

        $all_dict_value_arr = [];
        $all_dict_row = $this->utils->getResultObjectWithQuery("select * from lang_dictionary order by id asc");
        if ($all_dict_row) {
            foreach ($all_dict_row as $row) {
                $all_dict_value_arr[$row->lang . "_" . $row->langKey] = $row->word;
            }
        }

        $data['all_dict_value_arr'] = $all_dict_value_arr;


        //load the view
        $data['main_content'] = 'admin/' . $this->uri->segment(2) . '/list';
        $this->load->view('admin/includes/template', $data);
    }

//index

    public function submitDictionaryForm() {
        $success = false;

        $lang = $this->db->escape_str($this->input->post("lang"));
        $langKey = $this->db->escape_str($this->input->post("langKey"));
        $word = $this->db->escape_str($this->input->post("word"));


        $this->db->simple_query("delete from lang_dictionary where lang = '" . $lang . "' AND langKey = '" . $langKey . "' ");

        if ($word == "") {
            $success = true;
        } else {
            $success = $this->db->simple_query("insert into lang_dictionary (lang, langKey, word) values('" . $lang . "', '" . $langKey . "', '" . $word . "')");
        }

        echo json_encode(["success" => $success]);
    }

    public function genLanguageFilesForm() {
        $success = false;

        $this->load->helper('file');


        $langs = $this->utils->getResultObjectWithQuery("select distinct lang from langs ");
        if ($langs) {

            foreach ($langs as $lang_obj) {
                $lang = $lang_obj->lang;
                $dict_rows = $this->utils->getResultObjectWithQuery("select * from lang_dictionary where lang = '$lang' ");
                if (!$dict_rows) {
                    continue;
                }

                $file_str = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                /**
                *
                * Created:  " . date("Y-m-d H:i:s") . "
                *
                * Description:  " . $lang . " language file for general views
                *
                */" . "\n\n\n";

                foreach ($dict_rows as $row) {
                    $file_str .= "\$lang['" . $row->langKey . "'] = \"$row->word\";" . "\n";
                }


                if (!is_dir('./application/language/' . $lang)) {
                    mkdir('./application/language/' . $lang, 0755);
                }

                $success = write_file('./application/language/' . $lang . '/main_lang.php', $file_str);
            }
        }




        echo json_encode(["success" => $success]);
    }

}

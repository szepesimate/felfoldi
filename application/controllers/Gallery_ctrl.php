<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model('gallery');
    }

    public function index() {

        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $template = $data['template'];




        $last_index = $this->uri->total_segments();
        $last_segment = $this->uri->segment($last_index);

        if (is_numeric($last_segment)) {
            //részletek aloldal
            $data["galleryImages"] = $this->gallery->get_galleriesItems($last_segment);
            $template = 'galleryDetail';
            $data['bodyClasses'][] = $template;
        } else {
            //gallery index
            $galleries = $this->gallery->getGalleries();
            if ($galleries) {
                $data["galleries"] = $galleries;
            }
        }

        $this->load->view("template/" . $template, $data);
    }

}

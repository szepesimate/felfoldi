<?php
class Admin_charts extends CI_Controller {

    private $loadedModel;
    private $data;
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        global $data;
        parent::__construct();
        $this->load->model('utils');
        $this->load->model('crud_model');
        $this->crud_model->setTableName("jelentkezok");
        $this->loadedModel = $this->crud_model;

        $data["moduls"] = $this->loadedModel->getModuls();
        $data["currentModul"] = $this->loadedModel->getCurrentModul();

        if(!$this->loadedModel->checkPermission($data) || !$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }

    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
        global $data;

        $q = $this->utils->getResultObjectWithQuery("select tanfolyam_neve, count(tanfolyam_neve) as `counter` from jelentkezok where jelentkezes_datuma between (NOW() - INTERVAL 14 DAY) and NOW()  group by tanfolyam_neve order by counter desc");

        //die(var_dump($q));

        $data['chart_data'] = $q;


        //load the view
        $data['main_content'] = 'admin/'.$this->uri->segment(2).'/list';
        $this->load->view('admin/includes/template', $data);

    }//index
}

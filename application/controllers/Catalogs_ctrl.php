<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogs_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
    }

    public function index() {

        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $template = $data['template'];


        $catalogs = $this->article->getArticlesByParent(37);
        if ($catalogs) {
            $data["catalogs"] = $catalogs;
        }

        $this->load->view("template/" . $template, $data);
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        Page::loadLanguage();
    }

    public function index() {

        $this->page->findPageByUrlParts(array($this->uri->segment(1), $this->uri->segment(2)));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

        $this->load->view("template/" . $data['template'], $data);
    }

    public function handle_form() {
        $success = false;
        $output = "Error!";
        //form validation

        if ($this->input->post('phone')) {
            die();
        }

        $this->form_validation->set_rules('name', lang('Name'), 'required');
        //$this->form_validation->set_rules('phone_', 'Telefon', 'required');
        //$this->form_validation->set_rules('subject', lang('Subject'), 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');
        //$this->form_validation->set_rules('toEmail', lang('Contact person'), 'required');
        $this->form_validation->set_rules('message', lang('Message'), 'required');

        //$this->form_validation->set_rules('captcha', '', 'callback_captchaRule');
        //$this->form_validation->set_message('captchaRule','Hibás ellenőrzőkód!');

        $this->form_validation->set_message('required', '%s ' . lang("field is required") . '!');

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');

        //if the form has passed through the validation
        if ($this->form_validation->run()) {

            $success = $this->sendContactFormEmail();
            if ($success)
                $output = "<div>" . lang("Thank you!") . "</div>";
        }else {
            $output = validation_errors();
        }

        echo json_encode(array("output" => $output, "success" => $success));
    }

    public function sendContactFormEmail() {
        $shortDomainName = $this->config->item('shortDomainName');

        $name = $this->input->post('name');
        $email = $this->input->post('email');
        //$subject = $this->input->post('subject');
        $phone = $this->input->post('phone_');
        $message = nl2br($this->input->post('message'));
        
        //$toEmail = $this->input->post('toEmail');


        $emailContent = <<<EOF
			Új üzenet érkezett!
			<br/><br/>
			Üzenetküldő elérhetősége: <br/><br/>

			Név: $name <br/>
			Telefon: $phone <br/>
			E-mail: $email <br/>
			<br/>
			Üzenet: $message
			<br/><br/>
EOF;

        $this->load->model("utils");

        $success = $this->utils->sendmail('noreply@' . $shortDomainName, ucfirst($shortDomainName), $this->config->item('infoEmailAddress'), 'Kapcsolat űrlap - ' . $shortDomainName, $emailContent);


        return $success;
    }

}

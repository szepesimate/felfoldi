<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
    }

    public function index() {

        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

        if ($this->session->userdata('uid')) {
            redirect(base_url());
        }


        if ($this->input->get('token')) {
            $key = $this->input->get('token');
            $this->load->library('encrypt');
            $key = str_replace(array('-', '_', '~'), array('+', '/', '='), $key);
            $decrypted_string = $this->encrypt->decode($key);
            $key_arr = array();
            parse_str($decrypted_string, $key_arr);
            if ($key_arr["task"] == "forgotPassword") {
                $data["renewActionForEmail"] = $key_arr["email"];
                $data["token"] = $key;
            }
        }


        $this->load->view("template/" . $data['template'], $data);
    }

    public function is_in_dbRule($field_value) {
        $this->load->model("users");
        $user_exists = $this->users->checkUserEmailExists($field_value);
        return $user_exists;
    }

    public function passwordMatchRule($field_value) {
        $password = $this->input->post('password');
        $password_re = $this->input->post('password_re');

        if ($password == $password_re) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function handle_email_sender_form() {
        $success = false;
        $output = "Error!";
        //form validation

        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|callback_is_in_dbRule');

        $this->form_validation->set_message('required', '%s ' . lang('field is required!'));
        $this->form_validation->set_message('is_in_dbRule', lang('This e-mail is not registered yet!'));
        $this->form_validation->set_message('valid_email', lang('The e-mail field must contain a valid email address!'));

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');

        //if the form has passed through the validation
        if ($this->form_validation->run()) {
            $email = $this->input->post('email');


            $shortDomainName = $this->config->item('shortDomainName');

            $this->load->library('encrypt');
            $encrypted_string = $this->encrypt->encode("task=forgotPassword&email=" . $email);
            $encrypted_string = str_replace(array('+', '/', '='), array('-', '_', '~'), $encrypted_string);

            $this->load->model('page');
            $url = base_url() . Page::pageUrlByTpl("forgot_password") . '/?token=' . $encrypted_string;

            $emailContent = "
							" . lang("Kérjök kattints az alábi linkre és válassz új jelszót!") . "<br/><br/>
							<br/><br/>
							<a href=\"$url\">$url</a>
							<br/><br/>

							" . ucfirst($shortDomainName);

            $this->load->model("utils");
            $success = $this->utils->sendmail('noreply@' . $shortDomainName, ucfirst($shortDomainName), $email, lang('Elfelejtett jelszó') . ' - ' . $shortDomainName, $emailContent);
            if ($success) {
                $output = lang("E-mail succesfully sent!");
            }
        } else {
            $output = validation_errors();
        }

        echo json_encode(array("output" => $output, "success" => $success));
    }

    public function handle_password_save_form() {
        $success = false;
        $output = "Error!";
        //form validation

        $this->form_validation->set_rules('password', lang('Password'), 'required');
        $this->form_validation->set_rules('password_re', lang('Retype password'), 'required|callback_passwordMatchRule');

        $this->form_validation->set_message('required', '%s ' . lang('field is required!'));
        $this->form_validation->set_message('passwordMatchRule', lang('Jelszónak egyeznie kell!'));

        $this->form_validation->set_error_delimiters('<div class="">', '</div>');

        //if the form has passed through the validation
        if ($this->form_validation->run()) {

            $key = $this->input->post("token");
            $password = $this->input->post("password");

            $this->load->library('encrypt');
            $key = str_replace(array('-', '_', '~'), array('+', '/', '='), $key);
            $decrypted_string = $this->encrypt->decode($key);
            $key_arr = array();
            parse_str($decrypted_string, $key_arr);
            if ($key_arr["task"] == "forgotPassword") {
                $email = $key_arr["email"];
                $this->load->model("users");
                $this->users->changeUserPasswordByEmail($password, $email);
                $success = true;

                $output = lang("A jelszó sikeresen megváltozott!");
            }
        } else {
            $output = validation_errors();
        }

        echo json_encode(array("output" => $output, "success" => $success));
    }

}

<?php

class Admin_products extends CI_Controller {

    private $loadedModel;
    private $data;

    /**
     * Responsable for auto load the model
     * @return void
     */
    public function __construct() {
        global $data;
        parent::__construct();
        $this->load->model('crud_model');
        $this->crud_model->setTableName("products");
        $this->crud_model->setDefaultOrder("number");
        //$this->crud_model->setSearchFieldName("name");
        $this->loadedModel = $this->crud_model;

        $data["moduls"] = $this->loadedModel->getModuls();
        $data["currentModul"] = $this->loadedModel->getCurrentModul();

        if (!$this->loadedModel->checkPermission($data) || !$this->session->userdata('is_logged_in')) {
            redirect('admin/login');
        }



        /* if ($this->input->get('devMode') == 2) {

          $this->load->library('image_lib');

          $all = $this->utils->getResultObjectWithQuery('select * from products order by id asc ');
          foreach ($all as $p) {

          $images = $this->utils->getResultObjectWithQuery("select * from product_images where itemNumber = '$p->itemNumber' order by number asc ");
          if ($images) {
          foreach ($images as $i) {
          $fileNameOnServer = $i->fileName;

          if (is_file('./assets/images/webshop/small/' . $fileNameOnServer)) {
          continue;
          }




          $this->image_lib->clear();

          $config = array();

          // create thumb
          $config['image_library'] = 'GD2';
          $config['source_image'] = './assets/images/webshop/big/' . $fileNameOnServer;
          $config['new_image'] = './assets/images/webshop/small/' . $fileNameOnServer;
          $config['create_thumb'] = false;
          $config['maintain_ratio'] = true;
          $config['width'] = 88;
          $config['height'] = 200;
          $config['quality'] = 100;

          $this->image_lib->initialize($config);

          $this->image_lib->resize();
          }
          }
          }
          } */
    }

    /**
     * Load the main view with all the current model model's data.
     * @return void
     */
    public function index() {
        global $data;
        //$this->output->cache(1);
        //all the posts sent by the view
        $search_string = $this->input->post('search_string');
        $order = $this->input->post('order');
        $order_type = $this->input->post('order_type');

        //pagination settings
        $config['per_page'] = 50000;
        $config['base_url'] = base_url() . 'admin/' . $this->uri->segment(2);
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0) {
            $limit_end = 0;
        }

        //if order type was changed
        if ($order_type) {
            $filter_session_data['order_type'] = $order_type;
        } else {
            //we have something stored in the session?
            if ($this->session->userdata('order_type')) {
                $order_type = $this->session->userdata('order_type');
            } else {
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data
        //filtered && || paginated
        if ($search_string !== false && $order !== false || $this->uri->segment(3) == true) {

            /*
              The comments here are the same for line 79 until 99

              if post is not null, we store it in session data array
              if is null, we use the session data already stored
              we save order into the the var to load the view with the param already selected
             */


            if ($search_string) {
                $filter_session_data['search_string_selected'] = $search_string;
            } else {
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if ($order) {
                $filter_session_data['order'] = $order;
            } else {
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            //$this->session->set_userdata($filter_session_data);
            //fetch manufacturers data into arrays


            $data['count_pages'] = $this->loadedModel->count_rows($search_string, $order);
            $config['total_rows'] = $data['count_pages'];

            //fetch sql data into arrays
            if ($search_string) {
                if ($order) {
                    $data['rows'] = $this->loadedModel->get_rows($search_string, $order, $order_type, $config['per_page'], $limit_end);
                } else {
                    $data['rows'] = $this->loadedModel->get_rows($search_string, '', $order_type, $config['per_page'], $limit_end);
                }
            } else {
                if ($order) {
                    $data['rows'] = $this->loadedModel->get_rows('', $order, $order_type, $config['per_page'], $limit_end);
                } else {
                    $data['rows'] = $this->loadedModel->get_rows('', '', $order_type, $config['per_page'], $limit_end);
                }
            }
        } else {

            //clean filter data inside section

            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            //$this->session->set_userdata($filter_session_data);
            //pre selected options
            $data['search_string_selected'] = '';

            $data['order'] = 'number';

            //fetch sql data into arrays

            $data['count_pages'] = $this->loadedModel->count_rows();
            $data['rows'] = $this->loadedModel->get_rows('', $order_type, $config['per_page'], $limit_end);
            $config['total_rows'] = $data['count_pages'];
        }

        //initializate the panination helper
        $this->pagination->initialize($config);


        $this->load->model('product');
        $data['categoryOptions'] = $this->product->get_categoryOptions();
        //$data['stockOptions'] = array("1" => "Igen", "0" => "Nem");
        //load the view
        $data['main_content'] = 'admin/' . $this->uri->segment(2) . '/list';
        $this->load->view('admin/includes/template', $data);
    }

//index

    function connectedProducts() {
        global $data;

        $this->load->model('product');
        $data["connectedProductObjects"] = $this->product->getConnectedProductsFromOrders();

        //load the view
        $data['main_content'] = 'admin/products/connectedProducts';
        $this->load->view('admin/includes/template', $data);
    }

    function editor($path, $width) {
        //Loading Library For Ckeditor
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        //configure base path of ckeditor folder
        $this->ckeditor->basePath = base_url() . 'assets/js/ckeditor/';
        $this->ckeditor->config['toolbar'] = array(
            array('Source', '-', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-',
                'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-',
                'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-',
                'Link', 'Unlink', 'Anchor', '-', 'Image', '-',
                'Undo', 'Redo', '-', 'NumberedList', 'BulletedList', '-',
                'Styles', 'Format',
            )
        );
        $this->ckeditor->config['language'] = 'hu';
        $this->ckeditor->config['width'] = $width;
        //configure ckfinder with ckeditor config
        $this->ckfinder->SetupCKEditor($this->ckeditor, $path);
    }

    public function add() {
        global $data;
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $itemNumber = $this->input->post('itemNumber');

            $this->load->helper('text');
            $itemNumber = url_title(convert_accented_characters($itemNumber), '-');
            $_POST['itemNumber'] = $itemNumber;

            //form validation
            $this->form_validation->set_rules('itemNumber', 'itemNumber', 'required|is_unique[products.itemNumber]');
            $this->form_validation->set_rules('category', 'Category', 'required');
            $this->form_validation->set_rules('name', 'Megnevezés', 'required');
            $this->form_validation->set_rules('price', 'Price', 'required|numeric');

            $this->form_validation->set_message('is_unique', 'A megadott %s mező már létezik!');

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run()) {
                $data_to_store = array(
                    'itemNumber' => $this->input->post('itemNumber'),
                    'category' => $this->input->post('category'),
                    'name' => $this->input->post('name'),
                    'type' => $this->input->post('type'),
                    'packaging' => $this->input->post('packaging'),
                    'description' => $this->input->post('description'),
                    'ingredients' => $this->input->post('ingredients'),
                    'preparation' => $this->input->post('preparation'),
                    'composition' => $this->input->post('composition'),
                    'allergyInformation' => $this->input->post('allergyInformation'),
                    'expirationDate' => $this->input->post('expirationDate'),
                    'glutenfree' => ($this->input->post('glutenfree') === null) ? 0 : 1,
                    'lactosefree' => ($this->input->post('lactosefree') === null) ? 0 : 1,
                    'sugarfree' => ($this->input->post('sugarfree') === null) ? 0 : 1,
                    'bio' => ($this->input->post('bio') === null) ? 0 : 1,
                    'barCode' => $this->input->post('barCode'),
                    'alcoholContent' => $this->input->post('alcoholContent'),
                    'price' => $this->input->post('price'),
                    'discountPrice' => $this->input->post('discountPrice'),
                    'stock' => $this->input->post('stock'),
                    'number' => 0,
                    'store_date' => date('Y-m-d H:i:s')
                );

                // if (count($_FILES['productImage']) > 0) {
                //     $productImages = []
                //     for ($i = 0; $i < count($_FILES['productImage']['name']); $i++) {
                //         $imgData = base64_encode(file_get_contents($_FILES['articleImage']['tmp_name'][$i]));
                //         $filename = date('YmdHis') . '_' . $_FILES['articleImage']['name'][$i];
                //         $dataToWrite = base64_decode($imgData);
                //         echo file_put_contents('./assets/images/news/' . $filename, $dataToWrite);
                //         $productImages[] = 
                //     }
                // }

                // $productFields = $this->utils->getProductFields();
                // foreach ($productFields as $key => $value) {
                //     $data_to_store[$key] = $this->input->post($key);
                //     if ($key != 'gyarto') {
                //         $data_to_store[$key . '_spanish'] = $this->input->post($key . '_spanish');
                //     }
                // }

                //if the insert has returned true then we show the flash message
                if ($this->loadedModel->store_row($data_to_store)) {
                    $insert_id = $this->db->insert_id();
                    $this->load->helper('text');

                    $this->loadedModel->insert_seo_entry($insert_id, url_title(convert_accented_characters($data_to_store['name']), '-', true) . '-' . url_title(convert_accented_characters($data_to_store['fajta']), '-', true) . '-' . url_title(convert_accented_characters($data_to_store['kiszereles']), '-', true), 'product', $this->config->item('def_lang'));

                    //2. nyelvet is
                    // $this->loadedModel->insert_seo_entry($insert_id, url_title(convert_accented_characters($data_to_store['name_spanish']), '-', true), 'product', 'spanish');

                    $data['flash_message'] = TRUE;

                    redirect('admin/' . $this->uri->segment(2) . '/update/' . $insert_id);
                } else {
                    $data['flash_message'] = FALSE;
                }
            }
        }

        $this->load->model('product');
        $data['categoryOptions'] = $this->product->get_categoryOptions();
        //$gyartoOptions = $this->utils->get_gyartoOptions();
        //$data['gyartoOptions'] = array_merge(['' => 'Válassz'], $gyartoOptions);


        $this->editor('../../assets/js/ckfinder', '800px');

        //load the view
        $data['main_content'] = 'admin/' . $this->uri->segment(2) . '/add';
        $this->load->view('admin/includes/template', $data);
    }

    /**
     * Update item by his id
     * @return void
     */
    public function update() {
        global $data;
        //id
        $id = $this->uri->segment(4);

        $this->load->model('product');
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            //form validation
            $this->form_validation->set_rules('category', 'Category', 'required');
            $this->form_validation->set_rules('name', 'Megnevezés', 'required');
            $this->form_validation->set_rules('price', 'Price', 'required|numeric');

            $this->form_validation->set_message('integer', 'Az ár mezőkbe csak egész számok írhatók!');

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()) {

                $data_to_store = array(
                    'category' => $this->input->post('category'),
                    'name' => $this->input->post('name'),
                    'type' => $this->input->post('type'),
                    'packaging' => $this->input->post('packaging'),
                    'description' => $this->input->post('description'),
                    'ingredients' => $this->input->post('ingredients'),
                    'preparation' => $this->input->post('preparation'),
                    'composition' => $this->input->post('composition'),
                    'allergyInformation' => $this->input->post('allergyInformation'),
                    'expirationDate' => $this->input->post('expirationDate'),
                    'glutenfree' => ($this->input->post('glutenfree') === null) ? 0 : 1,
                    'lactosefree' => ($this->input->post('lactosefree') === null) ? 0 : 1,
                    'sugarfree' => ($this->input->post('sugarfree') === null) ? 0 : 1,
                    'bio' => ($this->input->post('bio') === null) ? 0 : 1,
                    'barCode' => $this->input->post('barCode'),
                    'alcoholContent' => $this->input->post('alcoholContent'),
                    'price' => $this->input->post('price'),
                    'discountPrice' => $this->input->post('discountPrice'),
                    'stock' => $this->input->post('stock'),
                    'number' => 0,
                    'mod_date' => date('Y-m-d H:i:s')
                );

                // $productFields = $this->utils->getProductFields();
                // foreach ($productFields as $key => $value) {
                //     $data_to_store[$key] = $this->input->post($key);
                //     if ($key != 'gyarto') {
                //         $data_to_store[$key . '_spanish'] = $this->input->post($key . '_spanish');
                //     }
                // }


                //if the insert has returned true then we show the flash message
                if ($this->loadedModel->update_row($id, $data_to_store) == TRUE) {
                    //$this->session->set_flashdata('flash_message', 'updated');
                    $this->product->setConnectedProductsWithString($this->input->post('itemNumber'), $this->input->post('connectedProducts_str'));
                    redirect('admin/' . $this->uri->segment(2));
                } else {
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/' . $this->uri->segment(2) . '/update/' . $id . '');
            }//validation run
        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data
        //row data
        $data['row'] = $this->loadedModel->get_row_by_id($id);

        $data['categoryOptions'] = $this->product->get_categoryOptions();
        //$gyartoOptions = $this->utils->get_gyartoOptions();
        //$data['gyartoOptions'] = array_merge(['' => 'Válassz'], $gyartoOptions);
        //$data['stockOptions'] = array("1" => "Igen", "0" => "Nem");
        $data['productImages'] = $this->product->get_productImages($data['row'][0]['itemNumber']);
        /*
          $data['allProducts'] = $this->product->get_allProduct();
          $connectedProductObjects = $this->product->getConnectedProductObjects($data['row'][0]['itemNumber']);
          $data['connectedProducts_str'] = "";
          if ($connectedProductObjects){
          foreach ($connectedProductObjects as $cp){
          $data['connectedProducts_str'] .= $cp->name;
          if ($cp != $connectedProductObjects[count($connectedProductObjects)-1])
          $data['connectedProducts_str'] .= ', ';
          }
          }
         */

        $this->editor('../../../assets/js/ckfinder', '800px');

        //load the view
        $data['main_content'] = 'admin/' . $this->uri->segment(2) . '/edit';
        $this->load->view('admin/includes/template', $data);
    }

//update

    public function uploadImage() {

        $productName = $this->input->post('productName');
        $itemNumber = $this->input->post('itemNumber');
       
        $ext = 'jpg';
        
        $this->load->helper('text');
        $desiredFileName = url_title(convert_accented_characters($productName), '-', true) . '-' . $itemNumber . '_.' . $ext;      
        
        $config['upload_path'] = './assets/images/webshop/big/';
        $config['allowed_types'] = '*';
        /* $config['max_size'] = '1900';
          $config['max_width'] = '3024';
          $config['max_height'] = '2768'; */
        $config['file_name'] = $desiredFileName;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('imageFile')) {
            //$error = array('method' => 'uploadProductImage', 'alertable' => $this->upload->display_errors());
            //$data["imageUploadError"] = $this->upload->display_errors();
            
            //$this->load->view('upload_form', $error);
            //echo '<script type="text/javascript">window.parent.postMessage('.json_encode($error).', "'.base_url().'");</script>';
        } else {
            //$data = array('upload_data' => $this->upload->data());
            //$this->load->view('upload_success', $data);

            $fileNameOnServer = $this->upload->data()["file_name"];
            // clear config array
            $config = array();

            // create resized image
            $config['image_library'] = 'GD2';
            $config['source_image'] = './assets/images/webshop/big/' . $fileNameOnServer;
            $config['new_image'] = './assets/images/webshop/big/' . $fileNameOnServer;
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = true;
            $config['width'] = 800;
            $config['height'] = 883;
            $config['quality'] = 100;


            $this->load->library('image_lib', $config);

            if (getimagesize('./assets/images/webshop/big/' . $fileNameOnServer)[0] > 800) {
                $this->image_lib->resize();
            }

            //$this->image_lib->display_errors();

            $this->image_lib->clear();

            $config = array();

            // create thumb
            $config['image_library'] = 'GD2';
            $config['source_image'] = './assets/images/webshop/big/' . $fileNameOnServer;
            $config['new_image'] = './assets/images/webshop/thumb/' . $fileNameOnServer;
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = true;
            $config['width'] = 290;
            $config['height'] = 320;
            $config['quality'] = 100;

            $this->image_lib->initialize($config);

            if (getimagesize('./assets/images/webshop/big/' . $fileNameOnServer)[0] > 290) {
                $this->image_lib->resize();
            } else {
                copy($config['source_image'], $config['new_image']);
            }




            $this->image_lib->clear();

            $config = array();

            // create thumb
            $config['image_library'] = 'GD2';
            $config['source_image'] = './assets/images/webshop/big/' . $fileNameOnServer;
            $config['new_image'] = './assets/images/webshop/small/' . $fileNameOnServer;
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = true;
            $config['width'] = 120;
            $config['height'] = 135;
            $config['quality'] = 100;

            $this->image_lib->initialize($config);

            if (getimagesize('./assets/images/webshop/big/' . $fileNameOnServer)[0] > 120) {
                $this->image_lib->resize();
            } else {
                copy($config['source_image'], $config['new_image']);
            }




            //$success = array('method'=>'uploadProductImage', 'success' => 1);
            //$data['success'] = $this->upload->data();
            //echo '<script type="text/javascript">window.parent.postMessage('.json_encode($success).', "'.base_url().'");</script>';

            $insertArray = array(
                'itemNumber' => $itemNumber,
                'fileName' => $fileNameOnServer,
                'date' => date("Y-m-d H:i:s")
            );
            $this->load->model('product');
            $this->product->insertProductPicToDb($insertArray);
        }


        $id = $this->uri->segment(4);
        redirect('admin/' . $this->uri->segment(2) . '/update/' . $id . '#images');
    }

    public function deleteImage() {
        $this->load->model('product');
        $imageId = $this->uri->segment(5);

        $this->product->deleteProductImageById($imageId);

        $id = $this->uri->segment(4);
        redirect('admin/' . $this->uri->segment(2) . '/update/' . $id . '#images');
    }

    public function delete() {
        //row id
        $id = $this->uri->segment(4);

        $this->load->model('product');
        $this->product->deleteProductImagesByProductId($id);

        $this->loadedModel->delete_row($id);

        $this->loadedModel->delete_seo_entry($id, 'product', null);


        redirect('admin/' . $this->uri->segment(2));
    }

//edit

    public function reorder() {
        $orderArr = $this->input->post('list');

        if ($orderArr) {
            $this->loadedModel->reorderTable($orderArr);
            echo 'success';
        }
    }

    public function switchProperties() {
        $dbId = $this->input->post('dbId');
        $dbField = $this->input->post('dbField');
        $currentValue = $this->input->post('currentValue');

        $this->loadedModel->switchProperties($dbId, $dbField, $currentValue);
        echo 'success';
    }

    public function reorderProductImages() {
        $orderArr = $this->input->post('listItem');

        if ($orderArr) {
            $this->load->model('product');
            $this->product->reorderProductImages($orderArr);
            echo 'success';
        }
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_chooser_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model('gallery');
    }

    public function index() {

        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $template = $data['template'];


        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);

        $this->load->view("template/" . $template, $data);
    }

}

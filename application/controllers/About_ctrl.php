<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
    }

    public function index() {

        $this->page->find_page_by_url($this->uri->segment(1));

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();
        
        $template = $data['template'];
        
        $last_index = $this->uri->total_segments();
        $last_segment = $this->uri->segment($last_index);

        if (is_numeric($last_segment)) {
            //részletek aloldal
            $template = 'newsDetail';
            $data['bodyClasses'][] = $template;
            $newsId = $last_segment;
            $data["article"] = $this->article->getArticleById($newsId);

        } else {
            $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);
        }


        $this->load->view("template/" . $template, $data);
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stripe_ctrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('page');
        $this->load->model('article');
        $this->load->model('users');
    }

    public function index() {

        if (!$this->session->userdata('order_id_for_stripe')) {
            redirect('/');
        }

        $this->page->findPageByUrlParts([$this->uri->segment(1), $this->uri->segment(2)]);

        //innentől van beállított session lang

        $data = $this->page->init_data_with_common_items();

        $data['articles'] = $this->article->getArticles($this->page->page->id, ['id', 'asc']);



        $this->load->view("template/" . $data['template'], $data);
    }

    public function charge() {
        if (!$this->session->userdata('order_id_for_stripe')) {
            redirect('/');
        }

        $order_obj = $this->utils->getFirstObject('orders', ['id' => $this->session->userdata('order_id_for_stripe')]);

        if (!$order_obj) {
            redirect('/');
        }

        $tetelek = $this->utils->getResultObject('order_items', ['order_id' => $order_obj->id], ['id', 'asc']);
        if (!$tetelek) {
            redirect('/');
        }
        $sum = 0;
        foreach ($tetelek as $tetel) {
            $reszOsszeg = $tetel->count * $tetel->price;
            $sum += $reszOsszeg;
        }
        $sum += $order_obj->shipping_cost;

        if (!$sum) {
            redirect('/');
        }

        $sum = $sum * 100; //centesítés

        require_once(getcwd() . '/application/libraries/stripe-php-6.4.2/init.php');

        \Stripe\Stripe::setApiKey("sk_live_EehYMMkrKiNNybfAE4DPxEpW");

        $token = $_POST['stripeToken'];

        // Charge the user's card:
        $charge = \Stripe\Charge::create(array(
                    "amount" => $sum,
                    "currency" => "eur",
                    "description" => "Felfoldi webshop",
                    "source" => $token
                        ), array(
                    "idempotency_key" => $order_obj->id . "_unique_order_id",
        ));

        //var_dump($charge);

        $this->db->where('id', $order_obj->id)->update('orders', ['stripe_charge_log' => json_encode($charge)]);

        $this->session->unset_userdata('order_id_for_stripe');


        $this->users->sendOrderEmails($order_obj->id);

        redirect(base_url() . Page::pageUrl("Thank you Stripe"));
    }

}

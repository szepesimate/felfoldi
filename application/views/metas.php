<!DOCTYPE html>
<html lang="hu">
    <head>

        <?php if ($this->config->item('ga_tracker_code') != ''): ?>

            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=<?= $this->config->item('ga_tracker_code') ?>"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag() {
                    dataLayer.push(arguments);
                }
                gtag('js', new Date());

                gtag('config', '<?= $this->config->item('ga_tracker_code') ?>');
            </script>

        <?php endif ?>

        <meta charset="UTF-8">
        <title><?php if (isset($seo_title)) echo $seo_title ?></title>
        <meta name="description" content="<?php if (isset($seo_desc)) echo $seo_desc ?>">
        <link rel="alternate" hreflang="hu" href="https://felfoldishop.hu/" />
        <link rel="shortcut icon" href='<?php echo base_url(); ?>favicon.ico' type="image/x-icon" />

        <link rel="apple-touch-icon" href="apple/appleicon60x60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="apple/appleicon76x76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="apple/appleicon120x120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="apple/appleicon152x152.png">

        <meta property="og:image" content="<?php if (isset($og_img_url)) echo $og_img_url ?>" />
        <meta property="og:title" content="<?php if (isset($seo_title)) echo $seo_title ?>" />
        <meta property="og:description" content="<?php if (isset($seo_desc)) echo $seo_desc ?>" />
        <meta property="og:site_name" content="FelfoldiShop" />

        <link rel="schema.dcterms" href="http://purl.org/dc/terms/">
        <meta name="DC.coverage" content="Hungary" />
        <meta name="DC.description" content="<?php if (isset($seo_desc)) echo $seo_desc ?>" />
        <meta name="DC.format" content="text/html" />
        <meta name="DC.identifier" content="https://felfoldishop.hu/" />
        <meta name="DC.publisher" content="Felfoldishop.hu/" />
        <meta name="DC.title" content="<?php if (isset($seo_title)) echo $seo_title ?>" />
        <meta name="DC.type" content="Text" />

        <meta name="DC.publisher" content="" />
        <meta name="DC.title" content="<?php if (isset($seo_title)) echo $seo_title ?>" />
        <meta name="DC.description" content="<?php if (isset($seo_desc)) echo $seo_desc ?>" />
        <meta name="DC.type" content="Text" />

        <link href='<?php echo base_url(); ?>assets/css/normalize.css' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate-min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css?v=2" />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />

        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fancybox-thumbs.css" media="screen" />

        <link href='<?php echo base_url(); ?>assets/css/jquery-ui.min.css' rel='stylesheet' type='text/css'>

        <link href='<?php echo base_url(); ?>assets/css/jquery.growl.css' rel='stylesheet' type='text/css'>

        <link href='<?php echo base_url(); ?>assets/css/ionicons.min.css' rel='stylesheet' type='text/css'>


        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">



        <script>
                var base_url = '<?php echo base_url() ?>';
                var current_uri = '<?php echo current_url() ?>';
                var productPageUrl = '<?php echo $productPageUrl ?>';
                var short_keyword_msg = '<?php echo lang('Keyword must be at least 4 character long!') ?>';
        </script>
    </head>
    <body<?php if (isset($bodyClasses)) echo ' class="' . implode(' ', $bodyClasses) . '"'; ?>>

        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

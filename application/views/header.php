<?php $this->load->view('metas'); ?>

<section class="hero">

    <div class="hero-head">
        <div class="columns is-gapless is-mobile">
            <div class="column language-select is-narrow">
                <!-- <a class="languageChooser rolldown" href="javascript:void(0)">
                    <img class="flag" alt="en" src="<?php echo base_url() ?>assets/images/flags/<?= $this->session->userdata('site_lang') ?>.png" />
                    <?php if ($this->session->userdata('site_lang') === 'hungarian'): ?>
                        <span>HU</span><i class="fa fa-angle-down" aria-hidden="true"></i>
                    <?php else: ?>
                        <span>ENG</span><i class="fa fa-angle-down" aria-hidden="true"></i>
                    <?php endif; ?>
                </a> -->

                <div class="languageChooserPanel">

                    <?php if (isset($languages)): ?>
                        <?php foreach ($languages as $language): ?>

                            <?php echo form_open('', ['class' => 'langSwitcherForm']); ?>
                            <input type="submit" name="switchLang" class="<?= $language->lang ?>" value="" />
                            <input type="hidden" name="lang" value="<?= $language->lang ?>" />
                            <input type="hidden" class="redirectUrl" value="<?php if (isset($urlTranslations) && isset($urlTranslations[$language->lang])) echo $urlTranslations[$language->lang]; ?>" />
                            <?php echo form_close(); ?>

                        <?php endforeach ?>
                    <?php endif ?>

                </div>
            </div>
            <div class="column">
                <span class=uppercase><?= lang('__notification_top__') ?></span>
            </div>
            <div class="column is-narrow">
                <a target="_blank" href="<?= $footer_fb_url ?>"><i class="fa fa-facebook-f wow fadeInUp" data-wow-delay="250ms" aria-hidden="true"></i></a>
                <a target="_blank" href="<?= $footer_insta_url ?>"><i class="fa fa-instagram wow fadeInUp" data-wow-delay="250ms" aria-hidden="true"></i></a>
                <a target="_blank" href="#"><i class="ion ion-ios-play wow fadeInUp"
			                               data-wow-delay="250ms" aria-hidden="true"></i></a>
                <a href="mailto:info@felfoldi.com"><i class="ion ion-email wow fadeInUp" data-wow-delay="250ms" aria-hidden="true"></i></a>

            </div>                
        </div>
    </div>

    <div class="hero-body">

        <?php if ($isHome): ?>

            <?php //$this->load->view('template/_cat_row'); ?>

            
            <?php if ($sliderElements): ?>
                <?php if (count($sliderElements) > 1): ?>
                    <div class="slider posRel">
                        <?php foreach ($sliderElements as $slide): ?>
                            <img onclick="document.location = '<?= base_url() . $productPageUrl . '/' . $this->utils->convertUrlFormat($slide->opt1) ?>'" style="cursor:pointer;z-index:0;opacity:0;display:block;position:absolute;top:0;left:0;" class="mySlide" data-wow-duration="2s" alt="Felfoldi" src="<?= base_url() ?>assets/images/slider/<?= $slide->filename ?>" />
                        <?php endforeach; ?>
                    </div>
                <?php else: ?>
                    <?php foreach ($sliderElements as $slide): ?>
                    <div class="solo-image posRel" style="background-image:url('<?= base_url() ?>assets/images/slider/<?= $slide->filename ?>');background-position:center;background-size:cover"></div>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endif; ?>
            

            <div class="hero-line"></div>

        <?php elseif ($template == 'contact'): ?>

            <div id="google_map_inner"></div>

        <?php endif; ?>

    </div>

</section>

<?php $this->load->view('template/_logo_menu'); ?>
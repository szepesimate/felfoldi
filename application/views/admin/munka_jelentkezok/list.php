








<?php /* <button style="margin-left:0" onclick="document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>/add'">Új hozzáadása</button> */ ?>

<br/><br/>

<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">

        <div class="tabs">
            <?php /*
              <ul class="navigation clearfix">
              <?php if ($page_objects) { ?>
              <?php foreach ($page_objects as $page_object): ?>
              <li><a href="#tab<?= $page_object->id ?>"<?php if ($page_object == $page_objects[0]) echo ' class="current"'; ?>><?= $page_object->name ?></a></li>
              <?php endforeach ?>
              <li><a href="#tab_other">Egyéb tartalmak</a></li>
              <?php } ?>
              </ul><!-- end navigation -->
             */ ?>
            <?php
            $other_object = new stdClass();
            $other_object->id = '_other';
            $page_objects[] = $other_object;

            $displayed = [];
            ?>

            <?php if ($page_objects) { ?>
                <?php foreach ($page_objects as $page_object): ?>

                    <div class="tab" id="tab<?= $page_object->id ?>">

                        <table class="datatable sortable">
                            <thead>
                                <tr>
                                    <th class="sorting_asc"><nobr>Jelentkezés időpontja</nobr></th>
                            <th class="sorting">Név</th>
                            <th class="sorting"><nobr>Születési év</nobr></th>
                            <th class="sorting">Telefon, Email</th>
                            <th class="sorting">Város</th>
                            <th class="sorting">CV</th>
                            <th class="sorting" width="300">Megjegyzés</th>
                            <th class="sorting">Munkaajánlat</th>


                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($rows as $row) {
                                    $displayed[] = $row['id'];

                                    $cv = '-';
                                    if ($row['cv'] != '') {
                                        $cv = '<a href="' . base_url() . 'assets/docs/cv/' . $row['cv'] . '" target="_blank">CV LETÖLTÉSE</a>';
                                    }

                                    //$statusClass = $row['status'] ? 'lathatosag' : 'lathatosag2';
                                    echo '<tr>';
                                    echo '<td>' . $row['store_date'] . '</td>';
                                    echo '<td>' . $row['name'] . '</td>';
                                    echo '<td>' . $row['birth_year'] . '</td>';
                                    echo '<td>' . $row['phone'] . '<br><a href="mailto:' . $row['email'] . '">' . $row['email'] . '</a></td>';
                                    echo '<td>' . $row['city'] . '</td>';
                                    echo '<td>' . $cv . '</td>';
                                    echo '<td>' . $row['message'] . '</td>';
                                    echo '<td>' . $row['munkaajanlat_title'] . '</td>';


                                    /* echo '<td>
                                      <a href="'.site_url("admin").'/'.$this->uri->segment(2).'/update/'.$row['id'].'" class="icon szerkesztes">szerkesztés</a>
                                      <a style="position:relative;top:5px" class="switch '.$statusClass.' icon" rev="status" href="javascript:void(0)" rel="'.$row['status'].'" name="'.$row['id'].'"></a>
                                      <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \''.site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row['id'].'\'" class="icon torles">törlés</a>
                                      </td>';
                                      echo '</tr>'; */
                                    /*
                                      echo '<td>
                                      <a href="'.site_url("admin").'/'.$this->uri->segment(2).'/view/'.$row['id'].'"><button style="background-color: #5e666a;">Részletek</button></a>
                                      </td>'; */
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>

                <?php endforeach ?>

            <?php } ?>

        </div>

    </div><!-- end content -->

</div><!-- end box -->

<?php
//echo '<div class="pagination">'.$this->pagination->create_links().'</div>';
?>

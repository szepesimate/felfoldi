
<h2>
    <?php echo $currentModul->name; ?> szerkesztése
</h2>


<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . '', $attributes);
?>
<fieldset>

    <div class="control-group">
        <label for="inputError" class="control-label">Megnevezés</label>
        <div class="controls">
            <input type="text" id="" name="name" value="<?php echo $row[0]['name']; ?>" >
        </div>
    </div>



    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>
        <button class="btn" type="reset">Mezők visszaállítása</button>
    </div>
</fieldset>

<?php echo form_close(); ?>




<div class="page-header">
    <h2>Videó hozzáadása</h2>
    <a name="images"></a>
</div>

<?php echo form_open_multipart('admin/' . $this->uri->segment(2) . '/addVideo/' . $this->uri->segment(4), array('id' => 'videoUploadForm', 'class' => 'form-horizontal'/* , 'target' => 'upload_target' */)); ?>

<fieldset>

    <div class="control-group">
        <label for="inputError" class="control-label">YouTube URL</label>
        <div class="controls">
            <input type="text" name="url" placeholder="YouTube url" />
        </div>
    </div>


    <?php
    $data = array(
        'videogallery_id' => $row[0]['id']
    );

    echo form_hidden($data);
    ?>

    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Videó hozzáadása</button>
    </div>


</fieldset>


<?php echo form_close(); ?>

<br style="clear:both"/>

<div id="imageHolder" style="float:left;width:100%;">
    <?php if ($videos): ?>
        <ul id="sortable_grid">
            <?php foreach ($videos as $video): ?>
                <li id="listItem_<?php echo $video->id ?>" class="ui-state-default" style="width:210px;height: auto">
                    <div style="position:relative;">
                        <img width="200" alt="" src="<?php echo $video->preview ?>" />
                        <img src="<?php echo base_url() ?>assets/images/admin/remove.png" style="cursor:pointer;position:absolute;top:-10px;right:0px;" onclick="if (window.confirm('Biztosan törli ezt az elemet?'))
                                            document.location = '<?php echo base_url() . 'admin/' . $this->uri->segment(2) . '/deleteVideo/' . $this->uri->segment(4) . '/' . $video->id ?>';" />
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
        <script>
            $(function () {
                $("#sortable_grid").sortable({
                    update: function () {
                        $.ajax({
                            url: base_url + 'admin/' + uri_segment2 + '/reorderVideos',
                            data: $('#sortable_grid').sortable('serialize'),
                            type: 'POST',
                            beforeSend: function () {
                                $('#sortable_grid').css('opacity', 0.5);
                            },
                            success: function (result) {
                                if (result != 'success')
                                    alert("Error. Reload this page please.");
                                $('#sortable_grid').css('opacity', 1);
                            }
                        });
                    }
                });
                $("#sortable_grid").disableSelection();
            });
        </script>
    <?php endif ?>
</div>

<br style="clear:both"/>
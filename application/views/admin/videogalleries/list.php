<button style="margin-left:0" onclick="document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>/add'">Új hozzáadása</button>


<br/><br/>

<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">

        <div class="tab">

            <ol id="sortable" class="pageOl sortable">

                <?php
                foreach ($rows as $row) {


                    echo '<li id="list_' . $row["id"] . '">

                          <div>

                              ' . $row["name"] . '

                              <span style="float:right">
                                <a href="' . site_url("admin") . '/' . $this->uri->segment(2) . '/update/' . $row["id"] . '" class="icon szerkesztes">szerkesztés</a>
                                <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \'' . site_url("admin") . '/' . $this->uri->segment(2) . '/delete/' . $row["id"] . '\'" class="icon torles">törlés</a>
                              </span>

                          </div>';


                    echo '</li>';
                }
                ?>

            </ol>

        </div>

        <?php echo '<div class="pagination">' . $this->pagination->create_links() . '</div>'; ?>

    </div>

</div>

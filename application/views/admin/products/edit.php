<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/admin/cropper.css">

<style>    
    .progress {
        display: none;
        margin-bottom: 1rem;
    }

    .alert {
        display: none;
    }

    .img-container img {
        max-width: 100%;
    }
</style>

<h2>
    <?php echo $currentModul->name; ?> szerkesztése
</h2>



<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>


<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');


//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . '', $attributes);
?>
<fieldset>
    <div class="control-group">
        <label for="inputError" class="control-label">Cikkszám</label>
        <div class="controls">
            <input type="text" id="" value="<?php echo $row[0]['itemNumber']; ?>" disabled>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Megnevezés</label>
        <div class="controls">
            <input type="text" name="name" value="<?php echo $row[0]['name']; ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Kategória</label>
        <div class="controls">
            <?php echo form_dropdown('category', $categoryOptions, $row[0]['category']); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Fajta / íz</label>
        <div class="controls">
            <input type="text" name="type" value="<?php echo $row[0]['type']; ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Kiszerelés / tömeg</label>
        <div class="controls">
            <input type="text" name="packaging" value="<?php echo $row[0]['packaging']; ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Leírás</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('description', $row[0]['description']); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Hozzávalók</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('ingredients', $row[0]['ingredients']); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Elkészítési javaslat</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('preparation', $row[0]['preparation']); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Összetétel</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('composition', $row[0]['composition']); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Allergia információk</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('allergyInformation', $row[0]['allergyInformation']); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Szavatossági idő</label>
        <div class="controls">
            <input type="text" id="expirationDate" name="expirationDate" value="<?php echo set_value('startDate', date('Y-m-d H:i:s')); ?>" >
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Megjelenés dátuma</label>
        <div class="controls">
            <input type="text" id="expirationDate" name="expirationDate" value="<?php echo $row[0]['expirationDate']; ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="glutenfree" <?= $row[0]['glutenfree'] ? 'checked': '' ?>><b>Gluténmentes</b>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="lactosefree" <?= $row[0]['lactosefree'] ? 'checked': '' ?>><b>Laktózmentes</b>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="sugarfree" <?= $row[0]['sugarfree'] ? 'checked': '' ?>><b>Cukormentes</b>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="bio" <?= $row[0]['bio'] ? 'checked': '' ?>><b>Bio</b>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Vonalkód</label>
        <div class="controls">
            <input type="text" id="" name="barCode" value="<?php echo $row[0]['barCode']; ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Alkoholtartalom</label>
        <div class="controls">
            <input type="text" id="" name="alcoholContent" value="<?php echo $row[0]['alcoholContent']; ?>">
        </div>
    </div>

    <div class="control-group" style="">
        <label for="inputError" class="control-label">Ár (bruttó)</label>
        <div class="controls">
            <input type="text" id="" name="price" value="<?php echo $row[0]['price']; ?>">
        </div>
    </div>
    
    <div class="control-group" style="">
        <label for="inputError" class="control-label">Kedvezményes ár (bruttó)</label>
        <div class="controls">
            <input type="text" id="" name="discountPrice" value="<?php echo $row[0]['discountPrice']; ?>">
        </div>
    </div>
    
    <div class="control-group" style="">
        <label for="inputError" class="control-label">Készlet (db)</label>
        <div class="controls">
            <input type="text" id="" name="stock" value="<?php echo $row[0]['stock']; ?>">
        </div>
    </div>

    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>
        <button class="btn" type="reset">Mezők visszaállítása</button>
    </div>
</fieldset>

<?php echo form_close(); ?>

<br/><br/>
<div class="page-header">
    <h2>Kép feltöltése</h2>
    <a name="images"></a>
</div>  

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script>
    $(document).ready(function(){
        $('#expirationDate').datetimepicker({
            format:'Y-m-d H:i:s',
            lang:'hu'
        });

        $('#productImage').picEdit({
            maxWidth: 600,
            formSubmitted: function(response){
                $('div.form-actions').hide();
                setTimeout(function(){
                    document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>#';
                }, 1000);
            }
        });
    });
    
    
</script>

<div>
    <label class="label" data-toggle="tooltip" title="Change your avatar">
        <img class="rounded" id="avatar" src="https://image.flaticon.com/icons/png/128/25/25635.png" alt="avatar">
        <input type="file" class="sr-only" id="input" name="image" accept="image/*">
    </label>
    <div class="progress">
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
    </div>
    <div class="alert" role="alert"></div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img id="image" src="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>
</div>

<br/><br/>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/js/admin/cropper.js"></script>

<script>


    function appendLoadingScreen() {
        if ($('.loadingScreenDiv')[0]) {
            //do nothing
        } else {
            var width = $(window).width();
            var height = $(window).height();

            $('body').append('<div class="loadingScreenDiv" style="background:#dedede;opacity:0.7;position:fixed;text-align:center;z-index:9999;top:0;left:0;width:' + width + 'px;height:' + height + 'px;"><img style="margin-top:' + (height / 2 - 40) + 'px;height:80px" src="/assets/images/loading.gif"/></div>');
        }
    }

    function removeLoadingScreen() {
        $('.loadingScreenDiv').remove();
    }

</script>



<script>
    window.addEventListener('DOMContentLoaded', function () {
        var avatar = document.getElementById('avatar');
        var image = document.getElementById('image');
        var input = document.getElementById('input');
        var $progress = $('.progress');
        var $progressBar = $('.progress-bar');
        var $alert = $('.alert');
        var $modal = $('#modal');
        var cropper;

        $('[data-toggle="tooltip"]').tooltip();

        input.addEventListener('change', function (e) {
            var files = e.target.files;
            var done = function (url) {
                input.value = '';
                image.src = url;
                $alert.hide();
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });

        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                aspectRatio: 0.90625,
                viewMode: 3,
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        document.getElementById('crop').addEventListener('click', function () {
            appendLoadingScreen();
            var initialAvatarURL;
            var canvas;

            $modal.modal('hide');

            if (cropper) {
                canvas = cropper.getCroppedCanvas({
                    width: 800,
                    height: 883,
                });

                initialAvatarURL = avatar.src;
                avatar.src = canvas.toDataURL();
                $progress.show();
                $alert.removeClass('alert-success alert-warning');
                canvas.toBlob(function (blob) {
                    var formData = new FormData();

                    formData.append('imageFile', blob);
                    formData.append('itemNumber', '<?=$row[0]['itemNumber']?>');
                    formData.append('productName', '<?=$row[0]['name']?>');

                    $.ajax('<?= base_url()?>admin/<?=$this->uri->segment(2)?>/uploadImage/<?=$this->uri->segment(4)?>', {
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,

                        xhr: function () {
                            var xhr = new XMLHttpRequest();

                            xhr.upload.onprogress = function (e) {
                                var percent = '0';
                                var percentage = '0%';

                                if (e.lengthComputable) {
                                    percent = Math.round((e.loaded / e.total) * 100);
                                    percentage = percent + '%';
                                    $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                                }
                            };

                            return xhr;
                        },

                        success: function () {
                            $alert.show().addClass('alert-success').text('Upload success');
                            //removeLoadingScreen();
                            location.reload(); 
                        },

                        error: function () {
                            avatar.src = initialAvatarURL;
                            $alert.show().addClass('alert-warning').text('Upload error');
                        },

                        complete: function () {
                            $progress.hide();
                        },
                    });
                });
            }
        });
    });
</script>


<div id="imageHolder" style="float:left;width:100%;">
    <?php if ($productImages): ?>
        <ul id="sortable_grid" style="width:100%">              
            <?php foreach ($productImages as $productImage): ?>
                <li id="listItem_<?php echo $productImage->id ?>" class="ui-state-default" style="width:auto;height:auto">
                    <div style="position:relative;">
                        <img width="" alt="" src="<?php echo base_url() ?>assets/images/webshop/thumb/<?php echo $productImage->fileName; ?>" />
                        <img src="<?php echo base_url() ?>assets/images/admin/remove.png" style="cursor:pointer;position:absolute;top:-10px;right:0px;" onclick="if (window.confirm('Biztosan törli a képet?'))
                                            document.location = '<?php echo base_url() . 'admin/' . $this->uri->segment(2) . '/deleteImage/' . $this->uri->segment(4) . '/' . $productImage->id ?>';" />
                    </div>
                </li>            
            <?php endforeach ?>
        </ul>
        <script>
            $(function () {
                $("#sortable_grid").sortable({
                    update: function () {
                        $.ajax({
                            url: base_url + 'admin/' + uri_segment2 + '/reorderProductImages',
                            data: $('#sortable_grid').sortable('serialize'),
                            type: 'POST',
                            beforeSend: function () {
                                $('#sortable_grid').css('opacity', 0.5);
                            },
                            success: function (result) {
                                if (result != 'success')
                                    alert("Error. Reload this page please.");
                                $('#sortable_grid').css('opacity', 1);
                            }
                        });
                    }
                });
                $("#sortable_grid").disableSelection();
            });
        </script>
    <?php endif ?>
</div>
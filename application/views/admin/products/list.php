        
<button style="margin-left:0" onclick="document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>/add'">Új hozzáadása</button>    

<br/><br/>

<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">        


        <div class="tab">


            <ol id="sortable" class="pageOl oneDimension sortable">

                <?php
                foreach ($rows as $row) {

                    //$inStockClass = $row['inStock'] ? 'active' : '';
                    //<a style="position:relative;top:5px" class="switch ' . $inStockClass . ' icon" rev="inStock" href="javascript:void(0)" rel="' . $row['inStock'] . '" name="' . $row['id'] . '"></a>
                    $isVisibleClass = $row['isVisible'] ? 'lathatosag' : 'lathatosag2';

                    $category = $this->utils->getFirstObject('product_cats', ['id' => $row['category']]);

                    echo '<li id="list_' . $row["id"] . '">

                          <div>

                              ' . $row["itemNumber"] . ', ' . $row["name"] . ' &nbsp; ('.$row["stock"].' db)  &nbsp; &nbsp;  <b>'.$category->name.'</b>

                              <span style="float:right">
                                <a href="' . site_url("admin") . '/' . $this->uri->segment(2) . '/update/' . $row["id"] . '" class="icon szerkesztes">szerkesztés</a>
                                
                                <a style="position:relative;top:5px" class="switch ' . $isVisibleClass . ' icon" rev="isVisible" href="javascript:void(0)" rel="' . $row['isVisible'] . '" name="' . $row['id'] . '"></a>
                                <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \'' . site_url("admin") . '/' . $this->uri->segment(2) . '/delete/' . $row["id"] . '\'" class="icon torles">törlés</a>
                              </span>

                          </div>';


                    echo '</li>';
                }
                ?>

            </ol>

        </div>
        
    </div>
</div>

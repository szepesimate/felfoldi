    <div class="container top">

      <ul class="breadcrumb">
        <li>
            <?php echo ucfirst($this->uri->segment(1));?> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo $currentModul->name;?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo $currentModul->name;?>          
        </h2>
      </div>
      
      <div class="row">
        <div class="span12 columns">
          

          <table class="table table-striped table-bordered table-condensed productListTable">
            <thead>
              <tr>                
                <th class="yellow header headerSortDown">Cikkszám</th>
                <th class="red header">Terméknév</th>
                <th class="red header">Rendelések száma</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($connectedProductObjects){
                foreach($connectedProductObjects as $row)
                {
                  
                  echo '<tr>';
                  echo '<td>'.$row->itemNumber.'</td>';
                  echo '<td>'.$row->name.'</td>';
                  echo '<td>'.$row->cntt.' db</td>';                  
                  echo '</tr>';
                }
              }
              ?>      
            </tbody>
          </table>



      </div>
    </div>
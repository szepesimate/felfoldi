

<h2>
    <?php echo $currentModul->name; ?> felvitele
</h2>



<?php
//flash messages
if (isset($flash_message)) {
    if ($flash_message == TRUE) {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> new element created with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>
<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/add', $attributes);
?>
<fieldset>
    <div class="control-group">
        <label for="inputError" class="control-label">Cikkszám</label>
        <div class="controls">
            <input type="text" id="" name="itemNumber" value="<?php echo set_value('itemNumber'); ?>" >
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Megnevezés</label>
        <div class="controls">
            <input type="text" id="" name="name" value="<?php echo set_value('name'); ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Kategória</label>
        <div class="controls">
            <?php echo form_dropdown('category', $categoryOptions, set_value('category')); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Fajta / íz</label>
        <div class="controls">
            <input type="text" id="" name="type" value="<?php echo set_value('type'); ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Kiszerelés / tömeg</label>
        <div class="controls">
            <input type="text" id="" name="packaging" value="<?php echo set_value('packaging'); ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Leírás</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('description', html_entity_decode(set_value('description'))); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Hozzávalók</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('ingredients', html_entity_decode(set_value('ingredients'))); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Elkészítési javaslat</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('preparation', html_entity_decode(set_value('preparation'))); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Összetétel</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('composition', html_entity_decode(set_value('composition'))); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Allergia információk</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('allergyInformation', html_entity_decode(set_value('allergyInformation'))); ?>
        </div>
    </div>
    
    <div class="control-group">
        <label for="inputError" class="control-label">Szavatossági idő</label>
        <div class="controls">
            <input type="text" id="expirationDate" name="expirationDate" value="<?php echo set_value('startDate', date('Y-m-d H:i:s')); ?>" >
        </div>
    </div>
    
    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="glutenfree"><b>Gluténmentes</b>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="lactosefree"><b>Laktózmentes</b>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="sugarfree"><b>Cukormentes</b>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="bio"><b>Bio</b>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Vonalkód</label>
        <div class="controls">
            <input type="text" id="" name="barCode" value="<?php echo set_value('barCode'); ?>">
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Alkoholtartalom</label>
        <div class="controls">
            <input type="text" id="" name="alcoholContent" value="<?php echo set_value('alcoholContent'); ?>">
        </div>
    </div>
    
    <div class="control-group" style="">
        <label for="inputError" class="control-label">Ár (bruttó)</label>
        <div class="controls">
            <input type="text" id="" name="price" value="<?php echo set_value('price'); ?>">
        </div>
    </div>
    
    <div class="control-group" style="">
        <label for="inputError" class="control-label">Kedvezményes ár (bruttó)</label>
        <div class="controls">
            <input type="text" id="" name="discountPrice" value="<?php echo set_value('discountPrice'); ?>">
        </div>
    </div>
    
    <div class="control-group" style="">
        <label for="inputError" class="control-label">Készlet (db)</label>
        <div class="controls">
            <input type="text" id="" name="stock" value="<?php echo set_value('stock', 9999); ?>">
        </div>
    </div>

    <!-- <div class="control-group">
        <label for="inputError" class="control-label">Kép</label>
        <div class="controls">
            <input type="file" multiple id="productImage" name="productImage[]">
        </div>
    </div> -->

    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>
        <button class="btn" type="reset">Mezők visszaállítása</button>
    </div>
</fieldset>

<?php echo form_close(); ?>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script>
    $(document).ready(function(){
        $('#expirationDate').datetimepicker({
            format:'Y-m-d H:i:s',
            lang:'hu'
        });

        // $('#productImage').picEdit({
        //     maxWidth: 600,
        //     formSubmitted: function(response){
        //         $('div.form-actions').hide();
        //         setTimeout(function(){
        //             //document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>#';
        //         }, 1000);
        //     }
        // });
    });
    
    
</script>
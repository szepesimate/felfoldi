    <div class="container top">

      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a>
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo $currentModul->name;?>
          </a>
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">Új</a>
        </li>
      </ul>

      <div class="page-header">
        <h2>
          <?php echo $currentModul->name;?> felvitele
        </h2>
      </div>

      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> new element created with success.';
          echo '</div>';
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';
        }
      }
      ?>

      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');



      //form validation
      echo validation_errors();

      echo form_open('admin/'.$this->uri->segment(2).'/add', $attributes);
      ?>
        <fieldset>

          <div class="control-group">
            <label for="inputError" class="control-label">Nyelv</label>
            <div class="controls">
              <?php echo form_dropdown('lang', $langOptions, set_value('lang')); ?>
            </div>
          </div>






          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Mentés</button>
            <button class="btn" type="reset">Mezők visszaállítása</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>




<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#articleImage').picEdit({
      maxWidth: 600,
      formSubmitted: function(response){
        $('div.form-actions').hide();
        setTimeout(function(){
          document.location = '<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>';
        }, 1000);
      }
    });
	});
</script>

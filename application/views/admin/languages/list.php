    <div class="container top">

      <ul class="breadcrumb">
        <li>
            <?php echo ucfirst($this->uri->segment(1));?>
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo $currentModul->name;?>
        </li>
      </ul>


      <div class="page-header users-header">
        <h2>
          <?php echo $currentModul->name;?>
          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Új hozzáadása</a>
        </h2>
      </div>

      <div class="row">
        <div class="span12 columns">

          <div class="well">

            <?php

            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');


            //save the columns names in a array that we will use as filter
            $options_table = array();
            foreach ($rows as $array) {
              foreach ($array as $key => $value) {
                $options_table[$key] = $key;
              }
              break;
            }

            echo form_open('admin/'.$this->uri->segment(2), $attributes);

              echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected, 'style="width: 170px;
height: 26px;"');


              //echo form_label('Order by:', 'order');
              //echo form_dropdown('order', $options_table, $order, 'class="span2"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              //$options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              //echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span1"');

              echo form_submit($data_submit);

            echo form_close();
            ?>

          </div>



          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="yellow header headerSortDown">Nyelv</th>
                <th class="red header"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($rows as $row)
              {
                echo '<tr>';
                echo '<td>'.$row["lang"].'</td>';
                echo '<td> <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \''.site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row["id"].'\'" class="btn btn-danger">törlés</a> </td>';
                echo '</tr>';
              }
              ?>
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>

      </div>
    </div>

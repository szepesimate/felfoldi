<div class="container top">



    <div class="page-header users-header">
        <h2>
            <?php echo $currentModul->name; ?>
        </h2>
        <br/><br/>
    </div>

    <div class="row">
        <div class="span12 columns">





            <table class="">
                <thead>
                    <tr>
                        <th class="yellow header headerSortDown">Név</th>
                        <th class="red header">Szállítás, fizetés</th>
                        <th class="red header">Számlázási adatok</th>
                        <th class="red header">Szállítási adatok</th>                        
                        <th class="red header">Tételek</th>                
                        <th class="red header">Dátum</th>
                        <th class="red header">Stripe státusz</th>
                        <!-- <th class="red header">&nbsp;</th> -->
                        <th class="red header">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($rows as $row) {
                        //ez csúnya itt, de hasznos :)
                        $tetelek = $this->utils->getResultObject('order_items', ['order_id' => $row['id']], ['id', 'asc']);
                        $tetelek_str = "";
                        $invoice_cell_str = "";
                        if ($tetelek) {
                            $sum = 0;
                            foreach ($tetelek as $tetel) {
                                $reszOsszeg = $tetel->count * $tetel->price;
                                $sum += $reszOsszeg;
                                $tetelek_str .= "-" . $tetel->name . ", $tetel->itemNumber ($tetel->price Ft) x $tetel->count db: " . $reszOsszeg . " Ft<br/>";
                            }
                            $tetelek_str .= "<p style=\"text-align:right;margin-top:10px\"><u>Termékek:</u> " . $sum . " Ft<br/>";
                            $tetelek_str .= "<u>Szállítás:</u> " . $row["shipping_cost"] . " Ft<br/>";
                            $tetelek_str .= "<u>Mindösszesen:</u> " . ($row["shipping_cost"] + $sum) . " Ft</p>";
                        }

                        if ($row['closed']) {
                            //show PDF link
                            $invoice_cell_str = '<a style="color:#8BC34A" href="' . base_url() . 'assets/docs/pdf/invoice_' . $row["id"] . '.pdf" target="_blank">DONE:<br/>SHOW PDF</a>';
                        } else {
                            //show PDF generate button
                            $invoice_cell_str = '<a style="color:orange" href="' . base_url() . 'Pdf_gen/generate_invoice/' . $row["id"] . '" onclick="setTimeout(function(){location.reload();}, 3000)" target="_blank">PENDING:<br/>CREATE INVOICE</a>';
                        }
                        
                        $stripe_status = '';
                        if ($row['stripe_charge_log'] == ''){
                            $stripe_status = 'nincs adat';
                        }else{
                            $obj = json_decode($row['stripe_charge_log']);
                            $stripe_status = $obj->status;
                        }

                        echo '<tr>';
                        echo '<td><b>' . $row["name"] . '</b><br/>' . $row["email"] . '<br/>' . $row["phone"] . '<br/>' . $row["comment"] . '<br/></td>';
                        echo '<td>' . $row["delivery_mode"] . '<br/>' . $row["payment_mode"] . '</td>';
                        echo '<td>' . $row["billing_name"] . '<br/>' . $row["billing_region"] . '<br/>' . $row["billing_zip"] . ' ' . $row["billing_city"] . ' ' . $row["billing_street"] . '</td>';
                        echo '<td>' . $row["delivery_name"] . '<br/>' . $row["delivery_region"] . '<br/>' . $row["delivery_zip"] . ' ' . $row["delivery_city"] . ' ' . $row["delivery_street"] . '</td>';

                        echo '<td>' . $tetelek_str . '</td>';
                        echo '<td>' . date('Y.m.d H:i:s', strtotime($row["date"])) . '</td>';
                        echo '<td><b>' . $stripe_status . '</b></td>';
                        //echo '<td><b>' . $invoice_cell_str . '</b></td>';
                        echo '<td>
                              <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \''.site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row['id'].'\'" class="icon torles">törlés</a>
                            </td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>

            <?php echo '<div class="pagination">' . $this->pagination->create_links() . '</div>'; ?>

        </div>
    </div>

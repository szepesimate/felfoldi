<!DOCTYPE html>
<html lang="hu">
    <head>
        <title>Admin</title>
        <meta charset="utf-8">

        <meta name="robots" content="noindex, nofollow" />


        <link href="<?php echo base_url(); ?>assets/css/admin/reset.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/admin/styles.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/admin/color3/color.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/admin/colourdesign_modd/color.css" rel="stylesheet" />



        
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/picedit.min.css" />
        <link href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">


        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.mjs.nestedSortable.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.js"></script>




        <!-- jQuery Datatables -->
        <script src="<?php echo base_url(); ?>assets/js/admin/jquery-datatables/jquery.dataTables.min.js"></script>







        <!-- chosen (select modd) -->
        <script src="<?php echo base_url(); ?>assets/js/admin/jquery.chosen.min.js"></script>
        <link href="<?php echo base_url(); ?>assets/css/admin/chosen.css" rel="stylesheet" type="text/css" />





        <!-- Script Loader -->
        <script src="<?php echo base_url(); ?>assets/js/admin/custom_functions.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/admin/loader.js"></script>



        <script src="<?php echo base_url(); ?>assets/js/admin.js"></script>





        <script>
            var base_url = '<?php echo base_url() ?>';
            var uri_segment2 = '<?php echo $this->uri->segment(2) ?>';
        </script>

    </head>
    <body class="altlayout">








        <div id="top_bar">

            <a href="<?php echo base_url(); ?>admin" id="logo">

            </a><!-- end logo -->

            <ul>

                <li class="sitemap<?php if ($this->uri->segment(2) == 'pages') echo ' current'; ?>">
                    <a href="<?php echo base_url(); ?>admin/pages">
                        <img src="<?php echo base_url(); ?>assets/css/admin/colourdesign_modd/images/icon_sitemap.png" alt="icon_sitemap" width="19" height="19" />
                        Sitemap
                    </a>
                </li>
                <?php /*
                  <li class="global_settings">
                  <a href="" target="_blank">
                  <img src="<?php echo base_url(); ?>assets/css/admin/colourdesign_modd/images/icon_global_settings.png" alt="icon_global_settings" width="19" height="19" />
                  Globális beállítások
                  </a>
                  </li>

                  <li class="personal_settings">
                  <a href="">
                  <img src="<?php echo base_url(); ?>assets/css/admin/colourdesign_modd/images/icon_personal_settings.png" alt="icon_personal_settings" width="19" height="19" />
                  Szegélyes beállítások
                  </a>
                  </li>
                 */ ?>
                <li class="see_webpage">
                    <a href="<?php echo base_url(); ?>" target="_blank">
                        <img src="<?php echo base_url(); ?>assets/css/admin/colourdesign_modd/images/icon_see_webpage.png" alt="icon_see_webpage" width="19" height="19" />
                        Weboldal megtekintése
                    </a>
                </li>

                <li class="quit">
                    <a href="<?php echo base_url(); ?>admin/logout">
                        <img src="<?php echo base_url(); ?>assets/css/admin/colourdesign_modd/images/icon_logout.png" alt="icon_logout" width="19" height="19" />
                        Kilépés
                    </a>
                </li>

                <!--<li class="dropdown">
                        <a href="settings.html">Settings</a>
                        <ul>
                                <li>Section</li>
                                <li><a href="#">Section #1</a></li>
                                <li><a href="#">Section #2</a></li>
                        </ul>--><!-- end dropdown menu --><!--
                </li>-->
            </ul><!-- end menu -->

        </div><!-- end top_Bar -->



        <div id="header">
            <div class="container clearfix" style="padding-left:0">

                <ul class="clearfix" id="navigation">
                    <?php /*
                      <li>
                      <a href="">
                      <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/config.png"/>
                      Globális weboldal beállítások
                      </a>
                      </li><!-- end sitemap -->
                     */ ?>

                    <?php
                    $icon_assoc = [
                        'articles' => 'blog',
                        'pages' => 'sitemap',
                        'galleries' => 'img',
                    ];
                    ?>


                    <?php foreach ($moduls as $modul_arr): ?>
                        <?php
                        $modul = $modul_arr["mainMenuItem"];
                        if (isset($icon_assoc[$modul->url])) {
                            $icon = $icon_assoc[$modul->url];
                        } else {
                            $icon = "dashboard";
                        }

                        if ($modul->url == "") {
                            ?>
                            <li<?php if (false) echo ' class="current"' ?>>
                                <a href="#">
                                    <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/<?= $icon ?>.png"/>
                                    <?php echo $modul->name ?>
                                </a>

                                <ul>
                                    <?php
                                    $submoduls = $modul_arr["subMenuItems"];
                                    foreach ($submoduls as $submodul):
                                        ?>
                                        <li<?php
                                        if ($this->uri->segment(2) == $modul->url) {
                                            echo ' class="current"';
                                        }
                                        ?>>
                                            <a href="<?php echo base_url(); ?>admin/<?php echo $submodul->url ?>"><?php echo $submodul->name ?></a>
                                        </li>
                            <?php endforeach ?>
                                </ul>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li<?php
                    if ($this->uri->segment(2) == $modul->url) {
                        echo ' class="current"';
                    }
                            ?>>
                                <a href="<?php echo base_url(); ?>admin/<?php echo $modul->url ?>">
                                    <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/<?= $icon ?>.png"/>
                            <?php echo $modul->name ?>
                                </a>
                            </li>
    <?php } ?>

                    <?php endforeach ?>



                    <?php /*

                      <li>
                      <a href="/admin/media">
                      <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/media.png"/>
                      Média
                      </a>
                      </li><!-- end sitemap -->

                      <li>
                      <a href="/admin/fooldal">
                      <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/dashboard.png"/>
                      Főoldal
                      </a>
                      </li><!-- end sitemap -->

                      <li>
                      <a href="/admin/galeria">
                      <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/img.png"/>
                      Galéria
                      </a>
                      </li><!-- end sitemap -->

                      <li>
                      <a href="/admin/blog">
                      <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/blog.png"/>
                      Blog
                      </a>
                      </li><!-- end sitemap -->

                      <li>
                      <a href="#">
                      <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/dashboard.png"/>
                      Hírlevél
                      </a>
                      <ul>
                      <li><a href="/admin/hirlevel/csoportok">Csoportok kezelése</a></li>
                      <li class="current"><a href="/admin/hirlevel/hirlevel-kuldes">Hírlevél küldés</a></li>
                      </ul>
                      </li><!-- end sitemap -->

                      <li>
                      <a href="/admin/jogosultsagok">
                      <img class="icon" src="<?php echo base_url(); ?>assets/css/admin/images/icons/dashboard.png"/>
                      Jogosultságok
                      </a>
                      </li><!-- end sitemap -->

                      <!-- end sitemap -->

                      <!--<li>
                      <a href="">
                      <img alt="inbox" class="icon" src="/admindesign/css/images/icons/inbox.png"/>Inbox
                      <span class="notification" rel="tooltip" original-title="You have 15 new messages">15</span>
                      </a>
                      </li>
                      <li>
                      <a href="#">
                      <img alt="colors" class="icon" src="/admindesign/css/images/icons/colors.png"/>Colors
                      <span class="notification">New</span>
                      </a>
                      <ul>
                      <li><a href="#" rel="normal" class="styleswitch">Normal</a></li>
                      <li><a href="#" rel="color1" class="styleswitch">Color 1</a></li>
                      <li><a href="#" rel="color2" class="styleswitch">Color 2</a></li>
                      <li><a href="#" rel="color3" class="styleswitch">Color 3</a></li>
                      </ul>
                      </li>
                      <li>
                      <a href="grid.html">
                      <img alt="layout" class="icon" src="/admindesign/css/images/icons/layout.png"/>Grid
                      <span class="notification">New</span>
                      </a>
                      </li>
                      <li>
                      <a href="">
                      <img alt="statistics" class="icon" src="/admindesign/css/images/icons/statistics.png"/>Statistics</a>
                      </li>-->

                     */ ?>
                </ul><!-- end navigation -->

            </div><!-- end container -->
        </div><!-- end header -->



        <!-- Content -->
        <div id="content_cd">
            <div class="container clearfix" style="margin-left:0">







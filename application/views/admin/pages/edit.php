<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name;?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">



        <?php
        //flash messages
        if($this->session->flashdata('flash_message')){
            if($this->session->flashdata('flash_message') == 'updated')
            {
                echo '<div class="alert alert-success">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Well done!</strong> element updated with success.';
                echo '</div>';
            }else{
                echo '<div class="alert alert-error">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
                echo '</div>';
            }
        }
        ?>

        <?php
        //form data
        $attributes = array('class' => 'form-horizontal', 'id' => '');



        //form validation
        echo validation_errors();

        echo form_open('admin/'.$this->uri->segment(2).'/update/'.$this->uri->segment(4).'', $attributes);
        ?>
        <fieldset>
            <div class="control-group">
                <label for="inputError" class="control-label">Név</label>
                <div class="controls">
                    <input type="text" id="name_field" name="name" value="<?php echo $row[0]['name']; ?>" >
                    <!--<span class="help-inline">Woohoo!</span>-->
                </div>
            </div>
            <?php /*
               <div class="control-group">
                <label for="inputError" class="control-label">Tpl</label>
                <div class="controls">
                    <input type="text" id="" name="tpl" value="<?php echo $row[0]['tpl'];?>">
                    <!--<span class="help-inline">Cost Price</span>-->
                </div>
            </div> */ ?>

            <div class="control-group">
                <label for="inputError" class="control-label">Aloldal típusa</label>
                <div class="controls">
                    <?php echo form_dropdown('tpl', $tplOptions, $row[0]['tpl']); ?>
                </div>
            </div>


            <div class="control-group">
                <label for="inputError" class="control-label">SEO Title</label>
                <div class="controls">
                    <input type="text" id="seo_title_field" name="seo_title" value="<?php echo $seo_title; ?>">
                </div>
            </div>

            <div class="control-group">
                <label for="inputError" class="control-label">SEO Description</label>
                <div class="controls">
                    <input type="text" id="" name="seo_description" value="<?php echo $seo_description; ?>">
                </div>
            </div>


            <div class="control-group">
                <label for="inputError" class="control-label">SEO UrlFriendly</label>
                <div class="controls">
                    <input type="text" id="seo_urlfriendly_field" name="seo_urlfriendly" value="<?php echo $seo_urlfriendly; ?>">
                </div>
            </div>


            <div class="form-actions">
                <button class="btn btn-primary" type="submit">Mentés</button>
            </div>
        </fieldset>

        <?php echo form_close(); ?>


        <script>

$(document).ready(function(){

    $('#name_field').on( 'change', function(){
        if ( $('#seo_title_field').val() == '' ){
            $('#seo_title_field').val( $('#name_field').val() );
        }

        if ( $('#seo_urlfriendly_field').val() == '' ){
            var url_format = szeplinkesites( $('#name_field').val() );
            $('#seo_urlfriendly_field').val( url_format );
        }
    })

});

        </script>

    </div>

</div>


<button style="margin-left:0" onclick="document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>/add'">Új hozzáadása</button>


<br/><br/>

<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">



        <div id="tab1" class="tab">

            <ol id="sortable" class="pageOl sortable">


                <?php
                foreach ($rows as $row) {
                    $rootObj = $row["rootObj"];

                    $inMainMenuClass = $rootObj->inMainMenu ? "lathatosag" : "lathatosag2";
                    $inSidebarMenuClass = $rootObj->inSidebarMenu ? "lathatosag" : "lathatosag2";
                    $inFooterMenuClass = $rootObj->inFooterMenu ? "lathatosag" : "lathatosag2";

                    $this->load->model('utils');

                    echo '<li id="list_' . $rootObj->id . '"><div>' . $rootObj->name . ' (' . (isset($this->utils->get_tplOptions()[$rootObj->tpl]) ? $this->utils->get_tplOptions()[$rootObj->tpl] : $rootObj->tpl) . ')';
                    echo '
                            <span style="float:right">
                                <a class="switch ' . $inMainMenuClass . '" rev="inMainMenu" href="javascript:void(0)" rel="' . $rootObj->inMainMenu . '" name="' . $rootObj->id . '">Főmenüben</a>
                                <a class="switch ' . $inSidebarMenuClass . '" rev="inSidebarMenu" href="javascript:void(0)" rel="' . $rootObj->inSidebarMenu . '" name="' . $rootObj->id . '">Oldalsó menüben</a>
                                <a class="switch ' . $inFooterMenuClass . '" rev="inFooterMenu" href="javascript:void(0)" rel="' . $rootObj->inFooterMenu . '" name="' . $rootObj->id . '">Footerben</a>

                                <a href="' . site_url("admin") . '/' . $this->uri->segment(2) . '/update/' . $rootObj->id . '" class="icon szerkesztes">szerkesztés</a>
                                <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \'' . site_url("admin") . '/' . $this->uri->segment(2) . '/delete/' . $rootObj->id . '\'" class="icon torles">törlés</a>

                            </span>
                        </div>';

                    if (isset($row["subObjects"])) {
                        echo "<ol>";
                        foreach ($row["subObjects"] as $subObject) {
                            $inMainMenuClass = $subObject->inMainMenu ? "lathatosag" : "lathatosag2";
                            $inSidebarMenuClass = $subObject->inSidebarMenu ? "lathatosag" : "lathatosag2";
                            $inFooterMenuClass = $subObject->inFooterMenu ? "lathatosag" : "lathatosag2";

                            echo '<li id="list_' . $subObject->id . '"><div>' . $subObject->name . ' (' . $subObject->tpl . ')';
                            echo '
                                  <span style="float:right">

                                    <a class="switch ' . $inMainMenuClass . '" rev="inMainMenu" href="javascript:void(0)" rel="' . $subObject->inMainMenu . '" name="' . $subObject->id . '">Főmenüben</a>
                                    <a class="switch ' . $inSidebarMenuClass . '" rev="inSidebarMenu" href="javascript:void(0)" rel="' . $subObject->inSidebarMenu . '" name="' . $subObject->id . '">Oldalsó menüben</a>
                                    <a class="switch ' . $inFooterMenuClass . '" rev="inFooterMenu" href="javascript:void(0)" rel="' . $subObject->inFooterMenu . '" name="' . $subObject->id . '">Footerben</a>


                                    <a href="' . site_url("admin") . '/' . $this->uri->segment(2) . '/update/' . $subObject->id . '" class="icon szerkesztes">szerkesztés</a>
                                    <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \'' . site_url("admin") . '/' . $this->uri->segment(2) . '/delete/' . $subObject->id . '\'" class="icon torles">törlés</a>
                                </span>
                            </div>';
                        }
                        echo "</ol>";
                    }

                    echo '</li>';
                }
                ?>

            </ol>

        </div>




    </div>

</div>


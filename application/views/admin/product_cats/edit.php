

<h2>
    <?php echo $currentModul->name; ?> szerkesztése
</h2>


<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');




//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . '', $attributes);
?>
<fieldset>
    <div class="control-group">
        <label for="inputError" class="control-label">Megnevezés</label>
        <div class="controls">
            <input type="text" id="" name="name" value="<?php echo $row[0]['name']; ?>" >
        </div>
    </div>


    <div class="control-group">
        <label for="inputError" class="control-label">Leírás</label>
        <div class="controls">
            <textarea cols="" rows="3" id="" name="description"><?php echo $row[0]['description']; ?></textarea>
        </div>
    </div>

    <?php if (isset($currentImgFullUrl)): ?>
      <div class="control-group">
        <label for="inputError" class="control-label">Jelenlegi kép</label>
        <div class="controls">
          <img alt="" width="200" src="<?php echo $currentImgFullUrl ?>" />
        </div>
      </div>
    <?php endif ?>



    <div class="control-group">
      <label for="inputError" class="control-label">Kép csere</label>
      <div class="controls">
        <input type="file" id="productCategoryImage" name="productCategoryImage">
      </div>
    </div>


    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>
        <button class="btn" type="reset">Mezők visszaállítása</button>
    </div>
</fieldset>

<?php echo form_close(); ?>

</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#productCategoryImage').picEdit({
      maxWidth: 600,
      formSubmitted: function (response) {
            setTimeout(function () {
                document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>';
            }, 3000);
        }
    });
  });
</script>


<button style="margin-left:0" onclick="document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>/add'">Új hozzáadása</button>    

<br/><br/>

<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">        


        <br/><br/><br/>

        <div class="tab">


            <ol id="sortable" class="pageOl sortable">


                <?php
                foreach ($rows as $row) {

                    $rootObj = $row["rootObj"];
                    echo '<li id="list_' . $rootObj->id . '"><div>' . $rootObj->name . ' (' . $rootObj->id . ')';
                    echo '
                              <span style="float:right">
                                <a href="' . site_url("admin") . '/' . $this->uri->segment(2) . '/update/' . $rootObj->id . '" class="icon szerkesztes">szerkesztés</a>
                                <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \'' . site_url("admin") . '/' . $this->uri->segment(2) . '/delete/' . $rootObj->id . '\'" class="icon torles">törlés</a>
                              </span>
                            </div>';

                    if (isset($row["subObjects"])) {
                        echo "<ol>";
                        foreach ($row["subObjects"] as $subObjects) {
                            echo '<li id="list_' . $subObjects->id . '"><div>' . $subObjects->name . ' (' . $subObjects->id . ')';
                            echo '
                                      <span style="float:right">
                                        <a href="' . site_url("admin") . '/' . $this->uri->segment(2) . '/update/' . $subObjects->id . '" class="icon szerkesztes">szerkesztés</a>
                                        <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \'' . site_url("admin") . '/' . $this->uri->segment(2) . '/delete/' . $subObjects->id . '\'" class="icon torles">törlés</a>
                                      </span>
                                    </div>';
                        }
                        echo "</ol>";
                    }

                    echo '</li>';
                }
                ?>

            </ol>

        </div>


    </div><!-- end content -->

</div><!-- end box -->        

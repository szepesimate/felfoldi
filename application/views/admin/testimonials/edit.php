


<h2>
    <?php echo $currentModul->name; ?> szerkesztése
</h2>



<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . '', $attributes);
?>

<fieldset>
    <table>
        <tr>
            <th>
                <div class="control-group">
                    <label for="inputError" class="control-label">Név</label>
                    <div class="controls">
                        <input type="text" id="name_field" name="name" value="<?php echo $row[0]['name']; ?>" >
                    </div>
                </div>
                
                

                <div class="control-group">
                    <label for="inputError" class="control-label">Értékelés</label>
                    <div class="controls">
                        <?php
                        //$this->ckeditor->config['height'] = 500;
                        //echo $this->ckeditor->editor('content', $row[0]['content']);
                        ?>
                        <textarea name="comment" cols="100" rows="10"><?=$row[0]['comment']?></textarea>
                    </div>
                </div>
            </th>
        </tr>
    </table>

    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>
    </div>
</fieldset>

<?php echo form_close(); ?>









<?php /*
  <button style="margin-left:0" onclick="document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>/add'">Új hozzáadása</button>

  <br/><br/>
 */ ?>

<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">

        <div class="tabs">
            <?php /*
              <ul class="navigation clearfix">
              <?php if ($page_objects) { ?>
              <?php foreach ($page_objects as $page_object): ?>
              <li><a href="#tab<?= $page_object->id ?>"<?php if ($page_object == $page_objects[0]) echo ' class="current"'; ?>><?= $page_object->name ?></a></li>
              <?php endforeach ?>
              <li><a href="#tab_other">Egyéb tartalmak</a></li>
              <?php } ?>
              </ul><!-- end navigation -->
             */ ?>
            <?php
            $other_object = new stdClass();
            $other_object->id = '_other';
            $page_objects[] = $other_object;

            $displayed = [];
            ?>

            <?php if ($page_objects) { ?>
                <?php foreach ($page_objects as $page_object): ?>

                    <div class="tab" id="tab<?= $page_object->id ?>">

                        <table class=" sortable">
                            <thead>
                                <tr>
                                    <th class="sorting">Név</th>
                                    <th class="sorting">Képzés</th>
                                    <th class="sorting">Város</th>
                                    <th class="sorting" style="width: 400px">Értékelés</th>                            
                                    <th class="sorting">Rögzítés dátuma</th>
                                    <th class="sorting">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($rows as $row) {
                                    $displayed[] = $row['id'];

                                    $tanfolyam_nev = '';
                                    $course = $this->course->get($row['tanfolyam_id']);
                                    if ($course) {
                                        $tanfolyam_nev = $course->nev;
                                    }

                                    $statusClass = $row['status'] ? 'lathatosag' : 'lathatosag2';
                                    echo '<tr>';
                                    echo '<td>' . $row['name'] . '</td>';
                                    echo '<td>' . $tanfolyam_nev . '</td>';
                                    echo '<td>' . $row['city'] . '</td>';
                                    echo '<td>' . $row['comment'] . '</td>';
                                    echo '<td>' . $row['store_date'] . '</td>';


                                    echo '<td>
                              <a href="' . site_url("admin") . '/' . $this->uri->segment(2) . '/update/' . $row['id'] . '" class="icon szerkesztes">szerkesztés</a>
                              <a style="position:relative;top:5px" class="switch ' . $statusClass . ' icon" rev="status" href="javascript:void(0)" rel="' . $row['status'] . '" name="' . $row['id'] . '"></a>
                              <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \'' . site_url("admin") . '/' . $this->uri->segment(2) . '/delete/' . $row['id'] . '\'" class="icon torles">törlés</a>
                            </td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>

                <?php endforeach ?>

            <?php } ?>

        </div>

    </div><!-- end content -->

</div><!-- end box -->

<?php
//echo '<div class="pagination">'.$this->pagination->create_links().'</div>';
?>




        <h2>
          <?php echo $currentModul->name;?> szerkesztése
        </h2>



      <?php
      //flash messages
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == 'updated')
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> element updated with success.';
          echo '</div>';
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';
        }
      }
      ?>

      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');



      //form validation
      echo validation_errors();

      echo form_open('admin/'.$this->uri->segment(2).'/update/'.$this->uri->segment(4).'', $attributes);
      ?>


          <div class="control-group">
            <label for="inputError" class="control-label">Felhasználónév</label>
            <div class="controls">
              <input type="text" id="" name="user_name" value="<?php echo $row[0]['user_name']; ?>">
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Jelszó változtatás</label>
            <div class="controls">
              <input type="text" id="" name="pass_word" value="">
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Vezetéknév</label>
            <div class="controls">
              <input type="text" id="" name="last_name" value="<?php echo $row[0]['last_name']; ?>">
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Keresztnév</label>
            <div class="controls">
              <input type="text" id="" name="first_name" value="<?php echo $row[0]['first_name']; ?>">
            </div>
          </div>



          <div class="control-group">
            <label for="inputError" class="control-label">Jogosultságok</label>
            <div class="controls">

                <?php foreach ($all_moduls as $modul):?>

                    <label><input type="checkbox" name="modul_ids[]" value="<?=$modul->id?>"<?php if (in_array($modul->id, $assigned_modul_id_array)) echo ' checked'; ?> /> <?=$modul->name?></label>

                <?php endforeach?>


            </div>
          </div>




          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Mentés</button>

          </div>


      <?php echo form_close(); ?>




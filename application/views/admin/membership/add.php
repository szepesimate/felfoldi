

        <h2>
          <?php echo $currentModul->name;?> felvitele
        </h2>



      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> new element created with success.';
          echo '</div>';
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';
        }
      }
      ?>

      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');



      //form validation
      echo validation_errors();

      echo form_open('admin/'.$this->uri->segment(2).'/add', $attributes);
      ?>
        <fieldset>


          <div class="control-group">
            <label for="inputError" class="control-label">Felhasználónév</label>
            <div class="controls">
              <input type="text" name="user_name" value="<?php echo set_value('user_name'); ?>" >
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Jelszó</label>
            <div class="controls">
              <input type="text" name="pass_word" value="<?php echo set_value('pass_word'); ?>" >
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Vezetéknév</label>
            <div class="controls">
              <input type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" >
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Keresztnév</label>
            <div class="controls">
              <input type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" >
            </div>
          </div>


          <div class="control-group">
            <label for="inputError" class="control-label">Jogosultságok</label>
            <div class="controls">

                <?php foreach ($all_moduls as $modul):?>

                    <label><input type="checkbox" name="modul_ids[]" value="<?=$modul->id?>" /> <?=$modul->name?></label>

                <?php endforeach?>


            </div>
          </div>



          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Mentés</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>


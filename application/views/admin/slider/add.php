<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?> felvitele</h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">



        <?php
        //flash messages
        if (isset($flash_message)) {
            if ($flash_message == TRUE) {
                echo '<div class="alert alert-success">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Well done!</strong> new element created with success.';
                echo '</div>';
            } else {
                echo '<div class="alert alert-error">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
                echo '</div>';
            }
        }
        ?>

        <?php
        //form data
        $attributes = array('class' => 'form-horizontal', 'id' => '');



        //form validation
        echo validation_errors();

        echo form_open('admin/' . $this->uri->segment(2) . '/add', $attributes);
        ?>
        <fieldset>
            <?php /*
              <div class="control-group">
              <label for="inputError" class="control-label">Cím</label>
              <div class="controls">
              <input type="text" id="" name="title" value="<?php echo set_value('title'); ?>" >
              </div>
              </div>

              <div class="control-group">
              <label for="inputError" class="control-label">Cím url</label>
              <div class="controls">
              <input type="text" id="" name="title_url" value="<?php echo set_value('title_url'); ?>" >
              </div>
              </div>

              <div class="control-group">
              <label for="inputError" class="control-label">Alcím 1</label>
              <div class="controls">
              <input type="text" id="" name="subtitle" value="<?php echo set_value('subtitle'); ?>" >
              </div>
              </div>

              <div class="control-group">
              <label for="inputError" class="control-label">Alcím 2</label>
              <div class="controls">
              <input type="text" id="" name="subtitle_url" value="<?php echo set_value('subtitle_url'); ?>" >
              </div>
              </div>

             */ ?>
            <div class="control-group">
                <label for="inputError" class="control-label">Gyártó</label>
                <div class="controls">
                    <?php echo form_dropdown('opt1', $gyartoOptions, set_value('opt1')); ?>
                </div>
            </div>

            <?php /*
              <div class="control-group">
              <label for="inputError" class="control-label">Gomb link</label>
              <div class="controls">
              <input type="text" id="" name="opt2" value="<?php echo set_value('opt2'); ?>" >
              </div>
              </div>
             */ ?>
            <div class="control-group">
                <label for="inputError" class="control-label">Kép</label>
                <div class="controls">
                    <input type="file" id="sliderImg" name="sliderImg">
                </div>
            </div>





            <div class="form-actions">
                <button class="btn btn-primary" type="submit">Mentés</button>
            </div>
        </fieldset>

        <?php echo form_close(); ?>

    </div>

</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('#sliderImg').picEdit({
            maxWidth: 600,
            formSubmitted: function (response) {
                $('div.form-actions').hide();
                setTimeout(function () {
                    document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>';
                }, 3000);
            }
        });
    });
</script>

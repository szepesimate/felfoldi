

<h2>
    <?php echo $currentModul->name; ?> felvitele
</h2>



<?php
//flash messages
if (isset($flash_message)) {
    if ($flash_message == TRUE) {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> new element created with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/add', $attributes);
?>
<fieldset>
    <table style="max-width: 1080px">
        <div style="display: table-row">
            <div style="display: table-cell; width: 400px; padding: 10px 10px 10px 20px;">
                <div class="control-group">
                    <label for="inputError" class="control-label">Képzés neve</label>
                    <div class="controls">
                        <input type="text" id="name_field" name="nev" tabindex="" value="<?php echo set_value('nev'); ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Képzés időtartama</label>
                    <div class="controls">
                        <input type="text" id="idotartam_elmelet" name="idotartam" tabindex="" value="<?php echo set_value('idotartam'); ?>" >
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="inputError" class="control-label">Kategória</label>
                    <div class="controls">
                        <?php echo form_dropdown('kategoria', ['Bármixer'=>'Bármixer','Pincér'=>'Pincér','Barista'=>'Barista'], set_value('kategoria')); ?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="inputError" class="control-label">Upsell</label>
                    <div class="controls">
                        <?php echo form_dropdown('upsell_id', $upsell_items, set_value('upsell_id')); ?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="inputError" class="control-label"></label>
                    <div class="controls">
                        <input type="checkbox" id="" name="kiemelt" value="1" tabindex="" <?php echo set_checkbox('kiemelt', '1'); ?> ><b>Kiemelt tanfolyam</b>
                    </div>
                </div>
                
                
            </div>
            <div style="display: table-cell; width: 400px">       
                
                <div class="control-group">
                    <label for="inputError" class="control-label">Ár</label>
                    <div class="controls">
                        <input type="text" class="dij" name="ar" tabindex="" value="<?php echo set_value('ar'); ?>" >
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="inputError" class="control-label">Akciós ár</label>
                    <div class="controls">
                        <input type="text" class="dij" name="akcios_ar" tabindex="" value="<?php echo set_value('akcios_ar'); ?>" >
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="inputError" class="control-label">Akció kezdete</label>
                    <div class="controls">
                        <input type="text" class="datepicker" id="" name="akcio_kezdete" tabindex="" value="<?php echo set_value('akcio_kezdete'); ?>" >
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="inputError" class="control-label">Akció vége</label>
                    <div class="controls">
                        <input type="text" class="datepicker" id="" name="akcio_vege" tabindex="" value="<?php echo set_value('akcio_vege'); ?>" >
                    </div>
                </div>
                
            </div>
        </div>
        <tr>
            <td>
                <div class="control-group">
                    <label for="inputError" class="control-label">Leírás</label>
                    <div class="controls">
                        <?php
                        $this->ckeditor->config['height'] = 500;
                        echo $this->ckeditor->editor('leiras', html_entity_decode(set_value('leiras')));
                        ?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="inputError" class="control-label">Tematika</label>
                    <div class="controls">
                        <?php
                        $this->ckeditor->config['height'] = 500;
                        echo $this->ckeditor->editor('tematika', html_entity_decode(set_value('tematika')));
                        ?>
                    </div>
                </div>
            </td>

        </tr>
    </table>

    <div class="control-group">
        <label for="inputError" class="control-label">Kép</label>
        <div class="controls">
            <input type="file" id="articleImage" name="articleImage">
        </div>
    </div>
    
    <div class="control-group">
        <label for="inputError" class="control-label">Videó</label>
        <div class="controls">
            <input type="text" name="video" tabindex="" value="<?php echo set_value('video'); ?>" >
        </div>
    </div>

    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>
    </div>
</fieldset>

<?php echo form_close(); ?>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script>
$(document).ready(function() {
    $('.datepicker').datetimepicker({
        format: 'Y-m-d',
        lang: 'hu',
        timepicker: false,
        dateonly: true,
        scrollMonth: false,
        scrollYear: false,
        scrollInput: false
    });

    $('#articleImage').picEdit({
        maxWidth: 600,
        formSubmitted: function(response){
            $('div.form-actions').hide();
            setTimeout(function(){
                document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>';
            }, 1000);
        }
    });

    $('.dij').keyup(function(event) {

        if(event.which >= 37 && event.which <= 40) return;

        $(this).val(function(index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            ;
        });
    });
});
</script>

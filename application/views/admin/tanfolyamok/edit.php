


<h2>
    <?php echo $currentModul->name; ?> szerkesztése
</h2>



<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . '', $attributes);
?>

<fieldset>
    <table style="max-width: 1080px">
        <div style="display: table-row">
            <div style="display: table-cell; width: 400px; padding: 10px 10px 10px 20px;">
                <div class="control-group">
                    <label for="inputError" class="control-label">Képzés neve</label>
                    <div class="controls">
                        <input type="text" id="name_field" name="nev" tabindex="" value="<?php echo $row[0]['nev']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Képzés időtartama</label>
                    <div class="controls">
                        <input type="text" id="idotartam_elmelet" name="idotartam" tabindex="" value="<?php echo $row[0]['idotartam']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Kategória</label>
                    <div class="controls">
                        <?php echo form_dropdown('kategoria', ['Bármixer' => 'Bármixer', 'Pincér' => 'Pincér', 'Barista' => 'Barista'], $row[0]['kategoria']) ?>
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Upsell</label>
                    <div class="controls">
                        <?php echo form_dropdown('upsell_id', $upsell_items, $row[0]['upsell_id']) ?>
                    </div>
                </div>




                <div class="control-group">
                    <label for="inputError" class="control-label"></label>
                    <div class="controls">
                        <?php
                        echo form_checkbox('kiemelt', '1', $row[0]['kiemelt'] ? TRUE : FALSE, set_checkbox('kiemelt', 1)) . "<b>Kiemelt tanfolyam</b>";
                        ?>
                    </div>
                </div>
            </div>
            <div style="display: table-cell; width: 400px">

                <div class="control-group">
                    <label for="inputError" class="control-label">Ár</label>
                    <div class="controls">
                        <input type="text" class="dij" name="ar" tabindex="" value="<?php echo number_format($row[0]['ar'], 0, ",", "."); ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Akciós ár</label>
                    <div class="controls">
                        <input type="text" class="dij" name="akcios_ar" tabindex="" value="<?php echo number_format($row[0]['akcios_ar'], 0, ",", "."); ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Akció kezdete</label>
                    <div class="controls">
                        <input type="text" class="datepicker" id="" name="akcio_kezdete" tabindex="" value="<?php echo $row[0]['akcio_kezdete']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Akció vége</label>
                    <div class="controls">
                        <input type="text" class="datepicker" id="" name="akcio_vege" tabindex="" value="<?php echo $row[0]['akcio_vege']; ?>" >
                    </div>
                </div>

            </div>
        </div>
        <tr>
            <td>
                <div class="control-group">
                    <label for="inputError" class="control-label">Leírás</label>
                    <div class="controls">
                        <?php
                        $this->ckeditor->config['height'] = 500;
                        echo $this->ckeditor->editor('leiras', $row[0]['leiras']);
                        ?>
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Tematika</label>
                    <div class="controls">
                        <?php
                        $this->ckeditor->config['height'] = 500;
                        echo $this->ckeditor->editor('tematika', $row[0]['tematika']);
                        ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <div class="control-group">
        <label for="inputError" class="control-label">Videó</label>
        <div class="controls">
            <input type="text" name="video" tabindex="" value="<?php echo $row[0]['video']; ?>" >
        </div>
    </div>


    <?php if (isset($currentImgFullUrl)): ?>
        <div class="control-group">
            <label for="inputError" class="control-label">Jelenlegi kép</label>
            <div class="controls">
                <img alt="" width="400" src="<?php echo $currentImgFullUrl ?>" />
            </div>
        </div>
        <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
                <input type="checkbox" id="" name="thumbnail_remove" value="1" <?php echo set_checkbox('thumbnail_remove', '1'); ?> ><b>Kép törlése</b>
            </div>
        </div>
    <?php endif ?>



    <div class="control-group">
        <label for="inputError" class="control-label">Kép csere</label>
        <div class="controls">
            <input type="file" id="articleImage" name="articleImage">
        </div>
    </div>

    <?php if (isset($currentImgFullUrl)): ?>
        <div class="control-group">
            <label for="inputError" class="control-label">Crop előnézet</label>
            <div class="controls">
                <a href="" style="display: inline-block; width: 100%;max-width: 365px;height: 285px;background-size: cover; background-repeat: no-repeat; background-image:url('<?= $currentImgFullUrl ?>'); background-position: center <?php echo $row[0]['crop_percent']; ?>%" id="imagePreview"></a>
            </div>
        </div>

        <div class="control-group">
            <label for="inputError" class="control-label">Crop (Le-Fel billentyűkkel is állítható)</label>
            <div class="controls">
                <input type="text" id="crop_percent" name="crop_percent" autocomplete="off" value="<?php echo $row[0]['crop_percent']; ?>" >
            </div>
        </div>

        <script>
            var crop_p = parseInt($('#crop_percent').val(), 10);
            $('#crop_percent').keyup(function (e) {
                var crop_p = parseInt($('#crop_percent').val(), 10);
                if (e.keyCode == 38 && (crop_p + 5) <= 100) {
                    crop_p += 5;
                }
                if (e.keyCode == 40 && (crop_p - 5) >= 0) {
                    crop_p -= 5;
                }
                $("#imagePreview").css("background-position", "center " + crop_p + "%");
                $("#crop_percent").val(crop_p);
            });
        </script>
    <?php endif ?>

    <div class="form-actions">
        <button class="btn btn-primary" id="save_exit">Mentés</button>
        <button class="btn btn-primary" id="save">Alkalmaz</button>
    </div>
</fieldset>

<?php echo form_close(); ?>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script>
            $(document).ready(function () {
                $('.datepicker').datetimepicker({
                    format: 'Y-m-d',
                    lang: 'hu',
                    timepicker: false,
                    dateonly: true,
                    scrollMonth: false,
                    scrollYear: false,
                    scrollInput: false
                });

                $('#articleImage').picEdit({
                    maxWidth: 600,
                    /*formSubmitted: function (response) {
                     $('div.form-actions').hide();
                     setTimeout(function () {
                     document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>?>';
                     }, 1000);
                     }*/
                });

                $('.dij').keyup(function (event) {

                    if (event.which >= 37 && event.which <= 40)
                        return;

                    $(this).val(function (index, value) {
                        return value
                                .replace(/\D/g, "")
                                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                                ;
                    });
                });

                $('#save').click(function () {
                    $('form').submit();
                });

                $('#save_exit').click(function () {
                    $('form').submit();
                    $('div.form-actions').hide();
                    setTimeout(function () {
                        document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>';
                    }, 1000);
                });
            });
</script>

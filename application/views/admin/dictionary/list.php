


<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">



        <div style="padding:20px">

            <div class="well">

                <h3>Használat:</h3>
                Írj, vagy módosíts egy mezőben, majd nyomj TAB-ot, vagy ENTER-t, vagy kattints mellé. <br/>
                Ha a mezőben látható halvány szó megfelelő, nem szükséges kitölteni a mezőt. <br/>
                Miután az összes módosítást megcsináltad, nyomd meg a "Nyelvi fájlok generálása" gombot.

            </div>


            <table class="">
                <thead>
                    <tr>
                        <?php if (isset($allLanguages)): ?>
                            <?php foreach ($allLanguages as $lang): ?>
                                <th class=""><?php echo $lang->lang ?></th>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tr>
                </thead>
                <tbody>
                    <?php if (isset($lang_keys_pages)): ?>

                        <tr><td colspan="<?= count($allLanguages) ?>"> <br/><center><b>ALOLDALAK</b></center> </td></tr>

                    <?php foreach ($lang_keys_pages as $lang_key): ?>

                        <tr>

                            <?php if (isset($allLanguages)): ?>
                                <?php foreach ($allLanguages as $lang): ?>

                                    <td>
                                        <?php echo form_open('', ['class' => 'dictionaryForm', 'style' => 'display:inline;']); ?>
                                        <input type="text" name="word" value="<?php if (isset($all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey])) echo $all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey] ?>" placeholder="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="langKey" value="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="lang" value="<?= $lang->lang ?>" />
                                        <?php echo form_close(); ?>
                                    </td>
                                <?php endforeach ?>
                            <?php endif ?>

                        </tr>

                    <?php endforeach ?>
                <?php endif ?>



                <?php if (isset($lang_keys_galleries)): ?>

                    <tr><td colspan="<?= count($allLanguages) ?>"> <br/><br/><center><b>GALÉRIA CÍMEK</b></center> </td></tr>

                    <?php foreach ($lang_keys_galleries as $lang_key): ?>

                        <tr>

                            <?php if (isset($allLanguages)): ?>
                                <?php foreach ($allLanguages as $lang): ?>

                                    <td>
                                        <?php echo form_open('', ['class' => 'dictionaryForm', 'style' => 'display:inline;']); ?>
                                        <input type="text" name="word" value="<?php if (isset($all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey])) echo $all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey] ?>" placeholder="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="langKey" value="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="lang" value="<?= $lang->lang ?>" />
                                        <?php echo form_close(); ?>
                                    </td>
                                <?php endforeach ?>
                            <?php endif ?>

                        </tr>

                    <?php endforeach ?>
                <?php endif ?>


                <?php if (isset($lang_keys_videogalleries)): ?>

                    <tr><td colspan="<?= count($allLanguages) ?>"> <br/><br/><center><b>VIDEÓGALÉRIA CÍMEK</b></center> </td></tr>

                    <?php foreach ($lang_keys_videogalleries as $lang_key): ?>

                        <tr>

                            <?php if (isset($allLanguages)): ?>
                                <?php foreach ($allLanguages as $lang): ?>

                                    <td>
                                        <?php echo form_open('', ['class' => 'dictionaryForm', 'style' => 'display:inline;']); ?>
                                        <input type="text" name="word" value="<?php if (isset($all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey])) echo $all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey] ?>" placeholder="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="langKey" value="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="lang" value="<?= $lang->lang ?>" />
                                        <?php echo form_close(); ?>
                                    </td>
                                <?php endforeach ?>
                            <?php endif ?>

                        </tr>

                    <?php endforeach ?>
                <?php endif ?>


                <?php if (isset($lang_keys_prodcat)): ?>

                    <tr><td colspan="<?= count($allLanguages) ?>"> <br/><br/><center><b>TERMÉK KATEGÓRIÁK</b></center> </td></tr>

                    <?php foreach ($lang_keys_prodcat as $lang_key): ?>

                        <tr>

                            <?php if (isset($allLanguages)): ?>
                                <?php foreach ($allLanguages as $lang): ?>

                                    <td>
                                        <?php echo form_open('', ['class' => 'dictionaryForm', 'style' => 'display:inline;']); ?>
                                        <input type="text" name="word" value="<?php if (isset($all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey])) echo $all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey] ?>" placeholder="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="langKey" value="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="lang" value="<?= $lang->lang ?>" />
                                        <?php echo form_close(); ?>
                                    </td>
                                <?php endforeach ?>
                            <?php endif ?>

                        </tr>

                    <?php endforeach ?>
                <?php endif ?>


                <?php if (isset($lang_keys)): ?>

                    <tr><td colspan="<?= count($allLanguages) ?>"> <br/><br/><center><b>EGYÉB KIFEJEZÉSEK</b></center> </td></tr>

                    <?php foreach ($lang_keys as $lang_key): ?>

                        <tr>

                            <?php if (isset($allLanguages)): ?>
                                <?php foreach ($allLanguages as $lang): ?>

                                    <td>
                                        <?php echo form_open('', ['class' => 'dictionaryForm', 'style' => 'display:inline;']); ?>
                                        <input style="max-width:94%" type="text" name="word" value="<?php if (isset($all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey])) echo $all_dict_value_arr[$lang->lang . '_' . $lang_key->langKey] ?>" placeholder="<?= $lang_key->langKey ?>" title="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="langKey" value="<?= $lang_key->langKey ?>" />
                                        <input type="hidden" name="lang" value="<?= $lang->lang ?>" />
                                        <?php echo form_close(); ?>
                                    </td>
                                <?php endforeach ?>
                            <?php endif ?>

                        </tr>

                    <?php endforeach ?>
                <?php endif ?>
                </tbody>
            </table>

            <br/><br/>

            <?php echo form_open('', ['id' => 'genLanguageFilesForm']); ?>
            <input type="submit" class="btn btn-warning" name="genLanguageFiles" value="Nyelvi fájlok generálása" />
            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<br/>

<?php if ($this->input->get('addMode')): ?>

    <?php echo form_open('/admin/dictionary?addMode=1', ['id' => '']); ?>
    <input type="text" name="langKey" />
    <input type="submit" class="btn btn-primary" name="addLangKey" value="Add LangKey" />
    <?php echo form_close(); ?>

    <?php if ($this->input->post('addLangKey')): ?>

        <script>
            $(document).ready(function () {
                $('input[name=langKey]').focus();
            });
        </script>

    <?php endif; ?>

<?php endif; ?>

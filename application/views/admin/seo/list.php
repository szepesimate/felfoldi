


<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">







        <div style="padding:20px">

            <h3>Használat:</h3>
            Módosíts egy mezőben, majd TAB, vagy ENTER lenyomásával, vagy mellé kattintással a változtatás mentve lesz.



            <?php
            echo form_open('admin/' . $this->uri->segment(2), ['style' => 'margin:20px 0 0 0']);


            echo form_dropdown('lang', $langOptions);

            $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Mehet');
            echo form_submit($data_submit);

            echo form_close();
            ?>

        </div>



        <br/><br/>


        <table class="">
            <caption><b>ALOLDALAK</b></caption>
            <thead>
                <tr>
                    <th class="sortable ">Név</th>
                    <th class="sortable ">Title</th>
                    <th class="sortable ">Description</th>
                    <th class="sortable ">URL Friendly</th>
                </tr>
            </thead>
            <tbody>

                <?php
                if ($allPages) {
                    foreach ($allPages as $row) {
                        ?>
                        <tr>
                            <td><?= $row->name ?></td>
                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['page_' . $row->id])) echo $all_seo_value_arr['page_' . $row->id]["title"] ?>" placeholder="Title" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="title" />
                                <input type="hidden" name="itemType" value="page" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>

                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['page_' . $row->id])) echo $all_seo_value_arr['page_' . $row->id]["description"] ?>" placeholder="Description" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="description" />
                                <input type="hidden" name="itemType" value="page" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>

                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['page_' . $row->id])) echo $all_seo_value_arr['page_' . $row->id]["urlFriendly"] ?>" placeholder="URL Friendly" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="urlFriendly" />
                                <input type="hidden" name="itemType" value="page" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>


                        </tr>

                        <?php
                    }
                }
                ?>

            </tbody>
        </table>


        <br/><br/>

        <table class="">
            <caption><b>CIKKEK</b></caption>
            <thead>
                <tr>
                    <th class="sortable ">Cikk cím</th>
                    <th class="sortable ">Title</th>
                    <th class="sortable ">Description</th>
                </tr>
            </thead>
            <tbody>


                <?php
                if ($allArticles) {
                    foreach ($allArticles as $row) {
                        ?>


                        <tr>
                            <td><?= $row->title ?></td>
                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['article_' . $row->id])) echo $all_seo_value_arr['article_' . $row->id]["title"] ?>" placeholder="Title" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="title" />
                                <input type="hidden" name="itemType" value="article" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>

                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['article_' . $row->id])) echo $all_seo_value_arr['article_' . $row->id]["description"] ?>" placeholder="Description" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="description" />
                                <input type="hidden" name="itemType" value="article" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>




                        </tr>

                        <?php
                    }
                }
                ?>



            </tbody>
        </table>




        <br/><br/>

        <table class="">
            <caption><b>TERMÉK KATEGÓRIÁK</b></caption>
            <thead>
                <tr>
                    <th class="sortable">Kategória</th>
                    <th class="sortable">Title</th>
                    <th class="sortable">Description</th>
                    <th class="sortable">URL Friendly</th>
                </tr>
            </thead>
            <tbody>


                <?php
                if ($allProductCats) {
                    foreach ($allProductCats as $row) {
                        ?>


                        <tr>
                            <td><?= $row->name ?></td>
                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['category_' . $row->id])) echo $all_seo_value_arr['category_' . $row->id]["title"] ?>" placeholder="Title" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="title" />
                                <input type="hidden" name="itemType" value="category" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>

                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['category_' . $row->id])) echo $all_seo_value_arr['category_' . $row->id]["description"] ?>" placeholder="Description" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="description" />
                                <input type="hidden" name="itemType" value="category" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>
                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['category_' . $row->id])) echo $all_seo_value_arr['category_' . $row->id]["urlFriendly"] ?>" placeholder="URL Friendly" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="urlFriendly" />
                                <input type="hidden" name="itemType" value="category" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>



                        </tr>

                        <?php
                    }
                }
                ?>


            </tbody>
        </table>

        <br/><br/>

        <table class="">
            <caption><b>TERMÉKEK</b></caption>
            <thead>
                <tr>
                    <th class="sortable">Kategória</th>
                    <th class="sortable">Title</th>
                    <th class="sortable">Description</th>
                    <th class="sortable">URL Friendly</th>
                </tr>
            </thead>
            <tbody>


                <?php
                if ($allProducts) {
                    foreach ($allProducts as $row) {
                        ?>


                        <tr>
                            <td><?= $row->name ?></td>
                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['product_' . $row->id])) echo $all_seo_value_arr['product_' . $row->id]["title"] ?>" placeholder="Title" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="title" />
                                <input type="hidden" name="itemType" value="product" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>

                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['product_' . $row->id])) echo $all_seo_value_arr['product_' . $row->id]["description"] ?>" placeholder="Description" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="description" />
                                <input type="hidden" name="itemType" value="product" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>
                            <td>
                                <?php echo form_open('', ['class' => 'seoForm', 'style' => 'display:inline;']); ?>
                                <input type="text" name="value" value="<?php if (isset($all_seo_value_arr['product_' . $row->id])) echo $all_seo_value_arr['product_' . $row->id]["urlFriendly"] ?>" placeholder="URL Friendly" />
                                <input type="hidden" name="lang" value="<?= $lang ?>" />
                                <input type="hidden" name="fieldName" value="urlFriendly" />
                                <input type="hidden" name="itemType" value="product" />
                                <input type="hidden" name="itemId" value="<?= $row->id ?>" />
                                <?php echo form_close(); ?>
                            </td>



                        </tr>

                        <?php
                    }
                }
                ?>


            </tbody>
        </table>



        <?php echo '<div class="pagination">' . $this->pagination->create_links() . '</div>'; ?>


    </div>
</div>

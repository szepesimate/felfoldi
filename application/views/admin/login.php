<!DOCTYPE HTML>
<html lang="hu">


<head>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
	<meta charset="UTF-8" />
	<title>Bejelentkezés</title>

	<meta name="robots" content="noindex, nofollow" />

	<link href="<?php echo base_url()?>assets/css/admin/reset.css" rel="stylesheet" />
	<link href="<?php echo base_url()?>assets/css/admin/styles.css" rel="stylesheet" />

	<script src="<?php echo base_url()?>assets/js/admin/jquery.min.js"></script>
	<script src="<?php echo base_url()?>js/admin/jquery.easing.min.js"></script>

	<script type="text/javascript">
		$(function() {

			$('input.autofocus').focus();

			// login oldal vertikálisan középre igazítás
			$('#login').css({
				'margin-top': ($(window).height() - $('#login').height()) / 2
			});
			$(window).resize(function() {
				$('#login').css({
					'margin-top': ($(window).height() - $('#login').height()) / 2
				});
			});

		});
	</script>
</head>

  <body>



	<div id="content">
		<div id="login" class="container">


			

			<div class="box">

				<div class="header">
					<h2>Bejelentkezés az admin felületre</h2>
				</div><!-- end header -->

				<div class="content">
					<div class="tabs">


						<div class="tab">



      <?php
      $attributes = array('class' => 'form');
      echo form_open('admin/login/validate_credentials', $attributes);
      echo '<p class="field">';
      echo '<label>Felhasználónév</label>';
      echo form_input('user_name', '', 'class="large autofocus"');
      echo '</p>';
      echo '<p class="field">';
      echo '<label>Jelszó</label>';
      echo form_password('password', '', 'class="large"');
      echo '</p>';

      if(isset($message_error) && $message_error){
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo 'Hibás felhasználónév vagy jelszó.';
          echo '</div>';
      }


      echo '<p class="field">';
      echo '<button type="submit">Bejelentkezés</button>';

      echo '</p>';
      echo form_close();
      ?>



                        </div><!-- end tab -->


					</div><!-- end tabs -->
				</div><!-- end content -->

			</div><!-- end box -->


		</div><!-- end login container -->
	</div><!-- end content -->

  </body>
</html>




<h2>
    <?php echo $currentModul->name; ?> szerkesztése
</h2>



<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . '', $attributes);
?>
<fieldset>

    <div class="control-group">
        <label for="inputError" class="control-label">Megnevezés</label>
        <div class="controls">
            <input type="text" id="" name="name" value="<?php echo $row[0]['name']; ?>" >
        </div>
    </div>


    <div class="control-group">
        <label for="inputError" class="control-label">Leírás</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('description', $row[0]['description']); ?>
        </div>
    </div>




    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>
        <button class="btn" type="reset">Mezők visszaállítása</button>
    </div>
</fieldset>

<?php echo form_close(); ?>




<div class="page-header">
    <h2>Kép feltöltése</h2>
    <a name="images"></a>
</div>

<?php echo form_open_multipart('admin/' . $this->uri->segment(2) . '/addImage/' . $this->uri->segment(4), array('id' => 'imageUploadForm', 'class' => 'form-horizontal'/* , 'target' => 'upload_target' */)); ?>

<fieldset>


    <div class="control-group">
        <label for="inputError" class="control-label">Kép címe</label>
        <div class="controls">
            <input type="text" id="" name="title" >
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Kép</label>
        <div class="controls">
            <input type="file" id="sliderImg" name="sliderImg">
        </div>
    </div>


    <?php
    $data = array(
        'gallery_id' => $row[0]['id']
    );

    echo form_hidden($data);
    ?>

    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Feltöltés</button>
    </div>


</fieldset>


<?php echo form_close(); ?>




<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('#sliderImg').picEdit({
            maxWidth: 600,
            formSubmitted: function (response) {
                $('div.form-actions').hide();
                setTimeout(function () {
                    document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . ''; ?>';
                }, 2000);
            }
        });
    });
</script>



<div class="page-header">
    <h2>Tömeges képfeltöltés</h2>
    <a name="images"></a>
</div>

<?php echo form_open_multipart('admin/' . $this->uri->segment(2) . '/addImages/' . $this->uri->segment(4)); ?>

<fieldset>

    <div class="control-group">
        <div class="controls">
            <input type="file" name="images[]" multiple>
        </div>
    </div>


    <?php
    $data = array(
        'gallery_id' => $row[0]['id']
    );

    echo form_hidden($data);
    ?>

    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Feltöltés</button>
    </div>


</fieldset>


<?php echo form_close(); ?>





<div id="imageHolder" style="float:left;width:100%;">
    <?php if ($images): ?>
        <ul id="sortable_grid" style="width:700px">
            <?php foreach ($images as $image): ?>
                <li id="listItem_<?php echo $image->id ?>" class="ui-state-default" style="border:none;height:200px;background:transparent;width:220px;">
                    <div style="position:relative;">
                        <img width="200" alt="" src="<?php echo base_url() . "assets/images/gallery/thumb/" . $image->filename ?>" />
                        <img src="<?php echo base_url() ?>assets/images/admin/remove.png" style="cursor:pointer;position:absolute;top:-10px;right:0px;" onclick="if (window.confirm('Biztosan törli ezt az elemet?'))
                                            document.location = '<?php echo base_url() . 'admin/' . $this->uri->segment(2) . '/deleteImage/' . $this->uri->segment(4) . '/' . $image->id ?>';" />
                        <br style="clear:both"/>




                    </div>


                    <?php echo form_open('', ['class' => 'pencilForm', 'onclick' => "$(this).find('input[type=text]').focus()"]); ?>
                    <input type="text" name="value" value="<?php echo $image->title ?>" style="width:120px" />
                    <input type="hidden" name="fieldName" value="title" />
                    <input type="hidden" name="tableName" value="gallery_images" />
                    <input type="hidden" name="id" value="<?= $image->id ?>" />
                    <?php echo form_close(); ?>


                </li>
            <?php endforeach ?>
        </ul>
        <script>
            $(function () {
                $("#sortable_grid").sortable({
                    update: function () {
                        $.ajax({
                            url: base_url + 'admin/' + uri_segment2 + '/reorderImages',
                            data: $('#sortable_grid').sortable('serialize'),
                            type: 'POST',
                            beforeSend: function () {
                                $('#sortable_grid').css('opacity', 0.5);
                            },
                            success: function (result) {
                                if (result != 'success')
                                    alert("Error. Reload this page please.");
                                $('#sortable_grid').css('opacity', 1);
                            }
                        });
                    }
                });
                $("#sortable_grid").disableSelection();
            });
        </script>
    <?php endif ?>
</div>



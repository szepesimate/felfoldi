
        <h2>
          <?php echo $currentModul->name;?> felvitele
        </h2>


      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> new element created with success.';
          echo '</div>';
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';
        }
      }
      ?>

      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');



      //form validation
      echo validation_errors();

      echo form_open('admin/'.$this->uri->segment(2).'/add', $attributes);
      ?>
        <fieldset>

          <div class="control-group">
            <label for="inputError" class="control-label">Megnevezés</label>
            <div class="controls">
              <input type="text" id="" name="name" value="<?php echo set_value('name'); ?>" >
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Leírás</label>
            <div class="controls">
              <?php echo $this->ckeditor->editor('description', html_entity_decode(set_value('description')));?>
            </div>
          </div>






          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Mentés</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>




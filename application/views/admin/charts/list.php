<html>
    <head>
        <!--Load the AJAX API-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">

// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.

<?php
$colors = array( '#deebf7', '#c6dbef', '#9ecae1', '#6baed6', '#4292c6', '#2171b5', '#08519c', '#08306b');
?>

function drawChart() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([
        ['Tanfolyam neve', 'Jelentkezők', { role: "style" }],
        <?php if(isset($chart_data) && !empty($chart_data)): ?>
        <?php foreach($chart_data as $data): ?>
        ['<?= $data->tanfolyam_neve ?>', <?= $data->counter ?>, '<?= $colors[mt_rand(0, count($colors) - 1)]; ?>'],
        <?php endforeach ?>
        <?php endif ?>
    ]);

    // Set chart options
    var options = {'width':1200,
                   'height':300,
                   'xAxis': {maxValue: 10, viewWindow:{min:0}, format: '0'},
                   'bar': {groupWidth: "35%"},
                   'backgroundColor': '#F0F0F0',
                   'fontSize': 12,
                   vAxis: {
                       textStyle:{color: 'gray'}
                   },
                   hAxis: {
                       textStyle:{color: 'gray'}
                   },
                   chartArea: {  width: "55%", height: "70%" }
                  };

    // Instantiate and draw our chart, passing in some options.
    <?php if(isset($chart_data) && !empty($chart_data)): ?>
    var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
    chart.draw(data, options);
    <?php endif ?>
}
        </script>
    </head>

    <body>
        <!--Div that will hold the pie chart-->

        <h2>Jelentkezési statisztikák</h2>
        <p>Itt látható az elmúlt 2 hét jelentkezési statisztikája. Ha egyéb időintervallum jelentkezéseit szeretnéd látni, használd a dátumkijelölőt.</p>

        <section>
            <div id="chart_div">
                <?php if($chart_data == ""): ?>
                <p style="margin-top: 50px;">Nem volt jelentkező az elmúlt két hétben.</p>
                <?php endif ?>
            </div>
        </section>

    </body>
</html>



<?php /* <button style="margin-left:0" onclick="document.location='<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add'">Új hozzáadása</button>

<br/><br/>

<div class="box">

    <div class="header">
	    <h2><?php echo $currentModul->name;?></h2>
    </div><!-- end header -->

	<!-- Content -->
    <div class="content clearfix">

        <div class="tabs">


            <ul class="navigation clearfix">
                <?php if ($page_objects){?>
                    <?php foreach ($page_objects as $page_object):?>
                        <li><a href="#tab<?=$page_object->id?>"<?php if ($page_object == $page_objects[0]) echo ' class="current"';?>><?=$page_object->name?></a></li>
                    <?php endforeach?>
                    <li><a href="#tab_other">Egyéb tartalmak</a></li>
                <?php }?>
            </ul><!-- end navigation -->

            <?php
                $other_object = new stdClass();
                $other_object->id = '_other';
                $page_objects[]= $other_object;

                $displayed = [];
?>

            <?php if ($page_objects){?>
                <?php foreach ($page_objects as $page_object):?>

                    <div class="tab" id="tab<?=$page_object->id?>">

                      <table class="datatable sortable">
                        <thead>
                          <tr>
                            <th class="sorting_asc">Cím</th>
                            <th class="sorting">Kategória</th>
                            <th class="sorting">Nyelv</th>
                            <th class="sorting">Dátum</th>
                            <th class="sorting">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($rows as $row){
                            if ( $page_object->id != '_other' && $row['parent'] != $page_object->id){

                                $parent_page_object = $this->utils->getFirstObject('pages', ['id'=>$row['parent']]);
                                if ($parent_page_object->parent != $page_object->id) continue;
                            }

                            if ($page_object->id == '_other' && in_array($row['id'],$displayed)) continue;

                            $displayed[] = $row['id'];

                            $statusClass = $row['status'] ? 'lathatosag' : 'lathatosag2';
                            echo '<tr>';
                            echo '<td>'.$row['title'].'</td>';
                            echo '<td>'.$categoryOptions[$row['parent']].'</td>';
                            echo '<td>'.$row['lang'].'</td>';
                            echo '<td>'.$row['startDate'].'</td>';

                            echo '<td>
                              <a href="'.site_url("admin").'/'.$this->uri->segment(2).'/update/'.$row['id'].'" class="icon szerkesztes">szerkesztés</a>
                              <a style="position:relative;top:5px" class="switch '.$statusClass.' icon" rev="status" href="javascript:void(0)" rel="'.$row['status'].'" name="'.$row['id'].'"></a>
                              <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \''.site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row['id'].'\'" class="icon torles">törlés</a>
                            </td>';
                            echo '</tr>';
                        }
?>
                        </tbody>
                      </table>

                    </div>

                <?php endforeach?>

            <?php }?>

        </div>

    </div><!-- end content -->

</div><!-- end box -->

          <?php
            //echo '<div class="pagination">'.$this->pagination->create_links().'</div>';
?>
*/ ?>

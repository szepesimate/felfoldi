<div class="container top">

    <div class="page-header users-header">
        <h2>
            <?php echo $currentModul->name; ?>
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

            <div class="well">

                <?php
                $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');


                //save the columns names in a array that we will use as filter
                $options_table = array();
                foreach ($rows as $array) {
                    foreach ($array as $key => $value) {
                        $options_table[$key] = $key;
                    }
                    break;
                }
                ?>

            </div>



            <table class="table table-striped table-bordered table-condensed">
                <thead>
                    <tr>
                        <th class="yellow header headerSortDown">E-mail</th>
                        <th class="yellow header headerSortDown">Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($rows as $row) {
                        echo '<tr>';
                        echo '<td>' . $row["email"] . '</td>';
                        echo '<td>' . $row["name"] . '</td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>

            <br/><br/>

            <form action="" method="post" target="_blank">
                <button type="submit" name="csv_gen_">Exportálás</button>
                <input type="hidden" name="csv_gen" value="1" />
            </form>

            <?php /* echo '<div class="pagination">' . $this->pagination->create_links() . '</div>'; */ ?>

        </div>
    </div>

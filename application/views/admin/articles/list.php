








<button style="margin-left:0" onclick="document.location='<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add'">Új hozzáadása</button>

<br/><br/>

<div class="box">

    <div class="header">
	    <h2><?php echo $currentModul->name;?></h2>
    </div><!-- end header -->

	<!-- Content -->
    <div class="content clearfix">

        <div class="tabs">


            <ul class="navigation clearfix">
                <?php if ($page_objects){?>
                    <?php foreach ($page_objects as $page_object):?>
                        <li><a href="#tab<?=$page_object->id?>"<?php if ($page_object == $page_objects[0]) echo ' class="current"';?>><?=$page_object->name?></a></li>
                    <?php endforeach?>
                    <li><a href="#tab_other">Egyéb tartalmak</a></li>
                <?php }?>
            </ul><!-- end navigation -->

            <?php
                $other_object = new stdClass();
                $other_object->id = '_other';
                $page_objects[]= $other_object;

                $displayed = [];
            ?>

            <?php if ($page_objects){?>
                <?php foreach ($page_objects as $page_object):?>

                    <div class="tab" id="tab<?=$page_object->id?>">

                      <table class="datatable sortable">
                        <thead>
                          <tr>
                            <th class="sorting_asc">Cím</th>
                            <th class="sorting">Kategória</th>
                            <th class="sorting">Nyelv</th>
                            <th class="sorting">Dátum</th>
                            <th class="sorting">&nbsp;</th>
                            <th class="sorting">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($rows as $row){
                            if ( $page_object->id != '_other' && $row['parent'] != $page_object->id){

                                $parent_page_object = $this->utils->getFirstObject('pages', ['id'=>$row['parent']]);
                                if ($parent_page_object->parent != $page_object->id) continue;
                            }

                            if ($page_object->id == '_other' && in_array($row['id'],$displayed)) continue;

                            $displayed[] = $row['id'];

                            $statusClass = $row['status'] ? 'lathatosag' : 'lathatosag2';
                            echo '<tr>';
                            echo '<td>'.$row['title'].'</td>';
                            echo '<td>'.$categoryOptions[$row['parent']].'</td>';
                            echo '<td>'.$row['lang'].'</td>';
                            echo '<td>'.$row['startDate'].'</td>';
                            
                            echo '<td><a href="'.site_url("admin").'/'.$this->uri->segment(2).'/duplicate/'.$row['id'].'/'.$page_object->id.'">Duplikálás</a></td>';

                            echo '<td>
                              <a href="'.site_url("admin").'/'.$this->uri->segment(2).'/update/'.$row['id'].'" class="icon szerkesztes">szerkesztés</a>
                              <a style="position:relative;top:5px" class="switch '.$statusClass.' icon" rev="status" href="javascript:void(0)" rel="'.$row['status'].'" name="'.$row['id'].'"></a>
                              <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \''.site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row['id'].'\'" class="icon torles">törlés</a>
                            </td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                      </table>

                    </div>

                <?php endforeach?>

            <?php }?>

        </div>

    </div><!-- end content -->

</div><!-- end box -->

          <?php
            //echo '<div class="pagination">'.$this->pagination->create_links().'</div>';
            ?>


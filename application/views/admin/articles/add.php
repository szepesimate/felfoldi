

<h2>
    <?php echo $currentModul->name; ?> felvitele
</h2>



<?php
//flash messages
if (isset($flash_message)) {
    if ($flash_message == TRUE) {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> new element created with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/add', $attributes);
?>
<fieldset>

    <div style="" class="control-group">
        <label for="inputError" class="control-label">Nyelv</label>
        <div class="controls">
            <?php echo form_dropdown('lang', $langOptions, set_value('lang')); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Cím</label>
        <div class="controls">
            <input type="text" id="title_field" name="title" value="<?php echo set_value('title'); ?>" >
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Aloldal</label>
        <div class="controls">
            <?php echo form_dropdown('parent', $categoryOptions, set_value('parent')); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Megjelenés dátuma</label>
        <div class="controls">
            <input type="text" id="startDate_field" name="startDate" value="<?php echo set_value('startDate', date('Y-m-d H:i:s')); ?>" >
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">SEO Title</label>
        <div class="controls">
            <input type="text" id="seo_title_field" name="seo_title" value="<?php echo set_value('seo_title'); ?>" >
        </div>
    </div>


    <div class="control-group">
        <label for="inputError" class="control-label">SEO Description</label>
        <div class="controls">
            <input type="text" id="" name="seo_description" value="<?php echo set_value('seo_description'); ?>" >
        </div>
    </div>

    <div class="control-group" style="">
        <label for="inputError" class="control-label">Tulajdonság 1</label>
        <div class="controls">
            <input type="text" id="" name="property1" value="<?php echo set_value('property1'); ?>" >
        </div>
    </div>

    <div class="control-group" style="display:none">
        <label for="inputError" class="control-label">Tulajdonság 2</label>
        <div class="controls">
            <input type="text" id="" name="property2" value="<?php echo set_value('property2'); ?>" >
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Előzetes</label>
        <div class="controls">
            <?php echo $this->ckeditor->editor('preview', html_entity_decode(set_value('preview'))); ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Tartalom</label>
        <div class="controls">
            <?php
            $this->ckeditor->config['height'] = 500;
            echo $this->ckeditor->editor('content', html_entity_decode(set_value('content')));
            ?>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Kép</label>
        <div class="controls">
            <input type="file" id="articleImage" name="articleImage">
        </div>
    </div>



    <script>

        var availableTags = [
<?php if (isset($allCategories)): ?>
    <?php foreach ($allCategories as $category): ?>
                {
                value: "<?php echo $category ?>",
                },
    <?php endforeach ?>
<?php endif ?>
        ];
        $(document).ready(function(){

        $("#categories")
                // don't navigate away from the field on tab when selecting an item
                .bind("keydown", function(event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
                }
                })
                .autocomplete({
                minLength: 0,
                        source: function(request, response) {
                        // delegate back to autocomplete, but extract the last term
                        response($.ui.autocomplete.filter(
                                availableTags, extractLast(request.term)));
                        },
                        focus: function() {
                        // prevent value inserted on focus
                        return false;
                        },
                        select: function(event, ui) {
                        var terms = split(this.value);
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(ui.item.value);
                        // add placeholder to get the comma-and-space at the end
                        terms.push("");
                        this.value = terms.join(", ");
                        return false;
                        }
                });
        });
    </script>

    <div class="control-group">
        <label for="inputError" class="control-label">Kategória</label>
        <div class="controls">
            <input id="categories" style="width:780px;font-size:12px;" name="categories" value="">
        </div>
    </div>





    <script>

        function split(val) {
        return val.split(/,\s*/);
        }
        function extractLast(term) {
        return split(term).pop();
        }


        var availableTags_cimkek = [
<?php if (isset($allTags)): ?>
    <?php foreach ($allTags as $tag): ?>
                {
                value: "<?php echo $tag ?>",
                },
    <?php endforeach ?>
<?php endif ?>
        ];
        $(document).ready(function(){

        $('#title_field').on('change', function(){
        if ($('#seo_title_field').val() == ''){
        $('#seo_title_field').val($('#title_field').val());
        }
        });
        /*$('select[name=categories]').change(function(){
         if ($(this).val() == '_new_'){
         var parent = $(this).parent();
         $(this).remove();
         parent.append('<input type="text" placeholder="Új kategória" name="categories" />');
         }
         });*/


        $('#startDate_field').datetimepicker({
        format:'Y-m-d H:i:s',
                lang:'hu'
        });
        $("#tags")
                // don't navigate away from the field on tab when selecting an item
                .bind("keydown", function(event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
                }
                })
                .autocomplete({
                minLength: 0,
                        source: function(request, response) {
                        // delegate back to autocomplete, but extract the last term
                        response($.ui.autocomplete.filter(
                                availableTags_cimkek, extractLast(request.term)));
                        },
                        focus: function() {
                        // prevent value inserted on focus
                        return false;
                        },
                        select: function(event, ui) {
                        var terms = split(this.value);
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(ui.item.value);
                        // add placeholder to get the comma-and-space at the end
                        terms.push("");
                        this.value = terms.join(", ");
                        return false;
                        }
                });
        });
    </script>

    <div class="control-group">
        <label for="inputError" class="control-label">Cimkék</label>
        <div class="controls">
            <input id="tags" style="width:780px;font-size:12px;" name="tags" value="">
        </div>
    </div>




    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>
    </div>
</fieldset>

<?php echo form_close(); ?>






<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script type="text/javascript">
        $(function() {
        $('#articleImage').picEdit({
        maxWidth: 600,
                formSubmitted: function(response){
                $('div.form-actions').hide();
                setTimeout(function(){
                    document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>#' + $('select[name=parent]').val();
                }, 1000);
                }
        });
        });
</script>




<h2>
    <?php echo $currentModul->name; ?> szerkesztése
</h2>



<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . '', $attributes);
?>


<div style="" class="control-group">
    <label for="inputError" class="control-label">Nyelv</label>
    <div class="controls">
        <?php echo form_dropdown('lang', $langOptions, $row[0]['lang']); ?>
    </div>
</div>

<div class="control-group">
    <label for="inputError" class="control-label">Cím</label>
    <div class="controls">
        <input type="text" id="" name="title" value="<?php echo $row[0]['title']; ?>">
    </div>
</div>

<div class="control-group">
    <label for="inputError" class="control-label">Aloldal</label>
    <div class="controls">
        <?php echo form_dropdown('parent', $categoryOptions, $row[0]['parent']); ?>
    </div>
</div>





<div class="control-group">
    <label for="inputError" class="control-label">Megjelenés dátuma</label>
    <div class="controls">
        <input type="text" id="startDate_field" name="startDate" value="<?php echo $row[0]['startDate']; ?>">
    </div>
</div>

<div class="control-group">
    <label for="inputError" class="control-label">SEO Title</label>
    <div class="controls">
        <input type="text" id="" name="seo_title" value="<?php echo $seo_title; ?>">
    </div>
</div>

<div class="control-group">
    <label for="inputError" class="control-label">SEO Description</label>
    <div class="controls">
        <input type="text" id="" name="seo_description" value="<?php echo $seo_description; ?>">
    </div>
</div>

<div class="control-group" style="">
    <label for="inputError" class="control-label">Tulajdonság 1</label>
    <div class="controls">
        <input type="text" id="" name="property1" value="<?php echo $row[0]['property1']; ?>">
    </div>
</div>

<div class="control-group" style="display:none">
    <label for="inputError" class="control-label">Tulajdonság 2</label>
    <div class="controls">
        <input type="text" id="" name="property2" value="<?php echo $row[0]['property2']; ?>">
    </div>
</div>

<div class="control-group">
    <label for="inputError" class="control-label">Előzetes</label>
    <div class="controls">
        <?php echo $this->ckeditor->editor('preview', $row[0]['preview']); ?>
    </div>
</div>
<div class="control-group">
    <label for="inputError" class="control-label">Tartalom</label>
    <div class="controls">
        <?php
        $this->ckeditor->config['height'] = 500;
        echo $this->ckeditor->editor('content', $row[0]['content']);
        ?>
    </div>
</div>

<?php if (isset($currentImgFullUrl)): ?>
    <div class="control-group">
        <label for="inputError" class="control-label">Jelenlegi kép</label>
        <div class="controls">
            <img alt="" width="400" src="<?php echo $currentImgFullUrl ?>" />
        </div>
    </div>
    <div class="control-group">
        <label for="inputError" class="control-label"></label>
        <div class="controls">
            <input type="checkbox" id="" name="thumbnail_remove" value="1" <?php echo set_checkbox('thumbnail_remove', '1'); ?> ><b>Kép törlése</b>
        </div>
    </div>
<?php endif ?>



<div class="control-group">
    <label for="inputError" class="control-label">Kép csere</label>
    <div class="controls">
        <input type="file" id="articleImage" name="articleImage">
    </div>
</div>

<?php if (false && isset($currentImgFullUrl)): ?>
    <div class="control-group">
        <label for="inputError" class="control-label">Crop előnézet</label>
        <div class="controls">
            <a href="" style="display: inline-block; width: 100%;max-width: 420px;height: 280px;background-size: cover; background-repeat: no-repeat; background-image:url('<?= $currentImgFullUrl ?>'); background-position: center <?php echo $row[0]['crop_percent']; ?>%" id="imagePreview"></a>
        </div>
    </div>

    <div class="control-group">
        <label for="inputError" class="control-label">Crop (Le-Fel billentyűkkel is állítható)</label>
        <div class="controls">
            <input type="text" id="crop_percent" name="crop_percent" autocomplete="off" value="<?php echo $row[0]['crop_percent']; ?>" >
        </div>
    </div>

    <script>
        var crop_p = parseInt($('#crop_percent').val(), 10);
        $('#crop_percent').keyup(function(e) {
        var crop_p = parseInt($('#crop_percent').val(), 10);
        if (e.keyCode == 38 && (crop_p + 5) <= 100) {
        crop_p += 5;
        }
        if (e.keyCode == 40 && (crop_p - 5) >= 0) {
        crop_p -= 5;
        }
        $("#imagePreview").css("background-position", "center " + crop_p + "%");
        $("#crop_percent").val(crop_p);
        });</script>
<?php endif ?>


<script>
    

    function split(val) {
    return val.split(/,\s*/);
    }
    function extractLast(term) {
    return split(term).pop();
    }

    var availableTags = [
<?php if (isset($allCategories)): ?>
    <?php foreach ($allCategories as $category): ?>
            {
            value: "<?php echo $category ?>",
            },
    <?php endforeach ?>
<?php endif ?>
    ];
    $(document).ready(function(){

    $("#categories")
            // don't navigate away from the field on tab when selecting an item
            .bind("keydown", function(event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
            }
            })
            .autocomplete({
            minLength: 0,
                    source: function(request, response) {
                    // delegate back to autocomplete, but extract the last term
                    response($.ui.autocomplete.filter(
                            availableTags, extractLast(request.term)));
                    },
                    focus: function() {
                    // prevent value inserted on focus
                    return false;
                    },
                    select: function(event, ui) {
                    var terms = split(this.value);
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push(ui.item.value);
                    // add placeholder to get the comma-and-space at the end
                    terms.push("");
                    this.value = terms.join(", ");
                    return false;
                    }
            });
    });</script>

<div class="control-group">
    <label for="inputError" class="control-label">Kategória</label>
    <div class="controls">
        <input id="categories" style="width:780px;font-size:12px;" name="categories" value="<?php echo $row[0]['categories'] ?>">
    </div>
</div>



<script>

    var availableTags_cimkek = [
<?php if (isset($allTags)): ?>
    <?php foreach ($allTags as $tag): ?>
            {
            value: "<?php echo $tag ?>",
            },
    <?php endforeach ?>
<?php endif ?>
    ];
    $(document).ready(function(){

    /*
     $('select[name=categories]').change(function(){
     if ($(this).val() == '_new_'){
     var parent = $(this).parent();
     $(this).remove();
     parent.append('<input type="text" placeholder="Új kategória" name="categories" />');
     }
     });
     */
    $('#startDate_field').datetimepicker({
    format:'Y-m-d H:i:s',
            lang:'hu'
    });
    $("#tags")
            // don't navigate away from the field on tab when selecting an item
            .bind("keydown", function(event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
            }
            })
            .autocomplete({
            minLength: 0,
                    source: function(request, response) {
                    // delegate back to autocomplete, but extract the last term
                    response($.ui.autocomplete.filter(
                            availableTags_cimkek, extractLast(request.term)));
                    },
                    focus: function() {
                    // prevent value inserted on focus
                    return false;
                    },
                    select: function(event, ui) {
                    var terms = split(this.value);
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push(ui.item.value);
                    // add placeholder to get the comma-and-space at the end
                    terms.push("");
                    this.value = terms.join(", ");
                    return false;
                    }
            });
    });</script>

<div class="control-group">
    <label for="inputError" class="control-label">Cimkék</label>
    <div class="controls">
        <input id="tags" style="width:780px;font-size:12px;" name="tags" value="<?php echo $row[0]['tags'] ?>">
    </div>
</div> 





<div class="form-actions">
    <button class="btn btn-primary" id="save_exit">Mentés</button>
    <button class="btn btn-primary" id="save">Alkalmaz</button>
</div>


<?php echo form_close(); ?>










<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script>
<script type="text/javascript">
    $(function() {
    $('#articleImage').picEdit({
    maxWidth: 600,
            /*formSubmitted: function(response){
             $('div.form-actions').hide();
             setTimeout(function(){
             document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>#<?php echo $row[0]['parent'] ?>';
             }, 1000);
             }*/
    });
    $('#save').click(function() {
    $('form').submit();
    });
    $('#save_exit').click(function() {
    $('form').submit();
    $('div.form-actions').hide();
    setTimeout(function(){
    document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>#<?php echo $row[0]['parent'] ?>';
        }, 1000);
        });
        });
</script>

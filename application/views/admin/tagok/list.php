
<div class="">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <div class="row">
        <div class="span12 columns">

            <div class="well">

                <?php
                $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');


                //save the columns names in a array that we will use as filter
                $options_table = array();
                foreach ($rows as $array) {
                    foreach ($array as $key => $value) {
                        $options_table[$key] = $key;
                    }
                    break;
                }
                ?>
            </div>

            <br><br>

            <table class="table table-striped table-bordered table-condensed">
                <thead>
                    <tr>
                        <th class="yellow header headerSortDown">Név</th>
                        <th class="red header">E-mail</th>
                        <th class="red header">Telefon</th>
                        
                        <th class="red header">Regisztrált</th>
                        <th class="red header">Legutóbb belépett</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($rows as $row) {
                        echo '<tr>';
                        echo '<td>' . $row["name"] . '</td>';
                        echo '<td>' . $row["email"] . '</td>';
                        echo '<td>' . $row["phone"] . '</td>';
                        //echo '<td>' . ($row["activated"] ? "Igen" : "Nem") . '</td>';
                        echo '<td>' . date('Y.m.d H:i:s', strtotime($row["reg_date"])) . '</td>';
                        echo '<td>' . date('Y.m.d H:i:s', strtotime($row["last_login_date"])) . '</td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>

<?php echo '<div class="pagination">' . $this->pagination->create_links() . '</div>'; ?>

        </div>
    </div>

    <div class="container top">

      <ul class="breadcrumb">
        <li>
            <?php echo ucfirst($this->uri->segment(1));?>
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo $currentModul->name;?>
          </a>
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">Szerkesztés</a>
        </li>
      </ul>

      <div class="page-header">
        <h2>
          <?php echo $currentModul->name;?> szerkesztése
        </h2>
      </div>


      <?php
      //flash messages
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == 'updated')
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> element updated with success.';
          echo '</div>';
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';
        }
      }
      ?>

      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');



      //form validation
      echo validation_errors();

      echo form_open('admin/'.$this->uri->segment(2).'/update/'.$this->uri->segment(4).'', $attributes);
      ?>
        <fieldset>
          <div class="control-group">
            <label for="inputError" class="control-label">Értékelés</label>
            <div class="controls">
              <textarea cols="" style="width: 400px" rows="5" name="comment"><?php echo $row[0]['comment']; ?></textarea>
            </div>
          </div>




          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Mentés</button>
            <button class="btn" type="reset">Mezők visszaállítása</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>






    </div>

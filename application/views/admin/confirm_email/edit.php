


<h2 style="margin-bottom: 50px;">
    <?php echo $row[0]['title']; ?>
</h2>



<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();

echo form_open('admin/' . $this->uri->segment(2) . '/update/' . $this->uri->segment(4) . '', $attributes);
?>
<section>

    <div class="control-group">
        <label for="inputError" class="control-label">Email tartalma</label>
        <div class="controls">
            <?php
            $this->ckeditor->config['height'] = 500;
            echo $this->ckeditor->editor('content', $row[0]['content']);
            ?>
        </div>
    </div>


    <div class="form-actions">
        <button class="btn btn-primary" type="submit">Mentés</button>

    </div>

</section>
<?php echo form_close(); ?>

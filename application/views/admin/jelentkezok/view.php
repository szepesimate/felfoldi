


<h2>
    Jelentkezés adatai
</h2>



<?php
//flash messages
if ($this->session->flashdata('flash_message')) {
    if ($this->session->flashdata('flash_message') == 'updated') {
        echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> element updated with success.';
        echo '</div>';
    } else {
        echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
        echo '</div>';
    }
}
?>

<?php
//form data
$attributes = array('class' => 'form-horizontal', 'id' => '');



//form validation
echo validation_errors();
?>

<fieldset>
    <table style="max-width: 1080px">
        <div style="display: table-row">
            <div style="display: table-cell; width: 400px; padding: 10px 10px 10px 20px;">
                <h3>Jelentkező adatai</h3>
                <div class="control-group">
                    <label for="inputError" class="control-label">Név</label>
                    <div class="controls">
                        <input type="text" id="name_field" name="nev" value="<?php echo $row[0]['nev']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Születési neve</label>
                    <div class="controls">
                        <input type="text"  name="szul_nev" value="<?php echo $row[0]['szul_nev']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Telefon</label>
                    <div class="controls">
                        <input type="text"  name="telefon" value="<?php echo $row[0]['telefon']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Email</label>
                    <div class="controls">
                        <input type="text"  name="email" value="<?php echo $row[0]['email']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Lakcím</label>
                    <div class="controls">
                        <input type="text"  name="lakcim" value="<?php echo $row[0]['lakcim']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Személyi ig. száma</label>
                    <div class="controls">
                        <input type="text"  name="szem_ig_szam" value="<?php echo $row[0]['szem_ig_szam']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">TAJ száma</label>
                    <div class="controls">
                        <input type="text"  name="taj_szam" value="<?php echo $row[0]['taj_szam']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Anyja neve</label>
                    <div class="controls">
                        <input type="text"  name="anyja_neve" value="<?php echo $row[0]['anyja_neve']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Állampolgárság</label>
                    <div class="controls">
                        <input type="text"  name="allampolgarsag" value="<?php echo $row[0]['allampolgarsag']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Születési hely</label>
                    <div class="controls">
                        <input type="text"  name="szul_hely" value="<?php echo $row[0]['szul_hely']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Születési időpont</label>
                    <div class="controls">
                        <input type="text"  name="szul_ido" value="<?php echo $row[0]['szul_ido']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Nyelvi ismeret</label>
                    <div class="controls">
                        <input type="text"  name="nyelvi_ismeret" value="<?php echo $row[0]['nyelvi_ismeret']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Legmagasabb iskolai végzettség</label>
                    <div class="controls">
                        <input type="text"  name="legmagasabb_iskolai_vegzettseg" value="<?php echo $row[0]['legmagasabb_iskolai_vegzettseg']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Munkajogi státusza</label>
                    <div class="controls">
                        <input type="text"  name="munkajogi_statusz" value="<?php echo $row[0]['munkajogi_statusz']; ?>" >
                    </div>
                </div>

            </div>
            <div style="display: table-cell; width: 400px">

                <h3>Költségviselő adatai</h3>

                <div class="control-group">
                    <label for="inputError" class="control-label">Költségviselő neve</label>
                    <div class="controls">
                        <input type="text"  name="koltsegviselo_neve" value="<?php echo $row[0]['koltsegviselo_neve']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Költségviselő címe</label>
                    <div class="controls">
                        <input type="text"  name="koltsegviselo_cime" value="<?php echo $row[0]['koltsegviselo_cime']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Költségviselő telefonszáma</label>
                    <div class="controls">
                        <input type="text"  name="koltsegviselo_tel" value="<?php echo $row[0]['koltsegviselo_tel']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Adószám (cégek esetén)</label>
                    <div class="controls">
                        <input type="text"  name="adoszam" value="<?php echo $row[0]['adoszam']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Bankszámla szám (cégek esetén)</label>
                    <div class="controls">
                        <input type="text"  name="bankszamla" value="<?php echo $row[0]['bankszamla']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Ügyintéző neve</label>
                    <div class="controls">
                        <input type="text"  name="ugyintezo" value="<?php echo $row[0]['ugyintezo']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Ügyintéző telefonszáma</label>
                    <div class="controls">
                        <input type="text"  name="ugyintezo_tel" value="<?php echo $row[0]['ugyintezo_tel']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Ügyintéző email címe</label>
                    <div class="controls">
                        <input type="text"  name="ugyintezo_email" value="<?php echo $row[0]['ugyintezo_email']; ?>" >
                    </div>
                </div>
            </div>
            <div style="display: table-cell; width: 400px">

                <h3>Egyéb adatok</h3>

                <div class="control-group">
                    <label for="inputError" class="control-label">Előzetes tudásszint felmérést kért-e</label>
                    <div class="controls">
                        <input type="text"  name="elozetes_tudasszintfelmeres" value="<?php echo $row[0]['elozetes_tudasszintfelmeres']; ?>" >
                    </div>
                </div>

                <div class="control-group">
                    <label for="inputError" class="control-label">Elhelyezkedési tanácsadást kért-e</label>
                    <div class="controls">
                        <input type="text"  name="elhelyezkedesi_tanacsadas" value="<?php echo $row[0]['elhelyezkedesi_tanacsadas']; ?>" >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <textarea class="form-control" id="textarea" name="megjegyzes" value="<?php echo $row[0]['megjegyzes']; ?>">Megjegyzés</textarea>
                    </div>
                </div>
            </div>
        </div>
    </table>


    <a href="<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>"><button class="btn btn-primary">Vissza</button></a>
</fieldset>

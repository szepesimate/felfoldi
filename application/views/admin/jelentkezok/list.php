








<?php /*<button style="margin-left:0" onclick="document.location = '<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>/add'">Új hozzáadása</button>*/ ?>

<br/><br/>

<div class="box">

    <div class="header">
        <h2><?php echo $currentModul->name; ?></h2>
    </div><!-- end header -->

    <!-- Content -->
    <div class="content clearfix">

        <div class="tabs">
            <?php /*
              <ul class="navigation clearfix">
              <?php if ($page_objects) { ?>
              <?php foreach ($page_objects as $page_object): ?>
              <li><a href="#tab<?= $page_object->id ?>"<?php if ($page_object == $page_objects[0]) echo ' class="current"'; ?>><?= $page_object->name ?></a></li>
              <?php endforeach ?>
              <li><a href="#tab_other">Egyéb tartalmak</a></li>
              <?php } ?>
              </ul><!-- end navigation -->
             */ ?>
            <?php
                $other_object = new stdClass();
                $other_object->id = '_other';
                $page_objects[]= $other_object;

                $displayed = [];
            ?>

            <?php if ($page_objects){?>
                <?php foreach ($page_objects as $page_object):?>

                    <div class="tab" id="tab<?=$page_object->id?>">

                      <table class="datatable sortable">
                        <thead>
                          <tr>
                            <th class="sorting_asc">Jelentkezés időpontja</th>
                            <th class="sorting">Név</th>
                            <th class="sorting">Telefon, Email</th>
                            <th class="sorting">Cím</th>
                            <th class="sorting">Tanfolyam adatai</th>
                            <th class="sorting">Tanfolyam díja</th>
                            <th class="sorting">Részletfizetés</th>
                            <th class="sorting">Upsell</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($rows as $row){
                            $displayed[] = $row['id'];
                            
                            $tanfolyam = $this->course->get($row['tanfolyam_id']);
                            $tanfolyam_neve = $tanfolyam->nev;
                            
                            $upsell = '';
                            if ($row['upsell_id']){
                                $courseUpsell = $this->course->get($row['upsell_id']);                                
                                $upsell = '<b>' . $courseUpsell->nev . '</b><br/>' . $row['upsell_date'] . ' - ' . number_format($row['upsell_ar'], 0, ',', '.').' Ft';
                            }

                            //$statusClass = $row['status'] ? 'lathatosag' : 'lathatosag2';
                            echo '<tr>';
                            echo '<td>'.$row['store_date'].'</td>';
                            echo '<td>'.$row['name'].'</td>';
                            echo '<td>'.$row['phone'].'<br><a href="mailto:'.$row['email'].'">'.$row['email'].'</a></td>';
                            echo '<td>'.$row['zip'].' '.$row['city'].',<br/>'.$row['street_address'].'</td>';
                            echo '<td><b>'.$tanfolyam_neve.'</b><br>'.$row['course_city'].' - '.$row['course_date'].'</td>';
                            echo '<td>'.number_format($row['tanfolyam_ar'],0,',','.').' Ft</td>';
                            echo '<td>'.$row['reszletfizetes'].'</td>';
                            echo '<td>'.$upsell.'</td>';

                            /*echo '<td>
                              <a href="'.site_url("admin").'/'.$this->uri->segment(2).'/update/'.$row['id'].'" class="icon szerkesztes">szerkesztés</a>
                              <a style="position:relative;top:5px" class="switch '.$statusClass.' icon" rev="status" href="javascript:void(0)" rel="'.$row['status'].'" name="'.$row['id'].'"></a>
                              <a href="javascript:void(0)" onclick=" if (window.confirm(\'Biztosan törlöd?\')) document.location = \''.site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row['id'].'\'" class="icon torles">törlés</a>
                            </td>';
                            echo '</tr>';*/
/*
                            echo '<td>
                              <a href="'.site_url("admin").'/'.$this->uri->segment(2).'/view/'.$row['id'].'"><button style="background-color: #5e666a;">Részletek</button></a>
                            </td>';*/
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                      </table>

                    </div>

                <?php endforeach?>

            <?php }?>

        </div>

    </div><!-- end content -->

</div><!-- end box -->

<?php
//echo '<div class="pagination">'.$this->pagination->create_links().'</div>';
?>

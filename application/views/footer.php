<section class="section footer">

    <div class="footer-inner">

        <div class="columns is-multiline">

            <div class="column is-12 is-8-desktop footer-left">

              <div class="columns is-multiline col-list">

                <div class="column is-3">

                  <h3 class="title is-3"><?= lang('Oldaltérkép') ?></h3>

                  <?php foreach ($menuItems as $item): ?>

                  <?php
                  if ($item["inFooterMenu"] == 0 || $item["urlSegment"] == "") {
                    continue;
                  }
                  ?>
                  <a href="<?= $item["urlFriendly"] ?>"><?= lang($item["name"]) ?></a>

                  <?php endforeach ?>

                </div>
                <div class="column is-3">
                  <h3 class="title is-3"><?= lang('ÜGYFÉLSZOLGÁLAT') ?></h3>
                  <a href="<?= base_url() . $aszf_page_url; ?>"><?= lang('ÁSZF') ?></a>
                  <a href="<?= base_url() . $deliveryInformation; ?>"><?= lang('Kiszállítás') ?></a>
                  <a href="<?= base_url() . $payInformation; ?>"><?= lang('Fizetés') ?></a>
                  <a href="<?= base_url() . $contact; ?>"><?= lang('Lépj kapcsolatba velünk') ?></a>
                </div>

                <div class="column is-3">
                  <h3 class="title is-3"><?= lang('A FELFÖLDI') ?></h3>
                  <a href="<?= base_url() . $aboutUs; ?>"><?= lang('Rólunk') ?></a>
                  <a href="<?= base_url() . $career; ?>"><?= lang('Karrier') ?></a>
                </div>

                <div class="column is-3">
                  <h3 class="title is-3"><?= lang('ADATKEZELÉS') ?></h3>
                  <a href="<?= base_url() . $adatvedelem_page_url; ?>"><?= lang('Adatvédelmi nyilatkozat') ?></a>
                  <a href="<?= base_url() . $cookiePolicy; ?>"><?= lang('Süti kezelés') ?></a>
                  <a href="<?= base_url() . $unsubscribePageUrl; ?>"><?= lang('Leiratkozás a hírlevélről') ?></a>
                </div>
              </div>

              <div class="column is-12 paycard-list">
                  <h3 class="title"><?= lang('BIZTONSÁGOS BANKKÁRTYÁS FIZETÉS') ?></h3>
                  <a class="paycard-item" href="https://www.barion.com/"><img src="<?= base_url() ?>assets/images/barion.jpg"/></a>
                  <a class="paycard-item"><img src="<?= base_url() ?>assets/images/visa1.jpg"/></a>
                  <a class="paycard-item"><img src="<?= base_url() ?>assets/images/visa2.png"/></a>
                  <a class="paycard-item"><img src="<?= base_url() ?>assets/images/mastercard1.png"/></a>
                  <a class="paycard-item"><img src="<?= base_url() ?>assets/images/mastercard2.png"/></a>
                  <a class="paycard-item"><img src="<?= base_url() ?>assets/images/amex.jpg"/></a>
              </div>

            </div>

            <div class="column is-12 is-4-desktop footer-right">

                <h2 class="title is-2"><span><?= lang('Értesülj elsőként az újdonságokról és speciális ajánlatainkról') ?></span></h2>

                <form id="newsletterForm" class="newsletter_form">           
                    <div class="field">
                        <p class="control">
                            <input class="input" type="text" name="name" placeholder="<?= lang('Teljes neved*') ?>">
                            <input class="input" type="text" name="email" placeholder="<?= lang('E-mail címed*') ?>">
                            <p class="chk">
                                <?php
                                $data = array(
                                    'name' => 'aszf',
                                    'id' => 'terms_checkbox',
                                    'value' => '1',
                                    'checked' => set_value('aszf', false),
                                    'class' => 'checkbox-style',
                                );

                                echo form_checkbox($data);
                                ?>

                                <label for="terms_checkbox">
                                    <?= lang('Elolvastam és elfogadom az ') ?> <a target="_blank" href="<?= $adatvedelem_page_url ?>"><?= lang('the Privacy Policy') ?></a>
                                </label>
                            </p>
                        </p>
                        <button type="submit" class="button gray"><?= lang('FELIRATKOZOM A HÍRLEVÉLRE') ?></button>
                    </div>

                </form>

                <h2 class="title2 is-2"><span><?= lang('Ne felejts el követni minket') ?></span></h2>

                <div class="socials">
                  <a target="_blank" href="https://www.facebook.com/FelfoldiInyencIzmuves"><i class="fa fa-facebook-f wow fadeInUp" aria-hidden="true"></i></a>
                  <a target="_blank" href="https://www.instagram.com/felfoldi_shop_debrecen/"><i class="fa fa-instagram wow fadeInUp"></i></a>
                  <?php /* <a target="_blank" href="<?= $footer_twitter_url ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a target="_blank" href="<?= $footer_gplus_url ?>"><i class="ion ion-social-googleplus"></i></a>
               */ ?>
                  <a target="_blank" href="#"><i class="ion ion-ios-play wow fadeInUp" aria-hidden="true"></i></a>
                  <a href="#"><i class="ion ion-email wow fadeInUp"></i></a>
                  <div class="clear"></div>
                </div>

                <!-- <div class="copy">

                  Niiza © 2001-2020. <?= lang('All rights reserved') ?>.<br/>
                  <span><a href="http://madsolution.ie" target="_blank">Design by MaD Solution</a></span>

                </div> -->
            </div>

        </div>
    </div>

</section>

<section class="section copy">
  <div class="columns">
    <p class="column is-6 has-text-left">Felfoldishop.hu © 2020.    All rights reserved.</p>
    <p class="column is-6 has-text-right">Design by MaD Solution</p>
  </div>
</section>


<div class="search-overlay-menu">
    <span class="search-overlay-close"><i class="ion ion-ios-close-empty"></i></span>
    <form id="searchform" action="" method="post">
        <input value="" name="newsSearchField" placeholder="<?= lang('Search') ?>..." type="search">
        <button type="submit"><i class="ion ion-ios-search"></i></button>
    </form>
</div>

<!-- Barion pixel -->
<script>
            // Create BP element on the window
            window["bp"] = window["bp"] || function () {
                (window["bp"].q = window["bp"].q || []).push(arguments);
            };
            window["bp"].l = 1 * new Date();
    
            // Insert a script tag on the top of the head to load bp.js
            scriptElement = document.createElement("script");
            firstScript = document.getElementsByTagName("script")[0];
            scriptElement.async = true;
            scriptElement.src = 'https://pixel.barion.com/bp.js';
            firstScript.parentNode.insertBefore(scriptElement, firstScript);
            window['barion_pixel_id'] = 'BP-nLAIpGGnwy-A0';            

            // Send init event
            bp('init', 'addBarionPixelId', window['barion_pixel_id']);
        </script>

        <noscript>
            <img height="1" width="1" style="display:none" alt="Barion Pixel" src="https://pixel.barion.com/a.gif?ba_pixel_id='BP-nLAIpGGnwy-A0'&ev=contentView&noscript=1">
        </noscript>
<!-- Barion pixel -->

<!-- EU süti -->
<script type="text/javascript">
   window.cookieconsent_options = {"message":"<?php echo lang("__cookie_policy_label__")?>","dismiss":"Ok","learnMore":"<?=lang('__more_info__')?> »","link":"<?= base_url() . $cookiePolicy; ?>","theme":"dark-bottom"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<!-- EU süti -->

<?php $this->load->view('veryend'); ?> 
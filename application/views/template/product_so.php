<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container is-fluid breadCrumbHolder">

        <?php if (isset($breadCrumb)): ?>

            <?php $cnt = 0; ?>
            <?php foreach ($breadCrumb as $item => $link): ?>
                <?php if ($cnt > 0): ?>
                    &nbsp;>&nbsp;
                <?php endif ?>
                <a href="<?php echo $link ?>"><?php echo lang($item) ?></a>

                <?php $cnt++; ?>
            <?php endforeach ?>

        <?php endif ?>

    </div>

    <div class="container is-fluid">

        <div class="columns">

            <div class="column">
                <div class="content has-text-centered">
                    <h1 class="title is-2 allProductsTitle"><?= lang('Special offers') ?></h1>
                </div>
            </div>

        </div>

        <div class="columns is-multiline boxesInner">

            <?php if (isset($special_offer_products) && $special_offer_products) { ?>

                <?php
                $data['productBoxes'] = $special_offer_products;
                $this->load->view("template/productBoxes.php", $data);
                ?>

            <?php } else { ?>

                <br/><br/>
                <?= lang('No product found.') ?>

            <?php } ?>

        </div>

        <div class="fw moreElementsTrigger">
            <br class="clear"/><br/>
        </div>



    </div>

</section>


<?php $this->load->view('footer'); ?>

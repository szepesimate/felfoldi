<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container">
        <?php
        if (isset($articles)) {
            foreach ($articles as $article) {
                ?>
                <div class="columns">
                    <div class="column is-6 is-offset-3 has-text-centered">

                        <h1 class="title is-1"><?= $article->title ?></h1>

                        <div class="content text-content"><?= $article->content ?></div>



                        <form id="ajanlat_form">
                            <p class="rjttt"><input type="text" class="field" name="phone" /></p>
                            <label class="label has-text-left">Név*</label>
                            <div class="field">
                                <p class="control">
                                    <input class="input" type="text" name="name" placeholder="Ajánlatkérő neve">
                                </p>
                            </div>
                            <div class="field">
                                <label class="label has-text-left">E-mail*</label>
                                <p class="control">
                                    <input class="input" type="text" name="email" placeholder="Ajánlatkérő e-mail címe">                                    
                                </p>
                            </div>
                            <div class="field">
                                <label class="label has-text-left">Telefonszám</label>
                                <p class="control">
                                    <input class="input" type="text" name="phone_" placeholder="Ajánlatkérő telefonszáma">
                                </p>
                            </div>

                            <div class="field">
                                <label class="label has-text-left">Épület típusa*</label>
                                <p class="control">
                                    <label class="radio"><input class="" type="radio" name="epulet_tipusa" value="Lakóépület"> Lakóépület</label> <br>
                                    <label class="radio"><input class="" type="radio" name="epulet_tipusa" value="Ipari-, üzemi létestmény"> Ipari-, üzemi létestmény</label> <br>
                                    <label class="radio"><input class="" type="radio" name="epulet_tipusa" value="Kereskedelmi létesítmény"> Kereskedelmi létesítmény</label> <br>
                                    <label class="radio"><input class="" type="radio" name="epulet_tipusa" value="Iroda, közigazgatás"> Iroda, közigazgatás</label> <br>
                                    <label class="radio"><input class="" type="radio" name="epulet_tipusa" value="Oktatás"> Oktatás</label> <br>
                                    <label class="radio"><input class="" type="radio" name="epulet_tipusa" value="Mezőgazdasági"> Mezőgazdasági</label> <br>
                                    <label class="radio"><input class="" type="radio" name="epulet_tipusa" value="Egyéb"> Egyéb</label> <br>
                                </p>
                            </div>

                            <div class="field">
                                <label class="label has-text-left">Statikai dokumentáció típusa*</label>
                                <p class="control">
                                    <label class="radio"><input class="" type="radio" name="dok_tipusa" value="Építés engedélyezési dokumentáció"> Építés engedélyezési dokumentáció</label> <br>
                                    <label class="radio"><input class="" type="radio" name="dok_tipusa" value="Kiviteli"> Kiviteli</label> <br>
                                    <label class="radio"><input class="" type="radio" name="dok_tipusa" value="Építés engedélyezési és kiviteli együttesen"> Építés engedélyezési és kiviteli együttesen</label> <br>
                                    <label class="radio"><input class="" type="radio" name="dok_tipusa" value="300m2"> 300 m2-t meg nem haladó lakóépület egyszerűsített bejelentési eljárás szerint</label> <br>
                                    <label class="radio"><input class="" type="radio" name="dok_tipusa" value="Oktatás"> Oktatás</label> <br>
                                    <label class="radio"><input class="" type="radio" name="dok_tipusa" value="Meglévő épület átalakításához szakértői vélemény"> Meglévő épület átalakításához szakértői vélemény</label> <br>
                                </p>
                            </div>

                            <div class="field">
                                <label class="label has-text-left">Építkezés helye - város</label>
                                <p class="control">                                    
                                    <input class="input" type="text" name="city" placeholder="Építkezés helye - város">
                                </p>
                            </div>

                            <div class="field">
                                <label class="label has-text-left">Hasznos alapterület, vagy becsült építési költség*</label>
                                <p class="control">
                                    <input class="input" type="text" name="alapterulet" placeholder="Hasznos alapterület, vagy becsült építési költség">
                                </p>
                            </div>

                            <div class="field">
                                <label class="label has-text-left">Egyéb közlendő</label>
                                <p class="control">
                                    <textarea class="textarea" name="message" placeholder="Egyéb közlendő"></textarea>
                                </p>
                            </div>



                            <button type="submit" class="button is-large ">Árajánlatkérés</button>
                        </form>



                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</section>

<?php $this->load->view('footer'); ?>

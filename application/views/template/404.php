<?php $this->load->view('header'); ?>


	<section class="fw container">

        <div class="siteInner">

            <article class="content">

                <?php
                    if (isset($articles)){
                        foreach ($articles as $article){
                            echo '<h1>'.$article->title.'</h1>';
                ?>
                            <?php if ( $article->imageFileName != '' ):?>
                                <img class="topImage" alt="<?=$article->title?>" src="<?=base_url()?>assets/images/news/<?=$article->imageFileName?>" />
                            <?php endif?>

                            <?php if ($article->preview != ''):?>
                                <div class="greenLeftBorder grayBg"><?=$article->preview?></div>
                            <?php endif?>



                <?php
                            echo $article->content;
                        }
                    }
                ?>

            </article>

        </div>

    </section>




<?php $this->load->view('footer'); ?>

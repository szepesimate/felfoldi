<?php $this->load->view('header'); ?>

<?php if ($articles && count($articles) > 0): ?>
<section class="section welcome-section">
    <div class="container is-fluid has-text-centered">
        <div>
            <div>
                <h2 class="title is-1"><span><?= lang('__articles_title__') ?></span></h2>
            </div>
        </div>
        <div class="columns is-gapless article-body">
            <div class="column is-5"><img src="<?php echo base_url() . 'assets/images/news/' . $articles[0]->imageFileName; ?>" alt="Felföldi Ínyenc Ízmûves"/></div>
            <div class="column is-7 has-text-right">
                <p><?=  $articles[0]->content; ?> </p>
                <a class="button gray" href="<?= $aboutUs ?>"><?= lang('TUDJ MEG TÖBBET'); ?></a>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section class="section our-products">
    <?php if ($categoryBoxes) { ?>
        <div class="container is-fluid has-text-centered">

            <div>
                <div>
                    <h2 class="title is-1"><span><?= lang('__main_categories_title__') ?></span></h2>
                </div>
                <p class="section-description">
                    <?= lang('__main_categories_text__') ?>
                </p>
            </div>

          <div class="columns is-gapless is-centered">

            <div id="slider-prev"></div>
            <div class="columns is-multiline is-centered bxslider">

                <?php
                $this->load->view("template/categoryBoxes.php", ['categoryBoxes' => $categoryBoxes, 'ourProductDisplayMode' => 'webshop_box_slider']);
                ?>
            </div>
            <div id="slider-next"></div>
          </div>

        </div>
    <?php } ?>
</section>

<?php if ($latestNews) { ?>
  <section class="section latest-news">

    <div class="has-text-centered">

      <div class="columns">
        <div class="column">
          <h2 class="title is-1"><span><?= lang('__neked_ajanljuk_title__') ?></span></h2>

          <p style="padding-bottom:40px"><?= lang('__neked_ajanljuk_description__')?></p>
        </div>
      </div>

      <div class="columns is-multiline is-centered is-mobile">
				<?php
				$this->load->view("template/newsBoxes.php", ['latestNews' => $latestNews]);
				?>
      </div>

    </div>

  </section>
<?php } ?>

<?php $this->load->view('footer'); ?>


<?php $this->load->view('header'); ?>

<script src="https://js.stripe.com/v3/"></script>

<script>
    var stripe = Stripe('pk_live_fdjxuQHUnlr4zWVK2EeJPgyn');
    var elements = stripe.elements();
</script>


<section class="section txt-article">
    <div class="container">
        <?php
        if (isset($articles)) {
            foreach ($articles as $article) {
                ?>
                <div class="columns has-text-centered">
                    <div class="column">
                        <h1 class="title is-1"><?= $article->title ?></h1>
                        <div class="content text-content"><?= $article->content ?></div>
                    </div>
                </div>
                <?php
            }
        }
        ?>


        <br/>
        <br/>


        <div class="columns">
            <div class="column is-12-mobile is-8-tablet is-offset-2-tablet is-6-desktop is-offset-3-desktop">

                <form action="/Stripe_ctrl/charge" method="post" id="payment-form" style="background:#f7f7f7;padding:20px;">
                    <div class="form-row">
                        <label for="card-element">
                            <?=lang('Credit or debit card')?>
                        </label>
                        <div id="card-element">
                            <!-- A Stripe Element will be inserted here. -->
                        </div>

                        <!-- Used to display Element errors. -->
                        <div id="card-errors" role="alert"></div>
                    </div>

                    <br/>
                    
                    <button class="button"><?=lang('Submit Payment')?></button>
                </form>

            </div>
        </div>

        <script>
            var style = {
                base: {
                    fontSize: '16px',
                    color: "#32325d",
                }
            };

            var card = elements.create('card', {style: style});
            card.mount('#card-element');



            card.addEventListener('change', function (event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function (event) {
                event.preventDefault();

                stripe.createToken(card).then(function (result) {
                    if (result.error) {
                        // Inform the customer that there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
            }
        </script>


    </div>
</section>

<?php $this->load->view('footer'); ?>

<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container">

        <div class="columns">

            <div class="column has-text-centered">

                <?php
                if (isset($articles)) {
                    foreach ($articles as $article) {
                        echo '<h1 class="title is-1">' . $article->title . '</h1>';
                        echo $article->content;
                    }
                }
                ?>
            </div>
        </div>

        <div class="columns has-text-centered gallery_type_boxes is-multiline">
            <?php /*
              <div class="column">

              <a href="<?php echo base_url() . Page::pageUrlByTpl("gallery") ?>">
              <img alt="" src="<?php echo base_url() ?>assets/images/gallery.jpg" />
              <span><?=lang('Photo gallery')?></span>
              </a>

              </div>
             * 
             */ ?>
            <div class="column">

                <a href="<?php echo base_url() . Page::pageUrlByTpl("videogallery") ?>">
                    <img alt="" src="<?php echo base_url() ?>assets/images/gallery.jpg" />
                    <span><?=lang('Video gallery')?></span>
                </a>

            </div>
            <div class="column">

                <a href="<?php echo base_url() . Page::pageUrlByTpl("catalogs") ?>">
                    <img alt="" src="<?php echo base_url() ?>assets/images/catalog.jpg" />
                    <span><?=lang('Catalogs')?></span>
                </a>

            </div>

        </div>

    </div>

</section>


<?php $this->load->view('footer'); ?>

<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container narrower">

        <?php if (isset($renewActionForEmail)) { ?>

            <div class="columns">
                <div class="column has-text-centered">
                    <h1 class="title is-1"><?= lang('Jelszó megváltoztatása') ?></h1>


                    <?php echo form_open('', ['id' => 'renewPasswordForm']); ?>


                    <div class="field">
                        <p class="control">

                            <label class="label"><?=lang('New password')?>:<sup>*</sup></label>
                            <input type="password" class="input" name="password" />
                        </p>
                    </div>

                    <div class="field">
                        <p class="control">

                            <label class="label"><?=lang('Retype password')?>:<sup>*</sup></label>
                            <input type="password" class="input" name="password_re" />
                        </p>
                    </div>

                    <div class="field">
                        <p class="control">
                            <input type="submit" class="button" name="submit" value="<?=lang('Save')?>" />
                        </p>
                    </div>

                    <input type="hidden" name="token" value="<?php echo $token ?>" />


                    <?php echo form_close(); ?>

                </div>
            </div>


        <?php } else { ?>

            <div class="columns">
                <div class="column has-text-centered">
                    <h1 class="title is-1"><?= lang('Forgot your password?') ?></h1>


                    <?php
                    if (isset($articles)) {
                        foreach ($articles as $article) {
                            echo $article->content;
                        }
                    }
                    ?>



                    <?php echo form_open('', ['id' => 'forgotPasswordForm']); ?>


                    <div class="field">
                        <p class="control">
                            <label class="label"><?= lang('E-mail address') ?>:<sup>*</sup></label>
                            <input type="text" class="input" placeholder="<?= lang('Please enter your e-mail address') ?> ..." name="email" />
                        </p>
                    </div>

                    <div class="field">
                        <p class="control">
                            <input type="submit" class="button" name="submit" value="<?= lang('Submit') ?>" />
                        </p>
                    </div>


                    <?php echo form_close(); ?>

                </div>
            </div>

        <?php } ?>


    </div>

</section>


<?php $this->load->view('footer'); ?>

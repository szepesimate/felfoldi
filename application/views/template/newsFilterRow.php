
	<div class="fw">

		<div class="filterRow">

			<div class="siteInner">

				<div class="left">

					<div class="page_logo"></div>

					<?php if (isset($archives)):?>

						<div class="select-style archive_select">
						<select id="archive_select">
							<option value="">Archive</option>
							<?php foreach ($archives as $archive_date):?>
								<option value="<?php echo base_url().$newsPageUrl.'/'.$activeTopicUrl.'/archive/'.rawurlencode($archive_date)?>"<?php if ($archive_date == rawurldecode($this->uri->segment(4))) echo ' selected';?>><?php echo date('Y.m.', strtotime($archive_date))?></option>
							<?php endforeach?>
						</select>
						</div>


					<?php endif?>


					<a href="javascript:void(0)" class="topic_button"><span></span></a>

				</div>

				<div class="right">

					<?php echo form_open('', ['id' => 'newsSearchForm']); ?>

						<input type="text" class="field" placeholder="search..." name="newsSearchField" value="<?php if (isset($keyword)) echo $keyword; ?>" />
						<input type="submit" class="button" value="" />

					<?php echo form_close(); ?>

				</div>

			</div>

		</div>

	</div>

<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container">

        <div class="columns">
            <div class="column has-text-centered">

                <h1 class="title is-1"><?=lang('Video gallery')?></h1>

            </div>
        </div>

        <div class="columns has-text-centered gallery_type_boxes is-multiline">

            <?php if (isset($videogalleries)): ?>

                <?php
                $cnt = 1;
                $js = "<script> var preview_url_dict = { ";
                ?>


                <?php foreach ($videogalleries as $videogallery_arr) { ?>

                    <div class="column is-4">

                        <a rel="slideshow-thumbs<?php echo $cnt ?>" href="<?php echo $videogallery_arr["videoGalleryPreview"] ?>">
                            <img alt="" src="<?php echo $videogallery_arr["videoGalleryPreview"] ?>" />
                            <span><?php echo $videogallery_arr["videoGalleryName"] ?></span>
                        </a>

                        <?php $js .= "'" . $videogallery_arr["videoGalleryPreview"] . "' : '" . $videogallery_arr["videos"][0]->url . "',"; ?>


                        <?php
                        array_shift($videogallery_arr["videos"]);
                        if ($videogallery_arr["videos"]) {
                            ?>
                            <div style="display:none">

                                <?php foreach ($videogallery_arr["videos"] as $video) { ?>
                                    <a class="thumb" rel="slideshow-thumbs<?php echo $cnt ?>" href="<?php echo $video->preview ?>"></a>
                                    <?php $js .= "'" . $video->preview . "' : '" . $video->url . "',"; ?>
                                <?php } ?>


                            </div>

                        <?php } ?>

                        <?php $cnt++; ?>
                    </div>
                <?php } ?>



                <?php echo $js . ' }</script>'; ?>

            <?php endif ?>

        </div>

    </div>

</section>

<?php $this->load->view('footer'); ?>

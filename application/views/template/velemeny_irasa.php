<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container">
        <?php
        if (isset($articles)) {
            foreach ($articles as $article) {
                ?>
                <div class="columns">
                    <div class="column is-8 is-offset-2">

                        <h1 class="title is-1"><?= $article->title ?></h1>

                        <?php if (false && $article->imageFileName != ''): ?>
                            <img alt="<?= $article->title ?>" src="<?= base_url() ?>assets/images/news/<?= $article->imageFileName ?>" />
                        <?php endif ?>
                        <div class="content text-content"><?= $article->content ?></div>









                        <form id="velemenyIrasaForm">

                            <p class="rjttt"><input type="text" name="phone" /></p>
                            <div class="field">
                                <p class="control">
                                    <input class="input" type="text" name="name" placeholder="NEVED">
                                </p>
                            </div>

                            <div class="field">
                                <p class="control">
                                    <span class="select is-medium is-fullwidth" style="margin-bottom:9px">
                                        <?= form_dropdown('tanfolyam_id', $tanfolyamOptions) ?>
                                    </span>
                                </p>
                            </div>

                            <div class="field">
                                <p class="control">
                                    <input class="input" type="text" name="city" placeholder="MELYIK VÁROSBA JÁRTÁL KÉPZÉSRE?">
                                </p>
                            </div>

                            <div class="field">
                                <p class="control">
                                    <textarea class="textarea" name="comment" placeholder="NÉHÁNY MONDATBAN ÍRD LE VÉLEMÉNYED"></textarea>
                                </p>
                            </div>
                            
                            
                            <div class="field">
                                <p class="control">
                                    <button type="submit" class="button is-large is-info mtop20">ELKÜLDÖM AZ ÉRTÉKELÉST</button>
                                </p>
                            </div>

                        </form>






                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</section>

<?php $this->load->view('footer'); ?>

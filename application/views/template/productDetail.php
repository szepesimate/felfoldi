<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container is-fluid breadCrumbHolder">

        <?php if (isset($breadCrumb)): ?>

            <?php $cnt = 0; ?>
            <?php foreach ($breadCrumb as $item => $link): ?>
                <?php if ($cnt > 0): ?>
                    &nbsp;>&nbsp;
                <?php endif ?>
                <a href="<?php echo $link ?>"><?php echo lang($item) ?></a>

                <?php $cnt++; ?>
            <?php endforeach ?>

        <?php endif ?>

    </div>


    <div class="container narrower">

        <div class="columns">

            <?php if (isset($product)): ?>

                <div class="showBelow760" style="display:none">
                    <br/>
                    <a class="printIcon" style="display:none" href="<?= base_url() ?>Pdf_gen/generate_card/<?= $product->id ?>" target="_blank"><img alt="Print" src="<?= base_url() ?>assets/images/printer.jpg" /></a>

                    <h1 class="product-title is-1"><?php echo $product->{"name"} ?></h1>
                    <div class="fajta"><?= $product->{"type"} ?></div>
                </div>

                <div class="column is-5 is-6-widescreen images">
                    <?php if ($productImages): ?>
                        <div class="bigImage">
                            <a class="fancybox" href="<?php echo base_url() . "assets/images/webshop/big/" . $productImages[0]->fileName ?>" data-fancybox-group="gallery">
                                <?php /* $imageSize = getimagesize(base_url() . "assets/images/webshop/big/" . $productImages[0]->fileName) */ ?>
                                <img alt="<?php echo $product->{"name"} ?>" class="pic" src="<?php echo base_url() . "assets/images/webshop/big/" . $productImages[0]->fileName ?>" />
                                <img alt="<?php echo $product->{"name"} ?>" class="picThumb" src="<?php echo base_url() . "assets/images/webshop/thumb/" . $productImages[0]->fileName ?>" />

                                <span class="magnifier"></span>
                            </a>
                        </div>
                        <div class="thumbnails"><?php array_shift($productImages); ?>
                            <?php $cnt = 1; ?>
                            <?php foreach ($productImages as $productImage): ?><?php
                                ?><a class="thumb fancybox" href="<?php echo base_url() . "assets/images/webshop/big/" . $productImage->fileName ?>" data-fancybox-group="gallery" style="background-image:url('<?php echo base_url() . "assets/images/webshop/small/" . $productImage->fileName ?>')">

                                    <span class="magnifier"></span>
                                </a><?php
                                ?><?php $cnt++; ?>
                            <?php endforeach ?><?php
                            ?></div>
                    <?php endif ?>
                </div>

                <div class="column is-7 is-6-widescreen right">

                    <!-- <a class="hideBelow760 printIcon" href="<?= base_url() ?>Pdf_gen/generate_card/<?= $product->id ?>" target="_blank"><img alt="Print" src="<?= base_url() ?>assets/images/printer.jpg" /></a> -->

                    <h1 class="hideBelow760 product-title is-1"><?php echo $product->{"name"} ?></h1>
                    <div class="hideBelow760 fajta"><?= $product->{"type"} ?></div>

                    <?php /*
                      <div class="fw rateHolder active">
                      <div class="jRate" id="jRate_<?= $product->itemNumber ?>"></div> (<span class="vote_count"><?= $vote_count ?></span>) &nbsp; <i><?= lang('rate this product') ?></i>
                      <input type="hidden" class="rating" value="<?= $rating ?>" />
                      <input type="hidden" class="itemId" value="<?= $product->id ?>" />
                      </div>


                      if ( isset($productForRank) ):?>

                      <form id="ertekelesForm" acton="" method="post">
                      <h3>Értékelje a terméket:</h3>

                      <textarea name="comment" cols="" rows="" style="display:block"></textarea>
                      <br/>
                      <input type="submit" class="button" value="Küldés" />

                      <br/><br/>

                      <input type="hidden" name="itemNumber" value="<?php echo $product->itemNumber ?>" />
                      <input type="hidden" name="order_id" value="<?php echo $productForRank["order_id"] ?>" />


                      </form>



                      <?php endif */ ?>



                    <?php echo $product->{"description"} ?>

                    <br/>
                    <?php
                    $productFields = $this->utils->getProductFields();
                    
                    foreach ($productFields as $key => $value):
                        ?>
                        <?php if ($product->{"$key"} != '') { ?>
                            <p>
                                <b><?= lang($productFields[$key]) ?>:</b><br /> 
                                <!-- <?php if (strstr($product->{"$key"}, '<p>')): ?></p><?php endif ?> -->
                            <?= $product->{"$key"} ?>
                                <!-- <?php if (!strstr($product->{"$key"}, '<p>')): ?></p><?php else: ?><br/><?php endif; ?> -->
                            </p>
                            <br />
                        <?php } ?>

                    <?php endforeach; ?>

                    <?php if ($product->glutenfree): ?>
                    <p><b>Gluténmentes:</b><br />
                        <span>Igen</span>
                    </p>
                    <br />
                    <?php endif; ?>

                    <?php if ($product->lactosefree): ?>
                    <p><b>Laktózmentes:</b><br />
                        <span>Igen</span>
                    </p>
                    <br />
                    <?php endif; ?>

                    <?php if ($product->sugarfree): ?>
                    <p><b>Cukormentes:</b><br />
                        <span>Igen</span>
                    </p>
                    <br />
                    <?php endif; ?>

                    <?php if ($product->bio): ?>
                    <p><b>Bio:</b><br />
                        <span>Igen</span>
                    </p>
                    <br />
                    <?php endif; ?>

                    <?php if($product->alcoholContent !== ''): ?>
                        <p>
                            <b>Alcohol tartalom:</b><br />
                            <?= $product->alcoholContent ?>
                        </p>
                    <?php endif; ?>

                    <?php
                    if ($product->stock == 0) {
                        echo '<div class="notLoggedWarning"><br/>' . lang('PLEASE REMEMBER THAT YOU CAN BUY THIS PRODUCT EVEN IT’S OUT OF STOCK. IN THIS CASE THE DELIVERY TIME 30 DAYS.') . '</div>';
                    }
                    ?>




                    <div class="">
                        <br/>

                        <?php if ($product->discountPrice > 0): ?>
                            <span class="discountPrice"><?php echo number_format($product->discountPrice, 2, '.', ' ') ?> Ft</span>
                            <span class="oldPrice"><?php echo number_format($product->price, 2, '.', ' ') ?> Ft</span>
                        <?php else: ?>
                            <span class="price"><?php echo number_format($product->price, 2, '.', ' ') ?> Ft</span>
                        <?php endif; ?>

                        <br/>

                        <?php
                        $stockClass = $product->stock ? 'in_stock blue' : 'out_of_stock red';
                        $stockStr = $product->stock ? '<span class="stockTick blue"></span>' . lang('raktáron') : '<span class="txt22">x</span> ' . lang('nincs raktáron');
                        ?>

                        <div class="stockRow <?= $stockClass ?>"><?= $stockStr ?></div>

                        <br/>

                        <?= lang('Mennyiség') ?>: &nbsp; &nbsp;

                        <a class="rectangleButton minus" href="javascript:void(0)">–</a>
                        <a class="rectangleButton numberHolder" href="javascript:void(0)">1</a>
                        <a class="rectangleButton plus" href="javascript:void(0)">+</a>

                        <br/>

                        <a class="button kosar" href="javascript:void(0)">
                            <i class="fa fa-shopping-cart"></i>
                            &nbsp;&nbsp;
                            <?= lang('Kosárba teszem') ?>
                        </a>
                        <input type="hidden" class="itemNumberHolder" value="<?php echo $product->itemNumber ?>" />

                    </div>



                    <div class="social_row">

                        <div class="share_holder">
                            <div class=""><?= lang('IF YOU LIKED THIS PRODUCT, PLEASE SHARE IT WITH YOUR FRIENDS') ?></div>

                            <a href="javascript:void(0)" onclick="return fbs_click()" title="Share on Facebook"><i class="fa fa-facebook-f" data-wow-delay="250ms" aria-hidden="true"></i></a>
                            <a target="_blank" href="https://twitter.com/share?url=<?php echo "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" target="_blank" title="Share on Twitter"><i class="fa fa-twitter" data-wow-delay="250ms" aria-hidden="true"></i></a>
                            <a href="https://plus.google.com/share?url=<?php echo "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" onclick="javascript:window.open(this.href,
                                                '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                                        return false;" title="Share on Google+"><i class="fa fa-google-plus" data-wow-delay="250ms" aria-hidden="true"></i></a>
                            <a target="_blank" href=""><i class="fa fa-pinterest" data-wow-delay="250ms" aria-hidden="true"></i></a>

                            <a class="email" href="mailto:?subject=<?php echo lang('Website share') ?>&body=<?php echo "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" title="Share on E-mail"><i class="fa fa-envelope" data-wow-delay="250ms" aria-hidden="true"></i></a>

                        </div>

                    </div>


                    <?php /*
                      <div class="fw comment_title">Szólj hozzá</div>

                      <div class="fb-comments" data-href="<?php echo "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" data-width="412" data-numposts="5"></div>
                     */ ?>
                </div>




                <?php /* if ( isset($rates) ):?>

                  <div class="fw">

                  <span class="whiteBgOrangeLabel">Értékelések</span>

                  </div>

                  <div class="fw ratesHolder">

                  <?php foreach ($rates as $rateObjAndName):?>

                  <div class="rateDiv">
                  <span><?php echo $rateObjAndName["name"]?> &raquo; <?php echo date('Y.m.d', strtotime($rateObjAndName["rateObject"]->date)) ?></span> <br/>
                  <?php echo $rateObjAndName["rateObject"]->comment?>
                  </div>

                  <?php endforeach ?>

                  </div>

                  <?php endif */ ?>



            <?php else: ?>

                Product not found

            <?php endif ?>

        </div>

    </div>

</section>


<section class="section related">

    <div class="container is-fluid has-text-centered">

        <?php if (isset($productBoxes)): ?>

            <h2 class="title is-1"><span><?= lang('Related products') ?><span></h2>

            <div class="columns is-multiline jcc">
                <?php
                $this->load->view("template/productBoxes.php");
                ?>
            </div>

        <?php endif ?>


    </div>

</section>


<?php $this->load->view('footer'); ?>

<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container">
        <div class="columns">
            <div class="column">
                <div class="content has-text-centered">
                    <h1 class="title is-1"><?=lang('__sikeres_fizetes_cim__')?></h1>
                    <div class="content text-content">
                        <p>
                            <?=lang('__sikeres_fizetes_szoveg__')?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('footer'); ?>
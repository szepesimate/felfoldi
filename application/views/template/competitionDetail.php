<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container">
        
        <div class="columns">

            <div class="column has-text-centered">
                <h1 class="title is-1 borderBottom"><?= '<b>' . lang('Competitions') . '</b>' ?></h1>
            </div>

        </div>

        <div class="columns">

            <div class="column articleHolder">

                <div class="content">
                    
                    <br/>

                    <?php
                    if (isset($article)) {
                        ?>

                        <h2 class="title is-2"><?php echo $article->title ?></h2>

                        <div class="columns is-mobile blue dateHolder">
                            <div class="column">
                                <i class="ion ion-ios-clock-outline"></i>
                                <?php if ($sesslang == 'hungarian'): ?>
                                    <?php echo date('Y. ', strtotime($article->startDate)) . ucfirst(lang($this->utils->getMonthName(date('m', strtotime($article->startDate))))) . ' ' . date('d.', strtotime($article->startDate)) ?>
                                <?php else: ?>
                                    <?php echo date('d. ', strtotime($article->startDate)) . ucfirst(lang($this->utils->getMonthName(date('m', strtotime($article->startDate))))) . ' ' . date('Y.', strtotime($article->startDate)) ?>
                                <?php endif ?>     
                            </div>
                        </div>

                        
                        <div class="preview_div fb"><?php echo $article->preview ?></div><br/>

                        <?php echo $article->content ?>

                    <?php } ?>

                </div>

            </div>


        </div>

    </div>

</section>

<?php $this->load->view('footer'); ?>
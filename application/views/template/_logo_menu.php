<header class="nav<?= $isHome ? '' : ' fixed posAbs' ?>">            
    <div class="container is-fluid is-marginless">

        <div id="menu-shade">
        </div>

        <div class="nav-left">
        </div>
        <span class="nav-toggle categoriesBtn">
            <span></span>
            <span></span>
            <span></span>
        </span>


        <div class="nav-center nav-menu">

            <a class="nav-item myLogo" href="<?= base_url() ?>">

            </a>



        </div>

        <div class="nav-right">

            <button class="magnifier"><i class="ion ion-ios-search"></i></button>

            <button class="profile"><i class="ion ion-ios-contact-outline"></i></button>

            <button class="cart" onclick="document.location = '<?= base_url() . $basketPageUrl ?>'"><i class="fa fa-shopping-cart"></i><span class="badge"><?php
                    if (isset($basketItemCount)) {
                        echo $basketItemCount;
                    }
                    ?></span></button>

        </div>        

    </div>



</header>


<div class="menu main">

    <!-- <button class="close"><i class="ion ion-ios-close-empty"></i></button> -->

    <?php foreach ($menuItems as $item): ?>

        <?php
        if ($item["inMainMenu"] == 0 || $item["urlSegment"] == "") {
            continue;
        }
        ?>

        <a href="<?= $item["urlFriendly"] ?>" class="nav-item <?= $item["template"] == 'product' ? "productLinkYYY" : "" ?> <?= ($this->uri->segment(1) == $item["urlSegment"]) ? "is-active" : "" ?>">
            <?= lang($item["name"]) ?>
        </a>

        <?php if ($item["template"] == 'product'): ?>
            <div class="sublinks" style="padding-left: 16px">
                <?php /* <a href="<?= $item["urlFriendly"] ?>" class="allProductLink nav-item <?= ($this->uri->segment(1) == $item["urlSegment"]) ? "is-active" : "" ?>">
                  <?= lang('All products') ?>
                  </a> */ ?>
                <?php foreach ($categoryMenuItems as $category) { ?>

                    <?php if ($category['subItems']): ?>
                        <a class="nav-item showOnlySidebar closed interactive main-item" data-target="<?= $category["id"]; ?>"><?= lang($category["name"]) ?></a>
                    
                        <div class="sub-items closed" id="sub-items-<?= $category["id"]; ?>">
                            <a class="nav-item showOnlySidebar" href="<?= $category["urlFriendly"] ?>"><?= lang('Összes') ?></a>
                        <?php foreach ($category['subItems'] as $category2) { ?>
                            <a class="nav-item showOnlySidebar" href="<?= $category2["urlFriendly"] ?>"><?= lang($category2["name"]) ?></a>
                        <?php } ?>
                        </div>
                    <?php else:?>
                        <a class="nav-item showOnlySidebar main-item" href="<?= $category["urlFriendly"] ?>"><?= lang($category["name"]) ?></a>
                    <?php endif;?>

                <?php } ?>
            </div>
        <?php endif ?>

        <!-- <?php if (in_array($item['template'], ['contact', 'basket'])): ?>
            <div class="spacer"></div>
        <?php endif; ?> -->

    <?php endforeach ?>


</div>

<div class="loginPanel<?= $this->session->userdata('uid') ? ' loggedIn' : '' ?>">

    <?php if ($this->session->userdata('uid')): ?>

        <a href="<?php echo base_url() . $profilPageUrl ?>"><i class="fa fa-user-circle-o"></i> <?= lang('Profile') ?></a>

        <?php echo form_open('', ['id' => 'logoutForm']); ?>        
        <button type="submit" class="button" name="logout"><i class="fa fa-sign-out"></i> <?= lang("Kijelentkezés") ?></button>
        <?php echo form_close(); ?>

    <?php else: ?>

        <div class="panel-title">
            <i class="ion ion-ios-person-outline"></i>
            <span class="is-size-4"><?= lang("Bejelentkezés") ?></span>
            <span class="is-size-7"><?= lang("e-mail címmel ") ?></span>
        </div>
        <?php echo form_open('', ['id' => 'loginForm']); ?>

        <p class="field-label"><?= lang('E-mail cím') ?><span>*</span></p>
        <input type="text" class="field" name="email"/>

        <p class="field-label"><?= lang('Password') ?><span>*</span></p>
        <input type="password" class="field" name="password"/>
        <a class="forgot-pswd-btn" href="<?php echo base_url() . $forgot_url ?>"><?= lang("Forgot your password?") ?></a>

        <input type="submit" class="blue-btn" name="login" value="<?= lang("Belépek") ?>"/>

        <?php echo form_close(); ?>

        <div>
            <span class="is-size-7"><?= lang("vagy") ?></span>
            <span class="is-size-4"><?= lang("Regisztrálj egy fiókot") ?></span>
        </div>

        <a href="<?php echo base_url() . $reg_url ?>" class="blue-btn"><?= lang("Regisztrálok") ?></a>

    <?php endif ?>

</div>



<div class="basketPanel">

    <div class="topRow">
        <i class="fa fa-shopping-cart"></i> <?= lang('Cart') ?>: <span class="badge"><?php if (isset($basketItemCount)) echo $basketItemCount; ?></span> <?=lang('Products')?>
    </div>

    <div class="productInner">

    </div>

    <div class="summary">
        <?= lang('Total') ?>: <span></span>
    </div>

    <a class="button" href="<?php echo base_url() . $basketPageUrl ?>"><?= lang('Check out / Full cart') ?></a>

</div>
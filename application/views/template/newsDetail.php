<?php $this->load->view('header'); ?>

<section class="section txt-article columns is-vcentered is-centered is-multiline">

    <div class="column is-12 breadCrumbHolder">

    <?php if (isset($breadCrumb)): ?>

        <?php $cnt = 0; ?>
        <?php foreach ($breadCrumb as $item => $link): ?>
            <?php if ($cnt > 0): ?>
                &nbsp;>&nbsp;
            <?php endif ?>
            <a href="<?php echo $link ?>"><?php echo lang($item) ?></a>

            <?php $cnt++; ?>
        <?php endforeach ?>

    <?php endif ?>

    </div>

    <div class="column prev">
        <?php if (isset($prevNewArticle)):?>
        <a href="<?= base_url() . $newsPageUrl . '/' . $prevNewArticle->urlFriendly . '/' . $prevNewArticle->id ?>"><img src="<?= base_url() ?>assets/images/arrow_left.png"></a>
        <?php endif; ?>
    </div>
    <div class="column is-narrow container">

            <div class="columns spaceBetween">

                <div class="column articleHolder">

                    <div class="content">

                        <?php
                        if (isset($article)) {
                            ?>

                            <h2 class="title is-2"><?php echo $article->title ?></h2>

                            <div class="columns is-mobile  dateHolder">
                                <div class="column">
                                    <i class="ion ion-ios-clock-outline"></i>
                                    <?php if ($sesslang == 'hungarian'): ?>
                                        <?php echo date('Y. ', strtotime($article->startDate)) . ucfirst(lang($this->utils->getMonthName(date('m', strtotime($article->startDate))))) . ' ' . date('d.', strtotime($article->startDate)) ?>
                                    <?php else: ?>
                                        <?php echo date('d. ', strtotime($article->startDate)) . ucfirst(lang($this->utils->getMonthName(date('m', strtotime($article->startDate))))) . ' ' . date('Y.', strtotime($article->startDate)) ?>
                                    <?php endif ?>     
                                </div>
                                <div class="column">
                                    <i class="fa fa-pencil-square-o" data-wow-delay="250ms" aria-hidden="true"></i> <?= lang($article->categories) ?>
                                </div>
                            </div>

                            
                            <!-- <div class="blog-hero"
                                style="background-image: url('<?= base_url() ?>assets/images/news/hero_<?php echo $article->imageFileName ?>')">
                            </div> -->
                            <div class="picHolder">
                                <img src="<?= base_url() ?>assets/images/news/hero_<?php echo $article->imageFileName ?>" />                           
                            </div>
                            

                            <div class="preview_div fb"><?php echo $article->preview ?></div><br/>

                            <?php echo $this->utils->preg_match_youtube($article->content) ?>

                            <?php if ($article->categories != ""): ?>
                                <br/>
                                <span class="tags"><span><?= lang('Kategória') ?>:</span> <?php echo $article->categories ?></span>
                            <?php endif ?>

                            <?php if ($article->tags != ""): ?>                        
                                <span class="tags"><span><?= lang('Cimkék') ?>:</span> <?php echo $article->tags ?></span>
                            <?php endif ?>

                            <?php /* echo $this->utils->preg_match_gallery($article->content) */ ?>


                            <?php /* if (isset($gallery_items)):?>

                            <div class="fw embeddedGallery">
                            <h2>Kapcsolódó képgaléria</h2>
                            <?php foreach ($gallery_items as $image){?>
                            <a class="thumb" rel="slideshow-thumbs" title="<?php echo $image->title?>" href="<?php echo base_url()."assets/images/gallery/big/".$image->filename?>" style="background-image:url(<?php echo base_url()."assets/images/gallery/thumb/".$image->filename?>)"></a>
                            <?php }?>
                            </div>

                            <?php endif */ ?>



                            <div class="separator_line"></div>

                            <div class="social_row">

                                <div class="share_holder">
                                    <div class="fb"><?= lang('Ha tetszett a cikk, ne felejtsd el megosztani a barátaiddal') ?></div>


                                    <a class="fb" href="javascript:void(0)" onclick="return fbs_click()" title="Share on Facebook"><i class="fa fa-facebook-f" aria-hidden="true"></i></a>
                                    <a class="twitter" href="https://twitter.com/share?url=<?php echo "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" target="_blank" title="Share on Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>


                                    <a class="pinterest" href="" title="Share on Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>

                                    <a class="email" href="mailto:?subject=<?php echo lang('Website share') ?>&body=<?php echo "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" title="Share in E-mail"><i class="ion ion-ios-email"></i></a>
                                </div>



                            </div>

                            <div class="separator_line"></div>


                            <div class="comment_title"><?= lang('Mondd el mit gondolsz') ?></div>

                            <div class="fb-comments" data-href="<?php echo "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" data-width="100%" data-numposts="5"></div>

                        <?php } ?>

                    </div>

                </div>

            </div>

        </div>

    <div class="column next">
        <?php if (isset($nextNewArticle)):?>
        <a href="<?= base_url() . $newsPageUrl . '/' . $nextNewArticle->urlFriendly . '/' . $nextNewArticle->id ?>"><img src="<?= base_url() ?>assets/images/arrow_right.png"></a>
        <?php endif; ?>
    </div>

</section>

<?php $this->load->view('footer'); ?>



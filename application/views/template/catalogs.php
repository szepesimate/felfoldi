<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container">

        <div class="columns">

            <div class="column has-text-centered">

                <h1 class="title is-1"><?=lang('Catalogs')?></h1>

            </div>
        </div>

        <div class="columns has-text-centered gallery_type_boxes is-multiline">

            <?php if (isset($catalogs)): ?>

                <?php foreach ($catalogs as $catalog): ?>

                    <div class="column is-4">

                        <a href="<?php echo $catalog->property1 ?>">
                            <img alt="" src="<?php echo base_url() ?>assets/images/news/<?=$catalog->imageFileName?>" />
                            <span><?=$catalog->title?></span>
                        </a>

                    </div>

                <?php endforeach; ?>

            <?php endif ?>


        </div>

    </div>

</section>


<?php $this->load->view('footer'); ?>

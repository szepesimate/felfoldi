<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container">
        <?php
        if (isset($articles)) {
            foreach ($articles as $article) {
                ?>
                <div class="columns">
                    <div class="column">

                        <h1 class="title is-1 has-text-centered"><span><?= $article->title ?></span></h1>


                        <?php if (false && $article->imageFileName != ''): ?>
                            <img alt="<?= $article->title ?>" src="<?= base_url() ?>assets/images/news/<?= $article->imageFileName ?>" />
                        <?php endif ?>
                        <div class="content text-content"><?= $article->content ?></div>
                    </div>
                </div>
                <?php
            }
        }else {
            ?>

            <div class="columns">
                <div class="column is-9-desktop is-12-tablet">
                    
                    <div class="forBorder"></div>

                    <h1 class="title is-1"><?=lang('Feltöltés alatt')?>...</h1>

                    <div class="content text-content">
                        <p>
                        
                        </p>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>
    </div>
</section>

<?php $this->load->view('footer'); ?>

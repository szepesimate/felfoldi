<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container">

        <?php
        if (isset($galleries)) {

            foreach ($galleries as $gallery_arr) {
                $firstImage = $gallery_arr["images"][0];
                $url = base_url() . $galleryPageUrl . '/' . $gallery_arr["id"];
                ?>

                <div class = "column is-4 galleryBox">

                    <a class="galleryThumb" href="<?= $url ?>" title="<?php echo $firstImage->title ?>" style="background-image:url(<?php echo base_url() . "assets/images/gallery/big/" . $firstImage->filename ?>);"></a>

                    <a class="uppercase galleryName" href="<?= $url ?>"><?php echo $gallery_arr["galleryName"] ?></a>

                    <?php //echo $gallery_arr["galleryDescription"]?>

                </div>

                <?php
            }
        }
        ?>

    </div>
</section>

<?php $this->load->view('footer'); ?>


<div class="cat_icons has-text-centered">
    <a class="cat_all" href="<?= base_url() . $productPageUrl ?>"><?= preg_replace("/ /", '<br/>', lang('All Products'), 1) ?></a>
    <a class="face_care" href="<?= $categoryMenuItems[19]['urlFriendly'] ?>"><?= preg_replace("/ /", '<br/>', lang('Face care'), 1) ?></a>
    <a class="body_care" href="<?= $categoryMenuItems[20]['urlFriendly'] ?>"><?= preg_replace("/ /", '<br/>', lang('Body care'), 1) ?></a>
    <?php /* <a class="hair_care" href="<?=$categoryMenuItems[21]['urlFriendly']?>"><?= preg_replace("/ /",'<br/>',lang('Hair care'),1) ?></a> */ ?>
    <a class="relax_fit" href="<?= $categoryMenuItems[22]['urlFriendly'] ?>"><?= lang('_menu_item_Relax&fit')?></a>
    <a class="special_offers" href="<?= $categoryMenuItems[29]['urlFriendly'] ?>"><?= preg_replace("/ /", '<br/>', lang('Best deals'), 1) ?></a>
</div>
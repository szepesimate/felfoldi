<?php $this->load->view('header'); ?>


<section class="section txt-article">

    <div class="container">

        <div class="content">

            <h1 class="title is-1"><?=lang('Sitemap')?></h1>

            <?php foreach ($menuItems as $dbId => $item): ?>
                <?php
                if ($item["inMainMenu"] == 0 && $item["inFooterMenu"] == 0) {
                    continue;
                }
                ?>

                <a<?php
                if ($item['subItems']) {
                    echo ' class="hasSubmenu"';
                }
                ?> href="<?= $item["urlFriendly"] ?>"><?= lang($item["name"]) ?></a><br/>

                <?php if ($item['subItems']): ?>
                    <?php foreach ($item['subItems'] as $subItem): ?>
                        <?php
                        if ($subItem["inMainMenu"] == 0 && $subItem["inFooterMenu"] == 0) {
                            continue;
                        }
                        ?>
                        <a class="subItem" href="<?= $subItem["urlFriendly"] ?>"><nobr>- <?= $subItem["name"] ?></nobr></a><br/>
                            <?php endforeach ?>
                        <?php endif ?>
                    <?php endforeach ?>

        </div>

    </div>

</section>


<?php $this->load->view('footer'); ?>
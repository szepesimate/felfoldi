<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container">

        <div class="columns">

            <div class="column has-text-centered">
                <h1 class="title is-1 borderBottom"><?= '<b>' . lang('Competitions') . '</b>' ?></h1>
            </div>
            

        </div>

        <div class="columns">

            <div class="column">

                <br/>

                <?php if ($articles) {
                    ?>

                    <div class="columns is-multiline">

                        <?php
                        for ($j = 0; $j < count($articles); $j++) {
                            $article = $articles[$j];
                            $url = base_url() . $this->uri->segment(1) . '/' . $this->utils->convertUrlFormat($article->title) . '/' . $article->id;
                            ?>

                            <div class="column is-12 competition_box">

                                <a href="<?php echo $url ?>"><h2 class="title is-2"><?= $article->title ?></h2></a>

                                <div class="blue">
                                    <i class="ion ion-ios-clock-outline"></i>
                                    <?php if ($sesslang == 'hungarian'): ?>
                                        <?php echo date('Y. ', strtotime($article->startDate)) . ucfirst(lang($this->utils->getMonthName(date('m', strtotime($article->startDate))))) . ' ' . date('d.', strtotime($article->startDate)) ?>
                                    <?php else: ?>
                                        <?php echo date('d. ', strtotime($article->startDate)) . ucfirst(lang($this->utils->getMonthName(date('m', strtotime($article->startDate))))) . ' ' . date('Y.', strtotime($article->startDate)) ?>
                                    <?php endif ?>     
                                </div>


                                <p>
                                    <?php echo strip_tags($article->preview) ?>
                                    <br/>
                                    <a href="<?php echo $url ?>"><?= lang('read more') ?> &raquo;</a>
                                </p>
                            </div>

                            <?php
                        }
                        ?>

                        <div class="column is-12 pagination">
                            <?php echo $this->pagination->create_links(); ?>                        
                        </div>

                    </div>
                    <?php
                }
                ?>

            </div>

        </div>

    </div>

</section>

<?php $this->load->view('footer'); ?>

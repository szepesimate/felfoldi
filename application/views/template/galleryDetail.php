<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container">
        
        <div id="tile_gallery" style="display:none;">

            <?php if ($galleryImages) {?>
                <?php foreach ($galleryImages as $image) { ?>
                    <img alt="<?= $image->title ?>" src="<?= base_url() ?>assets/images/gallery/thumb/<?= $image->filename ?>"
                         data-image="<?= base_url() ?>assets/images/gallery/big/<?= $image->filename ?>"
                         data-description="<?= $image->title ?>">
                     <?php } ?>
                 <?php } ?>
        </div>
        
    </div>
</section>

<?php $this->load->view('footer'); ?>
<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container">

        <div class="columns">
            <div class="column has-text-centered">

                <h1 class="title is-1"><b><?= lang('Resources') ?> -</b> <?= lang("Product photos, catalogs") ?></h1>

                <?php
                if (isset($articles)) {
                    foreach ($articles as $article) {
                        echo '<div class="topArticle">' . $article->content . '</div>';
                    }
                }
                ?>

            </div>
        </div>

        <?php
        $productImage_path = base_url() . 'assets/images/webshop/big/';
        $docs_path = base_url() . 'assets/docs/';
        $download_ctrl_path = base_url() . 'myDownloader/';
        ?>


        <span class="resource_title" onclick="$('#productPhotosTable').slideToggle()">Product photos |</span>  <span class="resource_format">.jpg format 585px x 1000px</span>

        <br/>

        <table id="productPhotosTable" class="table is-striped" style="display:none">
            <tr>
                <td>&nbsp;</td>
                <td width="90" class="has-text-centered"><b>view</b></td>
                <td width="90" class="has-text-centered"><b>download</b></td>
            </tr>

            <?php if ($recentProducts): ?>
                <?php foreach ($recentProducts as $product): ?>
                    <?php
                    $base_name = basename($product->imageFileName);
                    ?>
                    <tr>
                        <td><b><?= $product->productObject->name ?></b> <?= $product->productObject->fajta ?></td>
                        <td class="has-text-centered"> <a target="_blank" href="<?= $productImage_path . $base_name ?>"><i class="fa fa-eye"></i></a> </td>
                        <td class="has-text-centered"> <a href="<?= $download_ctrl_path ?>productImage/<?= $base_name ?>"><i class="fa fa-cloud-download"></i></a> </td>
                    </tr>
                <?php endforeach; ?>            
            <?php endif; ?>

        </table>

        <br/>


        
        
        
        
        
        <span class="resource_title" onclick="$('#productSheetTable').slideToggle()">Product sheet |</span>  <span class="resource_format">.pdf format</span>

        <br/>

        <table id="productSheetTable" class="table is-striped" style="display:none">
            <tr>
                <td>&nbsp;</td>
                <td width="90" class="has-text-centered"><b>view</b></td>
                <td width="90" class="has-text-centered"><b>download</b></td>
            </tr>

            <?php if ($recentProducts): ?>
                <?php foreach ($recentProducts as $product): ?>
                    <tr>
                        <td><b><?= $product->productObject->name ?></b> <?= $product->productObject->fajta ?></td>
                        <td class="has-text-centered"> <a target="_blank" href="<?= base_url()?>Pdf_gen/generate/<?=$product->productObject->id?> ?>"><i class="fa fa-eye"></i></a> </td>
                        <td class="has-text-centered"> <a href="<?= base_url()?>Pdf_gen/generate/<?=$product->productObject->id?>/?forceDownload=1"><i class="fa fa-cloud-download"></i></a> </td>
                    </tr>
                <?php endforeach; ?>            
            <?php endif; ?>

        </table>

        <br/>
        
        



        <span class="resource_title" onclick="$('#catalogTable').slideToggle()">Catalogs |</span>  <span class="resource_format">.pdf format</span>

        <br/>

        <table id="catalogTable" class="table is-striped" style="display:none">
            <tr>
                <td>&nbsp;</td>
                <td width="90" class="has-text-centered"><b>view</b></td>
                <td width="90" class="has-text-centered"><b>download</b></td>
            </tr>
            <tr>
                <td><b>Felfoldi catalog 2018</b></td>
                <td class="has-text-centered"> <a target="_blank" href="<?= $docs_path ?>FELFOLDI_catalog_2018.pdf"><i class="fa fa-eye"></i></a> </td>
                <td class="has-text-centered"> <a href="<?= $download_ctrl_path ?>catalog/FELFOLDI_catalog_2018.pdf"><i class="fa fa-cloud-download"></i></a> </td>
            </tr>
            <tr>
                <td><b>Classic Kitchen product catalog</b></td>
                <td class="has-text-centered"> <a target="_blank" href="<?= $docs_path ?>CK_2017_catalog.pdf"><i class="fa fa-eye"></i></a> </td>
                <td class="has-text-centered"> <a href="<?= $download_ctrl_path ?>catalog/CK_2017_catalog.pdf"><i class="fa fa-cloud-download"></i></a> </td>
            </tr>
            <tr>
                <td><b>Quick Products</b></td>
                <td class="has-text-centered"> <a target="_blank" href="<?= $docs_path ?>QUICK_catalog_2017.pdf"><i class="fa fa-eye"></i></a> </td>
                <td class="has-text-centered"> <a href="<?= $download_ctrl_path ?>catalog/QUICK_catalog_2017.pdf"><i class="fa fa-cloud-download"></i></a> </td>
            </tr>
            <tr>
                <td><b>The Million Chocolate</b></td>
                <td class="has-text-centered"> <a target="_blank" href="<?= $docs_path ?>TMC_catalog_2017.pdf"><i class="fa fa-eye"></i></a> </td>
                <td class="has-text-centered"> <a href="<?= $download_ctrl_path ?>catalog/TMC_catalog_2017.pdf"><i class="fa fa-cloud-download"></i></a> </td>
            </tr>
            <tr>
                <td><b>Innovative Sweets</b></td>
                <td class="has-text-centered"> <a target="_blank" href="<?= $docs_path ?>Candy_Catalog_2017.pdf"><i class="fa fa-eye"></i></a> </td>
                <td class="has-text-centered"> <a href="<?= $download_ctrl_path ?>catalog/Candy_Catalog_2017.pdf"><i class="fa fa-cloud-download"></i></a> </td>
            </tr>
            <tr>
                <td><b>Classic Kitchen Cake Decor</b></td>
                <td class="has-text-centered"> <a target="_blank" href="<?= $docs_path ?>CK_CAKE_DEKOR_2016_catalog.pdf"><i class="fa fa-eye"></i></a> </td>
                <td class="has-text-centered"> <a href="<?= $download_ctrl_path ?>catalog/CK_CAKE_DEKOR_2016_catalog.pdf"><i class="fa fa-cloud-download"></i></a> </td>
            </tr>
        </table>



    </div>

</section>

<?php $this->load->view('footer'); ?>

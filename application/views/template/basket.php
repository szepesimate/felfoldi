<?php $this->load->view('header'); ?>

<section class="section txt-article busket-section">

    <div class="container">

        <div class="columns">
            <div class="column has-text-centered">
                <h1 class="title is-1"><span><?= lang('Basket') ?></span></h1>
            </div>
        </div>


        <br/>

        <div class="column">
                <div class="content has-text-centered" style="padding-bottom: 20px">
                    <?php if (isset($articles)): ?>
                        <?= $articles[0]->preview ?>
                    <?php endif ?>
                </div>
            </div>

        <br/>

        <div class="columns">
            <div class="column">

                <div class="" style="display:<?php echo isset($validation_userData) ? 'block' : 'none' ?>" id="adatok_megadasa_div">

                    <?php if (!isset($userData)): ?>

                        <?=lang('To submit order, please')?> <a href="javascript:void()" class="loginBtnFizetes"><?=lang('log in')?></a>,
                        <?=lang('or fill out the following form!')?>

                        <br class="clear"/>
                        <br class="clear"/>

                    <?php endif ?>

                    <?php
                    if (isset($validation_userData)) {
                        echo $validation_userData;
                    }



                    //form data
                    $attributes = array('class' => '', 'id' => 'megrendelesForm');

                    echo form_open('', $attributes);
                    ?>

                    <div class="columns is-multiline labelAnim">

                        <div class="column is-6 is-4-desktop">
                            <h3 class="title is-3"><?=lang('Contact details')?></h3>

                            <br/>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Name') ?>:</label>
                                    <input type="text" class="input" name="name" value="<?php echo set_value('name', isset($userData) ? $userData->name : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('E-mail') ?>:</label>
                                    <input type="text" class="input" name="email" value="<?php echo set_value('email', isset($userData) ? $userData->email : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Phone') ?>:</label>
                                    <input type="text" class="input" name="phone" value="<?php echo set_value('phone', isset($userData) ? $userData->phone : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Note') ?>:</label>
                                    <input type="text" class="input" name="comment" value="<?php echo set_value('comment', ''); ?>">
                                </p>
                            </div>


                        </div>

                        <div class="column is-6 is-4-desktop">

                            <h3 class="title is-3"><?=lang('Billing details')?></h3>

                            <br/>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Name') ?>:</label>
                                    <input type="text" class="input" name="billing_name" value="<?php echo set_value('billing_name', isset($userData) ? $userData->billing_name : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Zip') ?>:</label>
                                    <input type="text" class="input" name="billing_zip" value="<?php echo set_value('billing_zip', isset($userData) ? $userData->billing_zip : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Country') ?>:</label>
                                <div class="select" style="margin-top:12px">
                                    <?php echo form_dropdown('billing_region', $regionOptions, set_value('billing_region')); ?>
                                </div>
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('City') ?>:</label>
                                    <input type="text" class="input" name="billing_city" value="<?php echo set_value('billing_city', isset($userData) ? $userData->billing_city : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Address') ?>:</label>
                                    <input type="text" class="input" name="billing_street" value="<?php echo set_value('billing_street', isset($userData) ? $userData->billing_street : ''); ?>">
                                </p>
                            </div>



                            <label style="float:left;clear:both;"><input class="fieldCopyCheckbox" type="checkbox" /> <?=lang('Shipping details are the same')?></label>

                        </div>

                        <div class="column is-6 is-4-desktop szall_div">

                            <h3 class="title is-3"><?=lang('Shipping details')?></h3>

                            <br/>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Name') ?>:</label>
                                    <input type="text" class="input" name="delivery_name" value="<?php echo set_value('delivery_name', isset($userData) ? $userData->delivery_name : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Zip') ?>:</label>
                                    <input type="text" class="input" id="" name="delivery_zip" value="<?php echo set_value('delivery_zip', isset($userData) ? $userData->delivery_zip : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Country') ?>:</label>
                                <div class="select" style="margin-top:12px">
                                    <?php echo form_dropdown('delivery_region', $regionOptions, set_value('delivery_region'), 'id="delivery_region_field"'); ?>
                                </div>
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('City') ?>:</label>
                                    <input type="text" class="input" name="delivery_city" value="<?php echo set_value('delivery_city', isset($userData) ? $userData->delivery_city : ''); ?>">
                                </p>
                            </div>

                            <div class="field">                        
                                <p class="control">
                                    <label class="label"><?= lang('Address') ?>:</label>
                                    <input type="text" class="input" name="delivery_street" value="<?php echo set_value('delivery_street', isset($userData) ? $userData->delivery_street : ''); ?>">
                                </p>
                            </div>

                        </div>

                    </div>

                    <br class="clear"/>
                    <br class="clear"/>
                    <br class="clear"/>

                    <div class="columns is-multiline">

                        <div class="column is-12 is-4-desktop">

                            <h3 class="title is-3"><?=lang('Delivery mode')?></h3>

                            <div class="select">
                                <?php echo form_dropdown('delivery_mode', $deliveryOptions, set_value('delivery_mode'), 'id="delivery_mode_select"'); ?>
                            </div>

                        </div>

                        <div class="column is-12 is-4-desktop">

                            <h3 class="title is-3"><?=lang('Payment mode')?></h3>

                            <div class="select">
                                <?php echo form_dropdown('payment_mode', $payOptions, set_value('payment_mode'), 'id="payment_mode_select"'); ?>
                            </div>

                            <input type="hidden" name="megrendeles" value="1" />
                            <input type="hidden" id="shipping_cost_hidden_field" name="shipping_cost" value="0" />

                        </div>
                    </div>

                    <div class="columns">

                        <div class="column">

                            <p class="chk">
                                <?php
                                $data = array(
                                    'name' => 'aszf',
                                    'id' => 'terms_checkbox',
                                    'value' => '1',
                                    'checked' => set_value('aszf', false),
                                    'class' => 'checkbox-style',
                                );

                                echo form_checkbox($data);
                                ?>

                                <label for="terms_checkbox">
                                    <?= lang('I’ve read and accepted') ?> <a target="_blank" href="<?= $adatvedelem_page_url ?>"><?= lang('the Privacy Policy') ?></a>
                                    <?= lang('and') ?> <a target="_blank" href="<?= $aszf_page_url ?>"><?= lang('the Terms&Conditions') ?></a>
                                </label>
                            </p>

                            <div class="paycard-list">
                                <h3 class="title"><?= lang('BIZTONSÁGOS BANKKÁRTYÁS FIZETÉS') ?></h3>
                                <a class="paycard-item" href="https://www.barion.com/"><img src="assets/images/barion.jpg"/></a>
                                <a class="paycard-item"><img src="assets/images/visa1.jpg"/></a>
                                <a class="paycard-item"><img src="assets/images/visa2.png"/></a>
                                <a class="paycard-item"><img src="assets/images/mastercard1.png"/></a>
                                <a class="paycard-item"><img src="assets/images/mastercard2.png"/></a>
                                <a class="paycard-item"><img src="assets/images/amex.jpg"/></a>
                            </div>

                        </div>

                    </div>

                    <?php echo form_close(); ?>

                    <br class="clear"/>
                    <br class="clear"/>
                    <br class="clear"/>

                </div>






                <?php if (isset($basketTableRows["rows"])) { ?>



                    <table class="basket_table" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th align="left" colspan="2"><?=lang('Product name')?></th>
                                <th><?=lang('Unit price')?></th>
                                <th><?=lang('Quantity')?></th>
                                <th><?=lang('Value')?></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($basketTableRows["rows"] as $row) {
                                ?>
                                <tr>
                                    <td class="first">
                                        <a class="image" style="background-image:url(<?php echo base_url() . "assets/images/webshop/thumb/" . $row->imageFileName ?>)" href="<?php echo $row->url ?>"></a>
                                    </td>
                                    <td class="txtl">
                                        <a class="name" href="<?php echo $row->url ?>"><?php echo $row->name ?></a>
                                    </td>
                                    <td>
                                        <?php
                                        if ($row->oldPrice > 0) {
                                            echo '<nobr><span class="oldPrice lt">' . $row->oldPrice . ' Ft</span></nobr>';
                                        }
                                        echo '<nobr><span class="price">' . $row->displayPrice . ' Ft</span></nobr>';
                                        ?>
                                    </td>
                                    <td>
                            <nobr>
                                <a class="rectangleButton minus" href="javascript:void(0)">–</a>
                                &nbsp;<a class="rectangleButton numberHolder" href="javascript:void(0)"><?php echo $row->count ?></a>&nbsp;
                                <a class="rectangleButton plus" href="javascript:void(0)">+</a>
                                <input type="hidden" class="itemNumberHolder" value="<?php echo $row->itemNumber ?>" />
                            </nobr>

                            </td>
                            <td class="partialSumPrice">
                            <nobr><?php echo $row->partialSumPrice ?> Ft</nobr>
                            </td>
                            <td class="last">
                                <button onclick="removeFromBasket('<?php echo $row->itemNumber ?>');"><i class="fa fa-trash"></i></button>
                            </td>
                            </tr>
                            <?php
                        }
                        ?>

                        <tr>
                            <td colspan="3" class="noborder">&nbsp;</td>
                            <td width="135">
                                <?=lang('Product total')?>
                                <br/>
                                <span id="bevaltvaHolder"></span>
                            </td>
                            <td width="100" class="last product_sumHolder">

                            </td>
                            <td class="last noborder">
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3" class="noborder">&nbsp;</td>
                            <td> <?=lang('Shipping')?> </td>
                            <td class="last shipping_costHolder">

                            </td>
                            <td class="last noborder">
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3" class="noborder">&nbsp;</td>
                            <td class="fb"> <?=lang('Total')?>: </td>
                            <td class="last overallHolder fb">

                            </td>
                            <td class="last noborder">
                                &nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>


                    <div class="fw nextBtnHolder">

                        <?php /*
                          $attributes = array('class' => '', 'id' => 'couponForm');

                          echo form_open('', $attributes);
                          ?>

                          <div class="fw labelAnim">
                          <label class="sectionLabel" for="couponField">Kuponkód:</label>
                          <input type="text" id="couponField" class="field" name="couponCode" value="<?php if ($this->session->userdata('coupon')) echo $this->session->userdata('coupon')['code']; ?>" />

                          <input type="submit" class="button" name="couponSend" value="beváltom" />
                          </div>

                          <?php echo form_close(); */ ?>


                        <br class="is-clearfix" />


                        <div class="has-text-right">
                            <button class="button" style="margin-right:60px;display:<?php echo isset($validation_userData) ? 'none' : 'inline-block' ?>" id="adatok_megadasa_btn" onclick="$('#adatok_megadasa_div').fadeIn();$('html, body').animate({scrollTop: $('#adatok_megadasa_div').top - 30}, 500);$(this).hide();$('#megrendeles_btn').fadeIn();"><?=lang('Adatok megadása')?></button>

                            <button class="button" style="margin-right:60px;display:<?php echo isset($validation_userData) ? 'inline-block' : 'none' ?>" id="megrendeles_btn" type="button" onclick="$('#megrendelesForm').submit()"><?=lang('Rendelés leadása')?></button>
                        </div>

                    </div>

                    <br class="clear"/>
                    <br class="clear"/>




                <?php } else { ?>

                    <br/><br/>
                    <?=lang('Your cart is empty!')?>
                    <br/><br/><br/><br/>

                <?php } ?>
            </div>
        </div>
    </div>

</section>


<?php $this->load->view('footer'); ?>

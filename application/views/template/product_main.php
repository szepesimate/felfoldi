<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container is-fluid">

        <div class="columns">

            <div class="column">
                <div class="content has-text-centered topArticle">
                    <?php if (isset($articles)): ?>
                        <h1 class="title is-1 borderBottom"><?= $articles[0]->title ?></h1>
                        
                        <br/>                        
                        <img alt="natural products" src="<?= base_url()?>assets/images/natural_products.jpg" />
                        &nbsp; &nbsp;
                        <img alt="not tested on animals" src="<?= base_url()?>assets/images/not_tested.jpg" />                        
                        <br/><br/>
                        
                        <?= $articles[0]->content ?>
                    <?php endif ?>
                </div>
            </div>

        </div>

        <!-- <div class="columns catBoxHolder is-multiline">            
            <?php //$this->load->view('template/_cat_row'); ?>            
        </div> -->

        <div class="columns">

            <div class="column">
                <div class="content has-text-centered">
                    <h1 class="title is-1 allProductsTitle"><span><?= isset($searchResultProducts) ? lang('Search result') : lang('__all_products__') ?></span></h1>
                </div>
            </div>

        </div>

        <div class="columns is-multiline boxesInner">

            <?php if (isset($recentProducts) && $recentProducts) { ?>

                <?php
                $data['productBoxes'] = $recentProducts;
                $this->load->view("template/productBoxes.php", $data);
                ?>

            <?php } ?>


            <?php /* if (isset($discountProducts) && $discountProducts){ ?>
              <br class="clear"/>
              <br class="clear"/>
              <h2 class="topTitle"><span class="line"></span>Akciós termékek<span class="line"></span></h2>

              <?php
              $data['productBoxes'] = $discountProducts;
              $this->load->view("template/productBoxes.php", $data);
              ?>

              <?php } */ ?>


            <?php if (isset($searchResultProducts) && $searchResultProducts) { ?>
                
                <?php
                $data['productBoxes'] = $searchResultProducts;
                $this->load->view("template/productBoxes.php", $data);
                ?>

            <?php } ?>



        </div>

        <div class="fw moreElementsTrigger">
            <br class="clear"/><br/>
        </div>



    </div>

</section>


<?php $this->load->view('footer'); ?>

<?php
$defaultImagePath = base_url() . "assets/images/webshop/thumb/default.jpg";

$cnt = 0;
//for ($k = 0; $k < 5; $k++) {
foreach ($productBoxes as $product) {
    $cnt++;
    $stockClass = $product->productObject->stock ? 'in_stock blue' : 'out_of_stock red';
    $stockStr = $product->productObject->stock ? '<span class="stockTick"></span>' . lang('raktáron') : '<span class="txt22">x</span> ' . lang('nincs raktáron');
    ?><div class="column webshop_box<?php
    if ($cnt >= 16)
        echo ' hidden';
    ?>">
        <a href="<?php echo $product->link ?>">
            <img class="product_img" alt="<?php echo $product->productObject->{"name"} ?>" <?php /* if ( isset($product->imageSize) && $product->imageSize[1] / $product->imageSize[0] > 0.9 ): ?> class="alloKep" <?php endif */ ?> src="<?php
            if ($cnt < 16 && isset($product->imageFileName))
                echo $product->imageFileName;
            else
                echo $defaultImagePath;
            ?>" <?php if (/* $cnt >= 17 && */ isset($product->imageFileName)) { ?> data-imgsrc="<?php echo $product->imageFileName ?>" <?php } ?>  
                 <?php if (isset($product->imageFileName2)) { ?> data-imgsrc2="<?php echo $product->imageFileName2 ?>" <?php } ?>
                 />
        </a>

        <span class="titleOverlay">
            <div class="stockRow <?= $stockClass ?>"><?= $stockStr ?></div>
            <div class="nev"><a href="<?php echo $product->link ?>"><?php echo $product->productObject->{"name"} ?></a></div>
            <div class="type"><span style="font-size:14px"><?php echo $product->productObject->{"type"} ?></span></div>
            <div class="priceHolder">


                <?php if ($product->productObject->discountPrice > 0): ?>                    
                    <span class="discountPrice"><?php echo $product->productObject->discountPrice ?>&nbsp;Ft</span>                                        
                    <span class="oldPrice"><?php echo $product->productObject->price ?>&nbsp;Ft</span>
                <?php else: ?>  
                    <span class="price"><?php echo $product->productObject->price ?>&nbsp;Ft</span>
                <?php endif ?>  
            </div>
        </span>

    </div><?php
}
//}
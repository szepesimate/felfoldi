<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container">

        <div class="columns">

            <div class="column">
                <div class="content has-text-centered">
                    <?php if (isset($articles)): ?>
                        <h2 class="title is-1"><span><?= $articles[0]->title ?></span></h2>
                        <!-- <h1 class="title is-1 borderBottom"><?= $articles[0]->title ?></h1> -->
                        <?= $articles[0]->preview ?>
                    <?php endif ?>
                </div>
            </div>
            
        </div>
        
    </div>

    <div class="container form">
        
        <div class="columns">

            <div class="column is-6">

                <form id="contactForm">

                    <p class="rjttt"><input type="text" name="phone" /></p>
                    
                    <div class="field">
                        <p class="control">
                            <input class="input" type="text" name="name" placeholder="<?=lang('Your name')?>:*">
                        </p>
                    </div>


                    <div class="field">
                        <p class="control">
                            <input class="input" type="text" name="email" placeholder="<?=lang('E-mail address')?>:*">
                        </p>
                    </div>


                    <div class="field">
                        <p class="control">
                            <input class="input" type="text" name="phone_" placeholder="<?=lang('Your phone number (width dialing code)')?>:*">
                        </p>
                    </div>

                    <div class="field">
                        <p class="control">
                            <textarea class="textarea" name="message" placeholder="<?=lang('Your message')?>:*"></textarea>
                        </p>
                    </div>

                    <button type="submit" class="button"><i class="ion ion-ios-email"></i> <?=lang('Send')?></button>

                </form>

            </div>


            <div class="column is-6">

                <div class="content right">

                    <?php if (isset($articles)): ?>
                        <?= $articles[0]->content ?>
                    <?php endif ?>
                    
                    <br/><br/>
                    
                    <h5><?=lang('Stay connected on facebook')?></h5>
                    
                    <br/>
                    
                    <div class="fb-like" 
                         data-href="https://www.facebook.com/FelfoldiInyencIzmuves" 
                         data-layout="standard" 
                         data-action="like" 
                         data-show-faces="true">
                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

<?php $this->load->view('footer'); ?>
<?php $defaultImagePath = base_url() . "assets/images/webshop/thumb/default.jpg"; ?>

<a class="mCat<?php
if (in_array('product_main', $bodyClasses)) {
    echo ' active';
}
?>" href="<?= base_url() . $productPageUrl ?>"><?= lang("All Products") ?></a>

<?php foreach ($categoryMenuItems as $category) { ?>

    <a class="mCat<?php
    if ($this->page->category["id"] == $category["id"]) {
        echo ' active';
    }
    ?>" href="<?= $category["urlFriendly"] ?>"><?= lang($category["name"]) ?><?php /* (<?= $category["count"] ?>) */ ?></a>

    <?php if ($this->page->category["id"] == $category["id"] && $category['subItems']): ?>

        <?php foreach ($category['subItems'] as $subItem) { ?>
            <?php
            $extraClass = "";
            if ($this->product->bottomCat->id == $subItem["id"]) {
                $extraClass = " active";
            }
            ?>
            <a class="subCat<?= $extraClass ?>" href="<?= $subItem["urlFriendly"] ?>"><?= lang($subItem["name"]) ?></a>
        <?php } ?>

    <?php endif; ?>

<?php } ?>
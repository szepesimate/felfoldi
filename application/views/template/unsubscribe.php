<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container narrower">

        <div class="columns">
            <div class="column has-text-centered">
                <h1 class="title is-1"><span><?= lang('__unsubscribe_title__') ?></span></h1>

                <p style="margin-top: 10px">
                    <?php echo lang('__unsubscribe_description__'); ?>
                </p>


                <?php echo form_open('', ['id' => 'unsubForm', 'style' => 'max-width:460px;margin:40px auto 0 auto;']); ?>

                <div class="field">
                    <p class="control">
                        <label class="label"><?= lang('E-mail address') ?>:<sup>*</sup></label>
                        <input type="text" class="input" placeholder="<?= lang('Please enter your e-mail address') ?> ..." name="email" />
                    </p>
                </div>

                <div class="field">
                    <p class="control">
                        <input type="submit" class="button is-fullwidth" name="submit" value="<?= lang('__Unsubscribe__') ?>" />
                    </p>
                </div>

                <?php echo form_close(); ?>

            </div>
        </div>

    </div>

</section>

<?php $this->load->view('footer'); ?>

<?php
$maxCount = 4;
if ($maxCount > count($latestNews)) {
	$maxCount = count($latestNews);
}
for ($j = 0; $j < $maxCount; $j++) {
	$article = $latestNews[$j];
	$url = base_url() . $this->uri->segment(1) . $newsPageUrl . '/' . $this->utils->convertUrlFormat($article->title) . '/' . $article->id;
	?>

	<div class="tile-box">
        <div class="tile-box-inner">
            <a class="tile-box-image" href="<?php echo $url ?>" style="background-image: url('<?= base_url() ?>assets/images/news/<?php echo $article->imageFileName ?>')">
                <div class="title-wrapper">
					<h3 class="title is-3"><?= $article->title ?></h3>
				</div>
            </a>
        </div>
	</div>

	<?php
}
?>
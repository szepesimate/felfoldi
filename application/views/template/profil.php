<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container">

        <div class="columns">
            <div class="column has-text-centered">

                <h1 class="title is-1"><?= lang("Modify your profile") ?></h1>

                <?php
                if (isset($articles)) {
                    foreach ($articles as $article) {
                        echo $article->content;
                    }
                }
                ?>

            </div>
        </div>


        <br/>
        <br/>


        <?php echo form_open('', ['id' => 'profilForm']); ?>

        <div class="columns is-multiline">

            <div class="column is-6 is-4-desktop">
                <h3 class="title is-3"><?= lang('Personal details') ?></h3>

                <br/>

                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Name') ?>:</label>
                        <input type="text" class="input" name="name" value="<?= $userData->name ?>">
                    </p>
                </div>

                <div class="field">                        
                    <p class="control">
                        <label class="label">E-mail:</label>
                        <input type="text" class="input" name="email" value="<?= $userData->email ?>">
                    </p>
                </div>

                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Phone') ?>:</label>
                        <input type="text" class="input" name="phone_" value="<?= $userData->phone ?>">
                    </p>
                </div>
                
                <br/><br/>

                <h3 class="title is-3"><?= lang('Login details') ?></h3>

                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Password') ?>:</label>
                        <input type="password" class="input" name="password" />
                    </p>
                </div>

                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Retype password') ?>:</label>
                        <input type="password" class="input" name="password_re" />
                    </p>
                </div>
                
                

                <?php /*
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('VAT') ?>:</label>
                        <input type="text" class="input" name="vat" value="<?= $userData->vat ?>">
                    </p>
                </div>

                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Web') ?>:</label>
                        <input type="text" class="input" name="web" value="<?= $userData->web ?>">
                    </p>
                </div>
                */ ?>

            </div>

            <div class="column is-6 is-4-desktop">

                <h3 class="title is-3"><?= lang('Billing details') ?></h3>

                <br/>
                
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Billing name') ?>:</label>
                        <input type="text" class="input" name="billing_name" value="<?= $userData->billing_name ?>">
                    </p>
                </div>
                
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Billing zip') ?>:</label>
                        <input type="text" class="input" name="billing_zip" value="<?= $userData->billing_zip ?>">
                    </p>
                </div>
                
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Billing city') ?>:</label>
                        <input type="text" class="input" name="billing_city" value="<?= $userData->billing_city ?>">
                    </p>
                </div>
                
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Billing address') ?>:</label>
                        <input type="text" class="input" name="billing_street" value="<?= $userData->billing_street ?>">
                    </p>
                </div>            
                

            </div>

            <div class="column is-6 is-4-desktop">

                
                <h3 class="title is-3"><?= lang('Delivery details') ?></h3>

                <br/>
                
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Delivery name') ?>:</label>
                        <input type="text" class="input" name="delivery_name" value="<?= $userData->delivery_name ?>">
                    </p>
                </div>
                
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Delivery zip') ?>:</label>
                        <input type="text" class="input" name="delivery_zip" value="<?= $userData->delivery_zip ?>">
                    </p>
                </div>
                
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Delivery city') ?>:</label>
                        <input type="text" class="input" name="delivery_city" value="<?= $userData->delivery_city ?>">
                    </p>
                </div>
                
                <div class="field">                        
                    <p class="control">
                        <label class="label"><?= lang('Delivery address') ?>:</label>
                        <input type="text" class="input" name="delivery_street" value="<?= $userData->delivery_street ?>">
                    </p>
                </div>
                
            </div>





            <div class="column is-12">

                <div class="field">                        
                    <p class="control">
                        <input type="submit" class="button" name="save" value="<?= lang("Save") ?>" />
                    </p>
                </div>

            </div>


        </div>

        <?php echo form_close(); ?>


    </div>

</section>

<?php $this->load->view('footer'); ?>

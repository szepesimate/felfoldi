<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container is-fluid breadCrumbHolder">

        <?php if (isset($breadCrumb)): ?>

            <?php $cnt = 0; ?>
            <?php foreach ($breadCrumb as $item => $link): ?>
                <?php if ($cnt > 0): ?>
                    &nbsp;>&nbsp;
                <?php endif ?>
                <a href="<?php echo $link ?>"><?php echo lang($item) ?></a>

                <?php $cnt++; ?>
            <?php endforeach ?>

        <?php endif ?>

    </div>


    <div class="container is-fluid">

        <div class="columns">

            <div class="column">
                <div class="content has-text-centered topArticle">
                    <?php
                    echo '<h1 class="title is-1 borderBottom"><span>' . lang($this->product->currentCategoryArray['mainCat']->name) . '</span></h1>';
                    echo '<p>' . lang($this->product->currentCategoryArray['mainCat']->description) . '</p>';
                    ?>
                </div>
            </div>

        </div>

        <?php
        if ($this->page->category["id"] != 21) {
            ?>

            <div class="columns">

                <div class="column has-text-centered catSelect">

                    <div class="select">
                        <select id="product_category_select">
                            <option value="<?php echo base_url() . $productPageUrl . '/' . $this->uri->segment(2) ?>"><?= lang('__all_products__') ?></option>
                            <?php foreach ($categoryMenuItems as $category) { ?>
                                <?php
                                if ($this->page->category["id"] == $category["id"] && $category['subItems']):
                                    ?>
                                    <?php foreach ($category['subItems'] as $subItem) { ?>
                                        <option value="<?= $subItem["urlFriendly"] ?>"<?php if ($this->product->bottomCat->id == $subItem["id"]) echo ' selected'; ?>><?= lang($subItem["name"]) ?></option>
                                    <?php } ?>

                                <?php endif ?>

                            <?php } ?>
                        </select>
                    </div>

                </div>

            </div>
        <?php } ?>

        <div class="columns">

            <?php if ($this->product->currentCategoryArray) { ?>



                <?php /*
                  <div class="fw order_row">

                  <?php echo form_open('', ['class' => 'productSearchForm']); ?>

                  <input type="text" class="field" placeholder="Keresendő szó, vagy kifejezés..." value="<?php if (isset($searchParam)) echo $searchParam ?>" />
                  <input type="image" class="btn" name="send" src="<?= base_url() ?>assets/images/magnifier_black.png" />

                  <?php echo form_close(); ?>


                  <?php $order_session = $this->session->userdata('product_order'); ?>
                  <div class="select-style">
                  <select id="product_order_select">
                  <option value="recent">rendezés újdonságok szerint</option>
                  <option value="price_asc"<?php if ($order_session == 'price_asc') echo ' selected'; ?>>rendezés ár szerint növekvő</option>
                  <option value="price_desc"<?php if ($order_session == 'price_desc') echo ' selected'; ?>>rendezés ár szerint csökkenő</option>
                  </select>
                  </div>

                  </div>
                 */ ?>

                <?php
                /*
                  <h2 class="topTitle"><?= $bottomCat->name ?></h2>
                 */
                ?>

            <?php } ?>


            <?php if (isset($productBoxes)) { ?>
                <div class="columns is-multiline boxesInner">
                    <?php $this->load->view("template/productBoxes.php"); ?>
                </div>
            <?php } else if (isset($categoryBoxes)) { ?>

                <?php $this->load->view("template/categoryBoxes.php"); ?>

            <?php } else { ?>

                <div style="width:100%">
                    <p class="has-text-centered"><?= lang('Jelenleg nincs ebben a kategóriában termék.') ?></p>
                </div>
            <?php } ?>


        </div>

        <div class="moreElementsTrigger">
            <br class="clear"/><br/>
        </div>

    </div>

</section>


<?php $this->load->view('footer'); ?>


<?php if (isset($allCategories)): ?>

    <div class="select category_select">
        <select id="category_select">
            <option value=""><?=lang('Kategóriák')?></option>
            <?php foreach ($allCategories as $category): ?>
                <option value="<?php echo base_url() . $newsPageUrl . '/kategoriak/' . rawurlencode($category) ?>"<?php if ($category == rawurldecode($this->uri->segment(3))) echo ' selected'; ?>><?php echo $category ?></option>
            <?php endforeach ?>
        </select>
    </div>

<?php endif ?>



<?php if (isset($archives)): ?>

    <div class="select archive_select">
        <select id="archive_select">
            <option value=""><?=lang('Arhivum')?></option>
            <?php foreach ($archives as $archive_date): ?>
                <option value="<?php echo base_url() . $newsPageUrl . '/archivum/' . rawurlencode($archive_date) ?>"<?php if ($archive_date == rawurldecode($this->uri->segment(3))) echo ' selected'; ?>><?php echo date('Y ', strtotime($archive_date)) . $this->utils->getMonthName(date('m', strtotime($archive_date))) ?></option>
            <?php endforeach ?>
        </select>
    </div>

<?php endif?>

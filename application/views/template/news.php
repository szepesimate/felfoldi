<?php $this->load->view('header'); ?>

<section class="section txt-article blog-page">

    <div class="container">

      <div class="columns">
        <div class="column has-text-centered">
          <h2 class="title is-1"><span><?= lang('Blog') ?></span></h2>
        </div>
      </div>
      <div class="page-description has-text-centered" style="max-width:1170px; margin: auto;">
        <p>
          <?= lang('__news_page_text__')?>
        </p>
      </div>

      <div class="columns spaceBetween">

            <div class="column boxes">

                <div class="columns is-centered">
                  <?php $this->load->view('template/newsMobilSelects'); ?>
                </div>

                <?php if (isset($searchResultPage)) { ?>
                    <h2 class="title is-2"><?= lang('Search result') ?>: <?php if (isset($searchKeyword)) echo '<i>' . $searchKeyword . '</i>'; ?></h2>
                    <br/>
                    <?php
                }

                if (isset($searchResultPage) && !isset($articles)) {
                    ?>

                    <?= lang('No result') ?>.

                <?php } else if ($articles) {
                    ?>

                    <div class="columns is-multiline">

                        <?php
                        for ($j = 0; $j < count($articles); $j++) {
                            $article = $articles[$j];
                            $url = base_url() . $this->uri->segment(1) . '/' . $this->utils->convertUrlFormat($article->title) . '/' . $article->id;
                            ?>

                            <div class="column is-one-quarter-widescreen is-one-third-desktop is-half-tablet is-full-mobile news_box">
                                <a class="pic" href="<?php echo $url ?>" style="background-image: url('<?= base_url() ?>assets/images/news/<?php echo $article->imageFileName ?>')"></a>

                                <a href="<?php echo $url ?>"><h3 class="title is-3"><?= $article->title ?></h3></a>

                                <div class="columns is-mobile al-tags">
                                    <div class="column is-narrow">
                                        <i class="ion ion-ios-clock-outline"></i>
                                        <?php if ($sesslang == 'hungarian'): ?>
                                            <?php echo date('Y. ', strtotime($article->startDate)) . ucfirst(lang($this->utils->getMonthName(date('m', strtotime($article->startDate))))) . ' ' . date('d.', strtotime($article->startDate)) ?>
                                        <?php else: ?>
                                            <?php echo date('d. ', strtotime($article->startDate)) . ucfirst(lang($this->utils->getMonthName(date('m', strtotime($article->startDate))))) . ' ' . date('Y.', strtotime($article->startDate)) ?>
                                        <?php endif ?>     
                                    </div>
                                    <div class="column">
                                        <i class="fa fa-pencil-square-o" data-wow-delay="250ms" aria-hidden="true"></i> <?= lang($article->categories) ?>
                                    </div>
                                </div>

                                <p>
                                    <?php echo strip_tags($article->preview) ?>
                                </p>
                            </div>

                            <?php
                        }
                        ?>

                        <div class="column is-12 pagination">
                            <?php echo $this->pagination->create_links(); ?>                        
                        </div>

                    </div>
                    <?php
                }
                ?>

            </div>

        </div>

    </div>

</section>

<?php $this->load->view('footer'); ?>

<?php $this->load->view('header'); ?>

<section class="section txt-article">
    <div class="container narrower">
        <?php
        if (isset($articles)) {
            $article = $articles[0];
            ?>
            <div class="columns">
                <div class="column has-text-centered">
                    <h1 class="title is-1"><?= $article->title ?></h1>
                    <?= $article->content ?>
                </div>
            </div>
            <?php
        }
        ?>

        <?php
        if (isset($articles[1])) {
            $article = $articles[1];
            ?>
            <div class="columns first_article">
                <div class="column is-narrow">
                    <?php if ($article->imageFileName != ''): ?>
                        <img alt="<?= $article->title ?>" src="<?= base_url() ?>assets/images/news/<?= $article->imageFileName ?>" />
                    <?php endif ?>
                </div>
                <div class="column">
                    <div class="content">
                        <h4 class="title is-4 uppercase"><?= $article->title ?></h4>
                        <?= $article->content ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

        <?php
        if (isset($articles[2])) {
            $article = $articles[2];
            ?>
            <div class="columns second_article">                
                <div class="column">
                    <div class="content">
                        <h4 class="title is-4 uppercase"><?= $article->title ?></h4>
                        <?= $article->content ?>
                    </div>
                </div>
                <div class="column is-narrow">
                    <?php if ($article->imageFileName != ''): ?>
                        <img alt="<?= $article->title ?>" src="<?= base_url() ?>assets/images/news/<?= $article->imageFileName ?>" />
                    <?php endif ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</section>

<?php $this->load->view('template/_counters_block'); ?>

<section class="section about_map">
    <div class="container">

        <div class="columns">

            <div class="column content has-text-centered">
                <?php //echo $article->content ?>
                <h2 class="title is-1 borderBottom"><b><?=lang('Trading')?></b> - <?=lang('all around the world')?></h2>
                <p class="topArticle"><?=lang('Our confectionery products are sold all over the world and are especially popular in: the UK, Germany, the Netherlands, France, Switzerland, Austria, Sweden, Norway, Spain, Canada, the USA, Japan, Korea, Australia, Russia, Ukraine, Azerbaijan, Lithuania, and Iraq. There is no similar sweets company in the Hungarian region.')?></p>
            </div>

        </div>    


        <div class="columns">

            <div class="column content has-text-centered">

                <div class="map wow fadeIn posRel">                        
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="250ms" style="top:155px;left:295px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="1250ms" style="top:198px;left:368px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="350ms" style="top:244px;left:378px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="750ms" style="top:294px;left:286px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="850ms" style="top:100px;left:673px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="750ms" style="top:169px;left:634px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="550ms" style="top:219px;left:622px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="550ms" style="top:194px;left:645px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="250ms" style="top:200px;left:673px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="1250ms" style="top:175px;left:677px;"></i><?php /* germany fölött */ ?>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="950ms" style="top:187px;left:709px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="1250ms" style="top:211px;left:714px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="670ms" style="top:218px;left:685px;"></i><?php /* italy */ ?>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="450ms" style="top:250px;left:683px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="750ms" style="top:236px;left:753px;"></i><?php /* turkey */ ?>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="650ms" style="top:180px;left:746px;"></i><?php /* ukraine */ ?>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="1450ms" style="top:262px;left:762px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="350ms" style="top:268px;left:758px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="1350ms" style="top:290px;left:740px;"></i><?php /* egyipt */ ?>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="450ms" style="top:286px;left:787px;"></i><?php /* saudiArabia */ ?>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="1250ms" style="top:304px;left:968px;"></i><?php /* thailand */ ?>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="350ms" style="top:346px;left:1065px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="1550ms" style="top:250px;left:1106px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="250ms" style="top:222px;left:895px;"></i>
                    <i class="blue ion ion-ios-location wow fadeInDownBig" data-wow-delay="1250ms" style="top:89px;left:828px;"></i><?php /**/ ?>

                </div>

            </div>

        </div>
    </div>
</section>

<?php $this->load->view('footer'); ?>

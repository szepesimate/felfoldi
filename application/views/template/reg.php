<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container">

        <div class="columns">
            <div class="column has-text-centered">

                <h1 class="title is-1"><?= lang('Registration') ?></h1>

                <?php
                if (isset($articles)) {
                    foreach ($articles as $article) {
                        echo $article->content;
                    }
                }
                ?>

            </div>
        </div>


        <br/>
        <br/>

        <?php echo form_open('', ['id' => 'reg_form']); ?>

        <div class="columns">

            <div class="column is-8-desktop is-offset-2-desktop">


                <div class="columns is-multiline">

                    <div class="column is-6">


                        <div class="rjttt">
                            <input type="text" name="phone" />
                        </div>

                        <div class="field">                        
                            <p class="control">
                                <label class="label"><?= lang('Name') ?>*:</label>
                                <input type="text" class="input" name="name">
                            </p>
                        </div>

                        <div class="field">                        
                            <p class="control">
                                <label class="label">E-mail*:</label>
                                <input type="text" class="input" name="email">
                            </p>
                        </div>

                    </div>



                    <div class="column is-6">


                        <div class="field">                        
                            <p class="control">
                                <label class="label"><?= lang('Password') ?>*:</label>
                                <input type="password" class="input" name="password" />
                            </p>
                        </div>

                        <div class="field">                        
                            <p class="control">
                                <label class="label"><?= lang('Retype password') ?>*:</label>
                                <input type="password" class="input" name="password_re" />
                            </p>
                        </div>


                    </div>


                    <div class="column is-12">

                        <div class="field" style="">                        
                            <p class="control">

                                <input class="checkbox-style" type="checkbox" id="terms_checkbox" name="terms" checked="checked" />
                                <label for="terms_checkbox">*
                                    <?= lang('I’ve read and accepted') ?> <a target="_blank" href="<?= $adatvedelem_page_url ?>"><?= lang('the Privacy Policy') ?></a>
                                    <?= lang('and') ?> <a target="_blank" href="<?= $aszf_page_url ?>"><?= lang('the Terms&Conditions') ?></a>
                                </label>

                            </p>
                        </div>

                        <br/>

                        <div class="field">                        
                            <p class="control">
                                <input type="submit" class="button" name="reg_submit" value="<?=lang('Register')?>" />
                            </p>
                        </div>

                    </div>

                </div>

            </div>
        </div>


        <?php echo form_close(); ?>




    </div>

</section>


<?php $this->load->view('footer'); ?>


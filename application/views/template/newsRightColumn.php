<h4 class="title is-4"><?= lang('Search') ?></h4>

<?php echo form_open('', ['id' => 'newsSearchForm']); ?>

<input type="text" class="field" placeholder="<?= lang('Search here') ?>" name="newsSearchField" value="<?php if (isset($keyword)) echo $keyword; ?>" />
<input type="submit" class="button" value="" />

<?php echo form_close(); ?>


<?php if (isset($allCategories)): ?>

    <h4 class="title is-4"><?= lang('Categories') ?></h4>
    <div class="section">
        <?php foreach ($allCategories as $category): ?>
            <a href="<?php echo base_url() . $newsPageUrl . '/kategoriak/' . rawurlencode($category) ?>"><?php echo lang($category) ?></a>
        <?php endforeach ?>
    </div>

<?php endif ?>

<?php if (isset($recent_post)): ?>

    <h4 class="title is-4"><?= lang('Recent post') ?></h4>
    <div class="section">
        <?php /* foreach ($archives as $archive_date): ?>
          <a href="<?php echo base_url() . $newsPageUrl . '/archivum/' . rawurlencode($archive_date) ?>"><?php echo date('Y ', strtotime($archive_date)) . $this->utils->getMonthName(date('m', strtotime($archive_date))) ?></a>
          <?php endforeach */ ?>

        <?php foreach ($recent_post as $news_struct): ?>
            <?php $news = $news_struct['object']; ?>
            <?php $url = base_url() . $newsPageUrl . '/' . $this->utils->convertUrlFormat($news->title) . '/' . $news->id; ?>
            <div class="recent_post_div">
                <a class="pic" href="<?php echo $url ?>" style="background-image: url('<?= base_url() ?>assets/images/news/<?php echo $news->imageFileName ?>')"></a>
                <a class="title" href="<?php echo $url ?>"><?php echo $news->title ?></a>
                <span class="date"><?php echo date('d', strtotime($news->date)) . '. ' . lang($this->utils->getMonthName(date('m', strtotime($news->date)))) . ' ' . date('Y', strtotime($news->date)) . '.' ?></span>                                
            </div>  
        <?php endforeach ?>
    </div>

<?php endif ?>


<?php if (isset($allTags)): ?>

    <h4 class="title is-4"><?= lang('Popular tags') ?></h4>
    <div class="section tags">
        <?php foreach ($allTags as $tag): ?>
            <a href="<?php echo base_url() . $newsPageUrl . '/cimkek/' . rawurlencode($tag) ?>"><?php echo $tag ?></a>
        <?php endforeach ?>
    </div>

<?php endif ?>

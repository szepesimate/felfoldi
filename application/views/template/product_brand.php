<?php $this->load->view('header'); ?>

<section class="section txt-article">

    <div class="container is-fluid">

        <div class="columns">

            <div class="column">
                <div class="content has-text-centered topArticle">
                    <?php if (isset($article)): ?>
                        <h1 class="title is-1 borderBottom"><?= $article->title ?></h1>                        
                        <?= $article->content ?>
                    <?php endif ?>
                </div>
            </div>

        </div>

        <div class="columns is-multiline boxesInner">

            <?php if (isset($brandProducts) && $brandProducts) { ?>

                <?php
                $data['productBoxes'] = $brandProducts;
                $this->load->view("template/productBoxes.php", $data);
                ?>

            <?php } ?>


           


        </div>

        <div class="fw moreElementsTrigger">
            <br class="clear"/><br/>
        </div>



    </div>

</section>


<?php $this->load->view('footer'); ?>

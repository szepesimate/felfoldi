<?php
	$cnt = 0;
	foreach ($categoryBoxes as $category){

		$cnt++;
?>
		<div class="category-item <?php echo $ourProductDisplayMode; ?>">

			<a href="<?= $productPageUrl . '/' . $category->catUrl ?>">
				<div class="picHolder">
					<img  alt="Felföldi Ínyenc Ízmûves" src="<?php echo base_url() . 'assets/images/category/' . $category->imageFileName; ?>" />
				</div>
        <div class="catTextCtn">
          <p class="title"><?php echo $category->name; ?></p>
          <?php if ($ourProductDisplayMode === 'is-4'): ?>
            <?php if (!empty($category->subCategories)): ?>
              <p class="description">
                <?php foreach ($category->subCategories as $index => $subCategory) {
                  if (($index + 1) === count($category->subCategories)) {
                    echo ' ' . $subCategory->name;
                  }
                  else {
	                  echo ' ' . $subCategory->name . ',';
                  }
                } ?>
              </p>
            <?php endif; ?>
          <?php else: ?>
            <p class="description"><?php echo $category->description; ?></p>
          <?php endif; ?>
        </div>
			</a>

		</div>

<?php
	}


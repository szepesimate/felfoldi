

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"><\/script>')</script>

<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.growl.js"></script>

<?php /*
<script src='<?php echo base_url(); ?>assets/js/unitegallery.min.js'></script>
<script src='<?php echo base_url(); ?>assets/js/ug-theme-tiles.js'></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/picedit.min.js"></script> */ ?>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>

<script src='<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js'></script>

<script src="<?php echo base_url(); ?>assets/js/utils.js?v=<?php echo date('His') ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/forms.js?v=<?php echo date('His') ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/product.js?v=<?php echo date('His') ?>"></script>


<?php if (isset($template) && ($template == 'contact')): ?>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq3mumZgZnbw91vIOIPUl-jHqSP7wvgFk&callback=initialize"></script>
<?php elseif (false): ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.appear.js"></script>
<?php elseif (in_array('product', $bodyClasses) && !in_array('productDetail', $bodyClasses)): ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.scrollappear.min.js"></script>    
<?php endif ?>
    
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox-thumbs.js"></script>


</body>
</html>
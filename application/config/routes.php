<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['admin'] = 'admin_user/index';
$route['admin/login'] = 'admin_user/index';
$route['admin/logout'] = 'admin_user/logout';
$route['admin/login/validate_credentials'] = 'admin_user/validate_credentials';

$route['admin/pages'] = 'admin_pages/index';
$route['admin/pages/add'] = 'admin_pages/add';
//$route['admin/pages/update'] = 'admin_pages/update';
$route['admin/pages/update/(:any)'] = 'admin_pages/update/$1';
$route['admin/pages/delete/(:any)'] = 'admin_pages/delete/$1';
$route['admin/pages/reorder'] = 'admin_pages/reorder';
$route['admin/pages/switchProperties'] = 'admin_pages/switchProperties';


$route['admin/seo'] = 'admin_seo/index';
//$route['admin/seo/update'] = 'admin_seo/update';
$route['admin/seo/update/(:any)'] = 'admin_seo/update/$1';
//$route['admin/seo/(:any)'] = 'admin_seo/index/$1'; //$1 = page number

$route['admin/seo/submitSeoForm'] = 'admin_seo/submitSeoForm';



$route['admin/slider'] = 'admin_slider/index';
$route['admin/slider/add'] = 'admin_slider/add';
//$route['admin/slider/update'] = 'admin_slider/update';
$route['admin/slider/update/(:any)'] = 'admin_slider/update/$1';
$route['admin/slider/delete/(:any)'] = 'admin_slider/delete/$1';
//$route['admin/slider/(:any)'] = 'admin_slider/index/$1'; //$1 = page number
$route['admin/slider/reorder'] = 'admin_slider/reorder';


$route['admin/galleries'] = 'admin_galleries/index';
$route['admin/galleries/add'] = 'admin_galleries/add';
//$route['admin/galleries/update'] = 'admin_galleries/update';
$route['admin/galleries/update/(:any)'] = 'admin_galleries/update/$1';
$route['admin/galleries/delete/(:any)'] = 'admin_galleries/delete/$1';
//$route['admin/galleries/(:any)'] = 'admin_galleries/index/$1'; //$1 = page number
$route['admin/galleries/reorder'] = 'admin_galleries/reorder';
$route['admin/galleries/reorderImages'] = 'admin_galleries/reorderImages';
$route['admin/galleries/switchProperties'] = 'admin_galleries/switchProperties';

$route['admin/galleries/addImage/(:any)'] = 'admin_galleries/addImage/$1';
$route['admin/galleries/deleteImage/(:any)/(:any)'] = 'admin_galleries/deleteImage/$1/$2';

//ez a tömeges képfeltöltéshez:
$route['admin/galleries/addImages/(:any)'] = 'admin_galleries/addImages/$1';

$route['admin/orders'] = 'admin_orders/index';
$route['admin/orders/delete/(:any)'] = 'admin_orders/delete/$1';

$route['admin/videogalleries'] = 'admin_videogalleries/index';
$route['admin/videogalleries/add'] = 'admin_videogalleries/add';
//$route['admin/videogalleries/update'] = 'admin_videogalleries/update';
$route['admin/videogalleries/update/(:any)'] = 'admin_videogalleries/update/$1';
$route['admin/videogalleries/delete/(:any)'] = 'admin_videogalleries/delete/$1';
//$route['admin/videogalleries/(:any)'] = 'admin_videogalleries/index/$1'; //$1 = page number
$route['admin/videogalleries/reorder'] = 'admin_videogalleries/reorder';
$route['admin/videogalleries/reorderVideos'] = 'admin_videogalleries/reorderVideos';

$route['admin/videogalleries/addVideo/(:any)'] = 'admin_videogalleries/addVideo/$1';
$route['admin/videogalleries/deleteVideo/(:any)/(:any)'] = 'admin_videogalleries/deleteVideo/$1/$2';


$route['admin/newsletter_list'] = 'admin_newsletter_list/index';


$route['admin/membership'] = 'admin_membership/index';
$route['admin/membership/add'] = 'admin_membership/add';
$route['admin/membership/update/(:any)'] = 'admin_membership/update/$1';
$route['admin/membership/delete/(:any)'] = 'admin_membership/delete/$1';


$route['admin/articles'] = 'admin_articles/index';
$route['admin/articles/add'] = 'admin_articles/add';
//$route['admin/articles/update'] = 'admin_articles/update';
$route['admin/articles/update/(:any)'] = 'admin_articles/update/$1';
$route['admin/articles/delete/(:any)'] = 'admin_articles/delete/$1';
//$route['admin/articles/(:any)'] = 'admin_articles/index/$1'; //$1 = page number
$route['admin/articles/switchProperties'] = 'admin_articles/switchProperties';
$route['admin/articles/duplicate/(:any)/(:any)'] = 'admin_articles/duplicate/$1/$2';


$route['admin/testimonials'] = 'admin_testimonials/index';
$route['admin/testimonials/add'] = 'admin_testimonials/add';
$route['admin/testimonials/update/(:any)'] = 'admin_testimonials/update/$1';
$route['admin/testimonials/delete/(:any)'] = 'admin_testimonials/delete/$1';
$route['admin/testimonials/(:any)'] = 'admin_testimonials/index/$1';
$route['admin/testimonials/switchProperties'] = 'admin_testimonials/switchProperties';


$route['admin/rates'] = 'admin_rates/index';
$route['admin/rates/update/(:any)'] = 'admin_rates/update/$1';
$route['admin/rates/delete/(:any)'] = 'admin_rates/delete/$1';
$route['admin/rates/switchProperties'] = 'admin_rates/switchProperties';


$route['admin/tagok'] = 'admin_tagok/index';

$route['admin/languages'] = 'admin_languages/index';
$route['admin/languages/add'] = 'admin_languages/add';
$route['admin/languages/delete/(:any)'] = 'admin_languages/delete/$1';
$route['admin/dictionary'] = 'admin_dictionary/index';
$route['admin/dictionary/submitDictionaryForm'] = 'admin_dictionary/submitDictionaryForm';
$route['admin/dictionary/genLanguageFilesForm'] = 'admin_dictionary/genLanguageFilesForm';


$route['admin/confirm_email'] = 'admin_confirm_email/index';
$route['admin/confirm_email/update/(:any)'] = 'admin_confirm_email/update/$1';





$route['admin/product_cats'] = 'admin_product_cats/index';
$route['admin/product_cats/add'] = 'admin_product_cats/add';
//$route['admin/product_cats/update'] = 'admin_product_cats/update';
$route['admin/product_cats/update/(:any)'] = 'admin_product_cats/update/$1';
$route['admin/product_cats/delete/(:any)'] = 'admin_product_cats/delete/$1';
//$route['admin/product_cats/(:any)'] = 'admin_product_cats/index/$1'; //$1 = page number
$route['admin/product_cats/reorder'] = 'admin_product_cats/reorder';

$route['admin/products'] = 'admin_products/index';
$route['admin/products/add'] = 'admin_products/add';
//$route['admin/products/update'] = 'admin_products/update';
$route['admin/products/update/(:any)'] = 'admin_products/update/$1';
$route['admin/products/delete/(:any)'] = 'admin_products/delete/$1';
$route['admin/products/uploadImage/(:any)'] = 'admin_products/uploadImage/$1';
$route['admin/products/deleteImage/(:any)/(:any)'] = 'admin_products/deleteImage/$1/$2';
//$route['admin/products/(:any)'] = 'admin_products/index/$1'; //$1 = page number
$route['admin/products/switchProperties'] = 'admin_products/switchProperties';
$route['admin/products/reorderProductImages'] = 'admin_products/reorderProductImages';
$route['admin/products/reorder'] = 'admin_products/reorder';

$route['admin/connectedProducts'] = 'admin_products/connectedProducts';

$route['addToBasket/(:any)/(:any)'] = 'basket/addItem/$1/$2';
$route['removeFromBasket/(:any)'] = 'basket/removeItem/$1';
$route['changeBasketItemCount/(:any)/(:any)'] = 'basket/changeBasketItemCount/$1/$2';
$route['refreshShippingAndOverallCost'] = 'basket/refreshShippingAndOverallCost';
$route['fetchLastBasketItems/(:any)'] = 'basket/fetchLastBasketItems/$1';

$route['setProductOrder/(:any)'] = 'basket/setProductOrder/$1';

$route['handleCouponForm'] = 'basket/handleCouponForm';

$route['myDownloader/(:any)/(:any)'] = 'download_ctrl/download/$1/$2';


require_once( BASEPATH .'database/DB.php' );
$db =& DB();
$query = $db->get('pages');
$result = $query->result_object();
// var_dump($result);
// die('Salslalsa');
foreach ($result as $row) {
    $query2 = $db->get_where('seo', ['itemType' => 'page', 'itemId' => $row->id]);
    if ($query2->num_rows()) {
        foreach ($query2->result_object() as $seo_object) {
            $route[$seo_object->urlFriendly] = $row->tpl . '_ctrl';
            if (true) {
                $route[$seo_object->urlFriendly . '/:any'] = $row->tpl . '_ctrl';
            }
            if ($row->tpl == 'news' || $row->tpl == 'product' || $row->tpl == 'competition'){
                $route[$seo_object->urlFriendly . '/:any/:any'] = $row->tpl . '_ctrl';
            }
            if ($row->tpl == 'product'){
                $route[$seo_object->urlFriendly . '/:any/:any/:any'] = $row->tpl . '_ctrl';
            }
        }
    }
}


$route['default_controller'] = 'index_ctrl';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;